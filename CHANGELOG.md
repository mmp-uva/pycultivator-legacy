## Version 0.2.0.dev1

* [Enhancement] Move code to an actual package format [WIP]
* [Bug] Rhythm object did not accept XML date format 

## Version 0.1.9.dev1

* [Feature] Calculate number of fluctuations run based on time left to next measurement.
* [Bug] Fix bug in setting loading using namespace in fluctuation and valve protocols
* [Feature] Make export of fluctuation intensities configurable
* [Bug] Fix bug in TimeDependency where isActive expects an argument
* [Bug] Fix bug in logger so log messages produced in subprocesses are diverted to separate log files.
* [Bug] Check for return value from TimeDependency in TimeProtocol
* [Bug] Fix bugs in export of turbidostat data

## Version 0.1.9
* [Feature] Implement Fluctuation protocol
* [Enhancement] Upgrade schema to version 1.6: Support settings in dependencies.
* [Enhancement] Default config uses schema with version in filename.

## Version 0.1.8
* [Bug] Fix reading of data in serial communication
* [Enhancement] Improve protocols and light affecting protocols
* [Feature] Implement sinusoid day-night rhythm

## Version 0.1.7
* [Feature] Provide configuration validation script (`check_configuration.py`)
* [Feature] Write status messages to console in stand-alone scripts
* [Feature] Add support for a gas-release valve
* [Enhancement] Make turbidostat and pumps more stable
* [Enhancement] Refactor PumpScheduler into a general device scheduler
* [Enhancement] Improve protocol structure
* [Bug] Many bug fixes

## Version 0.1.6
* [Bug] Fix problem with turbidostat
* [Bug] Improve configuration loading
* [Bug] Prevent crash when turbidostat table is missing
* [Bug] Fix negation of doReporting and doSimulate
* [Feature] Improve turbidostat data model
* [Feature] Improve random generation of OD Values when simulating

## Version 0.1.5.4
* [Bug] Fix problems with calibration models
* [Bug] Fix problems with commanding Reglo ICC pumps

## Version 0.1.5.3
* [Bug] Fix calibrator GUI

## Version 0.1.5.2
* [Bug] Fix multi-process logging

## Version 0.1.5.1
* [Documentation] Update documentation

## Version 0.1.5
* [Bug] Fix turbidostat protocol
* [Feature] Implement aggregation of OD Measurements
* [Feature] Implement writing to SQLite by Turbidostat Protocol
* [Maintenance] Deprecate all exporter classes except SQLite
* [Maintenance] Rename arguments to use dashes instead of underscores
* [Maintenance] Adopt better git branching model

## Version 0.1.4
* [Bug] Fix issue where receiving multiple messages lead to incorrect data handling.
* [Bug] Fix issue with switching lights state
* [Feature] Improve logging, use '-v' to set log level
* [Bug] Reduce number of refreshLightSettings calls when loading config
* [Feature] Upgrade to pySerial 3.x

## Version 0.1.3
* [Bug] Tolerate spaces in domain notation(#40 on Redmine)
* [Bug] Make light elements optional in channels
* [Maintenance] Some code cleaning

## Version 0.1.2

* [Feature] INI-files can be used to set global configuration
* [Feature] Added SQLite support

## Before version 0.1.2

* First release
