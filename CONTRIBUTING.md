# Contribution Guide for pyCultivator-legacy

__version__: 2017-04-18

## General remarks

* Test before pushing code to develop, release or master
* Test code with the "do_<name>" script, with the "fake" cultivator and the example config


## VCS Set-up

We use branches to organise the project according to code maturity and development status.

* master: production stable hot fixes
* release: transit code from develop to master
* develop: code under development
* feature/<name>: specific feature being developed

Code should flow from feature/<name> -> develop -> release -> master.
Hot fixes may be introduced directly into the master branch. No code will be developed in the release branch, the only 
purpose of this branch it to resolve conflicts between develop and master and test a upcoming release.

## Python Guidelines

* Use Object-Oriented-Programming as much as possible.
* Provide pydoc information to all "public" methods.
* Group functionality in packages, modules and classes.
* Use getLog() methods to write logging information.
* It is better to have multiple small methods than one large.
* Write with extendability in mind.
* Use unittests wherever possible.
