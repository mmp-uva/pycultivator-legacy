# pyCultivator

pyCultivator is a set of python packages that were (initially) created to control and monitor a Multi-Cultivator (MC-1000-OD, Photon Systems Instruments) from a desktop computer.

The package is intended to be highly flexible and versatile as many parts can be adapted to meet specific requirements.

This repository contains the legacy version (0.1.x) of pyCultivator. A reworked version of pyCultivator (v.1.0) is planned to be released by the end of this year.

|             |                                          |
|-------------|------------------------------------------|
| Maintainer  | Joeri Jongbloets <j.a.jongbloets@uva.nl> |
| Date        | September 27, 2017                       |
| Version     | 0.1.9.1                                  |

## Requirements

* lxml
* numpy
* pySerial (> v3)
* pySQLite (when using SQLite to store data)
* pycultivator-relay (when using gas valve)

* Cross-platform (NOT tested on Windows)

## Structure

The code is divided in several packages.

* __uvaCultivator__: The core package, with code for modelling a cultivator.
* __uvaSerial__: Contains support for connection over a serial link.
* __uvaTools__: Contains both text and graphical programs based on the package. For example measurement scripts.

## API Documentation

API Documentation can be generated using [sphinx](https://pypi.python.org/pypi/Sphinx). To generate HTML documentation files, run

In the console, go to the `docs` directory. First generate the API Documentation:

```
sphinx-apidoc -o source ../
```

This will create some .rst files in the source directory, containing references to all Python modules within 
pyCultivator-legacy.

Then to generate the documentation into an HTML website:

```
make html
```

This will generate the documentation as HTML in the `docs/build` folder.

## Usage

### Works with

* Photon Systems Instruments Multi-Cultivator MC-1000-OD[^1]
* Ismatec REGLO ICC Pump[^2]

### Configuration

The programs load two different configuration files; an INI file or XML file.

The INI files are intended to set global settings, for example per computer or set-up, while the xml files are used for per experiment settings.

Settings are applied in the following order:

0. The Default Settings (hard-coded)
1. The settings defined in the script code (if any, hard-coded)
2. The settings from INI files loaded at start-up (if an ini was found)
3. Command line options (if any)
4. The settings from the configuration file

#### INI Files

The scripts (see "Using the tools") will try to load ini files in the following order:

1. pycultivator.ini in the current directory (from where the python command is executed).
2. pycultivator.ini in the directory of the script that is being executed
3. pycultivator.ini in the parent directory of the directory with the script that is executed.

If the `--ini` parameter is given to the script than the script will load these settings over the previously loaded settings.

Minimal settings in the ini file are:

* __version__: Should equal the supported version of pyCultivator (currently 1)
* __root__: The root directory with the data/configuration/log folders.
* __config__: The location of the configuration folder.
* __log__: The folder where all the log files are written.
* __data__: The folder where data files are written.

Extra settings can be provided. Use sections to indicate the namespace of the setting.

### Using the tools

Front-end scripts are available in the uvaTools directory.

Batch cultivation routines

* **do\_measure\_od.py**: Performs a single OD measurement
* **do\_cultivation.py**: Performs a single OD measurements and adjusts light settings.
* **do\_batch_measure\_od.py** Measures OD and sets light of all active experiments (configuration files)
* **do\_batch\_cultivation.py** Measures OD of all active experiments (configuration files)

Turbidostat routines

* **do_turbidostat.py** Runs one instance of a turbidostat (set by parameters in file)

Additional parameters can be given to all scripts, use `-h` for an up-to-date overview and description.

|                           | CP^1 | h | f | v | s | n | S | CD^2 | OD^3 | LD^4 | ini      |
|---------------------------|------|---|---|---|---|---|---|------|------|------|----------|
| do\_measure\_od.py        | M    | O | O | O | O | O | O | O    | O    | O    | O        |
| do\_cultivation.py        | M    | O | O | O | O | O | O | O    | O    | O    | O        |
| do\_turbidostat.py        | M    | O | O | O | O | O | O | O    | O    | O    | O        |
| do\_batch\_measure\_od.py | N    | O | N | O | O | O | O | O    | O    | O    | O        |
| do\_batch\_cultivation.py | N    | O | N | O | O | O | O | O    | O    | O    | O        |

#### Table notes
All parameters are prepended with '-' if not noted otherwise.

__M__: Mandatory
__O__: Available, Optional
__N__: Not available

^1 :Abbreviation of `config_path`, a mandatory argument if available.

^2 :Abbreviation of `--config-dir`

^3 :Abbreviation of `--output-dir`

^4 :Abbreviation of `--log-dir`

# Footnotes

[^1]: http://www.psi.cz/products/photobioreactors/multi-cultivator-mc-1000
[^2]: http://www.ismatec.com/int_e/pumps/t_reglo/reglo.htm
