<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" version="1.5" targetNamespace="uvaCultivator"
           xmlns="uvaCultivator"
           attributeFormDefault="unqualified" elementFormDefault="qualified">
    <xs:element name="uvaExperiment" xmlns="uvaCultivator">
        <xs:annotation>
            <xs:documentation>Experiment element</xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="settings" type="settingsType"/>
                <xs:element name="instrument" maxOccurs="unbounded">
                    <xs:annotation>
                        <xs:documentation>Instrument element</xs:documentation>
                    </xs:annotation>
                    <xs:complexType>
                        <xs:all>
                            <xs:element name="settings" type="settingsType"/>
                            <xs:element name="serial" type="serialType"/>
                            <xs:element name="protocol" type="protocolType" minOccurs="0"/>
                            <xs:element name="channels" minOccurs="0">
                                <xs:annotation>
                                    <xs:documentation>Define the channels in this instrument</xs:documentation>
                                </xs:annotation>
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="calibrations" type="calibrationsType" minOccurs="0"/>
                                        <xs:element name="light" type="lightType" minOccurs="0"/>
                                        <xs:element name="channel" type="channelType" maxOccurs="unbounded"/>
                                    </xs:sequence>
                                    <xs:attribute type="xs:boolean" name="measure_od" use="required"/>
                                </xs:complexType>
                            </xs:element>
                        </xs:all>
                        <xs:attribute type="xs:string" name="id" use="optional"/>
                        <xs:attribute type="xs:boolean" name="active" use="optional"/>
                        <xs:attribute type="xs:string" name="type" use="optional"/>
                        <xs:attribute type="xs:string" name="brand" use="optional"/>
                    </xs:complexType>
                </xs:element>
            </xs:sequence>
            <xs:attribute type="xs:string" name="id" use="required"/>
            <xs:attribute type="xs:string" name="version"/>
            <xs:attribute type="xs:decimal" name="schema_version"/>
            <xs:attribute type="xs:boolean" name="state" default="false" use="optional"/>
            <xs:attribute type="xs:dateTime" name="start_date" use="required"/>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="variableType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="bool"/>
            <xs:enumeration value="int"/>
            <xs:enumeration value="float"/>
            <xs:enumeration value="string"/>
            <xs:enumeration value="str"/>
            <xs:enumeration value="list"/>
            <xs:enumeration value="dict"/>
            <xs:enumeration value="dt"/>
            <xs:enumeration value="datetime"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="settingsType">
        <xs:annotation>
            <xs:documentation>Set a group of settings</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="setting" type="settingType" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="settingType">
        <xs:annotation>
            <xs:documentation>Set a setting</xs:documentation>
        </xs:annotation>
        <xs:attribute type="xs:integer" name="id" use="required"/>
        <xs:attribute type="xs:string" name="name" use="required"/>
        <xs:attribute type="xs:string" name="value" use="required"/>
        <xs:attribute type="variableType" name="type" use="required"/>
    </xs:complexType>
    <xs:complexType name="serialType">
        <xs:annotation>
            <xs:documentation>Define a serial port</xs:documentation>
        </xs:annotation>
        <xs:all>
            <xs:element name="settings" type="settingsType"/>
            <xs:element name="rates">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="rate" maxOccurs="unbounded">
                            <xs:complexType>
                                <xs:simpleContent>
                                    <xs:extension base="xs:integer"/>
                                </xs:simpleContent>
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="protocolType">
        <xs:annotation>
            <xs:documentation>Define a protocol</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="step" maxOccurs="unbounded">
                <xs:complexType>
                    <xs:annotation>
                        <xs:documentation>Define a protocol step</xs:documentation>
                    </xs:annotation>
                    <xs:all>
                        <xs:element name="param" minOccurs="0">
                            <xs:complexType>
                                <xs:attribute type="xs:integer" name="id" use="required"/>
                                <xs:attribute type="xs:string" name="name" use="required"/>
                                <xs:attribute type="xs:string" name="value" use="required"/>
                                <xs:attribute type="xs:string" name="type" use="required"/>
                            </xs:complexType>
                        </xs:element>
                    </xs:all>
                    <xs:attribute type="xs:integer" name="id" use="required"/>
                    <xs:attribute type="xs:string" name="action" use="required"/>
                    <xs:attribute type="xs:string" name="subject" use="required"/>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="channelType">
        <xs:annotation>
            <xs:documentation>Define a channel</xs:documentation>
        </xs:annotation>
        <xs:all minOccurs="0">
            <xs:element name="calibrations" type="calibrationsType"/>
            <xs:element name="light" type="lightType"/>
            <xs:element name="pump" type="pumpType" minOccurs="0"/>
        </xs:all>
        <xs:attribute type="xs:integer" name="id" use="required"/>
        <xs:attribute type="xs:boolean" name="measure_od" use="optional"/>
    </xs:complexType>
    <xs:complexType name="calibrationsType">
        <xs:annotation>
            <xs:documentation>Define a set of calibrations</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="calibration" maxOccurs="unbounded">
                <xs:complexType>
                    <xs:annotation>
                        <xs:documentation>Define a calibration</xs:documentation>
                    </xs:annotation>
                    <xs:sequence>
                        <xs:element name="polynomial" type="polynomialType" maxOccurs="unbounded"/>
                    </xs:sequence>
                    <xs:attribute type="xs:integer" name="id"/>
                    <xs:attribute type="xs:string" name="source"/>
                    <xs:attribute type="xs:string" name="variable"/>
                    <xs:attribute type="xs:boolean" name="active" use="optional" default="true"/>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="lightType">
        <xs:annotation>
            <xs:documentation>Define a light setting</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="dependency" type="dependencyType" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute type="xs:decimal" name="intensity" use="optional" default="0.0"/>
        <xs:attribute type="xs:boolean" name="state" use="optional" default="false"/>
    </xs:complexType>
    <xs:simpleType name="pumpModeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="RPM_MODE"/>
            <xs:enumeration value="FLOWRATE_MODE"/>
            <xs:enumeration value="TIME_MODE"/>
            <xs:enumeration value="VOLUME_RATE_MODE"/>
            <xs:enumeration value="PAUSE_MODE"/>
            <xs:enumeration value="TIME_PAUSE_MODE"/>
            <xs:enumeration value="VOLUME_PAUSE_MODE"/>
            <xs:enumeration value="VOLUME_TIME_MODE"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="pumpDirection">
        <xs:restriction base="xs:string">
            <xs:enumeration value="left"/>
            <xs:enumeration value="right"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="pumpType">
        <xs:annotation>
            <xs:documentation>Define a pump setting</xs:documentation>
        </xs:annotation>
        <xs:attribute type="xs:string" name="url" use="required"/>
        <xs:attribute type="xs:boolean" name="state" use="optional" default="false"/>
        <xs:attribute type="pumpModeType" name="mode" use="required"/>
        <xs:attribute type="pumpDirection" name="direction" use="required"/>
        <xs:attribute type="xs:decimal" name="value" use="required"/>
    </xs:complexType>
    <xs:simpleType name="dependencySourceType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="self"/>
            <xs:enumeration value="right"/>
            <xs:enumeration value="left"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="dependencyVariableType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="od"/>
            <xs:enumeration value="intensity"/>
            <xs:enumeration value="time"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="dependencyTargetType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="od"/>
            <xs:enumeration value="state"/>
            <xs:enumeration value="intensity"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="dependencyType">
        <xs:annotation>
            <xs:documentation>
                Define a dependency of a setting on a variable from a certain source.
            </xs:documentation>
        </xs:annotation>
        <xs:choice>
            <xs:element name="polynomial" type="polynomialType" maxOccurs="unbounded"/>
            <xs:element name="rhythm" type="rhythmType"/>
        </xs:choice>
        <xs:attribute type="xs:integer" name="id" use="required"/>
        <xs:attribute type="dependencySourceType" name="source" use="required"/>
        <xs:attribute type="dependencyVariableType" name="variable" use="required"/>
        <xs:attribute type="dependencyTargetType" name="target" use="required"/>
        <xs:attribute type="xs:boolean" name="active" use="optional" default="false"/>
    </xs:complexType>
    <xs:complexType name="polynomialType">
        <xs:annotation>
            <xs:documentation>
                Define a polynomial which is used by a dependence. Polynomials describe the numerical transformation
                that is applied to the variable of the dependence.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="coefficient" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>
                        Define one coefficent of a polynomial. A coefficient has a
                        specific order and a value. A zeroth order coefficient is also known as the intercept or
                        constant.
                    </xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:simpleContent>
                        <xs:extension base="xs:decimal">
                            <xs:attribute type="xs:integer" name="order" use="required"/>
                        </xs:extension>
                    </xs:simpleContent>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
        <xs:attribute type="xs:string" name="domain" use="optional" default="(,)"/>
    </xs:complexType>
    <xs:complexType name="rhythmType">
        <xs:annotation>
            <xs:documentation>Define a rhythm dependence</xs:documentation>
        </xs:annotation>
        <xs:choice>
            <xs:element name="sinusoidal" type="sinusoidalType"/>
            <xs:element name="stepwise" type="stepwiseType"/>
        </xs:choice>
        <xs:attribute type="xs:dateTime" name="start" use="required"/>
        <xs:attribute type="xs:dateTime" name="stop" use="optional"/>
    </xs:complexType>
    <xs:complexType name="stepwiseType">
        <xs:annotation>
            <xs:documentation>Define a stepwise dependence</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="period" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>Define a period in a stepwise</xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:simpleContent>
                        <xs:extension base="xs:float">
                            <xs:attribute type="xs:integer" name="id" use="required"/>
                            <xs:attribute type="xs:integer" name="length" use="optional" default="1"/>
                        </xs:extension>
                    </xs:simpleContent>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
        <xs:attribute type="xs:dateTime" name="start" use="optional"/>
        <xs:attribute type="xs:dateTime" name="stop" use="optional"/>
    </xs:complexType>
    <xs:complexType name="sinusoidalType">
        <xs:annotation>
            <xs:documentation>Define a sinusoidal dependence</xs:documentation>
        </xs:annotation>
        <xs:attribute type="xs:float" name="amplitude" use="required"/>
        <xs:attribute type="xs:float" name="period" use="required"/>
        <xs:attribute type="xs:float" name="x_offset" use="required"/>
        <xs:attribute type="xs:float" name="y_offset" use="required"/>
    </xs:complexType>
</xs:schema>

