
from base import BaseConfig
from xml import XMLConfig

__all__ = [
    "BaseConfig",
    "XMLConfig"
]