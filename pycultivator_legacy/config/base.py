#!/usr/bin/python
"""
This module supports reading an XML formatted Config file.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from pycultivator_legacy.core import BaseObject
import uvaSerial


class BaseConfig(BaseObject):
    """
    Basic abstract class describing the configuration
    """

    default_settings = BaseObject.mergeDefaultSettings(
        {
            "project_id": "",
            "time_zero": None,
            "state": False,
            "exporter.name": None,
            "exporter.class": None,
        })

    @classmethod
    def loadConfig(cls, source, project_id):
        """Factory method to load a Configuration file into a AbstractCultivatorConfig class.

        This is a naive implementation; only implementing the instantiation of the class

        :param source: Reference to the source holding the configuration
        :type source: object
        :return: Reference to the created object
        :rtype: AbstractCultivatorConfig
        """
        return cls(settings={"project_id": project_id})

    def __init__(self, settings=None):
        super(BaseConfig, self).__init__(settings=settings)
        self._cultivator = None
        self._serial = None
        """:type AbstractCultivatorConfig._serial: uvaSerial.uvaSerialController.uvaSerialController """
        # from ..uvaPolynomialDependency import ODCalibrationDependency
        # self._default_od_calibration = ODCalibrationDependency.loadDefault()
        self._default_od_calibrations = None
        self._default_light = None
        self._valve = None

    def refresh(self):
        """
        Refresh the data contained by this object. Implementations are not required to save the data first,
        so perform a save before calling this method is data is to be preserved
        :return: True on success, False otherwise
        :rtype: bool
        """
        raise NotImplementedError

    def save(self):
        """
        Save the data to the underlying storage
        :return: True on success, False otherwise
        :rtype: bool
        """
        raise NotImplementedError

    def parseSettings(self, source=None, defaults=None, namespace=""):
        """Parses the source that contains settings into a dictionary

        :type source: object
        :type defaults: dict or None
        :param namespace: String that should printed before the settings name. Such as: cultivator to make
        cultivator.setting
        :type namespace: string
        :return: Dictionary with setting elements, where name is used as key to retrieve the value
        :rtype: dict
        """
        raise NotImplementedError

    #
    #   Construct objects from configuration
    #

    def loadControllerSettings(self, root=None):
        raise NotImplementedError

    def loadController(self, name, connect=False, force=False, root=None):
        """Create a uvaSerialConnector object with the parameters from the configuration file

        :param name: Name of the controller
        :type name: str
        :param connect:
        :type connect: bool
        :param force:
        :type force: bool
        :return:
        :rtype: uvaSerial.uvaSerialController.SerialController
        """
        if force or self._serial is None:
            self._serial = None
            # load settings
            settings = self.loadControllerSettings(root=root)
            # load controller
            controller = uvaSerial.loadController(name, settings=settings)
            """:type: uvaSerial.uvaSerialController.SerialController"""
            if controller is not None:
                self._serial = controller
                self._serial.getSerial().configure(**settings)
                if connect:
                    self._serial.connect()
            else:
                self.getLog().critical("Unable to load controller of type {}".format(name))
        return self._serial

    def loadPsiSerial(self, fake=False, connect=False, force=False, root=None):
        """Create a psiSerialConnector object with the parameters from the configuration source

        :rtype: uvaSerial.psiSerialConnector.psiSerialConnector
        """
        if force or self._serial is None:
            name = uvaSerial.SERIAL_PSIMC_CONTROLLER
            if fake:
                name = uvaSerial.FAKE_SERIAL_PSIMC_CONTROLLER
            self._serial = self.loadController(name, connect, force, root)
        return self._serial

    def loadRegloSerial(self, fake=False, connect=False, force=False, root=None):
        """Create a regloSerialConnector object with the parameters from the configuration source

        :rtype: uvaSerial.psiSerialConnector.psiSerialConnector
        """
        if force or self._serial is None:
            name = uvaSerial.SERIAL_REGLO_CONTROLLER
            if fake:
                name = uvaSerial.FAKE_SERIAL_REGLO_CONTROLLER
            self._serial = self.loadController(name, connect, force, root)
        return self._serial

    def loadCultivator(self, serial=None, force=False, fake=False):
        """Create a Cultivator object with the parameters from the configuration file

        :return:
        :rtype:
        """
        raise NotImplementedError

    def loadCultivatorSettings(self, source, channel_count=8):
        """Load Cultivator settings from the configuration source

        :param source: The source that can be read by parseSettings
        :rtype: dict
        """
        result = self.parseSettings(source, namespace="cultivator")
        if "cultivator.channel_count" not in result:
            result["cultivator.channel_count"] = channel_count
        return result

    def loadDefaultODCalibrations(self, default=None):
        """ Load all default OD Calibrations

        Will load all default calibrations once.

        :param default: None or uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        :return:
        :rtype: dict[int, uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency]
        """
        raise NotImplementedError

    def loadDefaultODCalibration(self, wavelength=720, default=None):
        """Load the default Calibration Models from the configuration file

        :return:
        :rtype: uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        """
        result = default
        # load default
        if result is None:
            from uvaCultivator.uvaPolynomialDependency import ODCalibrationDependency
            result = ODCalibrationDependency.loadDefault().copy()
            result.setId(str(wavelength))
            result.setSource(str(wavelength))
            result.setActive(True)
        # load calibrations
        defaults = self.loadDefaultODCalibrations(default=default)
        if wavelength in defaults.keys():
            result = defaults[wavelength]
        return result

    def loadODCalibrations(self, channel, default=None):
        """ Load the OD calibrations from the configuration source

        :param channel:
        :type channel: int or lxml.etree._Element._Element
        :return:
        :rtype: dict[int, uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency]
        """
        raise NotImplementedError

    def loadODCalibration(self, channel, wavelength=720, default=None):
        """ Load the OD Calibration of the given channel and wavelength

        :type channel: int, Element
        :rtype: uvaDependency.BasicDependency
        """
        calibrations = self.loadODCalibrations(channel)
        if default is None:
            default = self.loadDefaultODCalibration(wavelength=wavelength)
        result = calibrations.get(wavelength, default)
        return result

    def loadChannel(self, channel, serial=None):
        """
        Return the Channel object with the parameters from the configuration source
        :type channel: int
        :rtype: uva
        """
        raise NotImplementedError

    def loadDefaultLight(self):
        """Create the default LightRegime object from the configuration source

        :rtype: uvaCultivatorLightRegime.CultivatorLight
        """
        raise NotImplementedError

    def loadLight(self, channel):
        """Create a LightRegime object with the parameters from the configuration source

        :type channel: int, Element
        :rtype: uvaCultivatorLightRegime.CultivatorLight, None
        """
        raise NotImplementedError

    def loadProjectId(self):
        """Load the project id from the configuration source

        :rtype: str or None
        """
        raise NotImplementedError

    def loadStartDate(self):
        """Load the start date from the configuration source

        :rtype: datetime.datetime
        """
        raise NotImplementedError

    def loadState(self):
        """Load the current experiment state from the configuration source

        :rtype: bool
        """
        raise NotImplementedError

    def loadExperimentSettings(self, settings=None):
        """Loads the general experiment settings

        :return:
        :rtype:
        """
        if not isinstance(settings, dict):
            settings = {}
        # load settings dictionary from source
        settings = self.mergeDictionary(settings, self.parseSettings())
        # add project id and start time
        settings["project_id"] = self.loadProjectId()
        settings["state"] = self.loadState()
        settings["time_zero"] = self.loadStartDate()
        # store settings in this object
        self._settings = self.mergeSettings(settings, namespace="")
        return settings

    def loadDataClass(self, name=None, namespace="exporter"):
        """Returns an Exporter class

        :param name: Name of the class to be loaded, if None use name from setting
        :type name: str, None
        :param namespace: The namespace of the data class in the settings; either exporter or importer
        :type namespace: str
        :return: Class instance
        :rtype: uvaCultivator.uvaCultivatorExport.uvaCultivatorExport.uvaCultivatorExport
        """
        self.loadExperimentSettings()
        if name is None:
            name = self.retrieveSetting(name="name", default=self._default_settings, namespace=namespace)
        if name is not None:
            # load it and set it
            from uvaCultivator.uvaCultivatorExport import getClassByName
            # save to settings
            self._settings = self.mergeDictionary(
                self._settings, getClassByName(name, namespace=namespace), source_namespace="", copy_target=False
            )
        # return setting
        return self.retrieveSetting(name="class", default=self._default_settings, namespace=namespace)

    def loadExporterClass(self, force=False):
        if force or self.retrieveSetting(name="class", default=self._default_settings, namespace="exporter") is None:
            name = self.retrieveSetting(name="name", default=self._default_settings, namespace="exporter")
            self.loadDataClass(name=name)
        return self.retrieveSetting(name="class", default=self._default_settings, namespace="exporter")

    def loadImporterClass(self, force=False):
        if force or self.retrieveSetting(name="class", default=self._default_settings, namespace="importer") is None:
            name = self.retrieveSetting(name="name", default=self._default_settings, namespace="importer")
            self.loadDataClass(name=name)
        return self.retrieveSetting(name="class", default=self._default_settings, namespace="importer")

    def loadDataName(self, namespace="exporter"):
        """
        Get the name of the class that should be used given the namespace in the settings
        :param namespace: Namespace in settings
        :type namespace: str
        :return: Name of the class
        :rtype: str
        """
        if self.retrieveSetting(name="name", default=self._default_settings, namespace=namespace) is None:
            # load the class, will lead to load of correct name
            self.loadExporterClass(True)
        return self.retrieveSetting(name="name", default=self._default_settings, namespace=namespace)

    def loadExporterName(self, force=False):
        if force or self.retrieveSetting(name="name", default=self._default_settings, namespace="exporter") is None:
            self.loadDataName(namespace="exporter")
        return self.retrieveSetting(name="name", default=self._default_settings, namespace="exporter")

    def loadImporterName(self, force=False):
        if force or self.retrieveSetting(name="name", default=self.getDefaultSettings(), namespace="importer") is None:
            self.loadDataName(namespace="importer")
        return self.retrieveSetting(name="name", default=self.getDefaultSettings(), namespace="importer")
