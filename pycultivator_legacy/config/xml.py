#!/usr/bin/python
"""
This module supports reading an XML formatted Config file.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from os import path
from datetime import datetime as dt
import copy
from base import BaseConfig
from pycultivator_legacy.core.xml import XML


class XMLConfig(BaseConfig, XML):
    """
    AbstractCultivatorConfig implementations, using a XML-formatted configuration file
    """

    # Compatible versions of pycultivator-legacy configuration files
    COMPATIBLE_VERSIONS = {
        # dictionary with compatible versions
        # 2 = fully supported
        # 1 = deprecated
        # 0 = not supported
        "1.6": 2,
        "1.5": 1,
        "1.4": 1,
    }

    def __init__(self, settings=None):
        BaseConfig.__init__(self, settings=settings)
        XML.__init__(self)

    @classmethod
    def loadConfig(cls, source, project_id=None):
        """
        Factory method to load a XML Configuration file into a xmlCultivatorConfig class.
        :type cls: xmlCultivatorConfig
        :param source: Path of the XML File
        :type source: str
        :rtype: xmlCultivatorConfig
        """
        c = None
        try:
            if source is not None and path.isfile(source):
                project_id = cls.readProjectId(source)
                if cls.isValid(source):
                    c = super(XMLConfig, cls).loadConfig(source, project_id)
                    """ :type : xmlCultivatorConfig """
                    c._path = source
                    c.loadSource(source)
                    if not c.refresh():
                        c = None
                else:
                    cls.getLog().warning("{} is not a valid config file".format(source))
            else:
                cls.getLog().warning("Unable to read and parse {}".format(source))
        except Exception as e:
            cls.getLog().critical("Unknown error raised: {}".format(e))
        return c

    @classmethod
    def createFromFile(cls, xml_file):
        return cls.loadConfig(xml_file)

    @classmethod
    def createFromXML(cls, xml_source, config_dir=None):
        result = None
        if cls.isCompatible(xml_source, isPath=False, config_dir=config_dir):
            project_id = cls.readProjectId(xml_source, False)
            if project_id is not None:
                result = cls(settings={"project_id": project_id})
                result.loadSource(xml_source, False)
        return result

    @classmethod
    def readProjectId(cls, source, isPath=True):
        """Read the project id from a configuration file

        :param source: path to the configuration file
        :type source: str
        :return: the project id in the configuration file or None
        :rtype: str, None
        """
        result = None
        try:
            xml = cls.parseSource(source, isPath)
            if xml is not None:
                result = cls.validateValue(xml.get("id"), str)
        except Exception as e:
            cls.getLog().critical("Unable to read the project id: {}".format(e))
        return result

    @classmethod
    def readTimeZero(cls, source, isPath=True):
        """Read the time_zero from a configuration file

        :param source: path to the configuration file
        :type source: str
        :return: the time zero in the configuration file or None
        :rtype: datetime.datetime, None
        """
        result = None
        try:
            xml = cls.parseSource(source, isPath)
            if xml is not None:
                result = cls.validateValue(xml.get("start_date"), dt)
        except Exception as e:
            cls.getLog().critical("Unable to read the time zero: {}".format(e))
        return result

    @classmethod
    def readState(cls, source, isPath=True):
        """Read the experiment state from a configuration file

        :param source: path to the configuration file
        :type source: str
        :return: the state of the experiment in the configuration file or None
        :rtype: bool, None
        """
        result = None
        try:
            xml = cls.parseSource(source, isPath)
            if xml is not None:
                result = cls.validateValue(xml.get("state"), bool)
        except Exception as e:
            cls.getLog().critical("Unknown error raised: {}".format(e))
        return result

    @classmethod
    def readSerialPort(cls, source, isPath=True):
        result = None
        try:
            xml = cls.parseSource(source, isPath)
            ns = {'u': cls.extractSchemaNameSpace(xml)}
            if xml is not None:
                e_serial = xml.find("./u:instrument[@type='multicultivator']/u:serial", ns)
                if e_serial is not None:
                    result = cls.validateValue(e_serial.get("port"), str)
        except Exception as e:
            cls.getLog().critical("Unknown error raised: {}".format(e))
        return result

    def refresh(self, fromFile=True):
        result = False
        try:
            if fromFile:
                self.reload()
            if self._xml is not None:
                self.loadExperimentSettings()
                if self._serial is not None:
                    self._serial = self.loadController(type(self._serial), self._serial.isConnected(), True)
                if self._cultivator is not None:
                    self._cultivator = self.loadCultivator(self._serial, True)
                result = True
            else:
                self.getLog().error("Unable to parse the xml file")
        except IOError:
            self.getLog().error("Cannot open {}".format(self._path))
        except Exception as e:
            self.getLog().critical("Unknown error raised: {}".format(e))
        return result

    def save(self, xml_path=None, encoding="utf-8", pretty_print=True):
        return XML.save(self, xml_path=xml_path, encoding=encoding, pretty_print=pretty_print)

    def setXML(self, xml):
        if xml is not None:
            self._xml = xml

    def getPath(self):
        return self._path

    def setPath(self, config_path):
        if path.exists(path.dirname(config_path)):
            self._path = config_path
        return self._path

    #
    #   Object getters and setters
    #

    def getCultivatorElement(self, root=None):
        """
        :rtype: lxml.ElementTree._Element
        """
        result = None
        if root is None:
            root = self.getXML()
        if root is not None:
            result = root.find("./u:instrument[@type='multicultivator']", self.getNameSpace())
        return result

    def getSerialElement(self, root=None):
        """
        :rtype: lxml.ElementTree._Element
        """
        result = None
        if root is None:
         root = self.getCultivatorElement()
        if root is not None:
            result = root.find("./u:serial", self.getNameSpace())
        return result

    def getSerialRateElements(self, root=None):
        """
        :rtype: list
        """
        result = []
        e = self.getSerialElement(root=root)
        if e is not None:
            result = e.findall("./u:rates/u:rate", self.getNameSpace())
        return result

    def getChannelsElement(self, root=None):
        """
        :rtype: lxml.etree._Element._Element
        """
        result = None
        element = self.getCultivatorElement(root=root)
        if element is not None:
            result = element.find("./u:channels", self.getNameSpace())
        return result

    def getChannelElements(self, root=None):
        """ Return all channel elements found in the configuration file

        :rtype: list
        """
        result = []
        if root is None:
            root = self.getChannelsElement()
        if root is not None:
            result = root.findall("./u:channel", self.getNameSpace())
        return result

    def getChannelElement(self, channel, root=None):
        """ Return the Channel element

        :type channel: int
        :type root: lxml.etree._Element._Element
        :return:
        :rtype: lxml.etree._Element._Element
        """
        result = None
        elements = self.getChannelElements(root=root)
        elements = filter(lambda c: c.get("id") == str(channel), elements)
        if len(elements) > 0:
            result = elements[0]
        return result

    def getLightElements(self, root=None):
        """ Returns all light elements found under root

        :param root:
        :return: List of element
        :rtype: list[lxml.etree._Element._Element]
        """
        result = []
        if root is None:
            root = self.getChannelsElement()
        if root is not None:
            result = root.findall("./u:light", self.getNameSpace())
        return result

    def getLightElement(self, root=None):
        """Returns one light element

        :type root: lxml.etree._Element._Element
        :rtype: lxml.etree._Element._Element
        """
        result = None
        results = self.getLightElements(root)
        if len(results) > 0:
            result = results[0]
        return result

    def getDependencyElements(self, root=None):
        """Returns all dependency elements found under root

        :type root: lxml.etree._Element._Element
        :rtype: list
        """
        result = []
        if root is None:
            root = self.getLightElement(self.getChannelsElement())
        if root is not None:
            result = root.findall("./u:dependency", self.getNameSpace())
        return result

    def getSettingsElement(self, root=None):
        """Returns one settings element found under root

        :type root: lxml.etree._Element._Element
        :rtype: lxml.etree._Element._Element
        """
        result = None
        if root is None:
            root = self.getRoot()
        if root is not None:
            result = root.find("./u:settings", self.getNameSpace())
        return result

    def getSettingElements(self, root=None):
        """Find all setting elements under root

        :type root: lxml.etree._Element._Element
        :rtype: list[lxml.etree._Element._Element]
        """
        result = []
        settings_e = self.getSettingsElement(root)
        if settings_e is not None:
            result = settings_e.findall("./u:setting", self.getNameSpace())
        return result

    def getSettingElement(self, idx, root=None):
        """Find one setting element under root with the given index

        :rtype: None or lxml.etree._Element._Element
        """
        result = None
        results = self.getSettingElements(root)
        if len(results) > idx:
            result = results[idx]
        return result

    def getCalibrationsElement(self, root=None):
        result = None
        if root is None:
            root = self.getChannelsElement()
        if root is not None:
            result = root.find("./u:calibrations", self.getNameSpace())
        return result

    def getCalibrationElements(self, variable="od", root=None):
        """Return all calibration elements with the given variable

        :type root: lxml.etree._Element._Element or None
        :rtype: list[lxml.etree._Element._Element]
        """
        results= []
        if root is None:
            root = self.getCalibrationsElement()
        if root is not None:
            results = root.xpath(
                "./u:calibration[@variable=$variable]",
                namespaces=self.getNameSpace(), variable=variable
            )
        return results

    def getCalibrationElement(self, variable="od", root=None, **kwargs):
        """Return one calibration element with the given type

        :rtype: None or lxml.etree._Element._Element
        """
        result = None
        results = self.getCalibrationElements(variable=variable, root=root)
        # filter by extra arguments
        for k, v in kwargs.items():
            results = filter(lambda e: e.get(k) == v, results)
        if len(results) > 0:
            result = results[0]
        return result

    def getPolynomialElements(self, root=None):
        results = []
        if root is None:
            root = self.getCalibrationElement()
        if root is not None:
            results = root.findall(
                "./u:polynomial", self.getNameSpace()
            )
        return results

    def getPolynomialElement(self, root=None, **kwargs):
        result = None
        results = self.getPolynomialElements(root=root)
        # filter by extra arguments
        for k, v in kwargs.items():
            results = filter(lambda e: e.get(k) == v, results)
        if len(results) > 0:
            result = results[0]
        return result

    def setProjectId(self, project_id):
        """Set the project id of the configuration file"""
        default = self.retrieveSetting(name="project_id", default=self._default_settings)
        self._settings["project_id"] = self.validateValue(project_id, v_type=str, default_value=default)
        self.getRoot().set("id", self.retrieveSetting(name="project_id", default=self._default_settings))

    def setExperimentStartDate(self, exp_start_date):
        """Set the start date of the experiment in the configuration file"""
        default = self.retrieveSetting(name="time_zero", default=self._default_settings)
        self._settings["time_zero"] = self.validateValue(exp_start_date, v_type=dt, default_value=default)
        self.getRoot().set("start_date", self._settings["time_zero"].strftime(self.DT_FORMAT))

    def getExperimentState(self):
        """Get the state of the experiment in the configuration file"""
        return self.getAttribute(self.getRoot(), "state")

    def setExperimentState(self, state):
        """Set the state of the experiment in the configuration file"""
        default = self.retrieveSetting(name="state", default=self._default_settings)
        self._settings["state"] = self.validateValue(state, v_type=bool, default_value=default)
        self.getRoot().set("state", "true" if self._settings["state"] is True else "false")

    def getSerialPort(self):
        result = None
        settings = self.getSettingsElement(self.getSerialElement())
        setting_e = settings.findall("./u:setting[@name='port']", self.getNameSpace())
        if len(setting_e) > 0:
            result = self.getValue(setting_e[0])
        return result

    def setSerialPort(self, port):
        settings = self.getSettingsElement(self.getSerialElement())
        setting_e = settings.findall("./u:setting[@name='port']", self.getNameSpace())
        if len(setting_e) > 0:
            setting_e[0].set("value", port)
        #
        #   Construct objects from configuration
        #

    def parseSettings(self, source=None, defaults=None, namespace=""):
        """Reads the settings element and parse all found settings

        :param source: The Element that contains the settings element
        :type source: lxml.ElementTree._Element
        :return: Dictionary with setting elements, where name is used as key to retrieve the value
        :rtype: dict
        """
        if defaults is None:
            result = {}
        else:
            result = copy.copy(defaults)
        settings_e = self.getSettingElements(source)
        for setting_e in settings_e:
            setting_name = self.validateValue(setting_e.get("name"), str)
            setting_type = self.validateValue(setting_e.get("type"), str)
            setting_value = self.validateValue(setting_e.get("value"), str)
            if None not in [setting_name, setting_type, setting_value]:
                setting_value = self.validateValue(setting_value, setting_type)
                if setting_value is not None:
                    if namespace != "":
                        setting_name = "{}.{}".format(namespace, setting_name)
                    result[setting_name] = setting_value
        return result

    def loadControllerSettings(self, root=None, settings=None, **kwargs):
        """Read all controller settings"""
        if settings is None:
            settings = {}
        settings = self.mergeDictionary(settings, kwargs)
        e_serial = self.getSerialElement(root=root)
        if e_serial is not None:
            settings = self.parseSettings(source=e_serial, namespace="serial.controller")
        return settings

    def loadCultivator(self, serial=None, force=False, fake=False):
        """Create a Cultivator object with the parameters from the configuration file

        :return:
        :rtype: None or uvaCultivator.uvaCultivator.uvaCultivator
        """
        if force or self._cultivator is None:
            self._cultivator = None
            e_cult = self.getCultivatorElement()
            e_channels = self.getChannelsElement()
            if None not in [e_cult, e_channels]:
                if serial is None:
                    serial = self.loadPsiSerial(fake=fake, connect=True)
                from uvaCultivator.uvaCultivator import Cultivator
                n_channels = len(self.getChannelElements())
                self.loadDefaultODCalibrations()
                self.loadDefaultLight()
                if serial is not self._serial:
                    self._serial = serial
                    self._serial.connect()
                settings = self.loadCultivatorSettings(e_cult, n_channels)
                self._cultivator = Cultivator(self._serial, settings=settings)
                channels_e = self.getChannelElements()
                for channel_e in channels_e:
                    idx = self.validateValue(channel_e.get("id"), int)
                    # load channel
                    channel = self.loadChannel(idx, True)
                    self._cultivator.addChannel(channel, channel.getIndex())
                self._cultivator.refreshLightsSettings()
        return self._cultivator

    def _parseODWavelength(self, xml, default=720):
        wavelength = default
        # extract wavelength
        id = self.validateValue(xml.get("id"), int, None)
        source = self.validateValue(xml.get("source"), int, None)
        if id is not None:
            wavelength = id
        if source is not None:
            wavelength = source
        return wavelength

    def _parseODCalibration(self, xml, config, default=None):
        from uvaCultivator.uvaPolynomialDependency import ODCalibrationDependency
        # read wavelength
        wavelength = self._parseODWavelength(xml)
        # load the hard-coded default
        if default is None:
            default = ODCalibrationDependency.loadDefault().copy()
            default.setId(wavelength)
            default.setSource(wavelength)
        return ODCalibrationDependency.createFromXMLElement(xml, config=config, default=default)

    def loadDefaultODCalibrations(self, default=None):
        """ Load all default OD Calibrations

        Will load all default calibrations once.

        :param default: None or uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        :return:
        :rtype: dict[int, uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency]
        """
        # check if defaults is initialized
        if self._default_od_calibrations is None:
            self._default_od_calibrations = {}
            # load
            e_channels = self.getChannelsElement()
            elements = []
            if e_channels is not None:
                e_calibrations = self.getCalibrationsElement(e_channels)
                if e_calibrations is not None:
                    elements = self.getCalibrationElements(variable="od", root=e_calibrations)
            for element in elements:
                calibration = self._parseODCalibration(element, config=self, default=default)
                if calibration is not None:
                    self._default_od_calibrations[calibration.getId()] = calibration
        return self._default_od_calibrations

    def loadODCalibrations(self, channel, default=None):
        """ Load the OD calibrations from the configuration source

        :param channel:
        :type channel: int or lxml.etree._Element._Element
        :return:
        :rtype: dict[int, uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency]
        """
        results = {}
        defaults = self.loadDefaultODCalibrations(default=default)
        e_channel = channel
        if isinstance(e_channel, int):
            e_channel = self.getChannelElement(channel)
        elements = []
        if e_channel is not None:
            e_calibrations = self.getCalibrationsElement(e_channel)
            if e_calibrations is not None:
                elements = self.getCalibrationElements(variable="od", root=e_calibrations)
        for wavelength in defaults.keys():
            # load default
            calibration = defaults.get(wavelength).copy()
            element = None
            # find corresponding element
            for element in elements:
                # read wavelength
                if wavelength == self._parseODWavelength(element):
                    # decrease looping, by removing found element
                    elements.remove(element)
                    break
            # parse
            if element is not None:
                result = self._parseODCalibration(element, config=self, default=calibration)
                if result is not None and result.isActive():
                    calibration = result
            # store in dict
            if calibration is not None:
                results[calibration.getId()] = calibration
        return results

    def loadChannel(self, index, force=False):
        """
        Return the Channel object with the parameters from the configuration file
        :type index: int
        :rtype: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        """
        result = None
        if self._cultivator is not None and self._cultivator.hasChannel(index):
            result = self._cultivator.getChannel(index)
        if force:
            e_channel = self.getChannelElement(index)
            if e_channel is not None:
                from uvaCultivator.uvaCultivatorChannel import CultivatorChannel
                result = CultivatorChannel.createFromXMLElement(e_channel, self)
        return result

    def loadDefaultLight(self):
        """Create a LightRegime object from the default xml

        :rtype: uvaCultivator.uvaCultivatorLight.CultivatorLight
        """
        if self._default_light is None:
            e_channels = self.getChannelsElement()
            if e_channels is not None:
                from uvaCultivator.uvaCultivatorLight import CultivatorLight
                self._default_light = CultivatorLight.createFromDefaultXML(e_channels, self)
        return self._default_light

    def loadLight(self, idx, channel=None, force=False):
        """Create a LightRegime object with the parameters from the configuration file

        :type idx: int
        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :rtype: uvaCultivator.uvaCultivatorLight.CultivatorLight
        """
        result = None
        if channel is None:
            channel = self.loadChannel(idx)
        e_channel = self.getChannelElement(idx)
        if e_channel is not None:
            from uvaCultivator.uvaCultivatorLight import CultivatorLight
            result = CultivatorLight.createFromXMLElement(e_channel, self)
        if result is None:
            result = self._default_light
        return result

    def loadProjectId(self):
        """Reads the project id from the configuration xml

        :return: the project id
        :rtype: str
        """
        return self.validateValue(self.getRoot().get("id"), "")

    def loadStartDate(self):
        """Reads the start date time from the configuration xml

        :rtype: datetime.datetime
        """
        return self.validateValue(self.getRoot().get("start_date"), dt)

    def loadState(self):
        """Reads the state of the configuration source

        :rtype: bool
        """
        return self.validateValue(self.getRoot().get("state"), bool, False)
