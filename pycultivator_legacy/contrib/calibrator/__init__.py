"""The Calibrator Application"""

from PyQt5 import QtCore, QtGui, QtWidgets

__all__ = [
    'QtCore', 'QtGui', 'QtWidgets'
]

