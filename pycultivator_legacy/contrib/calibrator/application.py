#!/usr/bin/env python
# coding=utf-8
"""
Graphical application for blanking of the calibration models
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import sys
import argparse
import traceback

# get all necessary imports
from pycultivator_legacy.contrib.scripts.guiScript import GUIScript
from pycultivator_legacy.contrib.calibrator import QtWidgets
from controllers.main import MainController


class CalibratorScript(GUIScript):

    def execute(self):
        # load application
        app = QtWidgets.QApplication(sys.argv)
        try:
            # Load Main Controller
            c = MainController(app=app)
            c.setRootPath(self.getConfigDir())
            # Start Main Controller
            c.start(controller=c, app=app, execute=True)
        except:
            msg = "Error:\n{}".format(traceback.format_exc())
            self.getLog().critical(msg)
        finally:
            app.exit(0)


def start():
    controller = CalibratorScript
    gs = CalibratorScript(__file__, controller)
    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Run the cultivation protocol with all active config files.')
    # configuration settings
    parser.add_argument('--config-dir', action="store", dest="config_dir",
                        help='Path to the directory containing the config files.')
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that contains default values.')
    # log settings
    parser.add_argument('--log', action="store", dest='log_dir',
                        help='Path to the directory where log file will be written.')
    # protocol settings
    # doesnt make sense to force run all configurations
    # parser.add_argument('-f', action="store_true", dest='force_run',
    #                     help='Force running the (inactive) configuration.')
    # script settings
    parser.add_argument('-V', action="store_true", dest="verbose",
                        help="Print log messages to the console")
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    # set defaults
    parser.set_defaults(
        config_dir=gs.getConfigDir(), log_dir=gs.getLogDir(),
        verbose=gs.isVerbose(), simulate_fake=False, ini_path=None,
        simulate=gs.isReporting(), use_fake=gs.fakesConnection
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the script
    if not gs.loadArguments(args, settings={"controller.class": controller}, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    gs.start()


if __name__ == "__main__":
    start()