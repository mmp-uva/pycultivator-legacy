# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore, QtWidgets
from uvaCultivator import uvaLog

import sys
import threading
from Queue import Queue, Empty

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

APP = None


def loadApplication():
    """Load the QtGUi.QApplication

    :rtype: PyQt5.QtWidgets.QApplication.QApplication
    """
    global APP
    if APP is None:
        APP = QtWidgets.QApplication(sys.argv)
    return APP


class ClearableQueue(Queue):
    """ A custom queue subclass that provides a :meth:`clear` method.

    From: http://stackoverflow.com/a/31892187
    """

    def clear(self):
        """Clears all items from the queue.

        :raises: ValueError when unfinished is negative
        """
        with self.mutex:
            unfinished = self.unfinished_tasks - len(self.queue)
            if unfinished <= 0:
                if unfinished < 0:
                    raise ValueError('task_done() called too many times')
                self.all_tasks_done.notify_all()
            self.unfinished_tasks = unfinished
            self.queue.clear()
            self.not_full.notify_all()


class BaseController(QtCore.QObject):

    _log = None

    gui_changed = QtCore.pyqtSignal()

    def __init__(self, parent=None, app=None, **kwargs):
        """Initialise the controller

        :type parent: calibrator.controllers.base.BaseController
        :type app: PyQt5.QtWidgets.QApplication.QApplication
        """
        QtCore.QObject.__init__(self, parent)
        self._parent = parent
        # application
        self._app = app
        if self._app is None:
            self._app = loadApplication()
        # view
        self._view = None
        # workers
        self._threads = []

    def __del__(self):
        # clean threads
        self.clearThreads()

    @classmethod
    def start(cls, parent=None, app=None, **kwargs):
        """Starts the controller

        :type parent: calibrator.controllers.base.BaseController
        :type app: PyQt5.QtWidgets.QApplication.QApplication
        :rtype: bool
        """
        raise NotImplementedError

    def stop(self):
        """Method that will be called when this controller is finished

        :rtype: bool
        """
        return True

    @property
    def threads(self):
        """

        :rtype: list[pycultivator_legacy.contrib.calibrator.controllers.base.BaseWorker]
        """
        return self._threads

    def clearThreads(self):
        for thread in self.threads:
            thread.quit()

    @classmethod
    def getLog(cls):
        if cls._log is None:
            cls._log = uvaLog.getLogger(__name__)
        return cls._log

    def getApplication(self):
        """Returns the Application object of this controller

        :return:
        :rtype: PyQt5.QtWidgets.QApplication.QApplication
        """
        return self._app

    def getParent(self):
        """Returns the parent controller of this controller

        :rtype: None or calibrator.controllers.base.BaseController
        """
        return self._parent

    def hasParent(self):
        """Returns whether this controller has a parent controller"""
        return self.getParent() is not None

    def getView(self):
        """Returns the view of this controller

        :rtype: None or calibrator.views.base.BaseView
        """
        return self._view

    def hasView(self):
        return self.getView() is not None


class BaseWorker(QtCore.QThread):
    """A simple worker"""

    progressed = QtCore.pyqtSignal(int)
    _log = None

    def __init__(self, model=None, parent=None):
        """Initialise worker thread

        :type model: calibrator.models.base.BaseModel
        :type parent: calibrator.controllers.base.BaseController
        """
        QtCore.QThread.__init__(self, parent)
        self._lock = threading.RLock()
        self._seq_num = 0
        self._unfinished_jobs = ClearableQueue()
        """:type: Queue.Queue[BaseJob]"""
        self._has_unfinished_jobs = threading.Event()
        """:type: threading.Event"""
        self._finished_jobs = ClearableQueue()
        """:type: Queue.Queue[BaseJob]"""
        self._has_finished_jobs = threading.Event()
        """:type: threading.Event"""
        self._model = model
        self._active_job = None
        self._exiting = False

    def run(self):
        self.setExiting(False)
        while not self.isExiting():
            self.setActiveJob()
            try:
                # retrieve an unfinished job
                self.setActiveJob(self._getFromQueue(self._unfinished_jobs))
                if self.getActiveJob() is not None:
                    self.getLog().debug("Received new job: {}".format(self.getActiveJob().getName()))
                    # we got the job, so we clear the signal
                    self._has_unfinished_jobs.clear()
                    # execute unfinished job
                    self.getActiveJob().run()
                    # put job in finished jobs
                    self._addToQueue(self._finished_jobs, self.getActiveJob())
                    # in case somebody listens to this event, we set it
                    self._has_finished_jobs.set()
                    # reset active job
                    self.setActiveJob()
            except Exception as e:
                self.getLog().warning("Action - %s - failed: %s" % (self.getActiveJob(), e))
            # do some waiting, timeout on 1 second
            self._has_unfinished_jobs.wait(0.5)
        return

    def quit(self):
        self.setExiting(True)
        QtCore.QThread.quit(self)

    @classmethod
    def getLog(cls):
        if cls._log is None:
            cls._log = uvaLog.getLogger(cls.__name__)
        return cls._log

    def getLock(self):
        return self._lock

    def getActiveJob(self):
        """Returns the currently active job

        :return:
        :rtype: uvaTools.uvaGUI.calibrator.controllers.base.BaseJob
        """
        with self.getLock():
            result = self._active_job
        return result

    def setActiveJob(self, job=None):
        with self.getLock():
            self._active_job = job
        return self

    def isExiting(self):
        with self.getLock():
            result = self._exiting
        return result

    def setExiting(self, state):
        with self.getLock():
            self._exiting = state is True
        return self

    def getModel(self):
        """Returns the model used by this worker

        :return:
        :rtype: calibrator.models.base.BaseModel
        """
        return self._model

    def hasModel(self):
        return self.getModel() is not None

    def setModel(self, model):
        with self.getLock():
            self._model = model
        return self

    def _getFromQueue(self, queue, block=False):
        """Returns an action from the queue, or None if nothing is in the queue

        :rtype: BaseAction
        """
        result = None
        try:
            result = queue.get(block)
            queue.task_done()
        except Empty:
            pass
        return result

    def _addToQueue(self, queue, item):
        """Adds an item to a queue"""
        result = queue.put(item)
        # notify thread
        self._has_unfinished_jobs.set()
        return result

    def hasJob(self):
        return not self._unfinished_jobs.empty()

    def createJob(self, fun, callback=None, args=None, **kwargs):
        return BaseJob.createJob(fun=fun, args=args, callback=callback, **kwargs)

    def doJob(self, fun, callback=None, args=None, **kwargs):
        """Does the job and returns a succ

        :param fun: Function that will be called by the Job
        :type fun:
        :param callback: Callback function that will be called when the result is available. Will block if None.
        :type callback: None or calibrator.controllers.base.BaseWorker
        :return:
        :rtype: tuple[bool, calibrator.controllers.base.BaseJob]
        """
        result = None
        # add an additional self parameter
        job = self.createJob(fun=fun, callback=callback, args=args, **kwargs)
        if job is None:
            raise Exception("Unable to create measurement job, createJob returned None")
        # add job to queue
        self.addJob(job)
        if callback is None:
            job.finished.wait()
            # finished!
            self.getFinishedJob(block=False)
            result = job.succeeded()
        return result, job

    def queueJob(self, fun, *args, **kwargs):
        """Creates a job and adds it to the job queue"""
        job = BaseJob.createJob(fun, *args, **kwargs)
        return self.addJob(job)

    def addJob(self, job):
        """Adds a job to the unfinished job queue

        :param job:
        :type job: calibrator.controllers.base.BaseJob
        :return:
        :rtype:
        """
        job.setSequenceNumber(self.getSequenceNumber())
        self._addToQueue(self._unfinished_jobs, job)
        return self

    def getFinishedJobs(self, block=False):
        """Returns all finished jobs (until no is left)"""
        results = []
        has_result = True
        while has_result:
            result = self.getFinishedJob(block)
            has_result = result is not None
            if has_result:
                results.append(result)
        return results

    def getFinishedJob(self, block=False):
        """Returns a finished job (if present)"""
        return self._getFromQueue(self._finished_jobs, block)

    def hasFinishedJobs(self):
        """Returns whether there is a finished job"""
        return not self._finished_jobs.empty()

    def getSequenceNumber(self):
        with self.getLock():
            n = self._seq_num
            # increase with 1
            self._seq_num += 1
        return n

    def getProgression(self):
        return self.getActiveJob().getProgression()

    def setProgression(self, value):
        self.getActiveJob().setProgression(value)
        self.progressed.emit(self.getProgression())
        return self

    def increaseProgression(self, increase):
        self.setProgression(self.getProgression() + increase)
        return self

    def clearProgression(self):
        self.setProgression(0)
        return self


class BaseJob(object):
    """A job object, which holds a reference to the function to be executed and its result (Thread-safe)"""

    _log = None

    @classmethod
    def createJob(cls, fun, seqnr=0, callback=None, args=None, **kwargs):
        result = None
        if callable(fun):
            result = cls(fun, seqnr=seqnr, args=args, callback=callback, **kwargs)
        return result

    @classmethod
    def getLog(cls):
        if cls._log is None:
            cls._log = uvaLog.getLogger(cls.__name__)
        return cls._log

    def __init__(self, fun, seqnr=0, callback=None, args=None, **kwargs):
        self._lock = threading.RLock()
        # job settings
        self._seqnr = seqnr
        self._fun = fun
        self._callback = callback
        self._name = None
        if self._name is None:
            self._name = self._fun.__name__
        self._args = args
        if self._args is None:
            self._args = []
        self._kwargs = kwargs
        if self._kwargs is None:
            self._kwargs = {}
        # progress settings
        self._progress = 0
        self._show_progress = False
        self._progress_dialog = None
        self._progress_title = "Please wait.."
        self._progress_cancel = "Cancel"
        self._progress_minimum = 0
        self._progress_maximum = 100
        # result settings
        self._result = None
        self._success = False
        self.finished = threading.Event()
        """:type: threading.Event"""

    def run(self):
        """Execute the action"""
        try:
            # run function
            self.finished.clear()
            with self.getLock():
                # clear finish flags
                self._success = False
                self._result = None
                # create progress dialog
                if self.showsProgress():
                    self.openProgressDialog()
                # do the operation
                self.getLog().debug("Running: {}".format(self.getName()))
                self._result = self._fun(*self._args, **self._kwargs)
                self.getLog().debug("Finished: {}".format(self.getName()))
                # close progress dialog by setting it to max value
                if self.showsProgress():
                    self.getProgressDialog().setValue(self.getProgressMaximum())
            # set success state
            self._success = True
            # callback
            if self.hasCallback() and callable(self.getCallback()):
                # do callback
                self.getCallback()(self)
        except Exception as e:
            self.getLog().error("Failed to execute %s: %s" % (self.getName(), e))
        # set finish flags
        self.finished.set()
        return self.succeeded()

    @classmethod
    def getLog(cls):
        if cls._log is None:
            cls._log = uvaLog.getLogger(cls.__name__)
        return cls._log

    def getLock(self):
        return self._lock

    def getName(self):
        with self.getLock():
            result = self._name
        return result

    def setName(self, name):
        with self.getLock():
            self._name = name
        return self

    def getSequenceNumber(self):
        with self.getLock():
            result = self._seqnr
        return result

    def setSequenceNumber(self, number):
        with self.getLock():
            self._seqnr = number
        return self

    def getFunction(self):
        with self.getLock():
            result = self._fun
        return result

    def getCallback(self):
        with self.getLock():
            result = self._callback
        return result

    def hasCallback(self):
        return self.getCallback() is not None

    def setCallback(self, callback):
        with self.getLock():
            self._callback = callback
        return self

    def getArguments(self):
        with self.getLock():
            result = self._args
        return result

    def getKeywordArguments(self):
        with self.getLock():
            result = self._kwargs
        return result

    def succeeded(self):
        with self.getLock():
            result = self._success is True
        return result

    def isFinished(self):
        return self.finished.is_set()

    def hasResult(self):
        with self.getLock():
            result = self._result is not None
        return result

    def getResult(self):
        with self.getLock():
            result = self._result
        return result

    def getProgression(self):
        with self.getLock():
            result = self._progress
        return result

    def setProgression(self, value):
        self._progress = value
        if self.showsProgress() and self.hasProgressDialog():
            self.getProgressDialog().setValue(value)

    def increaseProgression(self, increase):
        self.setProgression(self.getProgression() + increase)

    def clearProgression(self):
        self.setProgression(0)

    def getProgressTitle(self):
        with self.getLock():
            result = self._progress_title
        return result

    def setProgressTitle(self, title):
        with self.getLock():
            self._progress_title = title
        return self

    def getProgressMinimum(self):
        with self.getLock():
            result = self._progress_minimum
        return result

    def setProgressMinimum(self, minimum):
        with self.getLock():
            self._progress_minimum = minimum
        return self

    def getProgressMaximum(self):
        with self.getLock():
            result = self._progress_maximum
        return result

    def setProgressMaximum(self, maximum):
        with self.getLock():
            self._progress_maximum = maximum
        return self

    @staticmethod
    def createProgressDialog(title, cancel=None, minimum=0, maximum=100, parent=None):
        if cancel is None:
            cancel = "Cancel"
        dialog = QtWidgets.QProgressDialog(
            title, cancel, minimum, maximum, parent
        )
        dialog.setValue(0)
        dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        dialog.setAutoReset(False)
        dialog.setAutoClose(False)
        return dialog

    def openProgressDialog(self, value_signal=None, parent=None):
        title = self.getProgressTitle()
        cancel = "Cancel"
        minimum, maximum = self.getProgressMinimum(), self.getProgressMaximum()
        dialog = self.createProgressDialog(
            title=title, cancel=cancel, minimum=maximum, maximum=maximum, parent=parent
        )
        if value_signal is not None:
            value_signal.connect(dialog.setValue)
        dialog.open()
        with self.getLock():
            self._progress_dialog = dialog

    def closeProgressDialog(self, value_signal=None):
        if value_signal is not None:
            value_signal.disconnect(self.getProgressDialog().setValue)
        self.getProgressDialog().done(QtWidgets.QDialog.Accepted)
        self.getProgressDialog().close()
        with self.getLock():
            # destroy dialog
            self._progress_dialog = None

    def getProgressDialog(self):
        """Returns the progress dialog

        :return:
        :rtype: QtGui.QProgressDialog
        """
        with self.getLock():
            result = self._progress_dialog
        return result

    def hasProgressDialog(self):
        return self.getProgressDialog() is not None

    def showsProgress(self):
        with self.getLock():
            result = self._show_progress is True
        return result

    def showProgress(self, state):
        with self.getLock():
            self._show_progress = state
        return self

    def __str__(self):
        return "Action #%s: %s" % (self.getSequenceNumber(), self.getName())
