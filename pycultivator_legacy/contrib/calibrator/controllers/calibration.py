# coding=utf-8

from base import BaseController
from pycultivator_legacy.contrib.calibrator import QtCore
from pycultivator_legacy.contrib.calibrator.models.base import Locked
from pycultivator_legacy.contrib.calibrator.views.calibration import CalibrationView
from pycultivator_legacy.contrib.calibrator.models.calibration import PolynomialTableModel, CoefficientTableModel
from uvaCultivator.uvaPolynomialDependency import ODCalibrationDependency

import threading
import traceback

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class CalibrationController(BaseController):
    _calibration_lock = threading.Semaphore()

    def __init__(self, calibration=None, parent=None, app=None, **kwargs):
        BaseController.__init__(self, parent=parent, app=app, **kwargs)
        # models
        self._calibration = calibration
        polynomials = []
        if self.hasCalibration():
            polynomials = self.getCalibration().getPolynomials()
        self._poly_model = PolynomialTableModel(polynomials=polynomials)
        self._coef_model = CoefficientTableModel(coefficients=[])
        self.getCoefficientsModel().setEditable(True)
        # view
        self._view = CalibrationView(controller=self, parent=self.getParent().getView())
        # update view from model
        self.processModelUpdate()
        # connect slots to signals from view and model
        self.connectPolynomialModel()
        self.connectCoefficientsModel()
        self.connectView()

    # slots

    @classmethod
    def start(cls, calibration=None, parent=None, app=None, **kwargs):
        """Start the controller and opens the view

        :param calibration: The calibration to be edited
        :type calibration: uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        :param parent: The parent controller of this contoller
        :type parent: calibrator.controllers.base.BaseController
        :param app: Reference to the GUI Application
        :type app: PyQt4.QtGui.QApplication.QApplication
        :return: The new calibration object
        :rtype: tuple[bool, uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency]
        """
        result = False
        if calibration is None:
            calibration = ODCalibrationDependency.loadDefault()
        old_calibration = calibration.copy()
        try:
            c = cls(calibration=calibration, parent=parent, app=app, **kwargs)
            # execute dialog
            result = c.getView().exec_() == c.getView().Accepted
            # retrieve changes from GUI to model
            # c.processViewUpdate(c.getView())
            # if accepted we update the original model with the new settings
            if not result:
                # restore old settings
                old_calibration.copy(calibration)
        except:
            cls.getLog().error(traceback.format_exc())
            pass
        return result, calibration

    @QtCore.pyqtSlot()
    @Locked(_calibration_lock)
    def processModelUpdate(self, has_lock=False, block=False):
        """Processes updates from the model

        :type model: calibrator.models.calibration.PolynomialTableModel
        """
        self.getView().setSource(self.getCalibration().getSource())
        self.getView().setIdentifier(self.getCalibration().getId())
        self.getView().setActive(self.getCalibration().isActive())
        self.processModelDomainUpdate(has_lock=True, block=block)

    @QtCore.pyqtSlot()
    @Locked(_calibration_lock)
    def processModelDomainUpdate(self, has_lock=False, block=False):
        """Processes updates from the Model on the domain

        :type model: calibrator.models.calibration.PolynomialTableModel
        """
        poly = self.getView().getSelectedPolynomial()
        if poly is not None:
            self.getView().updateDomainSettings()
            self.getView().updateDomainLabel()

    @QtCore.pyqtSlot()
    @Locked(_calibration_lock)
    def processCoefficientsUpdate(self, has_lock=False, block=False):
        """Processes updates from the model (indirectly GUI)"""
        if self.hasCoefficientsModel() and self.hasPolynomialsModel():
            # get selected polynomial (i.e. the one we want to update)
            index = self.getView().getSelectedIndex()
            if index is not None:
                self.getPolynomialsModel().setCoefficients(
                    index=index, coefficients=self.getCoefficientsModel().getCoefficients()
                )
            # update formula label
            self.getView().updateFormulaLabel()

    @QtCore.pyqtSlot(object)
    @Locked(_calibration_lock)
    def processViewUpdate(self, object, has_lock=False, block=False):
        """Processes updates from the GUI

        :type view: calibrator.views.calibration.CalibrationView
        """
        poly = None
        self.getCalibration().setSource(self.getView().getSource())
        self.getCalibration().setId(self.getView().getIdentifier())
        self.getCalibration().setActive(self.getView().isActive())
        # update
        self.processViewDomainUpdate(has_lock=True, block=block)

    @Locked(_calibration_lock)
    def processViewDomainUpdate(self, has_lock=False, block=False):
        """Processes changes in the GUI on the domain

        :type view: calibrator.views.calibration.CalibrationView
        :type index: QtCore.QModelIndex
        """
        index = self.getView().getSelectedIndex()
        if index is not None:
            # get domain from GUI
            domain = self.getView().parseDomainSettings()
            # store new domain
            if domain is not None:
                self.getPolynomialsModel().setDomain(index=index, domain=domain)
            # update label
            self.getView().updateDomainLabel()

    @Locked(_calibration_lock)
    def processSelectionUpdate(self, has_lock=False, block=False):
        # get selected poly
        poly = self.getView().getSelectedPolynomial()
        if poly is not None:
            # update coefficient model
            self.getCoefficientsModel().setCoefficients(poly.getCoefficients())
            # update domain definition
            self.getView().updateSelection()

    # getters / setters

    def getView(self):
        """Returns the view of this controller

        :return:
        :rtype: calibrator.views.calibration.CalibrationView
        """
        return BaseController.getView(self)

    def connectView(self):
        """Connects the view to the controller"""
        self.getView().getUi().cbIncludesStart.stateChanged.connect(self.processViewDomainUpdate)
        self.getView().getUi().cbStartInfinite.stateChanged.connect(self.processViewDomainUpdate)
        self.getView().getUi().dsbStartValue.valueChanged.connect(self.processViewDomainUpdate)
        self.getView().getUi().cbIncludesEnd.stateChanged.connect(self.processViewDomainUpdate)
        self.getView().getUi().cbEndInfinite.stateChanged.connect(self.processViewDomainUpdate)
        self.getView().getUi().dsbEndValue.valueChanged.connect(self.processViewDomainUpdate)

    def getPolynomialsModel(self):
        """Returns the domain item model connected to this controller

        :return:
        :rtype: calibrator.models.calibration.PolynomialTableModel
        """
        return self._poly_model

    def hasPolynomialsModel(self):
        return self.getPolynomialsModel() is not None

    def connectPolynomialModel(self):
        """Connects the domains model"""
        if self.hasPolynomialsModel():
            self.getView().getUi().lvPolynomials.setModel(self.getPolynomialsModel())
            selectionModel = self.getView().getUi().lvPolynomials.selectionModel()
            selectionModel.selectionChanged.connect(self.processSelectionUpdate)
            # self.getView().getUi().lvPolynomials.clicked.connect(self.processSelectionUpdate)
            self.getPolynomialsModel().layoutChanged.connect(self.processModelUpdate)
            self.getPolynomialsModel().dataChanged.connect(self.processModelUpdate)
        return self

    def getCoefficientsModel(self):
        """Returns the domain item model connected to this controller

        :return:
        :rtype: calibrator.models.calibration.CoefficientTableModel
        """
        return self._coef_model

    def hasCoefficientsModel(self):
        return self.getCoefficientsModel() is not None

    def connectCoefficientsModel(self):
        if self.hasCoefficientsModel():
            self.getView().getUi().lvCoefficients.setModel(self.getCoefficientsModel())
            self.getCoefficientsModel().dataChanged.connect(self.processCoefficientsUpdate)
            self.getCoefficientsModel().layoutChanged.connect(self.processCoefficientsUpdate)

    def getCalibration(self):
        """Returns the calibration model

        :return:
        :rtype: None or uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        """
        return self._calibration

    def hasCalibration(self):
        return self.getCalibration() is not None

    def setCalibration(self, calibration):
        self._calibration = calibration
        self.updateCalibration()
        return self
