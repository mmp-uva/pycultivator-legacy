# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore
from base import BaseController, BaseWorker

from pycultivator_legacy.contrib.calibrator.views.learn import LearnView
from pycultivator_legacy.contrib.calibrator.controllers.serial import SerialWorker
from pycultivator_legacy.contrib.calibrator.models.serial import SerialConnectionModel

from datetime import datetime as dt, timedelta as td
import time

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class LearnController(BaseController):
    """

    """

    def __init__(self, model=None, worker=None, parent=None, app=None):
        """

        :param parent: The parent controller
        :type parent: calibrator.controllers.base.BaseController
        :param app: The application
        :type app: PyQt4.QtGui.QApplication.QApplication
        """
        BaseController.__init__(self, parent=parent, app=app)
        # parameters
        self._update_model = True
        self._update_gui = True
        # connection worker
        self._serial_worker = worker
        if self._serial_worker is None:
            self._serial_worker = SerialWorker(SerialConnectionModel(self))
        # learn model
        self._serial_model = model
        if self._serial_model is None:
            self._serial_model = self.getSerialWorker().getModel()
        # view
        self._view = LearnView(self, parent=self.getParent().getView())
        # trigger update of view
        self.processModelUpdate(self.getModel())
        # connect view and model
        self.connectModel(self.getModel())
        self.connectView(self.getView())

    @classmethod
    def start(cls, model=None, worker=None, parent=None, app=None, **kwargs):
        """ Run the dialog, return the model

        :type model: calibrator.models.serial.SerialConnectionModel
        :type worker: calibrator.controllers.base.BaseWorker
        :type parent: calibrator.controllers.base.BaseController
        :type app: PyQt4.QtGui.QApplication.QApplication
        :return: Returns the models object and whether the
        :rtype: tuple[bool, calibrator.models.serial.SerialConnectionModel]
        """
        result = False
        if model is None and worker is not None:
            model = worker.getModel()
        if model is not None:
            old_model = model.copy()
            c = cls(model=model, worker=worker, parent=parent, app=app)
            # execute dialog
            result = c.getView().exec_() == c.getView().Accepted
            # retrieve changes from GUI to model
            c.processGUIUpdate(c.getView())
            # if accepted we update the original model with the new settings
            if not result:
                # restore old settings
                old_model.copy(model)
        return result, model

    def connectModel(self, model):
        model.changed.connect(self.processModelUpdate)

    def connectView(self, view):
        view.getUi().pbTestSerial.clicked.connect(self.processTestConnection)
        view.getUi().pbTestMeasurement.clicked.connect(self.processTestMeasurement)
        view.getUi().buttonBox.clicked.connect(self.processButtonBox)

    # slots

    @QtCore.pyqtSlot(object)
    def processModelUpdate(self, model):
        """Processes updates from the model

        :type model: calibrator.models.serial.SerialConnectionModel
        """
        if self.acceptsUpdatesFromModel():
            # prevent updates from gui when we change it
            self.acceptUpdatesFromGUI(False)
            self.getView().setPort(model.getPort())
            self.getView().fakeConnection(model.fakesConnection())
            self.getView().setGasPause(model.getGasPause())
            self.getView().setSamples(model.getSamples())
            self.getView().setReadings(model.getReadings())
            self.getView().setAggregation(model.getAggregation())
            self.acceptUpdatesFromGUI(True)

    @QtCore.pyqtSlot(object)
    def processGUIUpdate(self, gui):
        """Processes updates from the GUI

        :type gui: calibrator.views.serial.SerialView
        """
        if self.acceptsUpdatesFromGUI():
            # prevent update from model when we change it
            self.acceptUpdatesFromModel(False)
            self.getModel().setPort(gui.getPort())
            self.getModel().fakeConnection(gui.fakesConnection())
            self.getModel().setGasPause(gui.getGasPause())
            self.getModel().setSamples(gui.getSamples())
            self.getModel().setReadings(gui.getReadings())
            self.getModel().setAggregation(gui.getAggregation())
            self.acceptUpdatesFromModel(True)

    @QtCore.pyqtSlot()
    def processSerialUpdate(self):
        """Processes updates on the serial settings"""
        if self.acceptsUpdatesFromGUI():
            self.getModel().setPort(self.getView().getPort())
            self.getModel().fakeConnection(self.getView().fakesConnection())

    @QtCore.pyqtSlot()
    def processMeasurementUpdate(self):
        """Processes updates on the measurement settings"""
        if self.acceptsUpdatesFromGUI():
            self.getModel().setGasPause(self.getView().getGasPause())
            self.getModel().setSamples(self.getView().getSamples())
            self.getModel().setReadings(self.getView().getReadings())
            self.getModel().setAggregation(self.getView().getAggregation())

    @QtCore.pyqtSlot(object)
    def processButtonBox(self):
        """"""
        pass

    @QtCore.pyqtSlot()
    def processTestConnection(self):
        result, job = False, None
        # old setting
        old_model = self.getModel().copy()
        was_open = self.getSerialWorker().isConnected()
        try:
            # load new settings
            self.processGUIUpdate(self.getView())
            # reset worker
            succeeded, job = self.getSerialWorker().do_reset()
            result = job.getResult()
            self.getView().setProgress(25)
            # reset we automatically reopen connection, so only open it if we were not connected yet
            if not was_open:
                succeeded, job = self.getSerialWorker().do_connect()
                result = job.getResult()
                self.getView().setProgress(50)
        except Exception as e:
            self.getLog().warning("Unable to test connection: %s" % e)
        finally:
            if not was_open:
                succeeded, job = self.getSerialWorker().do_disconnect()
                result = result and job.getResult()
        self.getView().setProgress(90)
        # restore model
        self.acceptUpdatesFromModel(False)
        old_model.copy(self.getModel())
        self.acceptUpdatesFromModel(True)
        # show
        self.getView().showMessage(
            "%s connected to %s" % ("Successfully" if result else "Failed to", self.getModel().getPort())
        )
        self.getView().setProgress(100)

    def processTestMeasurement(self):
        result, job, od = False, None, 0
        # store old settings
        old_model = self.getModel().copy()
        was_open = self.getSerialWorker().isConnected()
        try:
            # load new settings
            self.processGUIUpdate(self.getView())
            # reset worker
            self.getSerialWorker().do_reset()
            self.getView().setProgress(25)
            if not was_open:
                self.getSerialWorker().do_connect()
            if self.getSerialWorker().isConnected():
                result, job = self.getSerialWorker().do_measure(channels=[0], wavelength=720)
                if isinstance(job.getResult(), list) and len(job.getResult()) > 0:
                    od = job.getResult()[0].getOD()
            else:
                self.getLog().warning("Not connect to Multi-Cultivator")
        except Exception as e:
            self.getLog().warning("Unable to test measurement: %s" % e)
        finally:
            if not was_open:
                self.getSerialWorker().do_disconnect()
        # restore model
        self.acceptUpdatesFromModel(False)
        old_model.copy(self.getModel())
        self.acceptUpdatesFromModel(True)
        self.getView().showMessage(
            "%s measured the OD (OD in channel 0: %s)" % ("Successfully" if result else "Failed to", od)
        )
        self.getView().setProgress(100)

    # getters and setters

    def getView(self):
        """ Returns the view of this object

        :rtype: calibrator.views.serial.SerialView
        """
        return self._view

    def getModel(self):
        """ Returns the model of this object

        :rtype: calibrator.models.serial.SerialConnectionModel
        """
        return self._model

    def hasModel(self):
        return self.getModel() is not None

    def getSerialWorker(self):
        """Returns the worker of this object

        :rtype: calibrator.controllers.serial.SerialWorker
        """
        return self._serial_worker

    def acceptsUpdatesFromModel(self):
        return self._update_model is True

    def acceptUpdatesFromModel(self, state):
        self._update_model = state is True

    def acceptsUpdatesFromGUI(self):
        return self._update_gui is True

    def acceptUpdatesFromGUI(self, state):
        self._update_gui = state is True


class LearnWorker(BaseWorker):
    """A worker for learning tasks"""

    def __init__(self, serial_worker, model, parent=None, **kwargs):
        BaseWorker.__init__(self, parent=parent, **kwargs)
        self._serial_worker = serial_worker
        self._learn_model = model
        self._t_start = None

    # tasks

    def _learn(self, channels=None, wavelength=720):
        """Learns the measurement parameters for a given"""
        result = False
        if channels is None:
            channels = range(8)
        # make a snapshot that we can change
        model = self.getLearnModel().copy()
        if self.getSerialWorker().isConnected():
            self.clearProgression()
            # learn sample size

            measurements = []
            # total progression = 50, progression per cycle = 50 / len(samples)
            for sample in self.getLearnModel().getSampleSizes():
                measurements.extend(self._learn_cycle(samples=sample, channels=channels, wavelength=wavelength))
                self.increaseProgression(50 // self.getLearnModel().countSampleSizes())
            self.setProgression(50)
        # process

        # store to used model
        if result:
            with self.getLock():
                model.copy(self.getLearnModel())
            self.setProgression(100)
        return self.getLearnModel()

    def _learn_readings(self, pool_size=100, channels=None, wavelength=720):
        """Collects a pool_size amount of single readings per channel"""
        pass

    def _learn_cycle(self, samples=1, channels=None, wavelength=720):
        """Collects samples of given sample size"""
        results = []
        self.setStartTime()
        next_measurement_time = dt.now()
        # collect measurements while
        while self.getTime().total_seconds() < self.getLearnModel().getMaximumTime():
            if next_measurement_time > dt.now():
                time.sleep(dt.now() - next_measurement_time)
            # collect measurements
            results.extend(self._batch_measure(channels=channels, wavelength=wavelength))
            # set next measurement time
            next_measurement_time = dt.now() + td(seconds=self.getLearnModel().getInterval())
        return results

    def _batch_measure(self, samples=1, channels=None, wavelength=720):
        results = []
        for sample in range(samples):
            results.extend(self._measure(channels=channels, wavelength=wavelength))
        return results

    def _measure(self, channels=None, wavelength=720):
        return self.getSerialWorker()._measure(channels=channels, wavelength=wavelength)

    # getters and setters

    def getStartTime(self):
        with self.getLock():
            result = self._t_start
        return result

    def hasStartTime(self):
        return self.getStartTime() is not None

    def setStartTime(self, t=None):
        with self.getLock():
            if t is None:
                t = dt.now()
            self._t_start = t
        return self

    def getTime(self):
        """Returns how long has passed since start time"""
        result = td()
        if self.hasStartTime():
            result = dt.now() - self.getStartTime()
        return result

    def getSerialWorker(self):
        """Returns the serial worker

        :rtype: calibrator.controllers.serial.SerialWorker
        """
        with self.getLock():
            result = self._serial_worker
        return result

    def getLearnModel(self):
        """Returns the learn model

        :rtype: calibrator.controllers.learn.LearnModel
        """
        with self.getLock():
            result = self._learn_model
        return result
