# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore, QtWidgets
# load other controllers
from .base import BaseController
from .serial import SerialController, SerialWorker
from .calibration import CalibrationController
from .learn import LearnController
# load models
from pycultivator_legacy.contrib.calibrator.models.serial import SerialConnectionModel
from pycultivator_legacy.contrib.calibrator.models.measurement import ODMeasurementTableModel
from pycultivator_legacy.contrib.calibrator.models.calibration import CalibrationTableModel
# load view
from pycultivator_legacy.contrib.calibrator.views.main import MainView

from pycultivator_legacy.config import XMLConfig

from os import path
from lxml import etree

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MainController(BaseController):
    """ Controller for the main window

    """

    saved_changed = QtCore.pyqtSignal(object)
    config_changed = QtCore.pyqtSignal(object)

    def __init__(self, channels=None, wavelengths=None, parent=None, app=None, **kwargs):
        BaseController.__init__(self, parent=parent, app=app, **kwargs)
        # settings
        self._channels = channels
        if self._channels is None:
            self._channels = range(8)
        self._wavelengths = wavelengths
        if self._wavelengths is None:
            self._wavelengths = [680, 720]
        # configuration
        self._config = None
        self._saved_xml = None
        self._xml_path = None
        self._root_path = None
        # models
        self._serial_model = None
        self._mc_model = None
        self._has_mc_data = False
        self._ind_model = None
        self._has_ind_data = False
        self._pred_model = None
        self._has_pred_data = False
        # view
        self._view = None
        # connection worker
        self._serial_worker = None
        # reset everything
        self.resetSerialModel()
        self.resetSerialWorker()
        self.resetView()
        self.resetCultivatorModel()
        self.resetIndependentModel()
        self.resetPredictionModel()

    @classmethod
    def start(cls, controller=None, parent=None, app=None, execute=False, **kwargs):
        """Starts the controller by loading it"""
        result = False
        try:
            if controller is None:
                controller = cls(parent=parent, app=app, **kwargs)
            if controller.hasView():
                controller.getView().show()
                controller.getView().setFocus()
            if app is not None and execute:
                app.exec_()
            result = True
            controller.stop()
        except:
            raise
        return result

    def stop(self):
        """The stop method of this controller"""
        result = self._saved
        if self.isConnected():
            self.getLog().warning("Serial connection is open, closing it before exiting.")
            self.disconnectSerial()
        if not self.isSaved():
            # open dialog -> whether the user wants to save
            result = self.askToSave()
        if result:
            # remove running threads
            result = self.stopSerialWorker()
            result = BaseController.stop(self) and result
        return result

    # ----- properties

    @property
    def config(self):
        """ Return reference to Configuration Handler

        :rtype: pycultivator_legacy.config.xml.XMLConfig
        """
        return self._config

    def getConfig(self):
        return self.config

    def hasConfig(self):
        return self.config is not None

    def setConfig(self, config):
        self._config = config
        if self.hasConfig():
            self.reset_calibrations()
            self._saved_xml = self.getConfig().getXML()
        # notify view
        self.config_changed.emit(self)

    def clearTableModel(self, model=None):
        """Clears the table model

        :type model: calibrator.models.measurement.ODMeasurementTableModel
        """
        if model is not None:
            model.clear()

    def resetCultivatorModel(self):
        """Resets and connect the cultivator measurement model object"""
        self._mc_model = ODMeasurementTableModel(channels=self._channels, wavelengths=self._wavelengths, parent=self)
        self.getCultivatorModel().setEditable(False)
        self.connectCultivatorModel()

    def connectCultivatorModel(self):
        """Connects the MC model to the controller and view"""
        self.getView().getUi().tvMC.setModel(self.getCultivatorModel())
        self.getCultivatorModel().dataChanged.connect(self.updateCultivatorModel)
        self.getCultivatorModel().layoutChanged.connect(self.updateCultivatorModel)

    @QtCore.pyqtSlot()
    def updateCultivatorModel(self):
        """Process updates from the cultivator model"""
        self._has_mc_data = self.hasCultivatorModel() and self.getCultivatorModel().hasData()
        self.getView().updateView()
        if self.hasPredictionModel():
            self.getPredictionModel().layoutChanged.emit()

    def resetIndependentModel(self):
        """Resets and connects the independent measurement model object"""
        self._ind_model = ODMeasurementTableModel(channels=self._channels, wavelengths=self._wavelengths, parent=self)
        self.getIndependentModel().setEditable(True)
        self.connectIndependentModel()

    @QtCore.pyqtSlot()
    def updateIndependentModel(self):
        self._has_ind_data = self.hasIndependentModel() and self.getIndependentModel().hasData()
        self.getView().updateView()
        if self.hasPredictionModel():
            self.getPredictionModel().layoutChanged.emit()

    def connectIndependentModel(self):
        """Connects the independent model to the controller and view"""
        self.getView().getUi().tvInd.setModel(self.getIndependentModel())
        self.getIndependentModel().dataChanged.connect(self.updateIndependentModel)
        self.getIndependentModel().layoutChanged.connect(self.updateIndependentModel)

    def resetPredictionModel(self):
        """Resets and connects the calibration table model"""
        self._pred_model = CalibrationTableModel(channels=self._channels, wavelengths=self._wavelengths, parent=self)
        self.connectPredictionModel()

    @QtCore.pyqtSlot()
    def updatePredictionModel(self):
        self._has_pred_data = self.hasPredictionModel() and self.getPredictionModel().hasData()
        self.getView().updateView()

    def connectPredictionModel(self):
        """Connects the prediction model to the controller and view"""
        self.getView().getUi().tvPred.setModel(self.getPredictionModel())
        self.getPredictionModel().dataChanged.connect(self.updatePredictionModel)
        self.getPredictionModel().layoutChanged.connect(self.updatePredictionModel)

    def resetSerialModel(self):
        """Resets and connects the serial model object"""
        self._serial_model = SerialConnectionModel()
        if self.hasConfig():
            self.getSerialModel().load(self.getConfig())
        self.connectSerialModel()
        self.updateSerialModel(self.getSerialModel())

    def connectSerialModel(self):
        """Connects the controller to the model"""
        self.getSerialModel().changed.connect(self.updateSerialModel)

    def resetSerialWorker(self):
        """Resets and connects the serial worker"""
        if self.hasSerialWorker() and self.getSerialWorker().isRunning():
            self.stopSerialWorker()
        # create new worker
        self._serial_worker = SerialWorker(self.getSerialModel())
        self.connectSerialWorker()
        self.getSerialWorker().start()

    def connectSerialWorker(self):
        """Connects the worker to the controller"""
        self.getSerialWorker().connection_changed.connect(self.updateSerialConnection)

    def disconnectSerialWorker(self):
        self.getSerialWorker().connection_changed.disconnect(self.updateSerialConnection)

    def resetView(self):
        """Resets and connect a view to the controller"""
        # close and remove old view
        if self.hasView():
            self.getView().close()
            self._view = None
        # create new view
        self._view = MainView(self, parent=self.getParent())
        self.connectView()

    def connectView(self):
        """Connects the controller to the view"""
        self.config_changed.connect(self.updateConfig)
        self.getView().getUi().cbWavelength.currentIndexChanged.connect(self.updateWavelength)
        self.getView().getUi().pbMeasure.clicked.connect(self.measureSerial)
        self.getView().getUi().pbCalibrate.clicked.connect(lambda: self.calibrate_model())
        self.getView().getUi().pbAddRow.clicked.connect(lambda: self.getIndependentModel().addRow())
        self.getView().getUi().pbClearMC.clicked.connect(
            lambda: self.clearTableModel(self.getCultivatorModel())
        )
        self.getView().getUi().pbClearInd.clicked.connect(
            lambda: self.clearTableModel(self.getIndependentModel())
        )
        self.getView().getUi().pbPredReset.clicked.connect(
            lambda: self.reset_calibrations(self.getPredictionModel().getActiveWavelength())
        )

    @QtCore.pyqtSlot(object)
    def updateSerialModel(self, model):
        """Process update from the serial model"""
        self._saved = False
        # reset worker to reload model
        if self.hasSerialWorker():
            self.getSerialWorker().setModel(model)
            if self.getSerialWorker().isConnected():
                self.getSerialWorker().do_reset()

    @QtCore.pyqtSlot(object, bool)
    def updateSerialConnection(self, worker, state):
        """Update the connection status"""
        if self.hasView():
            self.getView().updateView()

    @QtCore.pyqtSlot(object)
    def updateConfig(self, config):
        """Process update from Config"""
        if self.hasView():
            self.getView().updateView()
        # reload and connect serial model
        if self.hasSerialModel():
            self.resetSerialModel()

    @QtCore.pyqtSlot()
    def updateWavelength(self):
        """Pass new wavelength to """
        w = self.getView().getActiveWaveLength()
        self.getIndependentModel().setActiveWavelength(w)
        self.getCultivatorModel().setActiveWavelength(w)
        self.getPredictionModel().setActiveWavelength(w)

    @QtCore.pyqtSlot()
    def editSerialConnection(self):
        SerialController.start(
            model=self.getSerialModel(), worker=self.getSerialWorker(), parent=self, app=self.getApplication()
        )

    @QtCore.pyqtSlot()
    def learnSettings(self):
        """Learns the correct settings for measuring the serial """
        LearnController.start(
            worker=self.getSerialWorker(), parent=self, app=self.getApplication()
        )

    def edit_calibration(self, index=None, calibration=None):
        if index is not None and calibration is None:
            calibration = self.getPredictionModel().getCalibration(index=index)
        if calibration is not None:
            # edit
            CalibrationController.start(
                calibration=calibration, parent=self, app=self.getApplication()
            )

    # SLOTS FOR SERIAL COMMUNICATION

    @QtCore.pyqtSlot()
    def connectSerial(self):
        result = False
        if self.hasSerialWorker():
            # create wait view
            job = self.getSerialWorker().createJob(
                fun=self.getSerialWorker()._connect
            )
            job.setProgressTitle("Connecting, please wait..")
            job.openProgressDialog(value_signal=self.getSerialWorker().progressed, parent=self.getView())
            # start job
            self.getSerialWorker().progressed.connect(job.getProgressDialog().setValue)
            self.getSerialWorker().addJob(job)
            while not job.isFinished():
                # wait
                self.getApplication().processEvents()
            # remove finished job from queue
            self.getSerialWorker().getFinishedJob(block=False)
            job.closeProgressDialog(value_signal=self.getSerialWorker().progressed)
            if not job.succeeded() or not job.getResult():
                self.getView().showAlert(
                    title="Connecting failed",
                    text="Was unable to connect to Multi-Cultivator.",
                    information=
                    "Tried to connect to port: %s.\n"
                    "Job was %s successful and returned %s"
                    "" % (
                        self.getSerialModel().getPort(),
                        "" if job.succeeded() else "NOT",
                        job.getResult()
                    ),
                    parent=self.getView()
                )
            # end if

    # end if

    @QtCore.pyqtSlot()
    def disconnectSerial(self):
        if self.hasSerialWorker():
            # create job view
            job = self.getSerialWorker().createJob(
                fun=self.getSerialWorker()._disconnect
            )
            # set wait view parameters
            job.setProgressTitle("Disconnecting, please wait..")
            job.openProgressDialog(parent=self.getView())
            # start job
            self.getSerialWorker().progressed.connect(job.getProgressDialog().setValue)
            self.getSerialWorker().addJob(job)
            while not job.isFinished():
                # wait
                self.getApplication().processEvents()
            # remove finished job from queue
            self.getSerialWorker().getFinishedJob(block=False)
            job.closeProgressDialog(value_signal=self.getSerialWorker().progressed)
            if not job.succeeded() or not job.getResult():
                self.getView().showAlert(
                    title="Disconnecting failed",
                    text="Was unable to disconnect from the MultiCultivator.",
                    parent=self.getView()
                )
        return

    @QtCore.pyqtSlot()
    def measureSerial(self):
        """Will measure the OD in all the channels"""
        if self.hasSerialWorker():
            # create wait view
            job = self.getSerialWorker().createJob(
                fun=self.getSerialWorker()._measure,
                channels=None, wavelength=self.getView().getActiveWaveLength()
            )
            job.setProgressTitle(
                "Measuring OD @ %s nm,\n please wait.." % self.getView().getActiveWaveLength())
            job.openProgressDialog(parent=self.getView())
            # start job
            self.getSerialWorker().progressed.connect(job.getProgressDialog().setValue)
            self.getSerialWorker().addJob(job)
            while not job.isFinished():
                self.getApplication().processEvents()
            # remove finished job from queue
            self.getSerialWorker().getFinishedJob(block=False)
            job.closeProgressDialog(value_signal=self.getSerialWorker().progressed)
            if job.succeeded():
                self.processMeasurements(job.getResult())
            else:
                self.getView().showAlert(
                    title="OD Measurement", text="Was unable to measure the OD", parent=self.getView()
                )
        return

    def processMeasurements(self, measurements=None):
        """

        :param job:
        :type job: calibrator.controllers.base.BaseJob
        :return:
        :rtype:
        """
        if measurements is None:
            measurements = []
        if len(measurements) > 0:
            # add row
            self.getCultivatorModel().addRow()
            row = self.getCultivatorModel().lastRow()
            for m in measurements:
                col = self.getCultivatorModel().getChannels().index(m.getChannel())
                self.getCultivatorModel().setMeasurement(row=row, column=col, measurement=m)

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    @QtCore.pyqtSlot(int, int)
    def calibrate_model(self, index=None, column=None):
        """Will adjust the intercept of the calibration models"""
        if column is not None:
            self.getPredictionModel().calibrateModel(column=column)
        elif index is not None:
            self.getPredictionModel().calibrateModel(index=index)
        else:
            # do for every channel
            for column in range(len(self.getChannels())):
                self.getPredictionModel().calibrateModel(index=index, column=column)

    def getCalibration(self, channel, wavelength):
        """Returns the OD calibration model used yb this channel at this wavelength

        :rtype: uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        """
        return self.getPredictionModel().getCalibration(row=0, column=channel, wavelength=wavelength)

    def getView(self):
        """Returns the view connected to this controller

        :return:
        :rtype: calibrator.views.main.MainView
        """
        return self._view

    def getSerialModel(self):
        """Returns the serial connection model

        :rtype: calibrator.models.serial.SerialConnectionModel
        """
        return self._serial_model

    def hasSerialModel(self):
        return self.getSerialModel() is not None

    def getSerialWorker(self):
        """Returns the serial connection worker

        :rtype: calibrator.controllers.serial.SerialWorker
        """
        return self._serial_worker

    def hasSerialWorker(self):
        return self.getSerialWorker() is not None

    def runningSerialWorker(self):
        result = False
        if self.hasSerialWorker():
            result = self.getSerialWorker().isRunning()
        return result

    def stopSerialWorker(self):
        result = True
        if self.hasSerialWorker() and self.runningSerialWorker():
            w = self.getSerialWorker()
            w.setExiting(True)
            result = w.wait(5000)
            if not result:
                self.getLog().warning("%s did not stop in time, terminate it" % w.objectName())
                w.terminate()
                result = w.wait()
        return result

    def getCultivatorModel(self):
        """Returns the cultivator measurement model

        :return:
        :rtype: calibrator.models.measurement.ODMeasurementTableModel
        """
        return self._mc_model

    def hasCultivatorModel(self):
        return self.getCultivatorModel() is not None

    def hasCultivatorData(self):
        return self._has_mc_data is True

    def getIndependentModel(self):
        """Returns the independent measurement model

        :return:
        :rtype: calibrator.models.measurement.ODMeasurementTableModel
        """
        return self._ind_model

    def hasIndependentModel(self):
        return self.getIndependentModel() is not None

    def hasIndependentData(self):
        return self._has_ind_data is True

    def getPredictionModel(self):
        """Returns the prediction measurement model

        :return:
        :rtype: pycultivator_legacy.contrib.calibrator.models.calibration.CalibrationTableModel
        """
        return self._pred_model

    def hasPredictionModel(self):
        return self.getPredictionModel() is not None

    def hasPredictionData(self):
        return self._has_pred_data is True

    def askToSave(self):
        result = False
        response = self.getView().askToSave(path.split(self.getXMLPath())[1])
        if response is not None:
            if response is True:
                self.saveXML()
            result = True
        return result

    def getXMLPath(self):
        return self._xml_path

    @QtCore.pyqtSlot(name='load_xml')
    def loadXML(self, xml_path=None):
        """Open an XML file from a location

        :return: Whether it loaded
        :rtype: bool
        """
        result = False
        if xml_path is None:
            xml_path, response = QtWidgets.QFileDialog.getOpenFileName(
                parent=self.getView(),
                caption="Select a config file to open",
                directory=self.getRootPath(),
                filter="XML Config file (*.xml)"
            )
            xml_path = str(xml_path)
        if xml_path is not None and xml_path != "":
            self.readXML(xml_path)
            result = True
        return result

    def readXML(self, xml_path):
        result = False
        if xml_path != self.getXMLPath() and xml_path is not None and path.isfile(xml_path):
            # check status current document
            if self.hasConfig() and not self.isSaved():
                self.askToSave()
            # if we have a file -> load the xml -> if successful set hasConfig is True
            self._xml_path = xml_path
            self.setConfig(XMLConfig.createFromFile(self.getXMLPath()))
            if not self.hasConfig():
                self.getLog().error("Unable to load Configuration file at %s" % self.getXMLPath())
                self.getView().showAlert(
                    "Load Configuration File",
                    "Failed to load configuration file at",
                    self.getXMLPath(),
                    self.getView()
                )
            else:
                result = True
                self.updateConfig(self.getConfig())
                self.getView().showStatusBarMessage("Loaded configuration at {}".format(self.getConfig().getPath()))
        return result

    @QtCore.pyqtSlot(name="save_xml")
    def saveXML(self):
        """Save the Configuration to XML

        :return:
        :rtype: bool
        """
        result = False
        # save the calibration models
        self.writeXML()
        xml_path = self.getConfig().getPath()
        if xml_path is not None and xml_path != "":
            result = self.getConfig().save(xml_path)
            if result:
                self.getView().showStatusBarMessage("Saved {}".format(self.getConfig().getPath()))
                self._saved_xml = self.getConfig().getXML()
                dialog = self.getView().createMessageBox(
                    title="Configuration File Saved",
                    text="Tell GIT about your change NOW.\nSee details for instructions",
                    information="git commit -am \'changed %s\'\ngit pull\ngit push!" % path.split(xml_path)[1]
                )
                dialog.setIcon(QtWidgets.QMessageBox.Information)
                dialog.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Default)
                dialog.exec_()
        return result

    @QtCore.pyqtSlot(name="save_xml_as")
    def saveXMLAs(self):
        xml_path = self.getConfig().getPath()
        save_path, result = QtWidgets.QFileDialog.getSaveFileName(
            parent=self.getView(),
            caption="Save Config File as...",
            directory=path.dirname(xml_path if xml_path is not None else __file__),
            filter="XML Config file (*.xml)"
        )
        self.getConfig().setPath(str(save_path))
        return self.saveXML()

    def writeXML(self):
        """Build the XML Document"""
        # first write serial settings
        self.getSerialModel().save(self.getConfig())
        for channel in self.getChannels():
            self.saveChannel(channel)

    def saveChannel(self, channel, root=None):
        """Save Channel information to the Configuration File

        :type channel: int
        :return:
        """
        # first locate the channel
        if root is None:
            root = self.config.getChannelsElement()
        if root is None:
            raise ValueError("Invalid root element")
        element = self.config.getChannelElement(channel=channel, root=root)
        if element is None:
            element = self.config.addElement(root, "channel")
        # add information
        element.set("id", str(channel))
        # store calibration models
        self.saveCalibrationModels(channel, element)

    def saveCalibrationModels(self, channel, root=None):
        """Save the calibrations in this channel"""
        if root is None:
            root = self.config.getChannelElement(channel)
        if root is None:
            raise ValueError("Invalid root element")
        element = self.config.getCalibrationsElement(root)
        if element is None:
            # add to document
            element = self.config.addElement(root, "calibrations")
        # save OD calibrations
        for wavelength in self.getWavelengths():
            calibration = self.getCalibration(channel, wavelength)
            if calibration is not None:
                self.saveCalibrationModel(calibration, wavelength, root=element)

    def saveCalibrationModel(self, calibration, wavelength, root=None):
        """Saves a calibration object to xml

        :type calibration: uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        :type wavelength: int
        :type root: lxml.etree._Element._Element or None
        """
        element = None
        if root is None:
            root = self.config.getCalibrationsElement()
        if root is None:
            raise ValueError("Invalid root element")
        elements = self.config.getCalibrationElements(variable="od", root=root)
        # filter
        wavelengths = [str(wavelength)]
        if wavelength == 720:
            wavelengths.append("self")
        elements = filter(lambda e: "id" not in e.keys() or e.get("id") in wavelengths, elements)
        elements = filter(lambda e: "source" not in e.keys() or e.get("source") in wavelengths, elements)
        if len(elements) > 0:
            element = elements[0]
        # if found, remove it so we can replace it
        if element is not None:
            root.remove(element)
        # create new element
        element = self.config.addElement(root, "calibration")
        # add information
        element.set("id", str(calibration.getId()))
        element.set("source", str(calibration.getSource()))
        element.set("variable", str(calibration.getVariable()))
        if calibration.isActive():
            element.set("active", "true")
        # add polynomials
        for polynomial in calibration.getPolynomials():
            self.savePolynomial(polynomial, element)

    def savePolynomial(self, polynomial, root):
        element = self.config.addElement(root, "polynomial")
        element.set("domain", polynomial.getDomain().__str__())
        for order in range(len(polynomial.getCoefficients())):
            value = polynomial.getCoefficient(order)
            self.saveCoefficient(order, value, element)

    def saveCoefficient(self, order, value, root):
        element = self.config.addElement(root, "coefficient")
        element.set("order", str(order))
        element.text = str(value)

    def isSaved(self):
        result = True
        if self.hasConfig():
            result = self.getConfig().getXML() == self._saved_xml
        return result

    def reset_calibrations(self, wavelengths=None):
        if self.hasPredictionModel() and self.hasConfig():
            if wavelengths is None:
                wavelengths = self.getPredictionModel().getWavelengths()
            if isinstance(wavelengths, (float, int)):
                wavelengths = [wavelengths]
            self.getPredictionModel().clear()
            for c in self.getChannels():
                # pre load all the calibrations
                for w in wavelengths:
                    self.reset_calibration(channel=c, wavelength=w)
                # end for
        # end if

    def reset_calibration(self, channel, wavelength=None):
        if self.hasPredictionModel() and self.hasConfig():
            if wavelength is None:
                wavelength = self.getPredictionModel().getActiveWavelength()
            calibration = self.getConfig().loadODCalibration(channel=channel, wavelength=wavelength)
            self.setCalibrationModel(channel, wavelength, calibration)

    def setCalibrationModel(self, channel, wavelength, calibration):
        result = False
        col = None
        if channel in self.getPredictionModel().getChannels():
            col = self.getPredictionModel().getChannels().index(channel)
        if int(wavelength) in self.getPredictionModel().getWavelengths():
            result = True
        if result and col is not None and calibration is not None:
            if self.getPredictionModel().countRows(wavelength=wavelength) == 0:
                self.getPredictionModel().addRow(wavelength=wavelength)
            self.getPredictionModel().setItem(row=0, column=col, wavelength=wavelength, value=calibration)
            result = True
        else:
            self.getLog().warning("Unable to set Calibration model for channel {}@{} nm".format(
                channel, wavelength
            ))
        return result

    @QtCore.pyqtSlot(name='import_calibration')
    def importCalibration(self):
        """Import Calibration Models from CSV"""
        result = False
        csv_path, response = QtWidgets.QFileDialog.getOpenFileName(
            parent=self.getView(),
            caption="Select CSV file to import",
            directory=self.getRootPath(),
            filter="CSV File (*.csv)"
        )
        csv_path = str(csv_path)
        if csv_path is not None and csv_path != "":
            try:
                result = self.readCalibrationCSV(csv_path)
            except IOError:
                self.getLog().warning("Unable to import calibrations", exc_info=1)
        return result

    def readCalibrationCSV(self, csv_path):
        result = False
        if not path.exists(csv_path):
            raise IOError("Invalid path: file does not exist")
        models = {}
        from uvaCultivator.uvaPolynomial import Polynomial
        with open(csv_path) as src:
            import csv
            reader = csv.DictReader(src)
            for row_idx, row in enumerate(reader):
                try:
                    channel, wavelength, model, domain, order, value = self.polynomialFromCSV(row)
                    key = (channel, wavelength)
                    #
                    polynomial = Polynomial(domain)
                    polynomial.resetCoefficients()
                    # make sure the polynomial is registered
                    if key not in models.keys():
                        models[key] = {model: polynomial}
                    if model not in models[key].keys():
                        models[key][model] = polynomial
                    # obtain model from dictionary
                    polynomial = models[key][model]
                    # add coefficient
                    if polynomial.hasCoefficient(order):
                        polynomial.setCoefficient(value, order)
                    else:
                        polynomial.addCoefficient(value, order)
                except ValueError:
                    self.getLog().warning("Unable to read row {} from file".format(row_idx), exc_info=1)
        # apply collected models
        from uvaCultivator.uvaPolynomialDependency import ODCalibrationDependency
        for key in models.keys():
            channel, wavelength = key
            calibration = ODCalibrationDependency(
                dependency_id=wavelength, source=wavelength, state=True, variable="od", target="self"
            )
            for model in models[key].keys():
                polynomial = models[key][model]
                calibration.addPolynomial(polynomial)
            self.setCalibrationModel(channel, wavelength, calibration)
        return result

    def polynomialFromCSV(self, line):
        """

        :type line: dict[str, object]
        """
        channel = self.parse_value(line.get("channel"), int)
        if channel is None:
            raise ValueError("No Channel Column")
        wavelength = self.parse_value(line.get("wavelength"), int)
        if wavelength is None:
            raise ValueError("No wavelength Column")
        # we are more tolerant with the model
        model = self.parse_value(line.get("model"), int, 0)
        start_value = self.parse_value(line.get("from"), float)
        end_value = self.parse_value(line.get("to"), float)
        degree = self.parse_value(line.get("degree"), int, 0)
        value = self.parse_value(line.get("value"), float)
        # make domain
        from uvaCultivator.uvaPolynomial import Domain
        domain = Domain(start_value, True, end_value, False)
        return channel, wavelength, model, domain, degree, value

    def parse_value(self, value, type_, default=None):
        result = default
        try:
            result = type_(value)
        except (ValueError, TypeError):
            pass
        return result

    @QtCore.pyqtSlot(name="export_calibration")
    def exportCalibration(self):
        result = False
        save_path, result = QtWidgets.QFileDialog.getSaveFileName(
            parent=self.getView(),
            caption="Export Calibration Models to CSV",
            directory=self.getRootPath(),
            filter="CSV File (*.csv)"
        )
        save_path = str(save_path)
        if save_path is not None and save_path != "":
            try:
                result = self.writeCalibrationCSV(save_path)
            except IOError:
                self.getLog().warning("Unable to export calibrations", exc_info=1)
        return result

    def writeCalibrationCSV(self, csv_path, delim=","):
        result = False
        if not path.exists(path.dirname(csv_path)):
            raise IOError("Invalid path: {} is not accessible".format(csv_path))
        with open(csv_path, "w") as dest:
            # write headers
            dest.write(delim.join(["channel", "wavelength", "model", "from", "to", "degree", "value"]) + "\n")
            for channel in self.getChannels():
                for wavelength in self.getWavelengths():
                    calibration = self.getCalibration(channel, wavelength)
                    if calibration is not None:
                        for idx, polynomial in enumerate(calibration.getPolynomials()):
                            for line in self.polynomialToCSV(polynomial):
                                # write line
                                values = [channel, wavelength, idx] + line
                                dest.write(delim.join([self.value_to_string(v) for v in values]) + "\n")
            result = True
        return result

    def polynomialToCSV(self, polynomial):
        """

        :type polynomial: uvaCultivator.uvaPolynomial.Polynomial
        :rtype: list[list[object]]
        """
        results = []
        base = [
            polynomial.getDomain().getStartValue(),
            polynomial.getDomain().getEndValue(),
        ]
        for order in range(len(polynomial.getCoefficients())):
            value = polynomial.getCoefficient(order)
            results.append(base + [order, value])
        return results

    def value_to_string(self, v):
        if v is None:
            result = "NA"
        elif isinstance(v, int):
            result = "{:d}".format(v)
        elif isinstance(v, float):
            result = "{:.4f}".format(v)
        else:
            result = str(v)
        return result

    def getRootPath(self):
        """Returns the root path or the directory path of the current file"""
        # use object default
        result = self._root_path
        # if unset, use directory name of this file
        if result is None:
            result = path.dirname(__file__)
        # if another file is open, use the directory name of that file
        if self.getXMLPath() is not None:
            result = path.dirname(self.getXMLPath())
        return result

    def setRootPath(self, p):
        if path.exists(p):
            if not path.isdir(p):
                # retry with dirname
                self.setRootPath(path.dirname(p))
            else:
                # exists and is a directory
                self._root_path = p
        return self

    def isConnected(self):
        result = False
        if self.hasSerialWorker():
            result = self.getSerialWorker().isConnected()
        return result

    def getChannels(self):
        return self._channels

    def getWavelengths(self):
        return self._wavelengths
