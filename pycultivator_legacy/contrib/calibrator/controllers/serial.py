# coding=utf-8

import time

from pycultivator_legacy.contrib.calibrator import QtCore, QtWidgets
from .base import BaseController, BaseWorker
from pycultivator_legacy.contrib.calibrator.models.serial import SerialConnectionModel
from pycultivator_legacy.contrib.calibrator.models.measurement import MeasurementModel
from pycultivator_legacy.contrib.calibrator.views.serial import SerialView

from uvaCultivator.uvaAggregation import ReadingAggregation

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SerialController(BaseController):
    """

    """

    def __init__(self, model=None, worker=None, parent=None, app=None):
        """

        :param parent: The parent controller
        :type parent: calibrator.controllers.base.BaseController
        :param app: The application
        :type app: PyQt4.QtGui.QApplication.QApplication
        """
        BaseController.__init__(self, parent=parent, app=app)
        # parameters
        self._update_model = True
        self._update_gui = True
        # model
        self._model = model
        if self._model is None:
            self._model = SerialConnectionModel()
        # connection worker
        self._serial_worker = worker
        if self._serial_worker is None:
            self._serial_worker = SerialWorker(self.getModel())
        # view
        self._view = SerialView(self, parent=self.getParent().getView())
        self.getView().getUi().cbAggregation.clear()
        for key in ReadingAggregation.getAggregationMapping().keys():
            self.getView().getUi().cbAggregation.addItem(key)
        idx = self.getView().getUi().cbAggregation.findText("mean")
        self.getView().getUi().cbAggregation.setCurrentIndex(idx)
        # trigger update of view
        self.processModelUpdate(self.getModel())
        # connect view and model
        self.connectModel(self.getModel())
        self.connectView(self.getView())

    @classmethod
    def start(cls, model=None, worker=None, parent=None, app=None, **kwargs):
        """ Run the dialog, return the model

        :type model: calibrator.models.serial.SerialConnectionModel
        :type worker: calibrator.controllers.base.BaseWorker
        :type parent: calibrator.controllers.base.BaseController
        :type app: PyQt4.QtGui.QApplication.QApplication
        :return: Returns the models object and whether the
        :rtype: tuple[bool, calibrator.models.serial.SerialConnectionModel]
        """
        result = False
        # store old settings
        old_model = model.copy()
        c = cls(model=model, worker=worker, parent=parent, app=app)
        # execute dialog
        result = c.getView().exec_() == c.getView().Accepted
        # retrieve changes from GUI to model
        c.processGUIUpdate(c.getView())
        # if accepted we update the original model with the new settings
        if not result:
            # restore old settings
            old_model.copy(model)
        else:
            # reset controller = apply setting
            c.getSerialWorker().do_reset()
        return result, model

    def connectModel(self, model):
        model.changed.connect(self.processModelUpdate)

    def connectView(self, view):
        view.getUi().pbTestSerial.clicked.connect(self.processTestConnection)
        view.getUi().pbTestMeasurement.clicked.connect(self.processTestMeasurement)
        view.getUi().buttonBox.clicked.connect(self.processButtonBox)

    # slots

    @QtCore.pyqtSlot(object)
    def processModelUpdate(self, model):
        """Processes updates from the model

        :type model: uvaTools.uvaGUI.calibrator.models.serial.SerialConnectionModel
        """
        if self.acceptsUpdatesFromModel():
            # prevent updates from gui when we change it
            self.acceptUpdatesFromGUI(False)
            self.getView().setPort(model.getPort())
            self.getView().useFake(model.usesFake())
            self.getView().setGasPause(model.getGasPause())
            self.getView().setSamples(model.getSamples())
            self.getView().setReadings(model.getReadings())
            self.getView().setAggregation(model.getAggregation())
            self.acceptUpdatesFromGUI(True)

    @QtCore.pyqtSlot(object)
    def processGUIUpdate(self, gui):
        """Processes updates from the GUI

        :type gui: uvaTools.uvaGUI.calibrator.views.serial.SerialView
        """
        if self.acceptsUpdatesFromGUI():
            # prevent update from model when we change it
            self.acceptUpdatesFromModel(False)
            self.getModel().setPort(gui.getPort())
            self.getModel().useFake(gui.usesFake())
            self.getModel().setGasPause(gui.getGasPause())
            self.getModel().setSamples(gui.getSamples())
            self.getModel().setReadings(gui.getReadings())
            self.getModel().setAggregation(gui.getAggregation())
            self.acceptUpdatesFromModel(True)

    @QtCore.pyqtSlot()
    def processSerialUpdate(self):
        """Processes updates on the serial settings"""
        if self.acceptsUpdatesFromGUI():
            self.getModel().setPort(self.getView().getPort())
            self.getModel().useFake(self.getView().usesFake())

    @QtCore.pyqtSlot()
    def processMeasurementUpdate(self):
        """Processes updates on the measurement settings"""
        if self.acceptsUpdatesFromGUI():
            self.getModel().setGasPause(self.getView().getGasPause())
            self.getModel().setSamples(self.getView().getSamples())
            self.getModel().setReadings(self.getView().getReadings())
            self.getModel().setAggregation(self.getView().getAggregation())

    @QtCore.pyqtSlot(object)
    @QtCore.pyqtSlot(QtWidgets.QAbstractButton)
    def processButtonBox(self):
        """"""
        pass

    @QtCore.pyqtSlot()
    def processTestConnection(self):
        result, job = False, None
        # old setting
        old_model = self.getModel().copy()
        was_open = self.getSerialWorker().isConnected()
        try:
            # load new settings
            self.processGUIUpdate(self.getView())
            # reset worker
            succeeded, job = self.getSerialWorker().do_reset()
            result = job.getResult()
            self.getView().setProgress(25)
            # reset we automatically reopen connection, so only open it if we were not connected yet
            if not was_open:
                succeeded, job = self.getSerialWorker().do_connect()
                result = job.getResult()
                self.getView().setProgress(50)
        except Exception as e:
            self.getLog().warning("Unable to test connection: %s" % e)
        finally:
            if not was_open:
                succeeded, job = self.getSerialWorker().do_disconnect()
                result = result and job.getResult()
        self.getView().setProgress(90)
        # restore model
        self.acceptUpdatesFromModel(False)
        old_model.copy(self.getModel())
        self.acceptUpdatesFromModel(True)
        # show
        self.getView().showMessage(
            "%s connected to %s" % ("Successfully" if result else "Failed to", self.getModel().getPort())
        )
        self.getView().setProgress(100)

    def processTestMeasurement(self):
        result, job, od = False, None, 0
        # store old settings
        old_model = self.getModel().copy()
        was_open = self.getSerialWorker().isConnected()
        try:
            # load new settings
            self.processGUIUpdate(self.getView())
            # reset worker
            self.getSerialWorker().do_reset()
            self.getView().setProgress(25)
            if not was_open:
                self.getSerialWorker().do_connect()
            if self.getSerialWorker().isConnected():
                result, job = self.getSerialWorker().do_measure(channels=[0], wavelength=720)
                if isinstance(job.getResult(), list) and len(job.getResult()) > 0:
                    od = job.getResult()[0].getOD()
            else:
                self.getLog().warning("Not connect to Multi-Cultivator")
        except Exception as e:
            self.getLog().warning("Unable to test measurement: %s" % e)
        finally:
            if not was_open:
                self.getSerialWorker().do_disconnect()
        # restore model
        self.acceptUpdatesFromModel(False)
        old_model.copy(self.getModel())
        self.acceptUpdatesFromModel(True)
        self.getView().showMessage(
            "%s measured the OD (OD in channel 0: %s)" % ("Successfully" if result else "Failed to", od)
        )
        self.getView().setProgress(100)

    # getters and setters

    def getView(self):
        """ Returns the view of this object

        :rtype: uvaTools.uvaGUI.calibrator.views.serial.SerialView
        """
        return self._view

    def getModel(self):
        """ Returns the model of this object

        :rtype: uvaTools.uvaGUI.calibrator.models.serial.SerialConnectionModel
        """
        return self._model

    def hasModel(self):
        return self.getModel() is not None

    def getSerialWorker(self):
        """Returns the worker of this object

        :rtype: uvaTools.uvaGUI.calibrator.controllers.serial.SerialWorker
        """
        return self._serial_worker

    def acceptsUpdatesFromModel(self):
        return self._update_model is True

    def acceptUpdatesFromModel(self, state):
        self._update_model = state is True

    def acceptsUpdatesFromGUI(self):
        return self._update_gui is True

    def acceptUpdatesFromGUI(self, state):
        self._update_gui = state is True


class SerialWorker(BaseWorker):
    """ A threaded serial connection worker"""

    # signals
    connection_changed = QtCore.pyqtSignal(object, bool)

    def __init__(self, model=None, parent=None, **kwargs):
        """Initialise the SerialWorker thread

        :type model: calibrator.models.serial.SerialConnectionModel
        :type parent: calibrator.controllers.base.BaseController
        """
        BaseWorker.__init__(self, parent=parent, **kwargs)
        self._model = model
        self._serial = None
        self._isConnected = False

    def getSerial(self):
        """Returns the serial controller object

        :return:
        :rtype: uvaSerial.psiSerialController.psiSerialController
        """
        with self.getLock():
            result = self._serial
        return result

    def setSerial(self, serial):
        with self.getLock():
            self._serial = serial
        return self

    def hasSerial(self):
        return self.getSerial() is not None

    def isConnected(self):
        with self.getLock():
            result = self._isConnected
        return result

    def setConnected(self, state):
        with self.getLock():
            self._isConnected = state is True
            self.connection_changed.emit(self, self.isConnected())
        return self

    def do_connect(self, callback=None):
        """Opens a serial connection to the Multi-Cultivator

        :param callback: Callback function (blocks when callback is None)
        :rtype: tuple[bool, calibrator.controllers.base.BaseJob]
        """
        return self.doJob(fun=self._connect, callback=callback)

    def _connect(self):
        if not self.hasModel():
            raise Exception("No Serial Model available")
        if not self.hasSerial():
            self.setSerial(self.getModel().getType()())
        success = self.getSerial().configure(port=self.getModel().getPort(), rate=self.getModel().getRate())
        self.setProgression(50)
        success = success and self.getSerial().connect()
        self.setConnected(success)
        self.setProgression(100)
        return self.isConnected()

    def do_disconnect(self, callback=None):
        """Disconnects the serial connection to the Multi-Cultivator

        :param callback: Callback function (blocks when callback is None)
        :rtype: tuple[bool, calibrator.controllers.base.BaseJob]
        """
        return self.doJob(fun=self._disconnect, callback=callback)

    def _disconnect(self):
        if self.hasSerial() and self.getSerial().disconnect():
            # stop connection
            self.setConnected(False)
            # destroy serial object
            self._serial = None
        return not self.isConnected()

    def do_reset(self, callback=None):
        """Resets the serial connection to the Multi-Cultivator

        :param callback: Callback function (blocks when callback is None)
        :rtype: tuple[bool, calibrator.controllers.base.BaseJob]
        """
        return self.doJob(fun=self._reset, callback=callback)

    def _reset(self):
        """Reset the connection (if connected)"""
        result = False
        if self.isConnected():
            result = self._disconnect()
            # delete old serial object
            self.setSerial(None)
            if not self.isConnected():
                result = self._connect()
        else:
            # delete old serial object
            self.setSerial(None)
            result = True
        return result

    def do_measure(self, channels=None, wavelength=720, callback=None):
        """Measures the optical density in the given channels at the given wavelength.

        :param callback: Callback function (blocks when callback is None)
        :rtype: tuple[bool, calibrator.controllers.base.BaseJob]
        """
        return self.doJob(fun=self._measure, callback=callback, channels=channels, wavelength=wavelength)

    def _measure(self, channels=None, wavelength=720):
        """Measures the OD in all the channels"""
        self.clearProgression()
        results = []
        if channels is None or not isinstance(channels, list):
            channels = range(8)
        gas_state = True
        old_states = [True for __ in range(8)]
        if not self.isConnected():
            raise Exception("Not connected to a Multi-Cultivator")
        try:
            # 0
            # pre conditions
            gas_state = self.getSerial().getPumpState() is True
            self.getLog().warning(
                "Gas state was: %s (%s)" % ("On" if gas_state else "Off", self.getSerial().getPumpState()))
            gas_state = True
            if gas_state:
                self.getSerial().setPumpState()
                self.increaseProgression(5)
                pause = self.getModel().getGasPause()
                while pause > 0:
                    self.increaseProgression(int(15 / self.getModel().getGasPause()))
                    time.sleep(1)
                    pause -= 1
            self.setProgression(25)
            # 25
            for channel in range(8):
                old_state = self.getSerial().getLightSettings(channel)
                if old_state is not None:
                    old_state = old_state[0] is True
                else:
                    self.getLog().warning("Unable to read light state of channel %s, assume it was on" % channel)
                    old_state = True
                self.getLog().warning("Old light state of channel %s: %s" % (channel, old_state))
                old_states[channel] = old_state
            self.getSerial().setAllLightStates()
            self.setProgression(50)
            # 50
            # measure the OD
            for channel in channels:
                results.append(self.measureChannel(channel, wavelength))
                self.increaseProgression(2)
            # 75
            self.setProgression(75)
        except Exception as e:
            self.getLog().warning("Unable to measure OD:\n%s" % e)
        finally:
            # reset gas if necessary
            if True or gas_state:
                self.getSerial().setPumpState(gas_state)
            self.setProgression(85)
            # 85
            # reset light
            for channel in range(8):
                self.getLog().warning("Restore light to: %s" % "On" if old_states[channel] else "Off")
                self.getSerial().setLightState(channel, old_states[channel])
                self.increaseProgression(1)
        self.setProgression(100)
        # finished
        return results

    def measureChannel(self, channel=0, wavelength=720):
        result = None
        reading = self.readChannel(channel=channel, wavelength=wavelength)
        if None not in reading:
            result = MeasurementModel(
                channel=channel, wavelength=wavelength, flash=reading[0], background=reading[1], od=reading[2]
            )
        return result

    def readChannel(self, channel=0, wavelength=720):
        """Obtains (aggregated) OD Readings from one channel"""
        readings = []
        # collect
        for idx in range(self.getModel().getSamples()):
            readings.append(self.readOD(channel, wavelength))
        # aggregate
        method = self.getModel().getAggregation()
        if not ReadingAggregation.hasAggregationMethod(method):
            raise Exception("Unknown aggregation method")
        result = ReadingAggregation.aggregate(method, readings)
        if result is None:
            result = (None, None, None)
        return result

    def readOD(self, channel=0, wavelength=720):
        """Obtains an OD readings"""
        if not self.isConnected():
            raise Exception("Not connected to a Multi-Cultivator")
        od_led = 1 if wavelength == 720 else 0
        # returns (flash, bg, od)
        return self.getSerial().getODReading(channel, od_led, self.getModel().getReadings())
