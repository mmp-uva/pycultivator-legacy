# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore
from uvaCultivator import uvaLog

import threading

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Locked(object):
    """Decorator. Requires the function to have an open lock or receive a lock"""

    _default_lock = threading.Semaphore()

    def __init__(self, lock=None):
        self._lock = self._default_lock
        if lock is not None:
            self._lock = lock

    def __call__(self, f):
        def wrapped_f(*args, **kwargs):
            result = False
            # kwargs should contain the has_lock argument
            has_lock = kwargs.get("has_lock", False)
            block = kwargs.get("block", False)
            # acquire lock or accept if has_lock is True
            if self.acquireLock(has_lock=has_lock, block=block):
                # run function
                result = f(*args, **kwargs)
                # release lock
                self.releaseLock(received_lock=has_lock)
            return result

        return wrapped_f

    def getLock(self):
        """Returns the lock

        :return:
        :rtype: threading._Lock
        """
        return self._lock

    def acquireLock(self, has_lock=False, block=False):
        result = has_lock
        if not has_lock:
            result = self.getLock().acquire(blocking=1 if block else 0)
        return result

    def releaseLock(self, received_lock=False):
        result = received_lock
        if not received_lock:
            self.getLock().release()
            result = True
        return result


class BaseModel(QtCore.QObject):
    """Base model"""

    # Model Changed Signal with self reference
    changed = QtCore.pyqtSignal(object)

    _log = None

    def __init__(self, parent=None):
        QtCore.QObject.__init__(self, parent)
        self._lock = threading.Lock()

    def load(self, **kwargs):
        """Load data from a source into this model"""
        raise NotImplementedError

    def save(self, **kwargs):
        """Save data from to model to a target"""
        raise NotImplementedError

    @classmethod
    def getLog(cls):
        if cls._log is None:
            cls._log = uvaLog.getLogger(cls.__name__)
        return cls._log

    def getLock(self):
        """Returns the Thread lock for this object

        :rtype: threading._RLock
        """
        return self._lock

    def copy(self, to=None):
        if to is None:
            to = self.__class__()
        return to


class BaseTableModel(QtCore.QAbstractTableModel):
    """Basic Table Model with a one dimensional data structure"""

    _log = None

    def __init__(self, data=None, parent=None):
        """Initialise the Abstract Base Table Model

        :type parent: PyQt4.QtGui.QWidget
        """
        QtCore.QAbstractItemModel.__init__(self, parent)
        self._parent = parent
        self._editable = False
        self._data = data
        if self._data is None:
            self._data = []

    # Slots

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def flags(self, index):
        """Returns the flags for a given position in the model"""
        # default
        flag = super(BaseTableModel, self).flags(index)
        if self.isTableIndex(index=index) and self.isEditable():
            flag = flag | QtCore.Qt.ItemIsEditable
        return flag

    def firstRow(self):
        """Returns the number of the first row"""
        return 0

    def countRows(self):
        """Returns the number of rows in the data"""
        return len(self._data)

    def rowCount(self, QModelIndex_parent=None, *args, **kwargs):
        """Returns the number of rows shown in the table (not necessarily number of rows in data)"""
        return self.countRows()

    def firstColumn(self):
        """Returns the number of the first column"""
        return 0

    def countColumns(self):
        """Returns the number of columns in the data"""
        return 1

    def columnCount(self, QModelIndex_parent=None, *args, **kwargs):
        """Returns the number of columns shown in the table (not necessarily number of columns in data)"""
        return self.countColumns()

    @QtCore.pyqtSlot(QtCore.QModelIndex, int)
    def data(self, index, role=None):
        """Returns the value of the item in the table

        :param index:
        :type index: QtCore.QModelIndex
        :param role:
        :type role: QtCore.QString
        :return:
        :rtype: bool
        """
        result = QtCore.QVariant()
        if self.isTableIndex(index=index):
            result = self.tableData(index=index, role=role)
        return result

    def tableData(self, index, role=None, default=QtCore.QVariant()):
        """Returns the item at a given position in the model (depending on role), passes default to getValue"""
        result = QtCore.QVariant()
        if self.isTableIndex(index=index):
            if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
                result = self.getValue(index=index, default=default)
        return result

    def getValue(self, index=None, row=None, column=None, default=None):
        return self.getItem(index=index, row=row, column=column, default=default)

    def getItem(self, index=None, row=None, column=None, default=None):
        """Returns the item at the given position"""
        raise NotImplementedError

    def headerData(self, section, orientation, role=None):
        """Process the header data"""
        result = QtCore.QVariant()
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                pass
            if orientation == QtCore.Qt.Vertical:
                pass
        return result

    @QtCore.pyqtSlot(QtCore.QModelIndex, QtCore.QVariant, int)
    def setData(self, index, value, role=None):
        """Sets an item at the """
        result = QtCore.QVariant()
        if self.isTableIndex(index=index) and self.isEditable():
            result = self.setItem(index=index, item=value)
        return result

    def setItem(self, index=None, row=None, column=None, item=None):
        raise NotImplementedError

    def clear(self):
        """Clears the data in this model"""
        self._data = []
        self.layoutChanged.emit()

    # getters and setters

    @classmethod
    def getLog(cls):
        if cls._log is None:
            cls._log = uvaLog.getLogger(cls.__name__)
        return cls._log

    def getParent(self):
        return self._parent

    def hasParent(self):
        return self.getParent() is not None

    def getData(self):
        """ Returns the data of this model

        :return:
        :rtype: list
        """
        return self._data

    def _setData(self, data):
        """Sets the data of the model directly, does not emit a signal"""
        self._data = data

    def getHeaders(self):
        return range(self.countColumns())

    def isTableIndex(self, index=None, row=None, column=None):
        """Returns whether this index is valid index to get data from this Model"""
        if column is None and self.countColumns() == 1:
            column = self.firstColumn()
        if index is None and None not in (row, column):
            index = self.createIndex(row, column)
        return index is not None and index.isValid()

    def isEditable(self):
        """Returns whether the data in this model can be edited"""
        return self._editable is True

    def setEditable(self, state):
        self._editable = state is True
        return self
