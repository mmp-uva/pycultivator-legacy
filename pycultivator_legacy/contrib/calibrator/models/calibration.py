# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore, QtGui

from .base import *
from .measurement import MeasurementTableModel

from uvaCultivator.uvaPolynomial import Polynomial, Domain

import numpy as np

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PolynomialTableModel(BaseTableModel):
    """An Polynomial Item Model"""

    def __init__(self, polynomials=None, parent=None):
        """Initialise the Polynomial Item Model

        :type polynomials: list[uvaCultivator.uvaPolynomialDependency.Polynomial]
        :type parent: PyQt4.QtGui.QWidget
        """
        BaseTableModel.__init__(self, data=polynomials, parent=parent)

    # Slots

    def getValue(self, index=None, row=None, column=None, default=None):
        result = default
        # retrieve domain
        domain = self.getDomain(index=index, row=row, column=column, default=None)
        # process result
        if domain is not None:
            result = domain.__str__()
        return result

    def getDomain(self, index=None, row=None, column=None, default=None):
        """Returns the domain of a polynomial (if present)

        :rtype: uvaCultivator.uvaPolynomial.Domain
        """
        result = None
        poly = self.getPolynomial(index=index, row=row, column=column, default=default)
        if poly is not None:
            result = poly.getDomain()
        return result

    def getPolynomial(self, index=None, row=None, column=None, default=None):
        """Returns the polynomial at the given position

        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        return self.getItem(index=index, row=row, column=column, default=default)

    def getItem(self, index=None, row=None, column=None, default=None):
        """Returns the item at the given position

        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        result = default
        if column is None:
            column = 0
        if index is None and row is not None:
            index = self.createIndex(row, column)
        if self.isTableIndex(index=index):
            result = self.getData()[index.row()]
        return result

    def headerData(self, section, orientation, role=None):
        """Process the header data"""
        result = QtCore.QVariant()
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                pass
            if orientation == QtCore.Qt.Vertical:
                pass
        return result

    def setItem(self, index=None, row=None, column=None, item=None):
        result = False
        if column is None:
            column = self.firstColumn()
        if index is None and row is not None:
            index = self.createIndex(row, column)
        if item is None:
            item = Polynomial()
        if self.isTableIndex(index=index) and isinstance(item, Polynomial):
            self.getPolynomials()[index.row()] = item
            self.dataChanged.emit(index, index)
            result = True
        return result

    def setPolynomial(self, index=None, row=None, polynomial=None):
        return self.setItem(index=index, row=row, item=polynomial)

    def setDomain(self, index=None, row=None, column=None, domain=None):
        result = False
        if column is None:
            column = self.firstColumn()
        if index is None and row is not None:
            index = self.createIndex(row, column)
        if domain is None:
            domain = "(,)"  # will be used in next if
        if isinstance(domain, str):
            domain = Domain.createFromString(domain)
        if self.isTableIndex(index=index) and isinstance(domain, Domain):
            self.getItem(index=index).setDomain(domain=domain)
            self.dataChanged.emit(index, index)
            result = True
        return result

    def setCoefficients(self, index=None, row=None, column=None, coefficients=None):
        """Sets all coefficients of the polynomial"""
        result = False
        if column is None:
            column = self.firstColumn()
        if index is None and row is not None:
            index = self.createIndex(row, column)
        if coefficients is None:
            coefficients = []
        if self.isTableIndex(index=index) and isinstance(coefficients, list):
            self.getItem(index=index).setCoefficients(coefficients)
            self.dataChanged.emit(index, index)
            result = True
        return result

    def setCoefficient(self, index=None, row=None, coefficient=None, order=None):
        """Sets one coefficient in the polynomial"""
        result = False
        poly = None
        if index is None and row is not None:
            index = self.createIndex(row, 0)
        if self.isTableIndex(index=index):
            poly = self.getItem(index=index)
        if poly is not None and poly.hasCoefficient(order):
            poly.setCoefficient(coefficient=coefficient, order=order)
            self.dataChanged.emit(index, index)
            result = True
        return result

    # getters and setters

    def getPolynomials(self):
        """

        :return:
        :rtype: list[uvaCultivator.uvaPolynomialDependency.Polynomial]
        """
        return self.getData()

    def getPolynomialByIndex(self, index, default=None):
        result = default
        if self.hasPolynomialIndex(index=index):
            result = self.getPolynomials()[index]
        return result

    def getPolynomialByValue(self, value, default=None):
        result = default
        for poly in self.getPolynomials():
            if poly.contains(value):
                result = poly
                break
        return result

    def countPolynomials(self):
        return len(self.getPolynomials())

    def hasPolynomialIndex(self, index):
        return -self.countPolynomials() <= index < self.countPolynomials()

    def clearPolynomials(self):
        return self.clear()


class CoefficientTableModel(BaseTableModel):
    """Item model storing the coefficients of a polynomial"""

    def __init__(self, coefficients=None, parent=None):
        """Initialise the Polynomial Item Model

        :type coefficients: list[float]
        :type parent: PyQt4.QtGui.QWidget
        """
        BaseTableModel.__init__(self, data=coefficients, parent=parent)

    # Slots

    def headerData(self, section, orientation, role=None):
        """Process the header data"""
        result = QtCore.QVariant()
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            # result = self._channels[section]
            pass
        if orientation == QtCore.Qt.Vertical and role == QtCore.Qt.DisplayRole:
            order = "%s%s%s" % ("" if section < 1 else "x", "^" if section > 1 else "", section if section > 1 else "")
            result = order
        return result

    def getCoefficient(self, index=None, row=None, column=None, default=None):
        """Returns the coefficient at the given index"""
        return self.getItem(index=index, row=row, column=column, default=default)

    def getItem(self, index=None, row=None, column=None, default=None):
        """Returns the item at the given index"""
        result = default
        if column is None:
            column = self.firstColumn()
        if index is None and None not in (row, column):
            index = self.createIndex(row, column)
        if self.isTableIndex(index=index):
            result = self.getCoefficients()[index.row()]
        return result

    def setItem(self, index=None, row=None, column=None, item=None):
        return self.setCoefficient(index=index, row=row, column=column, coefficient=item)

    def setCoefficient(self, index=None, row=None, column=None, coefficient=None, order=None):
        """Sets one coefficient in the polynomial"""
        result = False
        if column is None:
            column = self.firstColumn()
        if index is None and order is not None:
            index = self.createIndex(order, column)
        if index is None and row is not None:
            index = self.createIndex(row, column)
        if self.isTableIndex(index=index):
            ok = True
            if isinstance(coefficient, QtCore.QVariant):
                coefficient, ok = coefficient.value()
            if ok:
                self.getCoefficients()[index.row()] = coefficient
                self.dataChanged.emit(index, index)
                result = True
        return result

    # getters and setters

    def isFooterIndex(self, index):
        """Returns whether this index is valid index to get data from this Model"""
        return False

    def getCoefficients(self):
        """Returns the coefficients stored in this model

        :return:
        :rtype: list[float]
        """
        return self.getData()

    def setCoefficients(self, coefficients):
        self._setData(coefficients)
        self.layoutChanged.emit()

    def getCoefficientByOrder(self, order, default=None):
        result = default
        if self.hasCoefficient(order=order):
            result = self.getCoefficients()[order]
        return result

    def countCoefficients(self):
        return len(self.getCoefficients())

    def hasCoefficient(self, order):
        return -self.countCoefficients() <= order < self.countCoefficients()

    def clearCoefficients(self):
        self._coefficients = []

    def isEditable(self):
        return self._editable is True


class CalibrationTableModel(MeasurementTableModel):
    """A table model where measurements are represented per channel"""

    def __init__(self, channels=None, wavelengths=None, parent=None):
        """Initialise the object

        :param channels: Number of channels to be stored
        :type channels: None or list[int]
        :param wavelengths: Number of wavelengths to store
        :type wavelengths: None or list[int]
        """
        MeasurementTableModel.__init__(self, channels=channels, wavelengths=wavelengths, parent=parent)
        self._editable = False
        self._show_mean = False
        self._show_std = False
        self._max_rows = 1

    # slots

    def tableData(self, index, role=None, default="NA"):
        """Returns the value in the table (passes `default` (NA by default) to getValue)"""
        result = QtCore.QVariant()
        if self.isTableIndex(index=index):
            if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
                result = self.getValue(index=index, default=default)
            if role == QtCore.Qt.BackgroundRole:
                result = QtGui.QColor("red")
                if self.predictsCorrect(index=index):
                    result = QtGui.QColor("green")
                result.setAlpha(60)
        return result

    def predictsCorrect(self, index=None, row=None, column=None, wavelength=None, default=False):
        result = default
        try:
            prediction = self.getValue(index=index, row=row, column=column, wavelength=wavelength, default=None)
            real_od = self.getIndependentMean(index=index, row=row, column=column, wavelength=wavelength)
            if None not in (prediction, real_od):
                precision = self.getValuePrecision()
                result = np.round(prediction, decimals=precision) == np.round(real_od, decimals=precision)
        except Exception as e:
            self.getLog().warning("Unable to predict OD: %s" % e)
        return result

    def getValue(self, index=None, row=None, column=None, wavelength=None, default="NA"):
        result = default
        try:
            calibration = self.getCalibration(index=index, row=row, column=column, wavelength=wavelength, default=None)
            mean_od = self.getCultivatorMean(index=index, row=row, column=column, wavelength=wavelength)
            if None not in (calibration, mean_od):
                pred_od = calibration.calculate(mean_od)
                if pred_od is not None:
                    result = pred_od
        except Exception as e:
            self.getLog().warning("Unable to predict OD: %s" % e)
        return result

    def getCalibration(self, index=None, row=None, column=None, wavelength=None, default=None):
        """ Returns the calibration at the given position in the model

        :param index: Model Index of the requested calibration
        :type index: QtCore.QModelIndex
        :param row: Row index from which to retrieve the calibration
        :type row: int
        :param column: Column index from which to retrieve the calibration
        :type column: int
        :param default: Default value to return when position cannot be found
        :type default: None or object
        :return: The requested calibration or None
        :rtype: uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency or None
        """
        return self.getItem(index=index, row=row, column=column, wavelength=wavelength, default=default)

    def calibrateModel(self, index=None, column=None, wavelength=None):
        """Will adjust the intercept of all model at the given index and wavelength"""
        if column is None and index is not None:
            column = index.column()
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        # check if we have a column and this column is valid
        is_ready = column is not None and self.isTableIndex(row=self.firstRow(), column=column)
        # check if we can get to the other models
        is_ready = is_ready and self.getParent().hasCultivatorModel() and self.hasIndependentModel()
        # check if there are measurements in the cultivator model
        is_ready = is_ready and self.getCultivatorModel().hasData(column=column, wavelength=wavelength)
        # check if there are measurements in the independent model
        is_ready = is_ready and self.getIndependentModel().hasData(column=column, wavelength=wavelength)
        # if all conditions are met proceed
        if is_ready:
            correction = None
            raw_od = self.getCultivatorMean(column=column, wavelength=wavelength)
            real_od = self.getIndependentMean(column=column, wavelength=wavelength)
            # determine which model to use
            valid_poly = self.getCalibration(
                row=self.firstRow(), column=column, wavelength=wavelength
            ).getPolynomialByDomain(raw_od)
            # if polynomial exists
            if valid_poly is not None:
                # make prediction
                pred_od = valid_poly.predict(raw_od)
                if pred_od is not None:
                    try:
                        # calculate required intercept correction
                        correction = float(real_od - pred_od)
                    except (ValueError, TypeError):
                        pass
            # apply correction if there is a correction
            if isinstance(correction, float):
                # apply correction
                self.getCalibration(
                    row=self.firstRow(), column=column, wavelength=wavelength
                ).adjustIntercepts(correction=correction)
                # notify view if view is showing the data (i.e. wavelength is active)
                if wavelength == self.getActiveWavelength():
                    self.dataChanged.emit(
                        self.createIndex(self.firstRow(), column), self.createIndex(self.firstRow(), column)
                    )

    def addRow(self, wavelength=None):
        """Adds a new row with empty calibrations"""
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        if self.countRows(wavelength=wavelength) < self.getMaxRowCount():
            # add an empty row
            self.getData()[wavelength].append({c: None for c in self.getChannels()})
            if wavelength == self.getActiveWavelength():
                self.layoutChanged.emit()

    def setEditable(self, state):
        raise NotImplementedError("This table model does not support editing, sorry")

    def getCultivatorModel(self):
        result = None
        if self.hasCultivatorModel():
            result = self.getParent().getCultivatorModel()
        return result

    def hasCultivatorModel(self):
        return self.hasParent() and self.getParent().hasCultivatorModel()

    def getCultivatorMean(self, index=None, row=None, column=None, wavelength=None):
        result = None
        if column is None and index is not None:
            column = index.column()
        if column is not None and column < self.countChannels(wavelength=wavelength):
            if self.hasCultivatorModel():
                result = self.getCultivatorModel().columnMean(column=column, wavelength=wavelength)
        return result

    def getIndependentModel(self):
        result = None
        if self.hasIndependentModel():
            result = self.getParent().getIndependentModel()
        return result

    def hasIndependentModel(self):
        return self.hasParent() and self.getParent().hasIndependentModel()

    def getIndependentMean(self, index=None, row=None, column=None, wavelength=None):
        result = None
        if column is None and index is not None:
            column = index.column()
        if column is not None and column < self.countChannels(wavelength=wavelength):
            if self.hasIndependentModel():
                result = self.getIndependentModel().columnMean(column=column, wavelength=wavelength)
        return result

    def getParent(self):
        """Returns the controller of this model

        :return:
        :rtype: calibrator.controllers.main.MainController
        """
        return MeasurementTableModel.getParent(self)

    def getMaxRowCount(self):
        return self._max_rows
