# coding=utf-8

from .base import BaseModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class LearnModel(BaseModel):
    """Stores learning parameters"""

    VARIABLE_SD = "SD"
    VARIABLES = [VARIABLE_SD]
    THRESHOLD_BELOW = -1
    THRESHOLD_ABOVE = 1
    THRESHOLDS = [THRESHOLD_BELOW]

    def __init__(self, parent=None):
        BaseModel.__init__(self, parent=parent)
        self._optimize = self.VARIABLE_SD
        self._threshold = self.THRESHOLD_BELOW
        self._threshold_value = 0
        self._sample_sizes = []
        self._wait_time = 0
        self._learn_time = 60
        self._interval = 1

    def getOptimizeVariable(self):
        with self.getLock():
            result = self._optimize
        return result

    def setOptimizeVariable(self, variable):
        if variable not in self.VARIABLES:
            raise ValueError("Unknown optimization variable - %s." % variable)
        with self.getLock():
            self._optimize = variable
        self.changed.emit(self)
        return self

    def getThreshold(self):
        with self.getLock():
            result = self._threshold
        return result

    def setThreshold(self, threshold):
        if threshold not in self.THRESHOLDS:
            raise ValueError("Unknown threshold - %s." % threshold)
        with self.getLock():
            self._threshold = threshold
        self.changed.emit(self)
        return self

    def getThresholdValue(self):
        with self.getLock():
            result = self._threshold_value
        return result

    def setThresholdValue(self, value):
        with self.getLock():
            self._threshold_value = value
        self.changed.emit(self)
        return self

    def getSampleSizes(self):
        with self.getLock():
            result = self._sample_sizes
        return result

    def setSampleSizes(self, samples):
        if isinstance(samples, list):
            with self.getLock():
                self._sample_sizes = samples
        self.changed.emit()
        return self

    def countSampleSizes(self):
        return len(self.getSampleSizes())

    def hasSampleSizes(self):
        return self.countSampleSizes() > 0

    def hasSampleSize(self, size):
        return size in self.getSampleSizes()

    def clearSampleSizes(self):
        return self.setSampleSizes([])

    def addSampleSize(self, size):
        if not self.hasSampleSize(size):
            with self.getLock():
                self._sample_sizes.append(size)
            self.changed.emit(self)
        return self

    def getWaitTime(self):
        """Get the time spent on waiting before learning in seconds"""
        with self.getLock():
            result = self._wait_time
        return result

    def setWaitTime(self, t):
        """Set the time spent on waiting before starting learning in seconds"""
        with self.getLock():
            self._wait_time = t
        self.changed.emit(self)
        return self

    def getLearnTime(self):
        """Get the maximum time spent on learning in seconds"""
        with self.getLock():
            result = self._learn_time
        return result

    def setLearnTime(self, t):
        """Set the maximum time spent on learning in seconds"""
        with self.getLock():
            self._learn_time = t
        self.changed.emit(self)
        return self

    def getInterval(self):
        with self.getLock():
            result = self._interval
        return result

    def setInterval(self, interval):
        with self.getLock():
            self._interval = interval
        self.changed.emit(self)
        return self

    def copy(self, to=None):
        """Copies a learn model"""
        to = BaseModel.copy(self, to=to)
        """:type: calibrator.models.learn.LearnModel"""
        to.setOptimizeVariable(self.getOptimizeVariable())
        to.setThreshold(self.getThreshold())
        to.setSampleSizes(self.getSampleSizes())
        to.setWaitTime(self.getWaitTime())
        to.setLearnTime(self.getLearnTime())
        to.setInterval(self.getInterval())
        return to
