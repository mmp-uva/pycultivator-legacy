# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore
from .base import BaseModel, BaseTableModel

import numpy as np

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MeasurementModel(BaseModel):
    """Stores the connection parameters"""

    model_changed = QtCore.pyqtSignal(object)

    def __init__(self, channel, wavelength, flash, background, od, calibration=None, parent=None):
        BaseModel.__init__(self, parent)
        self._channel = channel
        self._wavelength = wavelength
        self._flash = flash
        self._background = background
        self._od = od
        self._calibration = calibration

    def setChannel(self, channel):
        self._channel = channel
        self.model_changed.emit(self)
        return self

    def getChannel(self):
        return self._channel

    def setWavelength(self, wavelength):
        self._wavelength = wavelength
        self.model_changed.emit(self)
        return self

    def getWavelength(self):
        return self._wavelength

    def setFlash(self, flash):
        self._flash = flash
        self.model_changed.emit(self)
        return self

    def getFlash(self):
        return self._flash

    def setBackground(self, background):
        self._background = background
        self.model_changed.emit(self)
        return self

    def getBackground(self):
        return self._background

    def setOD(self, od):
        self._od = od
        self.model_changed.emit(self)
        return self

    def getOD(self, raw=True):
        result = self._od
        if not raw:
            # use calibration
            result = None
        return result

    def setCalibration(self, calibration):
        self._calibration = calibration
        self.model_changed.emit(self)
        # TODO: connect calibration update
        return self


class MeasurementTableModel(BaseTableModel):
    """A table model that stores items per channel and wavelength"""

    def __init__(self, channels=None, wavelengths=None, parent=None):
        """Initialise the object

        :param channels: Number of channels to be stored
        :type channels: None or list[int]
        :param wavelengths: Number of wavelengths to store
        :type wavelengths: None or list[int]
        """
        BaseTableModel.__init__(self, data={}, parent=parent)
        self._show_mean = True
        self._show_std = True
        # value precision
        self._precision = 5
        # require at least 1 column
        self._channels = channels
        if self._channels is None:
            self._channels = [0]
        self._wavelengths = wavelengths
        if self._wavelengths is None:
            self._wavelengths = [0]
        # clear data
        self.clear()
        self._active_wavelength = wavelengths[0]

    # slots

    def countRows(self, wavelength=None):
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        return len(self.getData()[wavelength])

    def rowCount(self, QModelIndex_parent=None, wavelength=None, *args, **kwargs):
        footer_rows = 0
        if self.showsMean():
            footer_rows += 1
        if self.showsSD():
            footer_rows += 1
        return self.countRows(wavelength=wavelength) + footer_rows

    def lastRow(self, wavelength=None):
        return self.countRows(wavelength=wavelength) - 1

    def countChannels(self, wavelength=None):
        return len(self._channels)

    def countColumns(self, wavelength=None):
        return self.countChannels(wavelength=wavelength)

    def columnCount(self, QModelIndex_parent=None, wavelength=None, *args, **kwargs):
        return self.countColumns(wavelength=wavelength)

    def lastColumn(self, wavelength=None):
        return self.countChannels(wavelength=wavelength) - 1

    def hasData(self, index=None, row=None, column=None, wavelength=None):
        result = False
        if index is not None and row is None and column is None:
            row = index.row()
            column = index.column()
        if row is None and column is None:
            # has any data
            result = self.countRows(wavelength=wavelength) > 0 and self.countColumns() > 0
            for ridx in range(self.countRows(wavelength=wavelength)):
                for cidx in range(self.countColumns()):
                    result = result and self.getItem(row=ridx, column=cidx, wavelength=wavelength) is not None
        if row is None and column is not None:
            # look in all rows
            result = self.countRows(wavelength=wavelength) > 0
            for idx in range(self.countRows(wavelength=wavelength)):
                result = result and self.getItem(row=idx, column=column, wavelength=wavelength) is not None
        if column is None and row is not None:
            # look in all columns
            result = self.countColumns() > 0
            for idx in range(self.countColumns()):
                result = result and self.getItem(row=row, column=idx, wavelength=wavelength) is not None
        if None not in (row, column):
            # look at specific location
            result = self.getItem(row=row, column=column, wavelength=wavelength) is not None
        return result

    def data(self, index, role=None):
        """Returns the value of a cell

        :param index:
        :type index: QtCore.QModelIndex
        :param role:
        :type role: QtCore.QString
        :return:
        :rtype: PyQt4.QtCore.QVariant.QVariant or object
        """
        result = QtCore.QVariant()
        if self.isFooterIndex(index=index):
            result = self.footerData(index=index, role=role)
        else:
            result = BaseTableModel.data(self, index=index, role=role)
        if isinstance(result, (int, float, np.float64)):
            # try to convert to float and set precision
            result = str(np.round(result, decimals=self.getValuePrecision()))
        return result

    def headerData(self, section, orientation, role=None):
        result = QtCore.QVariant()
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            result = self._channels[section]
        if orientation == QtCore.Qt.Vertical and role == QtCore.Qt.DisplayRole:
            result = section  # just show index
            if self.isMeanRow(row=section):
                result = "Mean"
            if self.isSDRow(row=section):
                result = "SD"
        return result

    def footerData(self, index, role=None, default=QtCore.QVariant()):
        result = default
        if role == QtCore.Qt.DisplayRole:
            if self.isMeanRow(index=index):
                result = self.columnMean(index=index)
            if self.isSDRow(index=index):
                result = self.columnSD(index=index)
            if result is None:
                result = 0
        return result

    def setActiveWavelength(self, w):
        try:
            wavelength = int(w)
        except:
            wavelength = None
        if wavelength in self._wavelengths:
            self._active_wavelength = wavelength
            self.layoutChanged.emit()

    def getValue(self, index=None, row=None, column=None, wavelength=None, default=None):
        return self.getItem(index=index, row=row, column=column, wavelength=wavelength, default=default)

    def getItem(self, index=None, row=None, column=None, wavelength=None, default=None):
        """

        :param index:
        :type index: PyQt4.QtCore.QModelIndex.QModelIndex
        :param row:
        :type row:
        :param column:
        :type column:
        :param default:
        :type default:
        :return:
        :rtype:
        """
        result = default
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        if index is None and (None not in (row, column)):
            # use row and column
            index = self.createIndex(row, column)
        if index is not None and self.isTableIndex(index=index, wavelength=wavelength):
            channel = self.getChannels()[index.column()]
            result = self.getData()[wavelength][index.row()].get(channel, default)
        return result

    def setData(self, index, value, role=None):
        """Sets the value of a cell

        :param index:
        :type index: QtCore.QModelIndex
        :param value:
        :type value: QtCore.QVariant
        :param role:
        :type role: QtCore.QString
        :return:
        :rtype: bool
        """
        result = False
        if self.isTableIndex(index=index) and self.isEditable():
            if role == QtCore.Qt.EditRole:
                result = self.setItem(row=index.row(), column=index.column(), value=value)
        return result

    def setItem(self, index=None, row=None, column=None, wavelength=None, value=None):
        """Sets the value of an item in this table model

        :param index:
        :type index: QtCore.QModelIndex
        :param row:
        :type row: int
        :param column:
        :type column: int
        :param value:
        :type value: object
        :return:
        :rtype: bool
        """
        result = False
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        if index is None and (None not in (row, column)):
            # use row and column
            index = self.createIndex(row, column)
        if index is not None and self.isTableIndex(index=index, wavelength=wavelength):
            channel = self.getChannels()[index.column()]
            self.getData()[wavelength][index.row()][channel] = value
            # trigger cell change
            if self.isActiveWavelength(wavelength=wavelength):
                self.dataChanged.emit(index, index)
            # trigger footer update
            self.triggerFooterUpdate(index=index)
            result = True
        return result

    def columnMean(self, index=None, column=None, wavelength=None):
        """Returns the mean of all OD values in the column"""
        result = None
        if column is None and index is not None:
            column = index.column()
        if column is not None:
            values = []
            for row in range(self.countRows(wavelength=wavelength)):
                value = self.getValue(row=row, column=column, wavelength=wavelength)
                if value is not None:
                    values.append(value)
            if len(values) >= 1:
                result = np.mean(np.array(values), axis=0)
        return result

    def triggerFooterUpdate(self, index=None, row=None, column=None):
        if self.showsMean():
            self.triggerMeanUpdate(index=index, row=row, column=column)
        if self.showsSD():
            self.triggerSDUpdate(index=index, row=row, column=column)

    def triggerMeanUpdate(self, index=None, row=None, column=None):
        if column is None and index is not None:
            column = index.column()
        if self.showsMean():
            self.dataChanged.emit(
                self.createIndex(self.getMeanRow(), column), self.createIndex(self.getMeanRow(), column)
            )

    def triggerSDUpdate(self, index=None, row=None, column=None):
        if column is None and index is not None:
            column = index.column()
        if self.showsSD():
            self.dataChanged.emit(
                self.createIndex(self.getSDRow(), column), self.createIndex(self.getSDRow(), column)
            )

    def columnSD(self, index=None, column=None, wavelength=None):
        """Returns the standard deviation of all OD values in the column"""
        result = None
        if column is None and index is not None:
            column = index.column()
        if column is not None:
            values = []
            for row in range(self.countRows(wavelength=wavelength)):
                value = self.getValue(row=row, column=column, wavelength=wavelength)
                if value is not None:
                    values.append(value)
            if len(values) >= 1:
                result = np.std(np.array(values), axis=0)
        return result

    def addRow(self, wavelength=None):
        """Adds a row to the measurements"""
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        # add an empty row
        self.getData()[wavelength].append({c: None for c in self.getChannels()})
        if self.isActiveWavelength(wavelength=wavelength):
            self.layoutChanged.emit()

    def deleteRow(self, index=None, row=None, wavelength=None):
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        if index is None and row is not None:
            index = self.createIndex(row, 0)
        if index is not None and self.isTableIndex(index=index, wavelength=wavelength):
            self.getData()[wavelength].pop(index.row())
            if self.isActiveWavelength(wavelength=wavelength):
                self.layoutChanged.emit()

    def clear(self):
        """Clear both wavelengths"""
        self._setData({})
        for w in self.getWavelengths():
            self.getData()[w] = []
        self.layoutChanged.emit()

    # getter setters

    def isTableIndex(self, index=None, row=None, column=None, wavelength=None):
        result = False
        if None in (row, column) and index is not None:
            row, column = index.row(), index.column()
        if None not in (row, column):
            result = 0 <= row < self.countRows(wavelength=wavelength) and \
                     0 <= column < self.countColumns(wavelength=wavelength)
        return result

    def isFooterIndex(self, index=None, row=None, wavelenght=None, column=None):
        return self.isMeanRow(index=index, row=row, wavelength=wavelenght) or \
               self.isSDRow(index=index, row=row, wavelength=wavelenght)

    def getChannels(self):
        return self._channels

    def getActiveWavelength(self):
        return self._active_wavelength

    def isActiveWavelength(self, wavelength=None):
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        return wavelength == self.getActiveWavelength()

    def getWavelengths(self):
        return self._wavelengths

    def countWavelengths(self):
        return len(self.getWavelengths())

    def getValuePrecision(self):
        return self._precision

    def setValuePrecision(self, precision):
        """Set how many decimals return"""
        self._precision = int(precision)
        # that affects all the data
        self.layoutChanged.emit()
        return self

    def showsMean(self):
        return self._show_mean is True

    def showMean(self, state):
        self._show_mean = state is True
        return self

    def getMeanRow(self, wavelength=None):
        result = None
        if self.showsMean():
            result = self.countRows(wavelength=wavelength)
        return result

    def isMeanRow(self, index=None, row=None, wavelength=None):
        result = False
        if row is None and index is not None:
            row = index.row()
        if self.showsMean() and row < self.rowCount(wavelength=wavelength):
            result = row == self.getMeanRow(wavelength=wavelength)
        return result

    def showsSD(self):
        return self._show_std is True

    def showSD(self, state):
        self._show_std = state is True
        return self

    def getSDRow(self, wavelength=None):
        result = None
        if self.showsSD():
            result = self.countRows(wavelength=wavelength) + (1 if self.showsMean() else 0)
        return result

    def isSDRow(self, index=None, row=None, wavelength=None):
        result = False
        if row is None and index is not None:
            row = index.row()
        if self.showsSD() and row < self.rowCount(wavelength=wavelength):
            result = row == self.getSDRow()
        return result


class ODMeasurementTableModel(MeasurementTableModel):
    """A table model where measurements are represented per channel"""

    def __init__(self, channels=None, wavelengths=None, parent=None):
        """Initialise the object

        :param channels: Number of channels to be stored
        :type channels: None or list[int]
        :param wavelengths: Number of wavelengths to store
        :type wavelengths: None or list[int]
        """
        MeasurementTableModel.__init__(self, channels=channels, wavelengths=wavelengths, parent=parent)

    # slots

    def tableData(self, index, role=None, default="NA"):
        """Returns the value in the table"""
        result = QtCore.QVariant()
        if self.isTableIndex(index=index):
            if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
                result = str(self.getValue(index=index, default=default))
        return result

    def getOD(self, index=None, row=None, column=None, wavelength=None, default=None):
        """Returns the Optical Density of the measurement in the given cell"""
        return self.getValue(index=index, row=row, column=column, wavelength=wavelength, default=default)

    def getValue(self, index=None, row=None, column=None, wavelength=None, default=None):
        """Returns the Value (OD) of the measurement in the given cell"""
        result = default
        item = self.getMeasurement(index=index, row=row, column=column, wavelength=wavelength, default=None)
        if item is not None:
            result = float(np.round(item.getOD(), decimals=self.getValuePrecision()))
        return result

    def getMeasurement(self, index=None, row=None, column=None, wavelength=None, default=None):
        """Returns the measurement stored at the given location in the table"""
        return self.getItem(index=index, row=row, column=column, wavelength=wavelength, default=default)

    def setData(self, index, value, role=None):
        """Sets the value of a cell

        :param index:
        :type index: QtCore.QModelIndex
        :param value:
        :type value: QtCore.QVariant
        :param role:
        :type role: QtCore.QString
        :return:
        :rtype: bool
        """
        result = False
        if self.isTableIndex(index=index) and self.isEditable():
            if role == QtCore.Qt.EditRole:
                value_float = 0.0
                try:
                    value_float = float(value)
                except (ValueError, TypeError):
                    self.getLog().warning("Unable to process given value %s" % value)
                    pass
                result = self.setOD(index=index, value=value_float)
        return result

    def setMeasurement(self, index=None, row=None, column=None, wavelength=None, measurement=None):
        """Replaces a measurement with a new Measurement item"""
        result = False
        if measurement is not None and isinstance(measurement, MeasurementModel):
            result = self.setItem(index=index, row=row, column=column, wavelength=wavelength, value=measurement)
        return result

    def setOD(self, index=None, row=None, column=None, wavelength=None, value=None):
        """Sets the Optical Density of an object"""
        result = False
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        if index is None and None not in (row, column):
            index = self.createIndex(row, column)
        if index is not None and self.isTableIndex(index=index, wavelength=wavelength):
            m = self.getMeasurement(index=index, wavelength=wavelength)
            if m is None:
                # create new Measurement
                channel = self.getChannels()[index.column()]
                m = MeasurementModel(channel=channel, wavelength=wavelength, flash=0, background=0, od=value)
                self.setMeasurement(index=index, wavelength=wavelength, measurement=m)
                result = True
            else:
                m.setOD(value)
                self.dataChanged.emit(index, index)
                self.triggerFooterUpdate(index=index)
                result = True
        return result

    def addRow(self, wavelength=None):
        """Adds a new row with empty measurements"""
        # add an empty row
        if wavelength is None:
            wavelength = self.getActiveWavelength()
        self.getData()[wavelength].append(
            {c: MeasurementModel(c, wavelength, 0.0, 0.0, 0.0) for c in self.getChannels()})
        self.layoutChanged.emit()
