# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore
from .base import BaseModel, Locked

from uvaSerial.psiSerialController import psiSerialController
from uvaSerial.psiFakeSerialController import psiFakeSerialController

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SerialConnectionModel(BaseModel):
    """Stores the connection parameters (Thread safe)"""

    # config signal
    config_changed = QtCore.pyqtSignal()
    # parameter signals
    port_changed = QtCore.pyqtSignal(str)
    rate_changed = QtCore.pyqtSignal(int)
    fake_changed = QtCore.pyqtSignal(bool)
    readings_changed = QtCore.pyqtSignal(int)
    samples_changed = QtCore.pyqtSignal(int)
    gas_pause_changed = QtCore.pyqtSignal(int)
    aggregation_changed = QtCore.pyqtSignal(str)

    def __init__(self, port=None, rates=None, serial_type=psiSerialController, parent=None):
        BaseModel.__init__(self, parent)
        self._enabled = True
        self._port = port
        self.port_changed.connect(self.notifyChange)
        self._rates = rates if rates is not None else [9600]
        self.rate_changed.connect(self.notifyChange)
        self._rate = self._rates[0] if len(self._rates) > 0 else None
        self._serial_type = serial_type
        self.fake_changed.connect(self.notifyChange)
        self._readings = 3
        self.readings_changed.connect(self.notifyChange)
        self._samples = 1
        self.samples_changed.connect(self.notifyChange)
        self._gas_pause = 10
        self.gas_pause_changed.connect(self.notifyChange)
        self._aggregation = "mean"
        self.aggregation_changed.connect(self.notifyChange)

    @QtCore.pyqtSlot(object)
    @QtCore.pyqtSlot(str)
    @QtCore.pyqtSlot(int)
    @QtCore.pyqtSlot(bool)
    def notifyChange(self, value):
        """Notifies other object about a change in this object"""
        self.changed.emit(self)

    def save(self, config):
        """Stores the parameters in the config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        config.setSerialPort(self.getPort())
        self.saveReadings(config, self.getReadings())
        self.saveSamples(config, self.getSamples())
        self.saveGasPause(config, self.getGasPause())
        self.saveAggregation(config, self.getAggregation())

    def saveSetting(self, config, name, value, root=None):
        """Stores a setting in the settings element directly under the root element.

        If root is None, use the Cultivator Element as root (if defined).

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        :type name: str
        :type value: object
        :return:
        :rtype:
        """
        if root is None:
            root = config.getCultivatorElement()
        if root is not None:
            e_settings = config.getSettingsElement(root)
            if e_settings is not None:
                e_setting = e_settings.findall("./u:setting[@name='%s']" % name, config.getNameSpace())
                if len(e_setting) > 0:
                    e_setting[0].set("value", str(value))
        return self

    def saveReadings(self, config, value):
        self.saveSetting(config, "od_readings", value)
        self.config_changed.emit()

    def saveSamples(self, config, value):
        self.saveSetting(config, "od_samples", value)
        self.config_changed.emit()

    def saveGasPause(self, config, value):
        self.saveSetting(config, "gas_pause", value)
        self.config_changed.emit()

    def saveAggregation(self, config, value):
        self.saveSetting(config, "aggregation_method", value)
        self.config_changed.emit()

    def load(self, config):
        """Loads the parameters from config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        self.setPort(self.loadPort(config))
        self._rates = self.loadRates(config)
        self.rate_changed.emit(self.getRate())
        self.useFake(False)
        self.setReadings(self.loadReadings(config))
        self.setSamples(self.loadSamples(config))
        self.setGasPause(self.loadGasPause(config))
        self.setAggregation(self.loadAggregation(config))

    def loadPort(self, config):
        """Loads the port from the config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        result = None
        if config is not None:
            result = config.getSerialPort()
        return result

    def loadRates(self, config):
        """Loads the preferred baudrates from the config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        result = [9600, 115200]
        if config is not None:
            pass
        # no implementation to get the rates from the config
        return result

    def loadSetting(self, config, name, default=None):
        """

        :param config:
        :type config:
        :param name:
        :type name:
        :param value:
        :type value:
        :return:
        :rtype:
        """
        result = default
        e_cult = config.getCultivatorElement()
        if e_cult is not None:
            settings = config.parseSettings(e_cult)
            result = settings.get(name, result)
        return result

    def loadReadings(self, config):
        """Loads the number of readings from the config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        return self.loadSetting(config, name="od_repeats", default=3)

    def loadSamples(self, config):
        """Loads the number of samples from the config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        return self.loadSetting(config, name="od_samples", default=1)

    def loadGasPause(self, config):
        """Loads the time to wait for bubbles to disappear from the config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        return self.loadSetting(config, name="gas_pause", default=0)

    def loadAggregation(self, config):
        """Loads how the samples get aggregated from the config

        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        return self.loadSetting(config, name="aggregation_method", default="mean")

    @Locked()
    def getPort(self):
        """Returns the port to which this model connects

        :rtype: None or str
        """
        result = None
        with self.getLock():
            result = self._port
        return result

    def setPort(self, port):
        if self.isEnabled():
            with self.getLock():
                self._port = port
            self.port_changed.emit("" if self.getPort() is None else self.getPort())
            self.changed.emit(self)
        return self

    def getRates(self):
        """Returns the baudrates that are preffered to be used with this model

        :rtype: list[int]
        """
        result = []
        with self.getLock():
            result = self._rates
        return result

    def setRates(self, rates):
        if self.isEnabled():
            with self.getLock():
                self._rates = rates
            self.rate_changed.emit(self.getRate())
            self.changed.emit(self)
        return self

    def countRates(self):
        return len(self.getRates())

    def hasRate(self, idx):
        count = self.countRates()
        return -count <= idx < count

    def getRate(self, idx=None):
        """Returns the baudrate at which this models will use the serial link

        :rtype: None or int
        """
        result = None
        with self.getLock():
            result = self._rate
        if idx is not None and self.hasRate(idx):
            result = self.getRates()[idx]
        return result

    def setRate(self, rate):
        if self.isEnabled():
            with self.getLock():
                self._rate = rate
            if rate not in self.getRates():
                self.getRates().append(rate)
            self.rate_changed.emit(self.getRate())
            self.changed.emit(self)
        return self

    def getType(self):
        """Returns the type of the serial controller that will be used to connect to the device

        :rtype: T <= uvaSerial.uvaSerialController.SerialController
        """
        result = None
        with self.getLock():
            result = self._serial_type
        return result

    def setType(self, serial_type):
        """Sets the type of the serial controller

        :type: T <= uvaSerial.uvaSerialController.SerialController
        """
        if self.isEnabled():
            with self.getLock():
                self._serial_type = serial_type
            self.fake_changed.emit(self.usesFake())
            self.changed.emit(self)
        return self

    def useFake(self, state=False):
        if state is True:
            self.setType(psiFakeSerialController)
        else:
            self.setType(psiSerialController)
        return self

    def usesFake(self):
        return self.getType() is psiFakeSerialController

    def getReadings(self):
        result = 0
        with self.getLock():
            result = self._readings
        return result

    def setReadings(self, readings):
        if self.isEnabled():
            with self.getLock():
                self._readings = readings
            self.readings_changed.emit(self.getReadings())
            self.changed.emit(self)
        return self

    def getSamples(self):
        result = 0
        with self.getLock():
            result = self._samples
        return result

    def setSamples(self, samples):
        if self.isEnabled():
            with self.getLock():
                self._samples = samples
            self.samples_changed.emit(self.getSamples())
        return self

    def getGasPause(self):
        result = 0
        with self.getLock():
            result = self._gas_pause
        return result

    def setGasPause(self, pause):
        if self.isEnabled():
            with self.getLock():
                self._gas_pause = pause
            self.gas_pause_changed.emit(self.getGasPause())
            self.changed.emit(self)
        return self

    def getAggregation(self):
        result = None
        with self.getLock():
            result = self._aggregation
        return result

    def setAggregation(self, aggregation):
        if self.isEnabled():
            with self.getLock():
                self._aggregation = aggregation
            self.aggregation_changed.emit(self.getAggregation())
            self.changed.emit(self)
        return self

    def isEnabled(self):
        result = False
        with self.getLock():
            result = self._enabled is True
        return result

    def setEnabled(self, state):
        with self.getLock():
            self._enabled = state is True
        return self

    def copy(self, to=None):
        """Return a copy of this object"""
        to = BaseModel.copy(self, to=to)
        """:type: calibrator.models.serial.SerialConnectionModel"""
        to.setEnabled(self.isEnabled())
        to.setPort(self.getPort())
        to.setType(self.getType())
        to.setRate(self.getRate())
        to.setRates(self.getRates())
        to.setReadings(self.getReadings())
        to.setSamples(self.getSamples())
        to.setAggregation(self.getAggregation())
        to.setGasPause(self.getGasPause())
        return to
