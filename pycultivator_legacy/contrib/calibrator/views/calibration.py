# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore, QtGui, QtWidgets
from pycultivator_legacy.contrib.calibrator.views.base import BaseView
from uvaCultivator.uvaPolynomial import Polynomial, Domain
from ui.calibration import Ui_Dialog as CalibrationDialog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class CalibrationView(BaseView, QtWidgets.QDialog):

    changed = QtCore.pyqtSignal(object, name="changed")

    def __init__(self, controller, parent=None):
        """

        :param parent:
        :type parent:
        """
        BaseView.__init__(self, controller)
        QtWidgets.QWidget.__init__(self, parent)
        self._ui = CalibrationDialog()
        self._ui.setupUi(self)
        self.getUi().cbStartInfinite.stateChanged.connect(lambda: self.updateInfinite(
            self.getUi().cbStartInfinite, self.getUi().dsbStartValue
        ))
        self.getUi().cbEndInfinite.stateChanged.connect(lambda: self.updateInfinite(
            self.getUi().cbEndInfinite, self.getUi().dsbEndValue
        ))

    # Slots

    @QtCore.pyqtSlot()
    def updateSelection(self):
        """update function to refresh the view when the selected polynomial changes"""
        self.updateDomainSettings()
        self.updateDomainLabel()
        self.updateFormulaLabel()

    def updateInfinite(self, checkbox_widget, spinbox_widget):
        is_infinite = checkbox_widget.isChecked()
        spinbox_widget.setEnabled(not is_infinite)

    def updateDomainLabel(self):
        domain = self.getSelectedDomain()
        if domain is not None:
            self.getUi().lbDomain.setText(domain.__str__())

    def updateFormulaLabel(self):
        coeffs = self.getSelectedCoefficients()
        if len(coeffs) > 0:
            self.getUi().lbFormula.setText(Polynomial.coefficientsToString(coeffs))

    def updateDomainSettings(self):
        domain = self.getSelectedDomain()
        if domain is not None:
            self.getUi().cbIncludesStart.setChecked(domain.includesStart())
            self.setDomainValue(domain.getStartValue(), self.getUi().cbStartInfinite, self.getUi().dsbStartValue)
            self.setDomainValue(domain.getEndValue(), self.getUi().cbEndInfinite, self.getUi().dsbEndValue)
            self.getUi().cbIncludesEnd.setChecked(domain.includesEnd())

    # Getters and setters

    def getSelectedIndex(self):
        result = None
        indexes = self.getUi().lvPolynomials.selectedIndexes()
        if len(indexes) > 0:
            result = indexes[0]
        return result

    def getSelectedPolynomial(self):
        result = None
        index = self.getSelectedIndex()
        if index is not None and self.getController().hasPolynomialsModel():
            # only accept first selection
            result = self.getController().getPolynomialsModel().getItem(index)
        return result

    def getSelectedDomain(self):
        result = None
        poly = self.getSelectedPolynomial()
        if poly is not None:
            result = poly.getDomain()
        return result

    def getSelectedCoefficients(self):
        result = []
        poly = self.getSelectedPolynomial()
        if poly is not None:
            result = poly.getCoefficients()
        return result

    def parseDomainSettings(self):
        """Parses the settings from GUI in a Domain object

        :rtype: uvaCultivator.uvaPolynomialDependency.Domain
        """
        include_start = self.getUi().cbIncludesStart.isChecked()
        include_end = self.getUi().cbIncludesEnd.isChecked()
        start_value = None
        end_value = None
        try:
            if not self.getUi().cbStartInfinite.isChecked():
                start_value = self.getUi().dsbStartValue.value()
            if not self.getUi().cbEndInfinite.isChecked():
                end_value = self.getUi().dsbEndValue.value()
        except ValueError:
            pass
        return Domain.createDomain(start=start_value, includes_start=include_start, end=end_value, includes_end=include_end)

    def setDomainValue(self, value, infinite_box, value_box):
        is_infinite = value is None
        infinite_box.setChecked(is_infinite)
        if is_infinite:
            value = 0
        value_box.setValue(value)

    def getUi(self):
        """
        :rtype: calibrator.views.ui.calibration.Ui_Dialog
        """
        return self._ui

    def getController(self):
        """
        :rtype: calibrator.controllers.calibration.CalibrationController
        """
        return self._controller

    def isActive(self):
        return self.getUi().cbActive.isChecked() is True

    def setActive(self, state):
        self.getUi().cbActive.setChecked(state is True)
        return self

    def getIdentifier(self):
        return str(self.getUi().leIdentifier.text())

    def setIdentifier(self, value):
        if value is None:
            value = ""
        self.getUi().leIdentifier.setText(str(value))
        return self

    def getSource(self):
        return str(self.getUi().leSource.text())

    def setSource(self, value):
        if value is None:
            value = ""
        self.getUi().leSource.setText(str(value))
        return self

    def getCoefficients(self):
        return self.getUi().lvCoefficients.items()

    def getCoefficient(self, index):
        return self.getUi().lvCoefficients.item(index)


class FloatItemDelegate(QtWidgets.QStyledItemDelegate):
    """A float delegate for the view"""

    def paint(self, painter, option, index):
        """

        :param painter:
        :type painter:
        :param option:
        :type option:
        :param index:
        :type index: QtCore.QModelIndex
        :return:
        :rtype:
        """
        item_var = index.data(QtCore.Qt.DisplayRole)
        """:type: QtCore.QVariant"""
        item_value, ok = item_var.toFloat()

        control = QtGui.QStyleOptionSpinBox()
        """:type: QtGui.QStyleOptionSpinBox"""
# control.
