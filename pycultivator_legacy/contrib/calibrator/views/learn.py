# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore, QtGui, QtWidgets
from pycultivator_legacy.contrib.calibrator.views.base import BaseView
from pycultivator_legacy.contrib.calibrator.models.learn import LearnModel
from ui.learn import Ui_Dialog as LearnDialog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class LearnView(QtWidgets.QDialog, BaseView):

    changed = QtCore.pyqtSignal(object, name="changed")

    def __init__(self, controller, parent=None):
        """

        :param parent:
        :type parent:
        """
        BaseView.__init__(self, controller)
        QtWidgets.QWidget.__init__(self, parent)
        self._ui = LearnDialog()
        self._ui.setupUi(self)
        self.connectView()

    def connectView(self):
        """Connects widgets in the view to intern update slots"""
        self.getUi().hsPause.valueChanged.connect(self.updateMinimumTime)
        self.getUi().hsMeasureTime.valueChanged.connect(self.updateMaximumTime)
        self.getUi().hsCycleTime.valueChanged.connect(self.updateCycleTime)
        self.getUi().pbAddSample.clicked.connect(self.addSample)

    # Slots and updaters

    def updateMinimumTime(self):
        # update label
        self.getUi().lbPauseValue.setText("%s sec." % self.getMinimumTime())
        # set cycle time
        self.setCycleTime(self.getMinimumTime() + self.getMaximumTime())

    def updateMaximumTime(self):
        # update label
        self.getUi().lbMeasureTimeValue.setText("%s sec." % self.getMaximumTime())
        # update cycle time
        self.setCycleTime(self.getMinimumTime() + self.getMaximumTime())

    def updateCycleTime(self):
        # update label
        self.getUi().lbCycleTimeValue.setText("%s sec." % self.getCycleTime())

    def updateInterval(self):
        # update label
        self.getUi().lbIntervalValue.setText("%s sec." % self.getInterval())

    def updateTotalMeasurements(self):
        return self.countSamples() * int(self.getMaximumTime() / self.getInterval())

    def updateTotalTime(self):
        return self.countSamples() * self.getCycleTime()

    # Getters and setters

    def getUi(self):
        """Returns the Interface object

        :rtype: calibrator.views.ui.learn.Ui_Dialog
        """
        return self._ui

    def getController(self):
        """Returns the controller

        :rtype: calibrator.controllers.learn.LearnController
        """
        return self._controller

    def getVariable(self):
        return str(self.getUi().cbVariable.currentText())

    def setVariable(self, variable=None):
        if variable is not None:
            if self.getUi().cbVariable.findText(str(variable)) < 0:
                self.getUi().cbVariable.addItem(str(variable))
            self.getUi().cbVariable.setCurrentIndex(self.getUi().cbVariable.findText(str(variable)))
        return self

    def getThreshold(self):
        return str(self.getUi().cbThreshold.currentText())

    def setThreshold(self, threshold):
        if threshold is not None:
            if self.getUi().cbThreshold.findText(str(threshold)) < 0:
                self.getUi().cbThreshold.addItem(str(threshold))
            self.getUi().cbThreshold.setCurrentIndex(self.getUi().cbThreshold.findText(str(threshold)))
        return self

    def getTarget(self):
        return float(self.getUi().dsbThreshold.value())

    def setTarget(self, value):
        self.getUi().dsbThreshold.setValue(float(value))

    def getMinimumTime(self):
        return int(self.getUi().hsPause.value())

    def setMinimumTime(self, t):
        self.getUi().hsPause.setValue(int(t))

    def getMaximumTime(self):
        return int(self.getUi().hsMeasureTime.value())

    def setMaximumTime(self, t):
        self.getUi().hsMeasureTime.setValue(int(t))

    def getCycleTime(self):
        return int(self.getUi().hsCycleTime.value())

    def setCycleTime(self, t):
        self.getUi().hsCycleTime.setValue(int(t))

    def getInterval(self):
        return int(self.getUi().hsInterval.value())

    def setInterval(self, t):
        self.getUi().hsInterval.setValue(int(t))

    def setTotalMeasurement(self, m):
        self.getUi().lbTotalMeasurements.setText(
            "Total measurements: %s" % m
        )

    def setTotalTime(self, t):
        self.getUi().lbTotalTime.setText(
            "Total time required: %s" % t
        )

    def addSample(self, s=None):
        if s is None:
            s = self.getNewSample()
        if s not in self.getUi().lwSamples.items():
            self.getUi().lwSamples.addItem(int(s))

    def getSamples(self):
        return [int(item) for item in self.getUi().lwSamples.items()]

    def countSamples(self):
        return int(self.getUi().lwSamples.count())

    def getNewSample(self):
        return int(self.getUi().sbSample.value())
