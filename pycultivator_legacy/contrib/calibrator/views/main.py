# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore, QtWidgets
from pycultivator_legacy.contrib.calibrator.views.base import BaseView
from ui.main import Ui_MainWindow as MainWindow

from os import path

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MainView(BaseView, QtWidgets.QMainWindow):

    def __init__(self, controller, parent=None, **kwargs):
        QtWidgets.QMainWindow.__init__(self, parent=parent)
        BaseView.__init__(self, controller, parent=parent)
        self._ui = MainWindow()
        self._ui.setupUi(self)
        # connect menu actions
        self.getUi().actionOpen.triggered.connect(self.getController().loadXML)
        self.getUi().actionSave.triggered.connect(self.getController().saveXML)
        self.getUi().actionSave_as.triggered.connect(self.getController().saveXMLAs)
        self.getUi().actionEdit.triggered.connect(self.getController().editSerialConnection)
        self.getUi().actionConnect.triggered.connect(self.getController().connectSerial)
        self.getUi().actionDisconnect.triggered.connect(self.getController().disconnectSerial)
        self.getUi().actionLearn.triggered.connect(self.getController().learnSettings)
        self.getUi().actionImport.triggered.connect(self.getController().importCalibration)
        self.getUi().actionExport.triggered.connect(self.getController().exportCalibration)
        # add context menu to cultivator measurements
        self.getUi().tvMC.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.getUi().tvMC.customContextMenuRequested.connect(self.mc_menu)
        # add context menu to independent measurements
        self.getUi().tvInd.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.getUi().tvInd.customContextMenuRequested.connect(self.ind_menu)
        # add context menu to prediction measurements
        self.getUi().tvPred.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.getUi().tvPred.customContextMenuRequested.connect(self.pred_menu)
        self.updateConfig()
        self.updateView()

    def getController(self):
        """Returns the Controller of this view

        :rtype: calibrator.controllers.main.MainController
        """
        return self._controller

    def getUi(self):
        """Returns the Interface connected to this view

        :return:
        :rtype: calibrator.views.ui.main.Ui_MainWindow
        """
        return self._ui

    def getActiveWaveLength(self):
        value_int = 0
        value = str(self.getUi().cbWavelength.currentText())
        try:
            value_int = int(value)
        except Exception as e:
            self.getLog().warning("Unable to convert - %s - to int" % value)
        return value_int

    def setConfigState(self):
        has_config = self.getController().hasConfig()
        self.getUi().actionSave.setEnabled(has_config)
        self.getUi().pbSave.setEnabled(has_config)
        self.getUi().actionSave_as.setEnabled(has_config)
        self.getUi().menuMC.setEnabled(has_config)
        self.getUi().actionEdit.setEnabled(has_config)
        self.getUi().pbEditSerial.setEnabled(has_config)
        self.getUi().cbWavelength.setEnabled(has_config)

    def setConnectionState(self):
        has_config = self.getController().hasConfig()
        is_connected = has_config and self.getController().isConnected()
        self.getUi().actionConnect.setEnabled(has_config and not is_connected)
        self.getUi().pbConnect.setEnabled(has_config and not is_connected)
        self.getUi().actionDisconnect.setEnabled(is_connected)
        # TODO: Enable when learning is implemented
        self.getUi().actionLearn.setEnabled(False)
        self.getUi().pbDisconnect.setEnabled(is_connected)
        self.getUi().pbMeasure.setEnabled(is_connected)

    def showStatusBarMessage(self, msg):
        self.getUi().statusbar.showMessage(msg)

    @QtCore.pyqtSlot(object)
    def updateConfig(self, controller=None):
        """ Updates the view according to the config state

        :return:
        :rtype:
        """
        if controller is None:
            controller = self.getController()
        title = "Calibrator"
        has_config = controller.hasConfig()
        if has_config:
            title = "%s - %s%s" % (
                title, path.split(controller.getXMLPath())[1],
                "" if controller.isSaved() else " *"
            )
        self.setWindowTitle(title)

    def updateView(self):
        self.setConfigState()
        self.setConnectionState()
        self.updateConfig()
        self.updateCultivatorControls()
        self.updateIndependentControls()
        self.updatePredictionControls()
        self.updateCalibrationControls()

    def updateCultivatorControls(self):
        has_config = self.getController().hasConfig()
        is_connected = has_config and self.getController().isConnected()
        has_mc_data = is_connected and self.getController().hasCultivatorData()
        self.getUi().gbMC.setEnabled(is_connected)
        self.getUi().tvMC.setEnabled(is_connected)
        self.getUi().pbClearMC.setEnabled(has_mc_data)

    def updateIndependentControls(self):
        has_config = self.getController().hasConfig()
        has_data = has_config and \
                   self.getController().hasIndependentModel() and \
                   self.getController().getIndependentModel().countRows() > 0
        self.getUi().gbInd.setEnabled(has_config)
        self.getUi().tvInd.setEnabled(has_config)
        self.getUi().pbAddRow.setEnabled(has_config)
        self.getUi().pbClearInd.setEnabled(has_data)

    def updatePredictionControls(self):
        has_config = self.getController().hasConfig()
        is_connected = has_config and self.getController().isConnected()
        has_pred_data = is_connected and self.getController().hasPredictionData()
        self.getUi().gbPred.setEnabled(has_config)
        self.getUi().tvPred.setEnabled(has_config)
        self.getUi().pbPredReset.setEnabled(has_pred_data)

    def updateCalibrationControls(self):
        has_config = self.getController().hasConfig()
        is_connected = has_config and self.getController().isConnected()
        has_pred_data = is_connected and self.getController().hasPredictionData()
        has_mc_data = is_connected and self.getController().hasCultivatorData()
        has_ind_data = is_connected and self.getController().hasIndependentData()
        has_all_data = has_pred_data and has_mc_data and has_ind_data
        self.getUi().pbCalibrate.setEnabled(has_all_data)

    @QtCore.pyqtSlot(QtCore.QPoint)
    def ind_menu(self, pos):
        # retrieve selection
        index = self.getUi().tvInd.indexAt(pos)
        # determine if the menu can be used
        menu_ready = self.getController().hasIndependentModel()
        # define menu
        menu = QtWidgets.QMenu()
        addAction = menu.addAction("Add row")
        deleteAction = menu.addAction("Delete row")
        deleteAction.setEnabled(
            menu_ready and self.getController().getIndependentModel().hasData(row=index.row())
        )
        menu.addSeparator()
        calibrateAction = menu.addAction("Calibrate")
        calibrateAction.setEnabled(
            menu_ready and
            self.getController().hasPredictionModel() and
            self.getController().getPredictionModel().hasData(column=index.column()) and
            self.getController().getIndependentModel().hasData(index = index) and
            self.getController().hasCultivatorModel() and
            self.getController().getCultivatorModel().hasData(column=index.column())
        )
        menu.addSeparator()
        clearAction = menu.addAction("Clear")
        clearAction.setEnabled(
            menu_ready and self.getController().getIndependentModel().hasData()
        )
        # show menu
        if menu_ready:
            action = menu.exec_(self.getUi().tvInd.mapToGlobal(pos))
            if action == addAction:
                self.getController().getIndependentModel().addRow()
            if action == deleteAction:
                self.getController().getIndependentModel().deleteRow(index)
            if action == calibrateAction:
                self.getController().calibrate_model(index=index)
            if action == clearAction:
                self.getController().clearTableModel(self.getController().getIndependentModel())

    @QtCore.pyqtSlot(QtCore.QPoint)
    def mc_menu(self, pos):
        # retrieve selection
        index = self.getUi().tvMC.indexAt(pos)
        # determine if the menu can be used
        menu_ready = self.getController().hasCultivatorModel()
        # define menu
        menu = QtWidgets.QMenu()
        measureAction = menu.addAction("Measure OD")
        measureAction.setEnabled(
            menu_ready and self.getController().isConnected()
        )
        deleteAction = menu.addAction("Delete row")
        deleteAction.setEnabled(
            menu_ready and self.getController().getCultivatorModel().hasData(row=index.row())
        )
        menu.addSeparator()
        calibrateAction = menu.addAction("Calibrate")
        calibrateAction.setEnabled(
            menu_ready and
            self.getController().hasPredictionModel() and
            self.getController().getPredictionModel().hasData(column=index.column()) and
            self.getController().hasIndependentModel() and
            self.getController().getIndependentModel().hasData(column=index.column()) and
            self.getController().hasCultivatorModel() and
            self.getController().getCultivatorModel().hasData(row=index.row(), column=index.column())
        )
        menu.addSeparator()
        clearAction = menu.addAction("Clear")
        clearAction.setEnabled(
            menu_ready and self.getController().getIndependentModel().hasData()
        )
        # show menu
        if menu_ready:
            action = menu.exec_(self.getUi().tvMC.mapToGlobal(pos))
            if action == measureAction:
                self.getController().measureSerial()
            if action == deleteAction:
                self.getController().getCultivatorModel().deleteRow(index)
            if action == calibrateAction:
                self.getController().calibrate_model(index=index)
            if action == clearAction:
                self.getController().clearTableModel(self.getController().getCultivatorModel())

    @QtCore.pyqtSlot(QtCore.QPoint)
    def pred_menu(self, pos):
        # retrieve selection
        index = self.getUi().tvPred.indexAt(pos)
        # determine if the menu can be used
        menu_ready = self.getController().hasPredictionModel()
        # define menu
        menu = QtWidgets.QMenu()
        calibrateAction = menu.addAction("Calibrate")
        calibrateAction.setEnabled(
            menu_ready and
            self.getController().getPredictionModel().hasData(index=index) and
            self.getController().hasIndependentModel() and
            self.getController().getIndependentModel().hasData(column=index.column()) and
            self.getController().hasCultivatorModel() and
            self.getController().getCultivatorModel().hasData(column=index.column())
        )
        editAction = menu.addAction("Edit calibration")
        editAction.setEnabled(
            menu_ready and self.getController().getPredictionModel().hasData(index=index)
        )
        menu.addSeparator()
        resetAction = menu.addAction("Reset calibration")
        resetAction.setEnabled(
            menu_ready and self.getController().getPredictionModel().hasData(index=index)
        )
        # show menu
        if menu_ready:
            action = menu.exec_(self.getUi().tvPred.mapToGlobal(pos))
            if action == calibrateAction:
                self.getController().calibrate_model(index=index)
            if action == editAction:
                self.getController().edit_calibration(index=index)
            if action == resetAction:
                channel = self.getController().getPredictionModel().getChannels()[index.column()]
                self.getController().reset_calibration(channel=channel)
