# coding=utf-8

from pycultivator_legacy.contrib.calibrator import QtCore, QtGui, QtWidgets
from pycultivator_legacy.contrib.calibrator.views.base import BaseView
from ui.serial import Ui_Dialog as SerialDialog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SerialView(BaseView, QtWidgets.QDialog):

    changed = QtCore.pyqtSignal(object, name="changed")

    def __init__(self, controller, parent=None):
        """

        :param parent:
        :type parent:
        """
        BaseView.__init__(self, controller)
        QtWidgets.QWidget.__init__(self, parent)
        self._ui = SerialDialog()
        self._ui.setupUi(self)
        self._model = None
        self.showMessage("")
        self.getUi().pbProgress.setValue(0)

    def getUi(self):
        """Returns the Interface object

        :rtype: calibrator.views.ui.serial.Ui_Dialog
        """
        return self._ui

    def getController(self):
        """Returns the controller

        :rtype: calibrator.controllers.serial.SerialController
        """
        return self._controller

    def getPort(self):
        """Returns the port as shown in the GUI

        :return:
        :rtype: str
        """
        return str(self.getUi().lePort.text())

    def setPort(self, port):
        if port is None:
            port = ""
        self.getUi().lePort.setText(port)

    def getRate(self):
        return int(self.getUi().cbRate.currentText())

    def setRate(self, rate):
        if rate is not None:
            if self.getUi().cbRate.findText(str(rate)) < 0:
                self.getUi().cbRate.addItem(str(rate))
            self.getUi().cbAggregation.setCurrentIndex(self.getUi().cbAggregation.findText(str(rate)))

    # end if

    def useFake(self, state):
        self.getUi().cbFake.setChecked(state is True)

    def usesFake(self):
        return self.getUi().cbFake.isChecked()

    def getReadings(self):
        return int(self.getUi().sbRepeats.value())

    @QtCore.pyqtSlot(int)
    def setReadings(self, readings):
        self.getUi().sbRepeats.setValue(readings)

    def getSamples(self):
        return int(self.getUi().sbSamples.value())

    @QtCore.pyqtSlot(int)
    def setSamples(self, samples):
        self.getUi().sbSamples.setValue(samples)

    def getAggregation(self):
        return str(self.getUi().cbAggregation.currentText())

    @QtCore.pyqtSlot(str)
    def setAggregation(self, aggregation):
        if aggregation is not None:
            if self.getUi().cbAggregation.findText(aggregation) < 0:
                self.getUi().cbAggregation.addItem(aggregation)
            self.getUi().cbAggregation.setCurrentIndex(self.getUi().cbAggregation.findText(aggregation))

    # end if

    def getGasPause(self):
        return int(self.getUi().sbGasPause.value())

    @QtCore.pyqtSlot(int)
    def setGasPause(self, pause):
        self.getUi().sbGasPause.setValue(pause)

    @QtCore.pyqtSlot(int)
    def setProgress(self, value):
        if value > 100:
            value = 100
        self.getUi().pbProgress.setValue(value)
        self.getUi().pbProgress.update()
        self.getApplication().processEvents()

    def showMessage(self, msg):
        self.getUi().lbStatus.setText(msg)

    @QtCore.pyqtSlot(str)
    def updatePort(self, port):
        self.setPort("" if port is None else port)

    @QtCore.pyqtSlot(int)
    def updateRate(self, rate):
        pass

    @QtCore.pyqtSlot(bool)
    def updateFake(self, is_fake):
        self.useFake(is_fake)

    def setControlsEnabled(self, state):
        self.getUi().buttonBox.setEnabled(state is True)
        self.getUi().pbTestSerial.setEnabled(state is True)
        self.getUi().pbTestMeasurement.setEnabled(state is True)

    def setBusyState(self, state):
        if state:
            self.getController().getApplication().setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        else:
            self.getController().getApplication().restoreOverrideCursor()
        self.setControlsEnabled(not state)
