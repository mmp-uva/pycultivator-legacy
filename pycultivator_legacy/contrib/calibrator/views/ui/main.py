# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(964, 819)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.gbPred = QtWidgets.QGroupBox(self.centralwidget)
        self.gbPred.setObjectName("gbPred")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.gbPred)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.pbPredReset = QtWidgets.QPushButton(self.gbPred)
        self.pbPredReset.setObjectName("pbPredReset")
        self.gridLayout_5.addWidget(self.pbPredReset, 1, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_5.addItem(spacerItem, 1, 0, 1, 1)
        self.tvPred = QtWidgets.QTableView(self.gbPred)
        self.tvPred.setEnabled(False)
        self.tvPred.setObjectName("tvPred")
        self.gridLayout_5.addWidget(self.tvPred, 0, 0, 1, 2)
        self.gridLayout.addWidget(self.gbPred, 5, 0, 1, 1)
        self.gbMC = QtWidgets.QGroupBox(self.centralwidget)
        self.gbMC.setObjectName("gbMC")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.gbMC)
        self.gridLayout_4.setObjectName("gridLayout_4")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem1, 3, 0, 1, 1)
        self.pbClearMC = QtWidgets.QPushButton(self.gbMC)
        self.pbClearMC.setObjectName("pbClearMC")
        self.gridLayout_4.addWidget(self.pbClearMC, 3, 1, 1, 1)
        self.tvMC = QtWidgets.QTableView(self.gbMC)
        self.tvMC.setEnabled(False)
        self.tvMC.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tvMC.setObjectName("tvMC")
        self.gridLayout_4.addWidget(self.tvMC, 2, 0, 1, 2)
        self.gridLayout.addWidget(self.gbMC, 3, 0, 1, 1)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.lbWavelength = QtWidgets.QLabel(self.frame)
        self.lbWavelength.setObjectName("lbWavelength")
        self.gridLayout_2.addWidget(self.lbWavelength, 1, 1, 1, 1)
        self.cbWavelength = QtWidgets.QComboBox(self.frame)
        self.cbWavelength.setEnabled(False)
        self.cbWavelength.setObjectName("cbWavelength")
        self.cbWavelength.addItem("")
        self.cbWavelength.addItem("")
        self.gridLayout_2.addWidget(self.cbWavelength, 1, 2, 1, 1)
        self.pbMeasure = QtWidgets.QPushButton(self.frame)
        self.pbMeasure.setEnabled(False)
        self.pbMeasure.setObjectName("pbMeasure")
        self.gridLayout_2.addWidget(self.pbMeasure, 1, 3, 1, 1)
        self.pbOpen = QtWidgets.QPushButton(self.frame)
        self.pbOpen.setObjectName("pbOpen")
        self.gridLayout_2.addWidget(self.pbOpen, 0, 1, 1, 1)
        self.pbConnect = QtWidgets.QPushButton(self.frame)
        self.pbConnect.setEnabled(False)
        self.pbConnect.setObjectName("pbConnect")
        self.gridLayout_2.addWidget(self.pbConnect, 0, 3, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem2, 0, 6, 1, 1)
        self.pbDisconnect = QtWidgets.QPushButton(self.frame)
        self.pbDisconnect.setEnabled(False)
        self.pbDisconnect.setObjectName("pbDisconnect")
        self.gridLayout_2.addWidget(self.pbDisconnect, 0, 5, 1, 1)
        self.pbSave = QtWidgets.QPushButton(self.frame)
        self.pbSave.setEnabled(False)
        self.pbSave.setObjectName("pbSave")
        self.gridLayout_2.addWidget(self.pbSave, 0, 2, 1, 1)
        self.pbEditSerial = QtWidgets.QPushButton(self.frame)
        self.pbEditSerial.setEnabled(False)
        self.pbEditSerial.setObjectName("pbEditSerial")
        self.gridLayout_2.addWidget(self.pbEditSerial, 0, 4, 1, 1)
        self.pbCalibrate = QtWidgets.QPushButton(self.frame)
        self.pbCalibrate.setEnabled(False)
        self.pbCalibrate.setObjectName("pbCalibrate")
        self.gridLayout_2.addWidget(self.pbCalibrate, 1, 4, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem3, 1, 5, 1, 2)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)
        self.gbInd = QtWidgets.QGroupBox(self.centralwidget)
        self.gbInd.setObjectName("gbInd")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.gbInd)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.tvInd = QtWidgets.QTableView(self.gbInd)
        self.tvInd.setEnabled(False)
        self.tvInd.setSelectionMode(QtWidgets.QAbstractItemView.ContiguousSelection)
        self.tvInd.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.tvInd.setObjectName("tvInd")
        self.gridLayout_6.addWidget(self.tvInd, 0, 0, 1, 3)
        self.pbClearInd = QtWidgets.QPushButton(self.gbInd)
        self.pbClearInd.setObjectName("pbClearInd")
        self.gridLayout_6.addWidget(self.pbClearInd, 1, 2, 1, 1)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_6.addItem(spacerItem4, 1, 0, 1, 1)
        self.pbAddRow = QtWidgets.QPushButton(self.gbInd)
        self.pbAddRow.setObjectName("pbAddRow")
        self.gridLayout_6.addWidget(self.pbAddRow, 1, 1, 1, 1)
        self.gridLayout.addWidget(self.gbInd, 4, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 964, 22))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuMC = QtWidgets.QMenu(self.menubar)
        self.menuMC.setEnabled(True)
        self.menuMC.setObjectName("menuMC")
        self.menuCalibrations = QtWidgets.QMenu(self.menubar)
        self.menuCalibrations.setObjectName("menuCalibrations")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setEnabled(False)
        self.actionSave.setObjectName("actionSave")
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionEdit = QtWidgets.QAction(MainWindow)
        self.actionEdit.setEnabled(False)
        self.actionEdit.setObjectName("actionEdit")
        self.actionConnect = QtWidgets.QAction(MainWindow)
        self.actionConnect.setEnabled(False)
        self.actionConnect.setObjectName("actionConnect")
        self.actionDisconnect = QtWidgets.QAction(MainWindow)
        self.actionDisconnect.setEnabled(False)
        self.actionDisconnect.setObjectName("actionDisconnect")
        self.actionSave_as = QtWidgets.QAction(MainWindow)
        self.actionSave_as.setEnabled(False)
        self.actionSave_as.setObjectName("actionSave_as")
        self.actionClear_Multi_Cultivator = QtWidgets.QAction(MainWindow)
        self.actionClear_Multi_Cultivator.setObjectName("actionClear_Multi_Cultivator")
        self.actionClear_Predicted = QtWidgets.QAction(MainWindow)
        self.actionClear_Predicted.setObjectName("actionClear_Predicted")
        self.actionSettings = QtWidgets.QAction(MainWindow)
        self.actionSettings.setObjectName("actionSettings")
        self.actionLearn = QtWidgets.QAction(MainWindow)
        self.actionLearn.setEnabled(False)
        self.actionLearn.setObjectName("actionLearn")
        self.actionImport = QtWidgets.QAction(MainWindow)
        self.actionImport.setObjectName("actionImport")
        self.actionExport = QtWidgets.QAction(MainWindow)
        self.actionExport.setObjectName("actionExport")
        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addAction(self.actionSave_as)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuMC.addAction(self.actionConnect)
        self.menuMC.addAction(self.actionDisconnect)
        self.menuMC.addSeparator()
        self.menuMC.addAction(self.actionEdit)
        self.menuMC.addAction(self.actionLearn)
        self.menuCalibrations.addAction(self.actionImport)
        self.menuCalibrations.addAction(self.actionExport)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuMC.menuAction())
        self.menubar.addAction(self.menuCalibrations.menuAction())

        self.retranslateUi(MainWindow)
        self.actionExit.triggered.connect(MainWindow.close)
        self.pbOpen.clicked.connect(self.actionOpen.trigger)
        self.pbSave.clicked.connect(self.actionSave.trigger)
        self.pbConnect.clicked.connect(self.actionConnect.trigger)
        self.pbDisconnect.clicked.connect(self.actionDisconnect.trigger)
        self.pbEditSerial.clicked.connect(self.actionEdit.trigger)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.gbPred.setTitle(_translate("MainWindow", "Model predictions"))
        self.pbPredReset.setText(_translate("MainWindow", "Reset"))
        self.gbMC.setTitle(_translate("MainWindow", "Multi-Cultivator Measurements"))
        self.pbClearMC.setText(_translate("MainWindow", "Clear"))
        self.lbWavelength.setText(_translate("MainWindow", "Measure at"))
        self.cbWavelength.setItemText(0, _translate("MainWindow", "680"))
        self.cbWavelength.setItemText(1, _translate("MainWindow", "720"))
        self.pbMeasure.setText(_translate("MainWindow", "Measure.."))
        self.pbOpen.setText(_translate("MainWindow", "Open.."))
        self.pbConnect.setText(_translate("MainWindow", "Connect"))
        self.pbDisconnect.setText(_translate("MainWindow", "Disconnect"))
        self.pbSave.setText(_translate("MainWindow", "Save.."))
        self.pbEditSerial.setText(_translate("MainWindow", "Edit Connection.."))
        self.pbCalibrate.setText(_translate("MainWindow", "Calibrate"))
        self.gbInd.setTitle(_translate("MainWindow", "Independent Measurements"))
        self.pbClearInd.setText(_translate("MainWindow", "Clear"))
        self.pbAddRow.setText(_translate("MainWindow", "Add row.."))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuMC.setTitle(_translate("MainWindow", "Multi-Cultivator"))
        self.menuCalibrations.setTitle(_translate("MainWindow", "Calibrations"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionOpen.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actionSave.setText(_translate("MainWindow", "Save"))
        self.actionSave.setShortcut(_translate("MainWindow", "Ctrl+S"))
        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.actionEdit.setText(_translate("MainWindow", "Settings"))
        self.actionConnect.setText(_translate("MainWindow", "Connect"))
        self.actionDisconnect.setText(_translate("MainWindow", "Disconnect"))
        self.actionSave_as.setText(_translate("MainWindow", "Save As.."))
        self.actionSave_as.setShortcut(_translate("MainWindow", "Ctrl+Shift+S"))
        self.actionClear_Multi_Cultivator.setText(_translate("MainWindow", "Clear Multi-Cultivator"))
        self.actionClear_Predicted.setText(_translate("MainWindow", "Clear Independent"))
        self.actionSettings.setText(_translate("MainWindow", "Settings"))
        self.actionLearn.setText(_translate("MainWindow", "Learn Settings"))
        self.actionImport.setText(_translate("MainWindow", "Import..."))
        self.actionExport.setText(_translate("MainWindow", "Export..."))

