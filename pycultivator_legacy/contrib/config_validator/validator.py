#!/usr/bin/env python
# coding=utf-8
"""Application to validate configuration files"""

import sys, os, argparse
cwd = os.path.dirname(__file__)
sys.path.insert(1, os.path.realpath(os.path.join(cwd, "..")))
from lxml import etree as et

from pycultivator_legacy.config.xml import XMLConfig
from pycultivator_legacy.contrib.scripts import simpleScript
from uvaCultivator import uvaException

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class ConfigValidationScript(simpleScript.SimpleScript):

    def __init__(self, script_path, protocol, settings=None):
        super(ConfigValidationScript, self).__init__(script_path, protocol, settings)

    def _loadArguments(self, args):
        return self.loadConfigArguments(args)

    def loadConfigArguments(self, args):
        """Loads the config settings from the given arguments"""
        # preload configuration directory setting, if given
        if args.get("config_dir") is not None:
            self.setConfigDir(args["config_dir"])
        if args.get("config_path") is None:
            raise ValueError("Unable to proceed, missing configuration file")
        config_dir = os.path.split(args["config_path"])[0]
        config_name = os.path.split(args["config_path"])[1]
        # store config file location
        if config_dir != "":
            self.setSetting("dir", config_dir, "config")
        self.setSetting("file.name", config_name, "config")
        return True

    def execute(self):
        result = False
        try:
            result = self.validate()
        except ValidationException as ve:
            self.error("Invalid Configuration file!\n>>>> {}".format(ve.getMessage()))
        except et.XMLSyntaxError as xse:
            self.error("Invalid SAMPLE_XML Syntax:\n>>>> {}".format(xse))
        except et.DocumentInvalid as di:
            self.error("Invalid SAMPLE_XML:\n>>>> {}".format(di))
        return result

    def validate(self):
        conf_paths = self.findConfigFile(paths=[".", "../" "../conf"], extend=True)
        conf_path = None
        if len(conf_paths) > 0:
            conf_path = conf_paths[0]
        if conf_path is None or not os.path.exists(conf_path):
            raise ValidationException("No configuration found at: {}".format(self.getConfigPath()))
        conf_file = os.path.basename(conf_path)
        config = XMLConfig.parseFile(conf_path)
        if config is None:
            raise ValidationException("Unable to parse: {}".format(conf_file))
        if not XMLConfig.isValid(conf_path):
            XMLConfig.assertValidateXML(conf_path)
        if not XMLConfig.isSupported(conf_path):
            raise ValidationException("{} is not supported".format(conf_file))
        self.inform("The configuration file {} is valid!".format(conf_file))
        return True


class ValidationException(uvaException.UVAException):

    def __init__(self, msg):
        super(ValidationException, self).__init__(msg)


def start():

    cs = ConfigValidationScript(__file__, None)

    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Measure OD using one experiment configuration.')
    parser.add_argument('config_path', action="store",
                        help='Path to the configuration file or the file name (with or without .xml).')
    parser.add_argument('--config-dir', action="store", dest="config_dir",
                        help="Path to the configuration files.")
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that should be used.')
    # script settings
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    # set defaults
    parser.set_defaults(
        # config_dir=cs.getConfigDir(), log_dir=cs.getLogDir(), output_dir=cs.getOutputDir(),
        # force_run=cs.isForced(), verbosity=0, simulate_fake=False, ini_path=None,
        verbosity=0, ini_path=None
        # simulate=not cs.isReporting(), use_fake=cs.fakesConnection
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the
    if not cs.loadArguments(args, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    cs.start()


if __name__ == "__main__":
    start()


