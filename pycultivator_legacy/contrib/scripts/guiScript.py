# coding=utf-8
"""A class for starting GUI applications"""

import sys, traceback, argparse
from uvaTools.uvaScript.baseScript import BaseScript
from PyQt5 import QtGui

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class GUIScript(BaseScript):
    """Class implementing the execution of a GUI application from the command-line"""

    _default_settings = BaseScript.mergeDefaultSettings(
        {
            "controller.class": None
        }, namespace=""
    )

    def __init__(self, script_path, controller, settings=None):
        BaseScript.__init__(self, script_path=script_path, settings=settings)
        self.setSetting("class", controller, "controller")

    def getControllerClass(self):
        """Loads the controller class used to start the GUI Application"""
        result = self.retrieveSetting("class", namespace="controller")
        if isinstance(result, str):
            path = result
            # parse reference
            try:
                result = self.importFromPath(path)
                if not isinstance(result, type):
                    raise ImportError("Unable to load module %s" % path)
            except ImportError:
                self.getLog().critical("Unable to load %s, no such class" % path)
                result = None
        return result

    def isVerbose(self):
        return self.retrieveSetting("verbose", default=self.getDefaultSettings(), namespace="log") is True

    def setVerbose(self, state):
        self.setSetting("verbose", state is True, namespace="log")

    @staticmethod
    def importFromPath(path, name=None, package=None):
        """Import a class or module from a path specification

        :param path: Path specification (in dot notation) to class
        :type path: str
        :param name: Name of the class to import. If None will try to derive the class name from the path
        specification.
        :type name: str
        :param package: Sets the package name, required when loading relative paths (starts with dot).
        :type package: str
        :return: class reference
        :rtype: object
        """
        result = None
        # make dummy variable, we will manipulate p not path
        p = path
        # if relative and no package is set, raise Error
        if package is None and path[0] == ".":
            raise ValueError("Cannot import relative path - %s - without package specification" % path)
        # if no name is given, derive from path
        if name is None:
            # take last element
            name = path.split(".")[-1]
            # if path is decomposable into multiple parts, we remove class name
            if len(path.split(".")) > 1:
                p = ".".join(path.split(".")[:-1])
        import importlib
        try:
            # load module
            m = importlib.import_module(p, package)
            result = getattr(m, name)
        except ImportError:
            raise
        return result

    def execute(self):
        """Execute the GUI"""
        result = False
        # load application
        app = QtGui.QApplication(sys.argv)
        try:
            # Load Main Controller
            controller = self.getControllerClass()(app=app)
            # Start Main Controller
            controller.start(controller=controller, app=app, execute=True)
            result = True
        except:
            msg = "Error:\n{}".format(traceback.format_exc())
            self.getLog().critical(msg)
        finally:
            app.exit(0)
        return result

    def loadArguments(self, args, settings=None, namespace=None):
        """Parses arguments given from argparse

        :param args: Arguments to parse and load
        :type args: dict or argparse.Namespace
        :param settings: Optional settings dict, to be loaded after all arguments have been parsed.
        :type settings: None or dict
        :param namespace: Optional namespace to be added to settings dict
        :type namespace: str
        :rtype: bool
        """
        result = False
        if settings is None:
            settings = {}
        # if Namespace object, convert to dict
        if isinstance(args, argparse.Namespace):
            args = vars(args)
        if not isinstance(args, dict):
            raise ValueError("Need a dict with arguments")
        # go and fetch configuration settings
        try:
            if args.get("ini_path") is not None:
                self.loadIni(args["ini_path"])
            self.loadLogArguments(args)
            self.loadConfigArguments(args)
            #self.loadExporterArguments(args)
            #self.loadProtocolArguments(args)
            # apply given settings
            self.mergeSettings(settings=settings, namespace=namespace)
            result = True
        except:
            raise
        return result

    def loadLogArguments(self, args):
        self.setVerbose(args.get("verbose", False))
        result = super(GUIScript, self).loadLogArguments(args)
        return result

    def loadConfigArguments(self, args):
        """Loads the config settings from the given arguments"""
        # preload configuration directory setting, if given
        if args.get("config_dir") is not None:
            self.setConfigDir(args["config_dir"])
        return True
