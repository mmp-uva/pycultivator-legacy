"""Module with base object and basic methods"""

from datetime import datetime as dt
import re
import copy
import log


class BaseObject(object):
    """Base class for all pycultivator classes"""

    _log = None
    _log_levels = log.LEVELS

    # Basic definitions
    EMPTY_VALUE = [None, ""]
    DIGIT_TEST = re.compile('-?\d*(\.\d+)?')
    LIST_TEST = re.compile('\[([^\]]+)\]')
    DT_FORMAT = "%Y-%m-%d %H:%M:%S"
    DT_FORMAT_YMD = "%Y%m%d"

    # settings definitions
    _namespace = ""
    _default_settings = {}

    def __init__(self, settings=None):
        """Initialize the

        :param settings: Dictionary with all settings
        :type settings: None or dict
        :return:
        :rtype:
        """
        # self.getLog().debug("Connected {} to logger".format(self.__class__.__name__))
        # Merge with the default settings
        if settings is None:
            settings = {}
        self._settings = self.mergeDictionary(self._default_settings, settings, source_namespace="")

    @classmethod
    def getRootLogger(cls):
        return log.getLogger()

    @classmethod
    def getLog(cls):
        """Returns the logger object of this class.

        :rtype: uvaCultivator.uvaLog.UVALogger
        """
        if cls._log is None:
            cls._log = log.getLogger(cls.__name__)
        return cls._log

    @classmethod
    def getLogLevels(cls):
        """Returns all the levels supported by the logger"""
        return cls._log_levels

    @classmethod
    def getNameSpace(cls):
        """Returns the settings namespace of this class."""
        return cls._namespace

    def getSettings(self):
        """Returns all the current settings of this object."""
        return self._settings

    def setSetting(self, name, value, namespace=None):
        """Sets a setting of this object to the value, if namespace is given use that one or default to the
        namespace of this object.

        :param name:
        :type name:  str
        :param value:
        :type value: object
        :param namespace:
        :type namespace: str or None
        :return: The new settings object
        :rtype: dict
        """
        if namespace is None:
            namespace = self._namespace
        if namespace != "":
            name = "{}.{}".format(namespace, name)
        self._settings[name] = value
        return self._settings

    def replaceSettings(self, settings, namespace=None, include_defaults=True, **kwargs):
        """Replaces the settings of this object with new settings

        Settings will be applied in the following order (higher level name duplicates overwrite lower level):
        0. Default settings (if included, see include_defaults)
        1. Settings from the settings dict
        2. Settings given via the kwargs dict
        :param settings: New settings
        :type settings: dict
        :param kwargs: Additional settings to be set.
        :type kwargs: dict
        :param include_defaults: Whether to include the default settings from this object.
        :type include_defaults: bool
        :return: The new settings of this object
        :rtype: dict
        """
        # reset the current settings
        if include_defaults:
            self._settings = self._default_settings
        else:
            self._settings = {}
        # apply new settings
        return self.updateSettings(settings=settings, namespace=namespace, **kwargs)

    def updateSettings(self, settings=None, namespace=None, **kwargs):
        """Updates the settings of this object by merging it with the given settings, overwriting duplicate settings

        Settings are merged in the following order (higher level name duplicates overwrite lower level):
        0. Current (old) settings
        1. Settings from the settings dict
        2. Settings from the kwargs dict

        :param settings: Dictionary with new settings
         :type settings: dict or None
        :param namespace: Namespace of the settings, this will be appended to all the setting names before merge,
        including the kwargs names. When None (default) the namespace of the object is appended, set to "" if no
        namespace should be appended.
        :type namespace: str or None
        :param kwargs: Additional settings to be set.
        :return: The now current settings of this object
        :rtype: dict
        """
        self._settings = self.mergeDictionary(self._settings, settings, source_namespace=namespace, copy_target=False)
        self._settings = self.mergeDictionary(self._settings, kwargs, source_namespace=namespace, copy_target=False)
        return self._settings

    def mergeSettings(self, settings=None, namespace=None, **kwargs):
        """Updates the settings of this object by merging it with the given settings, overwriting duplicate settings

        Settings are merged in the following order (higher level name duplicates overwrite lower level):
        0. Current (old) settings
        1. Settings from the settings dict
        2. Settings from the kwargs dict

        Wrapper to updateSettings.

        :param settings: Dictionary with new settings
         :type settings: dict or None
        :param namespace: Namespace of the settings, this will be appended to all the setting names before merge,
        including the kwargs names. When None (default) the namespace of the object is appended, set to "" if no
        namespace should be appended.
        :type namespace: str or None
        :param kwargs: Additional settings to be set.
        :return: The now current settings of this object
        :rtype: dict
        """
        return self.updateSettings(settings=settings, namespace=namespace, **kwargs)

    @classmethod
    def mergeDefaultSettings(cls, settings=None, namespace=None, default_settings=None, **kwargs):
        """Merges the default settings of the class with new default settings and stores it as default settings for
        the current class.

        This method will make a copy of the old default settings such the setting object are not shared between classes.

        :param settings: New default settings to be merged.
        :type settings: dict
        :param namespace: Namespace string that will be prepended to setting names (both kwargs and settings). If
        None use namespace of current class, if "" do not prepend a namespace.
        :type namespace: None or str
        :param default_settings: The default settings dictionary, if not given will take the
        :type default_settings: None or dict
        """
        # test if default settings are given
        if default_settings is None:
            # use empty dict as default
            default_settings = {}
            # if current class if a descendant from uvaObject, load current settings
            if issubclass(cls, BaseObject):
                default_settings = cls._default_settings
            # # if parent is uvaObject descendant, load those default settings
            # if issubclass(cls.__base__, uvaObject):
            #     default_settings = cls.__base__.getDefaultSettings()
        # merge default settings with given settings, store as new settings
        n_settings = cls.mergeDictionary(default_settings, settings, source_namespace=namespace)
        # merge new settings with kwargs
        cls._default_settings = cls.mergeDictionary(n_settings, kwargs, source_namespace=namespace, copy_target=False)
        # return current default settings
        return cls.getDefaultSettings()

    @classmethod
    def getDefaultSettings(cls, key=None, namespace=None, default=None):
        """Returns the default settings of this class, or if key is given the default setting corresponding to the
        key.

        :param key: Key name of the setting, or None to return all settings
        :type key: None or str
        :param namespace: Namespace that will be added to the key name. If None use namespace of object.
            If "" do not prepend namespace.
        :type namespace: None or str
        :param default: Default value to return when key name cannot be found.
        :type default: object
        :return: All the default settings, the default setting corresponding to the given key or the default value.
        :rtype; object
        """
        result = cls._default_settings
        if key is not None:
            key = cls.addNameSpace(key, namespace=namespace)
            result = result.get(key, default)
        return result

    @classmethod
    def addNameSpace(cls, name, namespace=None):
        """Add namespace to a setting name"""
        result = name
        if namespace is None:
            namespace = cls.getNameSpace()
        if namespace not in (None, ""):
            result = "{}.{}".format(namespace, name)
        return result

    @classmethod
    def prependNameSpace(cls, source, namespace=None, copy_source=True):
        """Prepend namespace (if any) to each key in source.

        >>> BaseObject.prependNameSpace({"c": 1}, "a.d")
        {'a.d.c': 1}
        >>> BaseObject.prependNameSpace({"c": 1}, "")
        {'c': 1}
        >>> BaseObject.prependNameSpace({"a.d.c": 1, "c": 2}, "a.d")
        {'a.d.a.d.c': 1, 'a.d.c': 2}

        :param source: Source dictionary
        :type source: dict
        :param namespace: Namespace that will be added to name
        :type namespace: None or str
        :param copy_source: If True make a copy of the source dictionary
        :type copy_source: bool
        :rtype: dict[str, object]
        """
        result = {}
        for key in source.keys():
            name = cls.addNameSpace(key, namespace=namespace)
            result[name] = source[key]
        return result

    @classmethod
    def reduceNameSpace(cls, source, namespace=None, copy_source=True):
        """Remove namespace prefix from all keys with that prefix

        >>> BaseObject.reduceNameSpace({"a.d.c": 1}, "a.d")
        {'c': 1}
        >>> BaseObject.reduceNameSpace({"a.d.c": 1}, "")
        {'a.d.c': 1}
        >>> BaseObject.reduceNameSpace({"a.d.c": 1}, "a.b.c")
        {'a.d.c': 1}
        >>> BaseObject.reduceNameSpace({"a.d.c": 1, "c": 2}, "a.d")
        {'c': 1}

        :param source: Source dictionary
        :type source: dict[str, object]
        :param namespace: The namespace prefix to remove
        :type namespace: None or str
        :param copy_source: If True make a copy of the source dictionary
        :type copy_source: bool
        :rtype: dict[str, object]
        """
        result = source
        if copy_source:
            result = copy.copy(source)
        if namespace is None:
            namespace = cls.getNameSpace()
        if namespace != "":
            namespace = "{}.".format(namespace)
            for key in source.keys():
                if key.startswith(namespace):
                    reduced_name = key.replace(namespace, "")
                    result[reduced_name] = source[key]
                    result.pop(key)
        return result

    @classmethod
    def isolateNameSpace(cls, source, namespace=None):
        """Keep only settings with the given namespace and remove the namespace from those settings

        >>> BaseObject.isolateNameSpace({'a': 1}, 'a')
        {}
        >>> BaseObject.isolateNameSpace({'b.a': 1}, 'a')
        {}
        >>> BaseObject.isolateNameSpace({'a.b': 1}, 'a')
        {'b': 1}
        >>> BaseObject.isolateNameSpace({'a.b': 1, 'b.c' : 2, 'a.c': 3, 'b.d': 4}, 'a')
        {'c': 3, 'b': 1}

        :param source: Source dictionary
        :type source: dict[str, object]
        :param namespace: Namespace prefix to isolate, if None use default
        :type namespace: None | str
        :rtype: dict[str, object]
        """
        result = {}
        if namespace is None:
            namespace = cls.getNameSpace()
        if namespace != "":
            namespace = "{}.".format(namespace)
            for key in source.keys():
                if key.startswith(namespace):
                    reduced_name = key.replace(namespace, "")
                    result[reduced_name] = source[key]
        return result

    @classmethod
    def mergeDictionary(cls, target, source, source_namespace=None, copy_target=True):
        """Merges the source into the target, all keys in target that are also in source will be replaced by the source.
        Keys in source but not in target will be added to target. Keys not in source but present in target will be
        preserved.

        :param target: Target dictionary that should be merged with the source, overwriting duplicates in target with
        source.
        :type target: dict
        :param source_namespace: Prepend the source key with the source namespace, to avoid conflicts. If None it
        will use the class defined namespace. A zero-length string will avoid adding a namespace.
        :type source_namespace: str | None
        :param source: Dictionary to merge with target.
        :type source: dict
        :param copy_target: Whether to make a copy of the target.
        :return: The target dictionary
        :rtype: dict
        """
        result = target
        if copy_target:
            result = copy.deepcopy(target)
        if source_namespace is None:
            source_namespace = cls._namespace
        for key in source.keys():
            target_key = key
            if source_namespace != "":
                target_key = "{}.{}".format(source_namespace, key)
            result[target_key] = source[key]
        return result

    @classmethod
    def retrieveFromDict(cls, source, name, default=None, namespace=None):
        """Retrieves a value from a dict, if present.

        :param source: The source dictionary
        :type source: dict
        :param name: Key for the value in the dictionary
        :type name: str
        :param default: Default value, to return when the key is not present
        :param namespace: Namespace of the key, prepend the name with it if present. If None use namespace of this
        object.
        :type namespace: str | None
        """
        result = default
        if namespace is None:
            namespace = cls._namespace
        if namespace != "":
            name = "{}.{}".format(namespace, name)
        if name in source.keys():
            result = source[name]
        return result

    def getSetting(self, name, default=None, namespace=None):
        """Retrieve a setting from the settings dictionary. If the default is a dict, we assume it is the settings dict
        with default values and use the name to retrieve the default value from it.

        :type name: str
        :param namespace: Namespace of the key, when None will default to namespace of this current object.
        :type namespace: str
        """
        result = default
        if isinstance(default, dict):
            result = self.retrieveFromDict(source=default, name=name, namespace=namespace)
        return self.retrieveFromDict(self._settings, name=name, default=result, namespace=namespace)

    def retrieveSetting(self, name, default=None, namespace=None):
        """Retrieve a setting from the settings dictionary. If the default is a dict, we assume it is the settings dict
        with default values and use the name to retrieve the default value from it.

        :type name: str
        :param namespace: Namespace of the key, when None will default to namespace of this current object.
        :type namespace: str
        """
        return self.getSetting(name, default=default, namespace=namespace)

    def listify(self, var):
        """Change a variable in a list or when it is already a list; keep it that way.

        :param var: Variable to change to list
        :type var: object or list
        :rtype: list
        """
        return var if isinstance(var, list) else [var]

    def listifyDictKey(self, key, dictionary, default=None):
        """Transform an item in a dictionary to a list, using a key. When the key is not in the dictionary; either
        return the default value as a list - when it has a non-None value - or return an empty list - when default is
        None.

        :param key: Key to the item in the dictionary
        :type key: object
        :param dictionary: Dictionary that may hold the key
        :type dictionary: dict
        :param default: The default value to return when either no valid dictionary is given or the key was not in
        the dictionary.
        :type default: object
        :return: Return a list
        :rtype: list
        """
        result = []
        if default is not None:
            result = self.listify(default)
        if dictionary is not None and isinstance(dictionary, dict) and key in dictionary.keys():
            result = self.listify(dictionary[key])
        return result

    @classmethod
    def listifyString(cls, value, list_test=None, separator=",", item_type=str):
        """Will change a string into a list using a regex expression and splitting the list contents with the split
        using a separator (default ","). Will also perform a cast on the items given by item_type

        :param value:
        :type value:
        :return:
        :rtype:
        """
        if list_test is None:
            list_test = cls.LIST_TEST
        result = list_test.findall(value)
        if len(result) > 0:  # else -> empty list we can return!
            result = result[0].split(separator)
            if item_type is not str:
                for idx, item in enumerate(result):
                    result[idx] = cls.validateValue(item, item_type)
        return result

    @staticmethod
    def matchesTest(value, test):
        """Tests whether the given string matches the given test pattern

        :param value: String to test
        :type value: str
        :param test: Pattern to use for test
        :type test: re.RegexObject
        """
        return test.match(value) is not None

    @staticmethod
    def isNumber(value):
        """Tests whether the given string is a number, by converting it to a float. If conversion succeeds: returns
        True else False

        :param value: The string to test
        :type value: str
        :rtype: bool
        """
        result = False
        try:
            i = float(value)
            result = True
        except:
            pass
        return result

    @classmethod
    def isDigit(cls, value, test=None):
        """Tests whether the given string is a digit, by testing it using the given test pattern.

        If no pattern is given, it will use the default DIGIT_TEST of this class.
        :param value: String to test
        :type value: str
        :param test: Test pattern to use for testing
        :type test: re.RegexObject
        :rtype: bool
        """
        if test is None:
            test = cls.DIGIT_TEST
        return cls.matchesTest(value, test)

    def isDate(self, value, fmt=None):
        """Tests whether the given string is a data, by trying to convert it to a date (using the format string)

        :param value: String to test
        :type value: str
        :param fmt: Format string to use, see datetime.strptime
        :type fmt: str
        """
        result = False
        try:
            d = dt.strptime(value, fmt)
            result = True
        except:
            pass
        return result

    @classmethod
    def validateValue(cls, value, v_type, default_value=None, date_format=None, digit_test=None):
        """Tries to parse the value into the desired type, if unsuccessful it returns the default value.

        :param value: Value to parse
        :type value: str
        :param v_type: Type of the desired parsed value
        :type v_type: T
        :param default_value: Default to return when parsing fails.
        :type default_value: object
        :return: value with the type of v_type
        :rtype: Any
        """
        result = default_value
        if date_format is None: date_format = cls.DT_FORMAT
        if digit_test is None: digit_test = cls.DIGIT_TEST
        try:
            if v_type in [bool, "bool"]:
                if value is not None:
                    result = (value == "1" or str(value).lower() == "true" or value is True)
            elif v_type in [int, "int"]:
                result = int(float(value)) if cls.isDigit(value, digit_test) else default_value
            elif v_type in [float, "float"]:
                if isinstance(value, str):
                    value = value.replace(",", ".")
                result = float(value) if cls.isDigit(value, digit_test) else default_value
            elif v_type in [dt, "dt", "datetime"]:
                # parse
                result = default_value
                try:
                    # try
                    result = dt.strptime(value, date_format)
                except:
                    # forgive
                    pass
            elif v_type in [str, "str", "string"]:
                if value is not None:
                    result = value
            else:
                result = value
        except:
            pass
        return result
