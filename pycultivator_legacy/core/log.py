# coding=utf-8
"""Module implementing the logging module"""

import sys
import logging
import os

from logging import StreamHandler
from logging.handlers import RotatingFileHandler
from datetime import datetime as dt

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

LEVELS = {
    logging.CRITICAL: 'CRITICAL',
    logging.ERROR: 'ERROR',
    logging.WARNING: 'WARNING',
    logging.INFO: 'INFO',
    logging.DEBUG: 'DEBUG',
    logging.NOTSET: 'NOTSET',
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARN': logging.WARNING,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
    'NOTSET': logging.NOTSET,
}


class UvaLogger(logging.Logger):
    """A Logger with some added capabilities"""

    def addStreamHandler(self, level=logging.NOTSET, propagate=False):
        """Connect a UVAStreamHandler to this logger.

        :param level: The minimal level of messages that will be accepted by this handler
        :type level: str or int
        :param propagate: Whether to allow propagation of messages, default False to prevent double messages.
        :type propagate: bool
        :rtype: pycultivator.foundation.pcLogger.PCLogger
        """
        handler = UvaStreamHandler()
        if isinstance(level, str):
            if level not in LEVELS:
                raise ValueError("Invalid level {} received".format(level))
            level = LEVELS[level]
        handler.setLevel(level)
        self.addHandler(handler)
        self.propagate = propagate
        return self

    def addFileHandler(self, logfile, level=logging.NOTSET, propagate=False):
        """Connect a FileHandler, with appropriate format, to the given logger and write log to given logfile.

        :param logfile: path to file to log to, or use <date>_<logger.name>.log
        :type logfile: str
        :param level: The minimal level of messages that will be accepted by this handler
        :type level: int
        :param propagate: Whether to allow propagation of messages, default False to prevent double messages.
        :type propagate: bool
        :rtype: pycultivator.foundation.pcLogger.PCLogger
        """
        if not logfile:
            logfile = "./{}_{}.log".format(dt.now().strftime('%Y%m%d'), self.name)
        handler = UvaRotatingFileHandler(logfile)
        handler.setLevel(level)
        self.addHandler(handler)
        self.propagate = propagate
        return self


class UvaStreamHandler(StreamHandler):
    """A StreamHandler that by default formats it's messages with UVAFormatter"""

    def __init__(self, stream=None, formatter=None, level=logging.NOTSET):
        super(UvaStreamHandler, self).__init__(stream=stream)
        if formatter is None:
            formatter = UvaLogFormatter()
        self.setFormatter(formatter)
        self.setLevel(level)


class UvaRotatingFileHandler(RotatingFileHandler):
    """A RotatingFileHandler that by default formats it's messages with UVAFormatter"""

    def __init__(self, filename, mode="a", maxBytes=5 * 1024 ** 2, backupCount=5, encoding=None, delay=0,
                 formatter=None, level=logging.NOTSET):
        super(UvaRotatingFileHandler, self).__init__(
            filename, mode=mode, maxBytes=maxBytes, backupCount=backupCount, encoding=encoding, delay=delay
        )
        if formatter is None:
            formatter = UvaLogFormatter()
        self.setFormatter(formatter)
        self.setLevel(level)


class UvaLogFormatter(logging.Formatter):
    """A formatting class; to format log message to string"""

    LOG_FORMAT = '(%(asctime)s) %(name)s [%(levelname)s]: %(message)s'
    LOG_DT_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
    _converter = dt.fromtimestamp

    def __init__(self, fmt=LOG_FORMAT, datefmt=LOG_DT_FORMAT):
        logging.Formatter.__init__(self, fmt=fmt, datefmt=datefmt)

    def format(self, record):
        """

        :param record:
        :type record: logging.LogRecord
        :return:
        """
        record.name = ".".join(record.name.split(".")[-2:])
        return super(UvaLogFormatter, self).format(record)

    def formatTime(self, record, fmt=None):
        """A function to format time"""
        ct = self._converter(record.created)
        if fmt:
            s = ct.strftime(fmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s


logging.setLoggerClass(UvaLogger)
_loggers = {
}


def hasLogger(name):
    return name in _loggers.keys()


def getRootLoggerName():
    return 'PID_{}'.format(os.getpid())


def getRootLogger():
    """

    :return:
    :rtype: uvaCultivator.uvaLog.UVALogger
    """
    return getLogger()


def getLogger(name=None, parent=None):
    """Returns a logger that is registered in the logger dictionary

    :param name: Name of the logger to retrieve, if None return root logger
    :type name: None or str
    :return: Object of the logger
    :rtype: pycultivator_legacy.core.log.UvaLogger
    """
    if name is None:
        name = getRootLoggerName()
    else:
        if parent is None:
            parent = "{}.{}".format(
                getRootLogger().name,
                sys.argv[0][sys.argv[0].rfind("/") + 1:sys.argv[0].rfind(".")]
            )
        if isinstance(parent, logging.Logger):
            parent = parent.name
        # add name if set (else set application as root)
        if name is not None and "" not in [name, parent]:
            name = "{parent}.{name}".format(parent=parent, name=name)
        elif name is None or name == "":
            name = parent
    return logging.getLogger(name)


def connectFileHandler(log, path, level=logging.NOTSET, propagate=False):
    """

    :type log: logging.Logger or uvaCultivator.uvaLog.UVALogger
    :param path:
    :param level:
    :param propagate: Whether to propagate messages
    :return:
    """
    if os.path.exists(os.path.dirname(path)):
        handler = UvaRotatingFileHandler(path, level=level)
        if log.getEffectiveLevel() > level:
            log.setLevel(level)
        connectHandler(log, handler, propagate)


def connectStreamHandler(log, level=logging.NOTSET, propagate=False):
    """Connects a stream handler to the logger

    :type log: logging.Logger or uvaCultivator.uvaLog.UVALogger
    """
    handler = UvaStreamHandler(level=level)
    if log.getEffectiveLevel() > level:
        log.setLevel(level)
    connectHandler(log, handler, propagate)


def connectHandler(log, handler, propagate=False):
    """Connects a handler to a log

    :type log: logging.Logger or uvaCultivator.uvaLog.UVALogger
    """
    log.addHandler(handler)
    log.propagate = propagate
