# coding=utf-8
"""A module implementing all infrastructure for XML support"""

from pycultivator_legacy.core import BaseObject
import os
import glob
from lxml import etree as et

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class XML(BaseObject):
    """Class describing a XML document"""

    DT_FORMAT = "%Y-%m-%dT%H:%M:%S"
    XSI = "http://www.w3.org/2001/XMLSchema-instance"
    COMPATIBLE_VERSIONS = {
        # dictionary with compatible versions
    }

    def __init__(self, xml=None, xml_path=None, settings=None, **kwargs):
        super(XML, self).__init__(settings=settings, **kwargs)
        self._xml = xml
        self._path = xml_path
        self._ns = None
        self._parser = None
        self._schema = None

    @classmethod
    def createFromFile(cls, xml_file):
        """
        Factory class method, to create an uvaXML object from a XML file.
        :param xml_file: Path to the xml file
        :type xml_file: str
        :rtype: None, uvaXML
        """
        result = None
        xml = cls.parseFile(xml_file)
        if xml is not None:
            result = cls(xml, xml_file)
        return result

    @classmethod
    def createFromXML(cls, xml_source):
        """
        Factory class method, to create an uvaXML object from a XML String.
        :param xml_source: XML
        :type xml_source: str
        :rtype: None, uvaXML
        """
        result = None
        xml = cls.parseXML(xml_source)
        if xml is not None:
            result = cls(xml)
        return result

    @classmethod
    def readFile(cls, xml_file):
        """
        Class method to read the contents of a file and return the XML contents as string
        :param xml_file: Path to the file to read
        :type xml_file: str
        :return: XML Contents of the file
        :rtype: str
        """
        result = None
        xml = cls.parseFile(xml_file)
        if xml is not None:
            result = et.tostring(xml)
        return result

    @classmethod
    def parseFile(cls, xml_file):
        """
        Class method to read a XML File, parse it and return the root of the XML as ElementTree Element.
        :param xml_file: Path to XML File (with isPath = True)
        :type xml_file: str
        :rtype: lxml.ElementTree._Element
        """
        result = None
        try:
            if not os.path.isfile(xml_file):
                cls.getLog().warning("Unable to read and parse {}".format(xml_file))
            else:
                parser = et.XMLParser(remove_blank_text=True)
                result = et.parse(xml_file, parser=parser).getroot()
        except et.XMLSyntaxError as xse:
            cls.getLog().warning("{}".format(xse))
        return result

    @classmethod
    def parseXML(cls, xml_source):
        """
        Class method to read a XML String, parse it and return the root of the XML as ElementTree Element.
        :param xml_source: String with XML contents
        :type xml_source: str
        :rtype: None, lxml.ElementTree._Element
        """
        result = None
        try:
            result = et.XML(xml_source)
        except et.XMLSyntaxError as xse:
            cls.getLog().warning("{}".format(xse))
        return result

    @classmethod
    def parseSource(cls, source, isPath=True):
        """Class method to parse a XML source into an ElementTree, either a file or string, depending on the isPath
        flag. Returns the root element of the XML or None.

        :param source: Either path to a file or XML String
        :type source: str
        :param isPath:
        :type isPath: bool
        :return: ElementTree representation of the source.
        :rtype: None, lxml.ElementTree._Element
        """
        if isPath:
            result = cls.parseFile(source)
        else:
            result = cls.parseXML(source)
        return result

    def loadSource(self, source, isPath=True):
        """Reads a XML source, which maybe a file or a string, depending on the state of isPath.

        :type source: str
        :type isPath: bool
        :return: Return this instance
        :rtype: uvaXML
        """
        xml = self.parseSource(source, isPath)
        if xml is not None:
            self._xml = xml
            if isPath:
                self._path = source
            else:
                self._path = None
            self.loadSchema()
        return self

    def loadSchema(self):
        """Loads the schema associated with the XML represented by this object. It will search for the schema in the
        directory of the xml file, in the directory of this python script and in the current working directory. The
        schema and namespace are loaded into the attributes of this object.

        :return: Return this instance
        :rtype: uvaXML
        """
        schema_ns = {'u': self.extractSchemaNameSpace(self._xml)}
        schema = self.findAssociatedSchema(self._xml, self._path)
        if schema is not None:
            self._schema = schema
        if schema_ns is not None:
            self._ns = schema_ns
        return self

    def save(self, xml_path=None, encoding="utf-8", pretty_print=True):
        """Saves the xml object to file

        :return:
        :rtype:
        """
        result = False
        try:
            if xml_path is None:
                xml_path = self._path
            if xml_path is not None and os.path.exists(os.path.dirname(xml_path)):
                if xml_path != self._path:
                    self._path = xml_path
                with open(xml_path, "w") as dest:
                    dest.write(
                        et.tostring(self._xml, encoding=encoding, pretty_print=pretty_print)
                    )
                result = True
        except IOError as ioe:
            self.getLog().error("Unable to save the xml configuration file:\n{}".format(ioe))
        return result

    def reload(self):
        """Reloads the xml from file

        :return:
        :rtype:
        """
        result = False
        if self._path is not None and os.path.isfile(self._path):
            xml = self.parseFile(self._path)
            if xml is not None and self.isCompatible(self._path):
                self._xml = xml
                result = True
        return result

    @classmethod
    def findFiles(cls, paths=None, names=None, ext=""):
        """Returns a list schema files with the given names and extension in the given paths

        :param paths: A list of directories to look in.
        :param names: A list of file names to look for.
        :param ext: Extension of the file names (if not present).
        :rtype: list[str]
        """
        result = []
        if paths is None:
            paths = []
        if names is None:
            names = []
        for path in paths:
            for name in names:
                if os.path.splitext(name)[1] == "":
                    # prepend with dot if extension is given without dot
                    if ext != "" and not ext.startswith("."):
                        ext = ".{}".format(ext)
                    names = "{}{}".format(names, ext)
                # create path pattern for matching
                pattern = os.path.join(path, name)
                # search for matching files
                result.extend(glob.glob(pattern))
                # end if
            # end for
        # end for
        return result

    @classmethod
    def findSchemaFiles(cls, names=None, paths=None, extend=False):
        """Returns the schema files, found in the given paths and with the given names.

        :param names: A list of schema names to try.
        :type names: list[str]
        :param paths: A list of directories to look for the schema file, or None when using default settings
        :type paths: None, list[str]
        :param extend: Whether to extend the paths and names with default values
        :rtype: str
        """
        if paths is None:
            paths = []
        if extend or len(paths) == 0:
            paths.extend(["", ".", os.getcwd()])
        if names is None:
            names = []
        if extend or len(names) == 0:
            names.extend(["schema", "schema.xml"])
        return cls.findFiles(paths=paths, names=names, ext=".xml")

    @classmethod
    def findSchemaXML(cls, names=None, paths=None, extend=False):
        """Locates the schema file that matches any of the given names, located in any of the given paths and parses
        the XML Schema document to ElementTree.

        If multiple schema files were found, parse and return the first.

        :param names: Name of the schema file to look for.
        :type names: None or list[str]
        :param paths: Paths to the directories to look in.
        :type paths: None or list[str]
        :param extend: Whether to extend the name and paths with default values
        :type extend: bool
        :rtype: lxml.ElementTree._Element
        """
        result = None
        schema_paths = cls.findSchemaFiles(names=names, paths=paths, extend=extend)
        if len(schema_paths) > 0:
            result = et.parse(schema_paths[0]).getroot()
        return result

    @classmethod
    def findSchema(cls, names=None, paths=None, extend=False):
        """Locates the schema file that matches any of the given names, located in any of the given paths and parses
        the XML Schema document to XML Schema Document"""
        result = None
        schema_xml = cls.findSchemaXML(names=names, paths=paths, extend=extend)
        if schema_xml is not None:
            result = et.XMLSchema(schema_xml)
        return result

    @classmethod
    def findAssociatedSchemaXML(cls, xml, paths=None, extend=False):
        """Returns the XML Schema document as a parsed ElementTree"""
        result = None
        schema_name = cls.extractSchemaName(xml)
        if schema_name is not None:
            schema_files = cls.findSchemaFiles(names=[schema_name], paths=paths, extend=extend)
            if len(schema_files) > 0:
                result = et.parse(schema_files[0]).getroot()
        return result

    @classmethod
    def findAssociatedSchema(cls, xml, paths=None, extend=False):
        """Returns the XML Schema document as a parsed Schema Document"""
        result = None
        schema_xml = cls.findAssociatedSchemaXML(xml=xml, paths=paths, extend=extend)
        if schema_xml is not None:
            result = et.XMLSchema(schema_xml)
        return result

    @classmethod
    def extractSchemaInfo(cls, xml):
        """Returns the found schema information from the XML Document"""
        result = None
        schema_location = xml.xpath("./@xsi:schemaLocation", namespaces={'xsi': cls.XSI})
        if len(schema_location) > 0:
            result = schema_location[0]
        return result

    @classmethod
    def extractSchemaNameSpace(cls, xml):
        """Returns the namespace of the schema as defined in the given XML Document"""
        result = None
        schema_info = cls.extractSchemaInfo(xml)
        if schema_info is not None:
            result = schema_info.strip().split()[0]
        return result

    @classmethod
    def extractSchemaName(cls, xml):
        """Returns the location of the schema file as defined in the given XML Document"""
        result = None
        schema_info = cls.extractSchemaInfo(xml)
        if schema_info is not None:
            result = schema_info.strip().split()[1]
        return result

    @classmethod
    def assertValidateXML(cls, xml, xml_dir=None):
        """
        Validate XML with its schema (if present). Throws an DocumentInvalid Exception when the schema rejects the XML.
        :param xml: Root of the XML
        :type xml: lxml.ElementTree.Element
        :param xml_dir: Path to the directory of the xml file. This is used to find the schema.
        :type xml_dir: None, str
        :rtype: bool
        """
        result = True
        schema = cls.findSchema(xml, xml_dir)
        ns = cls.extractSchemaNameSpace(xml)
        if None not in [schema, ns]:
            schema.assertValid(xml)
        return result

    @classmethod
    def validateXML(cls, xml, xml_dir=None):
        """
        Validate the XML with its schema (if present). Returns False if an Exception occurs otherwise it will return
        True. It will also return True when there is no Schema to validate against.
        :type xml: lxml.ElementTree.Element
        :type xml_dir: None, str
        :rtype: bool
        """
        try:
            result = cls.assertValidateXML(xml, xml_dir)
        except et.DocumentInvalid as di:
            cls.getLog().error("Document does not comply to schema:\n{}".format(di))
            result = False
        except Exception as e:
            cls.getLog().critical("Unexpected error:\n{}".format(e))
            result = False
        return result

    @classmethod
    def isValid(cls, source, isPath=True, schema=None, config_dir=None):
        """Validates the source by checking whether the source can be checked with the found schema file and whether
        the XML Source is accepted by the schema file.

        Will NOT return whether the used schema file is supported by this version of pyCultivator, use isSupported
        for this.

        :param source: XML or path to XML file
        :type source: str
        :param isPath: Flags whether the source is a file
        :type isPath: bool
        :param schema: The XML Schema used to validate the XML Source
        :type schema: lxml.ElementTree._Element
        :rtype: bool
        """
        result = False
        try:
            xml = cls.parseSource(source, isPath)
            if isPath and config_dir is None:
                config_dir = os.path.dirname(source)
            if schema is None and config_dir is not None:
                schema = cls.findAssociatedSchemaXML(xml, paths=[config_dir])
            if schema is not None:
                # read xml schema version
                xml_schema_version = xml.get('schema_version')
                # read schema version
                schema_version = schema.get("version")
                # test if schema versons are equal and validate
                schema_doc = et.XMLSchema(schema)
                if schema_doc is not None:
                    result = xml_schema_version == schema_version and schema_doc.validate(xml)
        except Exception as e:
            cls.getLog().critical("Unexpected error: {}".format(e))
        return result

    @classmethod
    def isSupported(cls, source, isPath=True):
        """Returns whether the schema version of this XML source is supported by this pyCultivator version.

        Will NOT return whether the XML source complies with the schema file, use isValid for this.

        :param source: Path to XML file or string containing the xml
        :type source: str
        :param isPath: Whether the given source is a path
        :type isPath: bool
        :return: Whether this XML source is supported by this pyCultivator version.
        """
        result, xml_schema_version = (False, None)
        try:
            xml = cls.parseSource(source, isPath)
            xml_schema_version = xml.get('schema_version')
            result = cls.checkVersion(xml_schema_version)
        except Exception as e:
            cls.getLog().critical("Unexpected error: {}".format(e))
        return result

    @classmethod
    def checkVersion(cls, version):
        result = False
        if version in cls.COMPATIBLE_VERSIONS.keys():
            state = cls.COMPATIBLE_VERSIONS[version]
            if state == 1:
                cls.getLog().warning("OLD XML Schema VERSION: please UPDATE your CONFIG FILE!")
            result = state > 0
        return result

    @classmethod
    def isCompatible(cls, source, isPath=True, config_dir=None):
        """ Will determine if the schema version used by this XML source is supported by this version of pyCultivator
        and whether the XML file complies with the found schema.

        :param source: Path to XML file or String containing XML
        :type source: str
        :param isPath: Whether the source is a path
        :type isPath: bool
        :param config_dir: Path to the directory of the xml file.
        :type config_dir: None, str
        :return: Return the result
        :rtype: bool
        """
        return cls.isSupported(source=source, isPath=isPath) and \
               cls.isValid(source=source, isPath=isPath, config_dir=config_dir)

    def getNameSpace(self):
        """
        Return the namespace of the XML elements. By default the namespace is a dict with the key "u" and value None.
        Will try to load the namespace if the current namespace is None.
        :rtype: dict
        """
        if self._ns is None:
            self.loadSchema()
        return self._ns

    def getSchema(self):
        if self._schema is None:
            self.loadSchema()
        return self._schema

    def hasSchema(self):
        return self.getSchema() is not None

    def getXML(self):
        """
        Return the ElementTree representation of the XML
        :rtype: lxml.ElementTree._Element
        """
        return self._xml

    def getXMLString(self):
        """
        Return the String representation of the XML
        :rtype: str
        """
        return et.tostring(self._xml)

    def getXMLPath(self):
        result = self._xml
        if result is None:
            result = os.path.join(os.getcwd(), "configuration.xml")
        return result

    def getXMLDir(self):
        return os.path.split(self.getXMLPath())[0]

    def getRoot(self):
        """
        :return:
        :rtype: lxml.ElementTree._Element
        """
        return self._xml

    def getElementByTag(self, element, root=None):
        if root is None:
            root = self.getRoot()
        return root.find(element)

    def getElementsByName(self, name):
        """
        Return all elements with an attributed named "name".
        :param name:
        :type name: str
        :return: A list of elements that have an attribute name and its value equals "name".
        :rtype: list
        """
        return self._xml.findall(".//u:*[@name='{}']".format(name), self._ns)

    def getAttribute(self, element, name, default=None):
        """
        Return the value of the attribute named "name" of the element "element"
        :type element: lxml.ElementTree._Element
        :type name: str
        :rtype: None, str
        """
        v = default
        if element is not None:
            v = element.get(name)
        return v

    def getValue(self, element):
        """
        Find the config item and return the value of the attribute named "value"
        :param element:
        :type element: lxml.ElementTree._Element
        :return:
        :rtype: None, str
        """
        return self.getAttribute(element, "value")

    def createElement(self, tag, attributes=None, prefix=None, namespace=None):
        """Create a new element (without parent)"""
        nsmap = None
        if namespace is None:
            namespace = self.getNameSpace()
        if namespace not in (None, ""):
            nsmap = {prefix: namespace}
            tag = et.QName(nsmap[prefix], tag)
        return et.Element(tag, attributes, nsmap=nsmap)

    def addElement(self, root, tag, attributes=None, prefix=None, namespace=None, position=None):
        """Add a new element to the root element

        :param root: Root element to add the new element to
        :type root: lxml.etree._Element._Element
        :param tag: Tag of the new element
        :type tag: str
        :param attributes: Attributes of the element
        :type attributes: dict[str, object]
        :param namespace: Namespace of the element
        :param position: Index of position to place element in root
        :return: New element as direct child of root
        """
        nsmap = None
        if namespace is None:
            namespace = self.getNameSpace().values()[0]
        if namespace not in (None, ""):
            nsmap = {prefix: namespace}
            tag = et.QName(nsmap[prefix], tag)
        if position is None:
            result = et.SubElement(root, tag, attributes, nsmap=nsmap)
        else:
            result = et.Element(tag, attrib=attributes, nsmap=nsmap)
            root.insert(position, result)
        return result