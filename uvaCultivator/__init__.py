# coding=utf-8
"""
Initialize stub for uvaCultivator package
"""

import uvaLog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# connect to logger
uvaLog.getLogger(__name__).debug("Connected {} module to logger".format(__name__))
