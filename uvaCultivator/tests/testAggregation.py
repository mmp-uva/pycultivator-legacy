# coding=utf-8
"""
Test module for the aggregation module
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import unittest
from uvaCultivator.uvaAggregation import *


class abstractAggregationTest(uvaObject, unittest.TestCase):

    def __init__(self, methodName='runTest'):
        uvaObject.__init__(self, settings=None)
        self.getLog().addStreamHandler()
        unittest.TestCase.__init__(self, methodName=methodName)

    @classmethod
    def setUpClass(cls):
        """
        Prepares the test class to test on orient database
        :param cls:
        :type cls:
        :return:
        :rtype:
        """
        cls._subject_class = AbstractAggregation

    def test_getAggregationMapping(self):
        self.assertTrue("nothing" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("same" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("first" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("last" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("min" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("max" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("mean" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("median" in self._subject_class.getAggregationMapping().keys())
        self.assertTrue("mode" in self._subject_class.getAggregationMapping().keys())

    def test_hasAggregationMethod(self):
        self.assertTrue(self._subject_class.hasAggregationMethod("nothing"))
        self.assertTrue(self._subject_class.hasAggregationMethod("NoTHing"))
        self.assertFalse(self._subject_class.hasAggregationMethod("foo"))
        self.assertFalse(self._subject_class.hasAggregationMethod("FOO"))

    def test_getAggregationMethod(self):
        self.assertEqual(self._subject_class.getAggregationMethod("nothing"), self._subject_class.aggregateToNothing)
        self.assertEqual(self._subject_class.getAggregationMethod("same"), self._subject_class.aggregateToSame)
        self.assertEqual(self._subject_class.getAggregationMethod("first"), self._subject_class.aggregateToFirst)
        self.assertEqual(self._subject_class.getAggregationMethod("last"), self._subject_class.aggregateToLast)
        self.assertEqual(self._subject_class.getAggregationMethod("min"), self._subject_class.aggregateToMinimum)
        self.assertEqual(self._subject_class.getAggregationMethod("max"), self._subject_class.aggregateToMaximum)
        self.assertEqual(self._subject_class.getAggregationMethod("mean"), self._subject_class.aggregateToMean)
        self.assertEqual(self._subject_class.getAggregationMethod("median"), self._subject_class.aggregateToMedian)
        self.assertEqual(self._subject_class.getAggregationMethod("mode"), self._subject_class.aggregateToMode)

    def test_aggregate(self):
        self.assertIsNone(self._subject_class.aggregate("nothing", [1,2,3]))
        self.assertEqual(self._subject_class.aggregate("same", [1,2,3]), [1,2,3])
        self.assertEqual(self._subject_class.aggregate("first", [1, 2, 3]), 1)
        self.assertEqual(self._subject_class.aggregate("last", [1, 2, 3]), 3)

    def test_aggregateToNothing(self):
        self.assertIsNone(self._subject_class.aggregateToNothing([]))
        self.assertIsNone(self._subject_class.aggregateToNothing([1,2,3]))

    def test_aggregateToSame(self):
        self.assertEqual(self._subject_class.aggregateToSame(1), 1)
        self.assertEqual(self._subject_class.aggregateToSame([]), [])
        self.assertEqual(self._subject_class.aggregateToSame([1, 2, 3]), [1, 2, 3])

    def test_aggregateToFirst(self):
        self.assertEqual(self._subject_class.aggregateToFirst([1, 2, 3]), 1)
        self.assertEqual(self._subject_class.aggregateToFirst([3]), 3)
        self.assertIsNone(self._subject_class.aggregateToFirst([]))

    def test_aggregateToLast(self):
        self.assertEqual(self._subject_class.aggregateToLast([1, 2, 3]), 3)
        self.assertEqual(self._subject_class.aggregateToLast([2]), 2)
        self.assertIsNone(self._subject_class.aggregateToLast([]))

    def test_aggregateToLowest(self):
        # Abstract method; no implementation so do not test
        pass

    def test_aggregateToHighest(self):
        # Abstract method; no implementation so do not test
        pass

    def test_aggregateToMean(self):
        # Abstract method; no implementation so do not test
        pass

    def test_aggregateToMedian(self):
        # Abstract method; no implementation so do not test
        pass

    def test_aggregateToMode(self):
        # Abstract method; no implementation so do not test
        pass

class floatAggregationTest(abstractAggregationTest):

    @classmethod
    def setUpClass(cls):
        cls._subject_class = FloatAggregation

    def test_aggregateToFirst(self):
        self.assertEqual(self._subject_class.aggregateToFirst([1, 2, 3]), 1.0)
        self.assertEqual(self._subject_class.aggregateToFirst([3]), 3.0)
        self.assertEqual(self._subject_class.aggregateToFirst([]), 0.0)

    def test_aggregateToLast(self):
        self.assertEqual(self._subject_class.aggregateToLast([1, 2, 3]), 3.0)
        self.assertEqual(self._subject_class.aggregateToLast([2]), 2.0)
        self.assertEqual(self._subject_class.aggregateToLast([]), 0.0)

    def test_aggregateToLowest(self):
        self.assertEqual(self._subject_class.aggregateToMinimum([1, 2, 3]), 1.0)
        self.assertEqual(self._subject_class.aggregateToMinimum([1, 2, 3, 1]), 1.0)
        self.assertEqual(self._subject_class.aggregateToMinimum([2]), 2.0)
        self.assertEqual(self._subject_class.aggregateToMinimum([]), 0.0)

    def test_aggregateToHighest(self):
        self.assertEqual(self._subject_class.aggregateToMaximum([1, 2, 3]), 3.0)
        self.assertEqual(self._subject_class.aggregateToMaximum([1, 2, 3, 3]), 3.0)
        self.assertEqual(self._subject_class.aggregateToMaximum([2]), 2.0)
        self.assertEqual(self._subject_class.aggregateToMaximum([]), 0.0)

    def test_aggregateToMean(self):
        values = [1, 2, 3, 4]  # average = 2.5
        self.assertEqual(self._subject_class.aggregateToMean(values), 2.5)
        values = [1]  # average = 1
        self.assertEqual(self._subject_class.aggregateToMean(values), 1)
        values = []  # average = 0
        self.assertEqual(self._subject_class.aggregateToMean(values), 0.0)

    def test_aggregateToMedian(self):
        values = [1, 2, 3]  # median = 2
        self.assertEqual(self._subject_class.aggregateToMedian(values), 2.0)
        values = [1, 2, 3, 4]  # median = 2.5
        self.assertEqual(self._subject_class.aggregateToMedian(values), 2.5)
        values = [1]
        self.assertEqual(self._subject_class.aggregateToMedian(values), 1.0)
        values = []
        self.assertEqual(self._subject_class.aggregateToMedian(values), 0.0)

    def test_aggregateToMode(self):
        values = [1,1,2]    # mode = 1
        self.assertEqual(self._subject_class.aggregateToMode(values), 1.0)
        values = [1]
        self.assertEqual(self._subject_class.aggregateToMode(values), 1.0)
        values = [1, 1, 2, 2, 3]    # mode = 1
        self.assertEqual(self._subject_class.aggregateToMode(values), 1.0)
        values = []
        self.assertEqual(self._subject_class.aggregateToMode(values), 0.0)


class readingAggregationTest(abstractAggregationTest):

    @classmethod
    def setUpClass(cls):
        cls._subject_class = ReadingAggregation

    def test_aggregateToFirst(self):
        readings = [(0, 0, 0), (1, 1, 1)]
        self.assertEqual(self._subject_class.aggregateToFirst(readings), (0, 0, 0))
        readings = [(0.0, 0.0, 0.0)]
        self.assertEqual(self._subject_class.aggregateToFirst(readings), (0.0, 0.0, 0.0))
        readings = []
        self.assertEqual(self._subject_class.aggregateToFirst(readings), [0.0, 0.0, 0.0])

    def test_aggregateToLast(self):
        readings = [(0,0,0), (1,1,1)]
        self.assertEqual(self._subject_class.aggregateToLast(readings), (1, 1, 1))
        readings = [(0.0, 0.0, 0.0)]
        self.assertEqual(self._subject_class.aggregateToLast(readings), (0.0, 0.0, 0.0))
        readings = []
        self.assertEqual(self._subject_class.aggregateToLast(readings), [0.0, 0.0, 0.0])

    def test_aggregateToHighest(self):
        readings = [(0, 0, 0), (1, 1, 1)]
        self.assertEqual(self._subject_class.aggregateToMaximum(readings), (1, 1, 1))
        readings = [(2, 2, 2), (2, 3, 1), (3, 2, 4)]
        self.assertEqual(self._subject_class.aggregateToMaximum(readings), (3, 2, 4))
        readings = [(0.0, 0.0, 0.0)]
        self.assertEqual(self._subject_class.aggregateToMaximum(readings), (0.0, 0.0, 0.0))
        readings = []
        self.assertEqual(self._subject_class.aggregateToMaximum(readings), [0.0, 0.0, 0.0])

    def test_aggregateToLowest(self):
        readings = [(0, 0, 0), (1, 1, 1)]
        self.assertEqual(self._subject_class.aggregateToMinimum(readings), (0, 0, 0))
        readings = [(2, 2, 2), (2, 3, 1), (3, 2, 1)]
        self.assertEqual(self._subject_class.aggregateToMinimum(readings), (2, 3, 1))
        readings = [(0.0, 0.0, 0.0)]
        self.assertEqual(self._subject_class.aggregateToMinimum(readings), (0.0, 0.0, 0.0))
        readings = []
        self.assertEqual(self._subject_class.aggregateToMinimum(readings), [0.0, 0.0, 0.0])

    def test_aggregateToMean(self):
        readings = [(1, 1, 1), (2, 2, 2), (3, 3, 3)]
        self.assertEqual(self._subject_class.aggregateToMean(readings), [2.0, 2.0, 2.0])
        readings = [(1, 2, 3), (2, 3, 4), (3, 4, 5)]
        self.assertEqual(self._subject_class.aggregateToMean(readings), [2.0, 3.0, 4.0])
        readings = [(1, 2, 3)]
        self.assertEqual(self._subject_class.aggregateToMean(readings), [1.0, 2.0, 3.0])
        readings = []
        self.assertEqual(self._subject_class.aggregateToMean(readings), [0.0, 0.0, 0.0])

    def test_aggregateToMedian(self):
        readings = [(1, 1, 1), (2, 2, 2), (3, 3, 3)]
        self.assertEqual(self._subject_class.aggregateToMedian(readings), [2.0, 2.0, 2.0])
        readings = [(1, 2, 3), (2, 3, 4), (3, 4, 5), (4, 5, 6)]
        self.assertEqual(self._subject_class.aggregateToMedian(readings), [2.5, 3.5, 4.5])
        readings = [(1, 2, 3)]
        self.assertEqual(self._subject_class.aggregateToMedian(readings), [1.0, 2.0, 3.0])
        readings = []
        self.assertEqual(self._subject_class.aggregateToMedian(readings), [0.0, 0.0, 0.0])

    def test_aggregateToMode(self):
        readings = [(1, 1, 1), (1, 1, 1), (2, 2, 2)]
        self.assertEqual(self._subject_class.aggregateToMode(readings), [1.0, 1.0, 1.0])
        readings = [(1, 2, 3)]
        self.assertEqual(self._subject_class.aggregateToMode(readings), [1.0, 2.0, 3.0])
        readings = [(1, 1, 1), (2, 2, 2), (3, 3, 3), (1, 1, 1), (2, 2, 2)]
        self.assertEqual(self._subject_class.aggregateToMode(readings), [1.0, 1.0, 1.0])
        readings = []
        self.assertEqual(self._subject_class.aggregateToMode(readings), [0.0, 0.0, 0.0])
