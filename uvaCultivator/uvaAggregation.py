# coding=utf-8
"""
This module provides class that deal with the aggregation of data, such as floats or OD Readings (lists of 3 floats).
The  AbstractAggregation class provides the basis and should be extended by classes that provide aggregation for
specific data types.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from pycultivator_legacy.core import BaseObject


class AbstractAggregation(BaseObject):
    """
    Class that defines the basis for data aggregation.
    """

    AGGREGATE_NOTHING = "nothing"
    AGGREGATE_SAME = "same"
    AGGREGATE_FIRST = "first"
    AGGREGATE_LAST = "last"
    AGGREGATE_MINIMUM = "min"
    AGGREGATE_MAXIMUM = "max"
    AGGREGATE_AVERAGE = "mean"
    AGGREGATE_MEDIAN = "median"
    AGGREGATE_MODE = "mode"
    AGGREGATE_DEFAULT = AGGREGATE_SAME

    @classmethod
    def getAggregationMapping(cls):
        """Returns the mapping between aggregation keywords and aggregation methods.

        :return: Dictionary with the mapping
        :rtype: dict
        """
        return {
            cls.AGGREGATE_NOTHING: cls.aggregateToNothing,
            cls.AGGREGATE_SAME: cls.aggregateToSame,
            cls.AGGREGATE_FIRST: cls.aggregateToFirst,
            cls.AGGREGATE_LAST: cls.aggregateToLast,
            cls.AGGREGATE_MINIMUM: cls.aggregateToMinimum,
            cls.AGGREGATE_MAXIMUM: cls.aggregateToMaximum,
            cls.AGGREGATE_AVERAGE: cls.aggregateToMean,
            cls.AGGREGATE_MEDIAN: cls.aggregateToMedian,
            cls.AGGREGATE_MODE: cls.aggregateToMode,
        }

    @classmethod
    def hasAggregationMethod(cls, name):
        return name.lower() in cls.getAggregationMapping().keys()

    @classmethod
    def getAggregationMethod(cls, name):
        """
        Returns the method name mapped to the aggregation name, if present. Otherwise return the aggregation to same
        method.
        :param name: Name of the aggregation method, present in
        :type name: str
        :return: Function reference
        :rtype: func
        """
        result = cls.aggregateToSame
        if cls.hasAggregationMethod(name=name):
            mapping = cls.getAggregationMapping()
            result = mapping[name]
        return result

    @classmethod
    def aggregate(cls, method, values):
        """
        Aggregates a list of values to one value by using the given method.
        :type method: str
        :type values: list[object]
        :return: Aggregation result
        :rtype: object
        """
        result = None
        if len(values) > 0:
            if cls.hasAggregationMethod(name=method):
                result = cls.getAggregationMethod(name=method)(values)
        return result

    @staticmethod
    def aggregateToNothing(values):
        """
        Aggregate to nothing; i.e. will return None
        :param values: Values to aggregate
        :type values: list[object]
        :return: Return nothing
        :rtype: None
        """
        return None

    @staticmethod
    def aggregateToSame(values):
        """
        Aggregate to same; i.e. return what is given
        :param values: Values to aggregate
        :type values: list[object]
        :return: Return values
        :rtype: list[object]
        """
        return values

    @classmethod
    def aggregateToFirst(cls, values):
        """
        Aggregate to first
        :param values:
        :type values: list[object]
        :return:
        :rtype: object or None
        """
        result = None
        if len(values) > 0:
            result = values[0]
        return result

    @classmethod
    def aggregateToLast(cls, values):
        """
        Aggregate to last
        :type values: list[object]
        :return:
        :rtype: object or None
        """
        result = None
        if len(values) > 0:
            result = values[-1]
        return result

    def aggregateToMinimum(cls, values):
        """
        Aggregate to lowest
        :param values: list of objects
        :type values: list[object]
        :return:
        :rtype: object
        """
        raise NotImplementedError

    def aggregateToMaximum(cls, values):
        """
        Aggregate to highest
        :param values:
        :type values: list[object]
        :return:
        :rtype: object
        """
        raise NotImplementedError

    def aggregateToMean(cls, values):
        """
        Aggregate OD readings by calculating the average.
        :type values: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        raise NotImplementedError

    def aggregateToMedian(cls, values):
        """
        Aggregate OD readings by calculating the median.
        :type values: list[object]
        :return:
        :rtype: list[object]
        """
        raise NotImplementedError

    def aggregateToMode(cls, values):
        """
        Aggregate a list of values by finding the most common value
        :type values: list[object]
        :return:
        :rtype: object
        """
        raise NotImplementedError


class FloatAggregation(AbstractAggregation):
    """
    Class implementing the aggregation of floats
    """

    @classmethod
    def aggregateToFirst(cls, values):
        """
        Aggregate a list of floats by returning the first value in the list
        :param values:
        :type values:
        :return:
        :rtype:
        """
        result = AbstractAggregation.aggregateToFirst(values)
        if result is None:
            result = 0.0
        return result

    @classmethod
    def aggregateToLast(cls, values):
        """
        Aggregate a list of floats by return the last value in the list
        :param values:
        :type values:
        :return:
        :rtype:
        """
        result = AbstractAggregation.aggregateToLast(values)
        if result is None:
            result = 0.0
        return float(result)

    @classmethod
    def aggregateToMinimum(cls, values):
        """
        Aggregate a list of floats by returning the lowest value from the list
        :param values:
        :type values: list[float]
        :return:
        :rtype: float
        """
        result = 0.0
        if len(values) > 0:
            result = float(sorted(values)[0])
        return result

    @classmethod
    def aggregateToMaximum(cls, values):
        """
        Aggregate a list of floats by return the highest value in the list
        :param values:
        :type values: list[float]
        :return:
        :rtype: float
        """
        result = 0.0
        if len(values) > 0:
            result = float(sorted(values, reverse=True)[0])
        return result

    @classmethod
    def aggregateToMean(cls, values):
        """
        Aggregates a list of floats by calculating the average.
        :param values:
        :type values: list[float]
        :return:
        :rtype: float
        """
        result = 0.0
        if len(values) > 0:
            v_sum = sum(values)
            result = float(v_sum) / len(values)
        return result

    @classmethod
    def aggregateToMedian(cls, values):
        """Aggregates a list of floats by calculating the median.

        :type values: list[object]
        :return:
        :rtype: float
        """
        result = 0.0
        if len(values) > 0:
            v_order = sorted(values)
            l_values = len(values)
            if l_values % 2 == 0:
                # even: 2, 4, 6 .. take middle pair i.e. 0,1 , 1,2 , 2,3 ..
                result = float(v_order[(l_values / 2) - 1] + v_order[l_values / 2]) / 2
            else:
                # odd: 1, 3, 5 .. take middle value i.e. 0, 1, 2 ..
                result = float(v_order[l_values / 2])
        return result

    @classmethod
    def aggregateToMiddle(cls, values, lower=False):
        """Aggregates a list of floats to the middle value

        :param values: List of floats
        :param lower: Whether to pick to lower value on a tie, or pick the higher value

        :return: the middle value
        """
        result = 0.0
        if len(values) > 0:
            v_order = sorted(values)
            l_values = len(values)
            if l_values % 2 == 0:
                # even: 2, 4, 6 .. take middle pair i.e. 0,1 , 1,2 , 2,3 ..
                result = float(v_order[(l_values / 2) + 0 if lower else 1])
            else:
                # odd: 1, 3, 5 .. take middle value i.e. 0, 1, 2 ..
                result = float(v_order[l_values / 2])
        return result


    @classmethod
    def aggregateToMode(cls, values):
        """
        Returns the most common value in the given list of floats. If there is more than one such value,
        only the first (lowest) is returned (after scipy implementation).
        :param values:
        :type values: list[float]
        :return:
        :rtype: float
        """
        result = 0.0
        # store the values and their occurences
        bins = {}
        for v in values:
            if v in bins.keys():
                bins[v] += 1
            else:
                bins[v] = 1
        if len(bins.keys()) > 0:
            # use double sorting, return the lowest
            result = float(sorted(sorted(bins.keys(), key=lambda x: bins[x], reverse=True))[0])
        return result


class ReadingAggregation(FloatAggregation):
    """
    Class implementing the aggregation of readings (lists of 3 floats)
    """

    # overwrite aggregation default
    AGGREGATE_DEFAULT = AbstractAggregation.AGGREGATE_MINIMUM

    @classmethod
    def aggregateToLast(cls, readings):
        """
        Aggregate OD Readings by returning the last reading in the list
        :type readings: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        result = AbstractAggregation.aggregateToLast(readings)
        if result is None:
            result = [0.0, 0.0, 0.0]
        return result

    @classmethod
    def aggregateToFirst(cls, readings):
        """
        Aggregate OD Readings by return the first reading in the list
        :type readings: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        result = AbstractAggregation.aggregateToFirst(readings)
        if result is None:
            result = [0.0, 0.0, 0.0]
        return result

    @classmethod
    def aggregateToMaximum(cls, readings):
        """
        Aggregate OD Readings by returning the lowest.
        :param readings: List of tuples with flash, background, od raw
        :type readings: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        result = [0.0, 0.0, 0.0]
        if len(readings) > 0:
            result = sorted(readings, key=lambda x: x[2], reverse=True)[0]
        return result

    @classmethod
    def aggregateToMinimum(cls, readings):
        """
        Aggregate OD Readings by returning the lowest.
        :param readings: List of tuples with flash, background, od raw
        :type readings: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        result = [0.0, 0.0, 0.0]
        if len(readings) > 0:
            result = sorted(readings, key=lambda x: x[2])[0]
        return result

    @classmethod
    def aggregateToMean(cls, readings):
        """
        Aggregate OD readings by calculating the average.
        :type readings: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        result = [0.0, 0.0, 0.0]
        if len(readings) > 0:
            for v in range(3):
                v_sum = 0
                for r in readings:
                    v_sum += r[v]
                result[v] = float(v_sum) / len(readings)
        return result

    @classmethod
    def aggregateToMedian(cls, readings):
        """
        Aggregate OD readings by calculating the median.
        :type readings: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        result = [0.0, 0.0, 0.0]
        if len(readings) > 0:
            for v in range(3):
                values = []
                for r in readings:
                    values.append(r[v])
                result[v] = FloatAggregation.aggregateToMedian(values)
        return result

    @classmethod
    def aggregateToMode(cls, readings):
        """
        Aggregate OD readings by calculating the median.
        :type readings: list[tuple[float, float, float]]
        :return:
        :rtype: list[float]
        """
        result = [0.0, 0.0, 0.0]
        if len(readings) > 0:
            for v in range(3):
                values = []
                for r in readings:
                    values.append(r[v])
                result[v] = FloatAggregation.aggregateToMode(values)
        return result


class ODMeasurementAggregation(AbstractAggregation):
    """Aggregates"""

    # overwrite aggregation default
    AGGREGATE_DEFAULT = AbstractAggregation.AGGREGATE_MINIMUM

    @classmethod
    def aggregate(cls, method, values):
        """
        Aggregates a list of values to one value by using the given method.
        :type method: str
        :type values: list[object]
        :return: Aggregation result
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        return super(ODMeasurementAggregation, cls).aggregate(method, values)

    @classmethod
    def aggregateToLast(cls, measurements):
        """Aggregate OD Measurements by returning the last reading in the list
        
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        result = AbstractAggregation.aggregateToLast(measurements)
        if result is None:
            result = [0.0, 0.0, 0.0]
        return result

    @classmethod
    def aggregateToFirst(cls, measurements):
        """Aggregate OD Measurements by return the first reading in the list
        
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        result = AbstractAggregation.aggregateToFirst(measurements)
        if result is None:
            result = [0.0, 0.0, 0.0]
        return result

    @classmethod
    def aggregateToMaximum(cls, measurements):
        """Aggregate OD Measurements by returning the lowest.
        
        :param measurements: List of OD Measurements
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        result = [0.0, 0.0, 0.0]
        if len(measurements) > 0:
            def f(x): x.getCalibratedODValue()
            result = sorted(measurements, key=f, reverse=True)[0]
        return result

    @classmethod
    def aggregateToMinimum(cls, measurements):
        """Aggregate OD Measurements by returning the lowest.
        
        :param measurements: List of OD Measurements
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        result = [0.0, 0.0, 0.0]
        if len(measurements) > 0:
            def f(x): x.getCalibratedODValue()
            result = sorted(measurements, key=f)[0]
        return result

    @classmethod
    def aggregateToMean(cls, measurements):
        """Aggregate OD Measurements by calculating the average.
        
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        from uvaODMeasurement import ODMeasurement
        result = None
        if len(measurements) > 0:
            values = cls.listify([m.getAttribute("id") for m in measurements])
            identifier = FloatAggregation.aggregateToMiddle(values)
            # aggregate od_raw
            values = cls.listify([m.getODRaw() for m in measurements])
            od_raw = FloatAggregation.aggregateToMean(values)
            # aggregate od_value
            values = cls.listify([m.getCalibratedODValue() for m in measurements])
            od_value = FloatAggregation.aggregateToMean(values)
            # aggregate intensity
            values = cls.listify([m.getIntensity() for m in measurements])
            intensity = FloatAggregation.aggregateToMean(values)
            # aggregate time
            time = measurements[-1].getTimePoint()
            # aggregate flash
            values = cls.listify([m.getFlash() for m in measurements])
            flash = FloatAggregation.aggregateToMean(values)
            # aggregate background
            values = cls.listify([m.getBackground() for m in measurements])
            background = FloatAggregation.aggregateToMean(values)
            # aggregate temperature
            values = cls.listify([m.getTemperature() for m in measurements])
            temperature = FloatAggregation.aggregateToMean(values)
            result = ODMeasurement(
                od_raw=od_raw, od_value=od_value, intensity=intensity, time=time, flash=flash,
                background=background, temperature=temperature, id=identifier
            )
        return result

    @classmethod
    def aggregateToMedian(cls, measurements):
        """Aggregate OD readings by calculating the median.
        
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        from uvaODMeasurement import ODMeasurement
        result = None
        if len(measurements) > 0:
            values = cls.listify([m.getAttribute("id") for m in measurements])
            identifier = FloatAggregation.aggregateToMiddle(values)
            # aggregate od_raw
            values = cls.listify([m.getODRaw() for m in measurements ])
            od_raw = FloatAggregation.aggregateToMedian(values)
            # aggregate od_value
            values = cls.listify([m.getODValue() for m in measurements])
            od_value = FloatAggregation.aggregateToMedian(values)
            # aggregate intensity
            values = cls.listify([m.getIntensity() for m in measurements])
            intensity = FloatAggregation.aggregateToMedian(values)
            # aggregate time
            time = measurements[-1].getTimePoint()
            # aggregate flash
            values = cls.listify([m.getFlash() for m in measurements])
            flash = FloatAggregation.aggregateToMedian(values)
            # aggregate background
            values = cls.listify([m.getBackground() for m in measurements])
            background = FloatAggregation.aggregateToMedian(values)        
            # aggregate temperature
            values = cls.listify([m.getTemperature() for m in measurements])
            temperature = FloatAggregation.aggregateToMedian(values)        
            result = ODMeasurement(
                od_raw=od_raw, od_value=od_value, intensity=intensity, time=time, flash=flash,
                background=background, temperature=temperature, id=identifier
            )
        return result

    @classmethod
    def aggregateToMode(cls, measurements):
        """Aggregate OD Measurements by calculating the median.
        
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        from uvaODMeasurement import ODMeasurement
        result = None
        if len(measurements) > 0:
            values = cls.listify([m.getAttribute("id") for m in measurements])
            identifier = FloatAggregation.aggregateToMiddle(values)
            # aggregate od_raw
            values = [m.getODRaw() for m in measurements]
            od_raw = FloatAggregation.aggregateToMedian(values)
            # aggregate od_value
            values = [m.getODValue() for m in measurements]
            od_value = FloatAggregation.aggregateToMedian(values)
            # aggregate intensity
            values = [m.getIntensity() for m in measurements]
            intensity = FloatAggregation.aggregateToMedian(values)
            # aggregate time
            time = measurements[-1].getTimePoint()
            # aggregate flash
            values = [m.getFlash() for m in measurements]
            flash = FloatAggregation.aggregateToMedian(values)
            # aggregate background
            values = [m.getBackground() for m in measurements]
            background = FloatAggregation.aggregateToMedian(values)
            # aggregate temperature
            values = [m.getTemperature() for m in measurements]
            temperature = FloatAggregation.aggregateToMedian(values)
            result = ODMeasurement(
                od_raw=od_raw, od_value=od_value, intensity=intensity, time=time, flash=flash,
                background=background, temperature=temperature, id=identifier
            )
        return result

    @classmethod
    def listify(cls, values, remove_None=True):
        if not isinstance(values, list):
            values = [values]
        results = []
        for value in values:
            if not remove_None or value is not None:
                results.append(value)
        return results