# coding=utf-8
"""
Basic module layer that facilitates the translation of commands to packets and also keeps track of the protocol
"""

from pycultivator_legacy.core import BaseObject
from uvaODMeasurement import ODMeasurement as oDM
from uvaAggregation import ReadingAggregation
from datetime import datetime as dt
import copy

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Cultivator(BaseObject):
    """
    A class implementing the basic functionality of running a cultivator
    """

    _namespace = "cultivator"
    _default_settings = BaseObject.mergeDefaultSettings(
        {
            "load_channels": True,
            "channel_count": 8,
            "od_leds": [720],
            "od_repeats": 3,
            "od_samples": 1,
            "max_sample_fails": 3,
            "aggregation_method": ReadingAggregation.AGGREGATE_DEFAULT,
            "sample_wait": 0,
            "gas_pause": 0,
            "solver": None
        }, namespace=_namespace
    )

    def __init__(self, serial, settings=None):
        """Initialize a Cultivator Object

        :type serial: uvaSerial.psiSerialController.psiSerialController
        :rtype: Cultivator
        """
        super(Cultivator, self).__init__(settings=settings)
        self._serial = serial
        # self._solver = None
        self._channels = {}  # { index : channel }

    def getChannels(self):
        """Get all the channels owned by this cultivator

        :return: List of channels owned by this cultivator
        :rtype: dict[uvaCultivator.uvaCultivatorChannel.CultivatorChannel]
        """
        return self._channels

    def getChannel(self, index):
        """Get a channel by its index

        :type index: int
        :rtype: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        """
        result = None
        if self.hasChannel(index):
            result = self._channels[index]
        return result

    def hasChannel(self, index):
        return index in self._channels.keys()

    def setChannel(self, index, channel):
        """
        Sets a channel in the channel list of this Cultivator Object, the index should already exist.
        :param index: The index of the channel to be set
        :type index: int
        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        """
        if self.hasChannel(index):
            self._channels[index] = channel
            self.getChannel(index).setIndex(index)
        return self

    def addChannel(self, channel, idx=None):
        """
        Adds a channel to the channel list of this Cultivator Object
        :param channel:
        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :param idx: The index where the channel should be inserted
        :type idx: int or None
        :return: The channel that was added
        :rtype: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        """
        if idx is None:
            # add one to the highest key
            idx = sorted(self._channels.keys())[-1] + 1
            channel.setIndex(idx)
        self._channels[idx] = channel
        self._settings["channel_count"] = len(self._channels)
        return self.getChannel(idx)

    def getChannelCount(self):
        """
        Get the number of channels
        :rtype: int
        """
        return len(self._channels)

    def getChannelRange(self):
        """
        Get a list of channel keys
        :return:
        :rtype: list[int]
        """
        return self.getChannels().keys()

    def getChannelLight(self, idx):
        """Get the light regime currently used by the channel

        :param idx: Index of the channel
        :type idx: int
        :return: The currently used Light Regime
        :rtype: uvaCultivator.uvaCultivatorLight.CultivatorLight
        """
        result = None
        if self.hasChannel(idx):
            result = self.getChannel(idx).getLight()
        return result

    def refreshLightsSettings(self, channels=None):
        """Ask all channels to refresh the stored light settings

        :param channels: List of channel indexes
        :type channels: list
        :rtype: bool
        """
        result = False
        if channels is None:
            channels = self.getChannelRange()
        for c in channels:
            if self.getChannelLight(c) is not None:
                result = self.getChannelLight(c).refreshLightSettings()
        return result

    def getLightsState(self):
        """Retrieve the state of all lights"""
        result = {}
        for c in self.getChannelRange():
            self.getLog().log(1, "Retrieve current light state of channel {}".format(c))
            result[c] = self.getChannel(c).getLightState()
        return result

    def setLightsState(self, new_state=False):
        """Set the state of all lights, using the 255 index (set the state for all lights with 1 command)

        :param new_state:
        :return: Returns a list with the states of all lights before this command was issued
        :rtype: list
        """
        return self.getSerial().setAllLightStates(new_state)

    def equalityInRange(self, x, y, offset=0):
        """
        Check if y is between the lower limit (x-offset) and upper limit (x+offset). Not including the boundaries.
        :param x: Value to determine the limits
        :type x: int, float
        :param y: Value that has to be between the limits
        :type y: int, float
        :param offset: The distance from x to set the lower/upper limit to
        :type offset: int, float
        :return: Whether y is between the limits; thus is within a certain range of x.
        :rtype: bool
        """
        # check if y is between lower and upper bound
        return (x - offset) < y < (x + offset)

    def adjustLightsSettings(self, channels=None, current_state=None, simulate=False):
        """
        Will check the settings and alter them given the REGIME settings
        :param channels: List of channel indexes to adjust, when None; all channels will be adjusted
        :type channels: list, None
        :param current_state: The state reflecting the current state of the lights when the state of all lights
        was changed without refreshing the channels. When None the information stored in the channel object is in
        sync and therefore considered to be the previous state.
        :type current_state: None, bool
        :return: Whether the operation was successful
        :rtype: bool
        """
        result = False
        if channels is None: channels = self.getChannelRange()
        for c in channels:
            # first deal with the state
            # get before state as reported by the channels
            bstate = self.getChannelLight(c).getLightState()
            if current_state is None:  # no change in state without refreshing -> current = previous
                cstate = bstate  # set current state to before state
            # new state = time dependent state or config state
            nstate = self.getChannelLight(c).getDesiredLightState(
                default=self.getChannelLight(c).getConfiguredLightState()
            )
            if nstate is None:  # no regime -> new state = before state (restore)
                nstate = bstate
            # only change light states when the new state is different.
            if nstate != cstate:
                result = self.getChannelLight(c).setLightState(nstate, simulate)
                self.getLog().info("Current state ({}) doesn't match desired state ({}), adjust it!".format(
                    "On" if cstate else "Off", "On" if nstate else "Off"
                ))
            else:
                # report success even nothing has changed!
                result = True

            current_od = 0
            # only adjust intensity when light state is On.
            if nstate is True:
                # TODO: Make LED wavelength dependent on the regime applied.
                if self.getChannel(c).getMeasurement(nm=720, idx=-1) is not None:
                    m = self.getChannel(c).getMeasurement(nm=720, idx=-1)
                    current_od = m.getCalibratedODValue()
                desired_intensity = self.getChannelLight(c).getDesiredLightIntensity(
                    od=current_od, default=self.getChannelLight(c).getConfiguredLightIntensity()
                )
                current_intensity = self.getChannelLight(c).getLightIntensity()
                # Test whether the current intensity is in 1 uE/m2/s of the desired intensity
                if not self.equalityInRange(desired_intensity, current_intensity, 1):
                    self.getLog().info("Current intensity ({}) doesn't match desired intensity ({}), adjust it!".format(
                        current_intensity, desired_intensity
                    ))
                    result = self.getChannelLight(c).setLightIntensity(desired_intensity, simulate=simulate)
                else:
                    result = True
        return result

    def setNeighbourDependentIntensities(self, simulate=False):
        """
        Calculate the correct intensity by solving the system of equations in which the contribution of each
        neighbour is calculated. Supports linear equations only!
        :return:
        :rtype:
        """
        if self.getLightSolver() is not None:
            self.getLightSolver().solve(simulate)

    # def measureOD(self, channels=None):
    # 	"""
    # 	Measure the OD of the given channels
    # 	:param channels:
    # 	:type channels: list
    # 	:return: List with the measurements [ (led : [channel : m]) ]
    # 	:rtype: list
    # 	"""
    # 	if channels is None: channels = self.getChannelRange()
    # 	# initialize two lists
    # 	result = ({c:[] for c in channels }, {c:[] for c in channels})
    # 	# get temperature
    # 	temperature = self.getTemperature()
    # 	# aggregation method:
    # 	method = self.getSampleAggregation()
    # 	# sample size
    # 	samples = self.getODSampleSize()
    # 	# sample wait time
    # 	sample_wait = self.getSampleWaitTime()
    # 	for c in channels:
    # 		for led in self.getMeasurementLeds(indices=True):
    # 			self.getLog().info("Measuring OD in channel {} is {}".format(
    # 				c, "enabled" if self.getChannel(c).measuresOD() else "disabled"
    # 			))
    # 			if self.getChannel(c).measuresOD():
    # 				m = self.getChannel(c).measureOD(
    # 					repeats=self.getRepeats(), nm=led, samples=samples, method=method,
    # 					sample_wait=sample_wait
    # 				)
    # 				if m is None:
    # 					self.getLog().critical("Unable to measure OD of channel {} at {}nm".format(c,
    # 				                     "680" if led == 0 or led == 680 else "720" ))
    # 				else:
    # 					m.setTemperature(temperature)
    # 					result[led][c].append(m)
    # 	return result

    def measureOD(self, channels=None):
        """
        Measure the OD of the given channels
        :param channels:
        :type channels: list
        :return: List with the measurements [ (led : [channel : m]) ]
        :rtype: tuple[dict[int, uvaCultivator.uvaODMeasurement.ODMeasurement]]
        """
        results = ([], [])
        if channels is None: channels = self.getChannelRange()
        leds = self.getMeasurementLeds(indices=True)
        results = self.collectMeasurements(channels=channels, leds=leds)
        return results

    def collectMeasurements(self, channels, leds):
        """
        Collects n readings from each channel, led combination and aggregates them to one measurement
        :return:
        :rtype: tuple[dict[int, list[uvaCultivator.uvaODMeasurement.ODMeasurement or None]]]
        """
        results = ({}, {})
        for c in channels:
            if self.getChannel(c).measuresOD():
                for led in leds:
                    results[led][c] = None
        # sample size
        samples = self.getODSampleSize()
        # maximum consecutive fails
        max_fails = self.getMaxSampleFail()
        # collect readings
        readings = self.collectReadings(channels=channels, leds=leds, samples=samples, max_fails=max_fails)
        # record temperature of MC
        temperature = self.getTemperature()
        # Define aggregation method
        method = self.getSampleAggregation()
        for c in channels:
            if self.getChannel(c).measuresOD():
                for led in leds:
                    if len(readings[led][c]) > 0:
                        reading = ReadingAggregation.aggregate(method=method, values=readings[led][c])
                        """:type: tuple[float, float, float] """
                        m = self.createMeasurement(
                            channel=self.getChannel(c), nm=led, repeats=self.getRepeats(),
                            flash=reading[0], background=reading[1], od_raw=reading[2]
                        )
                        m.setTemperature(temperature)
                        self.getLog().info(
                            "Measure Channel {}@{}: MC OD = {:6.3f}, REF OD = {:6.3f}, BGR = {:6.3f}, LI = {:6.2f}"
                            "".format(
                                c, m.getLED(asIndex=False), m.getODRaw(), m.getODValue(),
                                m.getBackground(), m.getIntensity()
                            ))
                        results[led][c] = [m]
                        # add measurement to channel
                        self.getChannel(c).addMeasurement(m)
        return results

    def collectReadings(self, channels, leds, samples=1, max_fails=1):
        """
        Collects n readings from each channel, led combination, where n = samples.
        :return:
        :rtype: tuple[dict[int, list[list[float]]]]
        """
        readings = ({}, {})  # ({c: [] for c in channels}, {c: [] for c in channels})
        fails = ({}, {})  # ({c: 0 for c in channels}, {c: 0 for c in channels})
        for c in channels:
            if self.getChannel(c).measuresOD():
                for led in leds:
                    readings[led][c] = []
                    fails[led][c] = 0
        self.getLog().info("Measure OD in {} channel(s) at {} wavelength(s): collect {} readings per channel/led".format(
            len(readings[0]), len(readings), samples
        ))
        finished = False
        while not finished:
            finished = True
            for c in channels:
                if self.getChannel(c).measuresOD():
                    for led in leds:
                        # only try if we need measurements, and skip if it failed to many times
                        if len(readings[led][c]) < samples and fails[led][c] < max_fails:
                            # get reading
                            r = self.getChannel(c).getODReading(repeats=self.getRepeats(), nm=led)
                            if r is None or None in r:
                                # failed; register it
                                fails[led][c] += 1
                                self.getLog().warning(
                                    "Unable to measure OD, so we exclude this one from the sample pool "
                                    "(fail #{})".format(fails[led][c])
                                )
                            else:
                                # add to readings
                                readings[led][c].append(r)
                                # reset fail count
                                fails[led][c] = 0
                            # end of if
                        # finished = we skipped and all other skipped to
                        finished = not (len(readings[led][c]) < samples and fails[led][c] < max_fails) and finished
                    # end of for led
                    # end of if measures OD
                    # end of for channel
        # end of while
        return readings

    def createMeasurement(self, channel, nm, repeats, flash, background, od_raw):
        ps_od = channel.getODCalibration(nm=nm).calculate(od_raw)
        if ps_od is None or ps_od < 0:
            self.getLog().warning("Invalid OD value prediction ({}), set it to 0 instead.".format(ps_od))
            ps_od = 0
        attributes = {
            't_point': dt.now(),
            'od_value': ps_od,
            'od_raw': od_raw,
            'intensity': channel.getGivenLight(),
            'background': background,
            'flash': flash,
            'od_repeats': repeats,
            'od_led': nm,
            'temperature': 30.0,
        }
        return oDM(**attributes)

    def getSerial(self):
        return self._serial

    def getConnection(self):
        return self.getSerial()

    def hasSerial(self):
        return self._serial is not None

    def hasConnection(self):
        return self.hasSerial()

    def isConnected(self):
        result = False
        if self.hasSerial():
            result = self.getSerial().isConnected()
        return result

    def getGasPause(self):
        return self.retrieveSetting(name="gas_pause", namespace="cultivator")

    def setGasPause(self, pause):
        """
        :type pause: int
        :rtype: Cultivator
        """
        self.setSetting(name="gas_pasue", value=pause, namespace="cultivator")
        return self.getGasPause()

    def getTemperature(self):
        result = None
        if self.hasSerial():
            result = self.getSerial().getTemperature()
        return result

    def getRepeats(self):
        return self.retrieveSetting(name="od_repeats", namespace="cultivator")

    def setRepeats(self, repeats):
        """
        :type repeats: int
        :rtype: Cultivator
        """
        self.setSetting(name="od_repeats", value=repeats, namespace="cultivator")
        return self.getRepeats()

    def getMeasurementLeds(self, indices=False):
        """
        :param indices: Will transform the wavelengths to the corresponding LED Indices
        :return:
        :rtype: list
        """
        result = self.retrieveSetting(name="od_leds", namespace="cultivator")
        if isinstance(result, str):
            result = self.listifyString(result, item_type=int)
            if len(result) == 0:
                result = self.getDefaultSettings("od_leds", namespace="cultivator")
            self.setSetting(name="od_leds", value=result, namespace="cultivator")
        if indices:
            result = copy.copy(result)
            for idx, led in enumerate(result):
                result[idx] = 0 if led == 680 else 1
        return result

    def setMeasurementLeds(self, use680=False, use720=False):
        """
        :type use680: bool
        :type use720: bool
        :rtype: Cultivator
        """
        current_leds = self.getMeasurementLeds()
        if use680 and 680 not in current_leds:
            current_leds.insert(0, 680)
        if use720 and 720 not in current_leds:
            current_leds.insert(1, 720)
        self.setSetting(name="od_leds", value=current_leds, namespace="cultivator")
        return self

    def use680Led(self):
        return 680 in self.getMeasurementLeds()

    def use720Led(self):
        return 720 in self.getMeasurementLeds()

    def setLightSolver(self, solver):
        """
        :type solver: uvaCultivator.uvaLightSolver.BasicLightSolver
        """
        self.setSetting(name="solver", value=solver, namespace="cultivator")
        return self.getLightSolver()

    def getLightSolverName(self):
        return self.retrieveSetting(name="solver", default=self.getDefaultSettings(), namespace="cultivator")

    def getLightSolver(self):
        """
        :rtype: uvaCultivator.uvaLightSolver.BasicLightSolver
        """
        result = None
        solver_name = self.getLightSolverName()
        if solver_name is not None:
            if solver_name.lower() == "Simple".lower():
                from uvaLightSolver import StrategicLightSolver
                result = StrategicLightSolver(self)
            if solver_name.lower() == "Linear".lower():
                from uvaLightSolver import LinearLightSolver
                result = LinearLightSolver(self)
        return result

    def getODSampleSize(self):
        return self.retrieveSetting(
            name="od_samples", default=self.getDefaultSettings(), namespace="cultivator"
        )

    def setODSampleSize(self, size=1):
        """
        Set the number of samples that should be taken in order to get a good estimate of the OD
        :param size: Number of samples
        :type size: int
        :return: The new number of samples to be taken
        :rtype: int
        """
        self.setSetting(name="od_samples", value=size, namespace="cultivator")
        return self.getODSampleSize()

    def getSampleAggregation(self):
        return self.retrieveSetting(
            name="aggregation_method", default=self.getDefaultSettings(), namespace="cultivator"
        )

    def setSampleAggregation(self, method=None):
        self.setSetting(name="aggregation_method", value=method, namespace="cultivator")
        return self.getSampleAggregation()

    def getSampleWaitTime(self):
        return self.retrieveSetting(
            name="sample_wait", default=self.getDefaultSettings(), namespace="cultivator"
        )

    def setSampleWaitTime(self, value=0):
        self.setSetting(name="sample_wait", value=value, namespace="cultivator")
        return self.getSampleWaitTime()

    def getMaxSampleFail(self):
        return self.retrieveSetting(
            name="max_sample_fails", default=self.getDefaultSettings(), namespace="cultivator"
        )

    def setMaxSampleFail(self, value):
        self.setSetting(name="max_sample_fails", value=value, namespace="cultivator")
        return self.getMaxSampleFail()
