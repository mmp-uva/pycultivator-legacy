# coding=utf-8
"""
Basic module layer that represents a Cultivator Vessel. It abstracts the communication with the device.
"""

from pycultivator_legacy.core import BaseObject
from uvaCultivatorLight import CultivatorLight
from uvaPolynomialDependency import ODCalibrationDependency
from uvaODMeasurement import ODMeasurement as odM
from uvaAggregation import ReadingAggregation
from datetime import datetime as dt
from time import sleep
import copy

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class CultivatorChannel(BaseObject):
    """
    A class representing a vessel.
    """

    OD_LED_680 = 680
    OD_LED_680_IDX = 0
    OD_LED_720 = 720
    OD_LED_720_IDX = 1
    OD_LEDS_NM = [OD_LED_680, OD_LED_720]
    OD_LEDS_IDX = [OD_LED_680_IDX, OD_LED_720_IDX]
    OD_LED_MAPPING = {
        OD_LED_680: OD_LED_680_IDX,
        OD_LED_720: OD_LED_720_IDX
    }

    _namespace = "cultivator.channel"
    _default_settings = BaseObject.mergeDefaultSettings(
        {
            "od_aggregation": ReadingAggregation.AGGREGATE_MINIMUM
        }, namespace=_namespace
    )

    def __init__(self, cultivator, channel_idx, light=None, measure_od=False, settings=None):
        """Initialize the object, with a connection to the serial device and the channel number

        :type cultivator: uvaCultivator.uvaCultivator.Cultivator or None
        :type channel_idx: int
        :type light: uvaCultivator.uvaCultivatorLight.CultivatorLight
        :return:
        :rtype: CultivatorChannel
        """
        super(CultivatorChannel, self).__init__(settings=settings)
        self._cultivator = cultivator
        self._channel_idx = channel_idx
        self._measure_od = measure_od
        self._light = light
        if self._light is None:
            self._light = CultivatorLight(self)
        # attach the channel to the light
        self._light.setChannel(self)
        self._measurements = [[], []]
        # self._od_calibration = ODCalibrationDependency.loadDefault()
        self._od_calibrations = {}

    @staticmethod
    def loadDefault():
        return CultivatorChannel(None, 0)

    @classmethod
    def createFromXMLElement(cls, xml, config):
        """
        Create a Channel object from XML
        :param xml:
        :type xml:
        :param config:
        :type config: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        :return:
        :rtype:
        """
        channel_id = config.validateValue(xml.get("id"), int)
        channels_e = config.getChannelsElement()
        measure_od = config.validateValue(channels_e.get('measure_od'), bool, default_value=False)
        measure_od = config.validateValue(xml.get("measure_od"), bool, default_value=measure_od)
        # load the light of this channel
        light = copy.copy(config.loadDefaultLight())
        if config.getLightElement(xml) is not None:
            light = CultivatorLight.createFromXMLElement(xml, config)
        # load calibration
        # calibrations_e = config.getCalibrationElements(xml)
        result = cls(config.loadCultivator(), channel_idx=channel_id, light=light, measure_od=measure_od)
        # default_od_calibration = config.loadDefaultODCalibration()
        # od_calibration = default_od_calibration
        # load od calibrations
        od_calibrations = config.loadODCalibrations(xml)
        for nm in od_calibrations.keys():
            calibration = od_calibrations[nm]
            result.addODCalibration(calibration.getId(), calibration)
        return result

    def getChannel(self):
        """

        :rtype: CultivatorChannel
        """
        return self._channel_idx

    @property
    def index(self):
        return self._channel_idx

    def getIndex(self):
        return self.index

    def setIndex(self, idx):
        if idx < 0 or idx > 8:
            raise ValueError("Invalid channel index; {} not in [0, 7]".format(idx))
        self._channel_idx = idx
        return self

    def measuresOD(self):
        return self._measure_od is True

    def setMeasureOD(self, state=False):
        self._measure_od = state is True
        return self._measure_od

    def getCultivator(self):
        """
        :rtype: uvaCultivator.uvaCultivator.Cultivator
        """
        return self._cultivator

    def hasCultivator(self):
        return self._cultivator is not None

    def getSerial(self):
        """
        :rtype: uvaSerial.psiSerialController.psiSerialController
        """
        return self.getCultivator().getSerial()

    def hasSerial(self):
        return self.getCultivator().hasSerial()

    def isConnected(self):
        result = False
        if self.hasCultivator():
            result = self.getCultivator().isConnected()
        return result

    def getLight(self):
        """
        :rtype: CultivatorLight
        """
        return self._light

    def hasLight(self):
        return self.getLight() is not None

    def getLightIntensity(self, unit=CultivatorLight.LIGHT_INTENSITY_ABSOLUTE):
        result = None
        if self.hasLight():
            result = self.getLight().getLightIntensity(unit)
        return result

    def setLightIntensity(self, value, unit=CultivatorLight.LIGHT_INTENSITY_ABSOLUTE, simulate=False):
        result = False
        if self.hasLight():
            result = self.getLight().setLightIntensity(value, unit, simulate)
        return result

    def getLightState(self):
        result = None
        if self.hasLight():
            result = self.getLight().getLightState()
        return result

    def setLightState(self, state, simulate=False):
        result = False
        if self.hasLight():
            result = self.getLight().setLightState(state, simulate)
        return result

    def getGivenLight(self, unit=CultivatorLight.LIGHT_INTENSITY_ABSOLUTE):
        result = None
        if self.hasLight():
            result = self.getLight().getGivenLight(unit)
        return result

    def getODCalibration(self, nm=OD_LED_720):
        """
        :rtype: uvaCultivator.uvaPolynomialDependency.ODCalibration
        """
        result = ODCalibrationDependency.loadDefault()
        if nm in self.OD_LEDS_IDX:
            nm = self.OD_LEDS_NM[nm]
        if nm in self._od_calibrations.keys():
            result = self._od_calibrations[nm]
        return result

    def addODCalibration(self, nm, calibration):
        self._od_calibrations[nm] = calibration

    def clearODCalibrations(self):
        self._od_calibrations.clear()

    def measureOD(self, repeats, nm, samples=1, method=None, sample_wait=0):
        """
        Measures, stores and returns the actual OD of this channel.
        *Assumes that gas pump and light are switched off*
        :param repeats: number of OD Measurements to do and use average of that
        :type repeats: int
        :param nm: Wavelength to measure OD at
        :type nm: int
        :return: Measurement object
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement or None
        """
        result = None
        # only measure if enabled
        if self.measuresOD():
            # if only 1 reading is required, use the simple
            if samples == 1:
                result = self.getODReading(repeats=repeats, nm=nm)
            else:
                result = self.getODReadings(
                    samples=samples, repeats=repeats, nm=nm, method=method, sample_wait=sample_wait
                )
            if result is not None:
                result = self.createMeasurement(
                    nm=nm, repeats=repeats, flash=result[0], background=result[1], od_raw=result[2]
                )
                self.getLog().info(
                    "Measure Channel {}@{}: MC OD = {:6.3f}, REF OD = {:6.3f}, BGR = {:6.3f}, LI = {:6.2f}".format(
                    self.getIndex(),
                    result.getLED(asIndex=False),
                    result.getODRaw(), result.getODValue(),
                    result.getBackground(), result.getIntensity()
                ))
                self.addMeasurement(result)
        return result

    def getODReading(self, repeats, nm):
        """
        Does a OD Reading and returns an OD Measurement containing that reading.
        :param repeats: How often the MC will measure the OD and average those measurements.
        :type repeats: int
        :param nm: Wavelength index (0 = 680nm, 1 = 720nm)
        :type nm: int
        :return: OD Measurement with the OD Readings
        :rtype: list[float or None] or None
        """
        result = None
        if self.isConnected():
            t_start = dt.now()
            result = self.getSerial().getODReading(self.getIndex(), nm, repeats)
            # m = ( flash, background, od_raw)
            self.getLog().log(
                0, "Reading OD took {:0.3f} seconds".format(
                    (dt.now() - t_start).total_seconds())
            )
        return result

    def createMeasurement(self, nm, repeats, flash, background, od_raw):
        ps_od = self.getODCalibration(nm=nm).calculate(od_raw)
        if ps_od is None or ps_od < 0:
            self.getLog().warning("Invalid OD value prediction ({}), set it to 0 instead.".format(ps_od))
            ps_od = 0
        attributes = {
            't_point': dt.now(),
            'od_value': ps_od,
            'od_raw': od_raw,
            'intensity': self.getGivenLight(),
            'background': background,
            'flash': flash,
            'od_repeats': repeats,
            'od_led': nm,
            'temperature': 30.0,
        }
        return odM(**attributes)

    def getODReadings(self, samples, repeats, nm, method=None, max_fails=3, sample_wait=0):
        """
        Does multiple readings and aggregates them into a single measurement
        :param samples: Number of samples to be taken
        :type samples:
        :param repeats: Number of measurements to average in the MC
        :type repeats: int
        :param nm: Wavelength at which to measure
        :type nm: int
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement or None
        """
        result = None
        if not self.isConnected():
            self.getLog().warning("Not connected to a multicultivator, try connecting now...")
            self.getSerial().connect()
        if self.isConnected():
            readings = []
            fails = 0
            while len(readings) < samples and fails < max_fails:
                m = self.getSerial().getODReading(self.getIndex(), nm, repeats)
                # m = ( flash, background, od_raw)
                if None not in m:
                    fails = 0
                    readings.append(m)
                else:
                    fails += 1
                    self.getLog().warning(
                        "Unable to measure OD, so we exclude this one from the sample pool (fail #{})".format(fails)
                    )
                if sample_wait > 0:
                    sleep(sample_wait)
            if fails == max_fails:
                self.getLog().critical(
                    "Unable to receive {} subsequent OD readings, continue with what we have ({} samples)".format(
                        fails, len(readings))
                )
            if method is None:
                method = self.getAggregationMethod()
            if method is None:
                method = ReadingAggregation.AGGREGATE_MINIMUM
            result = ReadingAggregation.aggregate(method=method, values=readings)
        return result

    def getAggregationMethod(self):
        return self.retrieveSetting(
            name="od_aggregation", default=self.getDefaultSettings(), namespace="cultivator.channel"
        )

    def setAggregationMethod(self, method=ReadingAggregation.AGGREGATE_MINIMUM):
        if ReadingAggregation.hasAggregationMethod(name=method):
            self.setSetting(name="od_aggregation", value=method, namespace="cultivator.channel")
        return self.getAggregationMethod()

    def getMeasurementCount(self, nm=720):
        return len(self.getMeasurements(nm=nm))

    def getMeasurements(self, nm=720):
        """

        :param nm: Wavelength or index of LED at which the measurements should be measured
        :type nm: int
        :return:
        :rtype: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        """
        result = []
        if nm in self.OD_LEDS_NM:
            nm = self.OD_LED_MAPPING[nm]
        if nm in self.OD_LEDS_IDX:
            result = self._measurements[nm]
        return result

    def getMeasurement(self, idx, nm=720):
        """
        :type idx: int
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        result = None
        if nm in self.OD_LEDS_NM:
            nm = self.OD_LED_MAPPING[nm]
        if nm in self.OD_LEDS_IDX:
            if -len(self._measurements[nm]) <= idx < len(self._measurements[nm]):
                result = self._measurements[nm][idx]
        return result

    def addMeasurement(self, m):
        """
        Add a measurement to the list of stored measurements
        :param m: Measurement to be added
        :type m: uvaCultivator.uvaODMeasurement.ODMeasurement
        :return: The last stored measurement
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        nm = m.getLED()
        if nm in self.OD_LEDS_IDX:
            self._measurements[nm].append(m)
        return self.getMeasurement(-1, nm=nm)

    def clearMeasurements(self, nm=720):
        """
        Clears the measurements
        :type nm: int
        """
        if nm in self.OD_LEDS_NM:
            nm = self.OD_LED_MAPPING[nm]
        if nm in self.OD_LEDS_IDX:
            self._measurements[nm] = []
