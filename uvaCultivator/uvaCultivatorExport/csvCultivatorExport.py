# coding=utf-8
"""
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from uvaCultivatorExport import AbstractCultivatorExport as aCE
from datetime import datetime as dt, timedelta as td
import tarfile
import numpy as np
from os import path, rename, remove
from ..uvaODMeasurement import ODMeasurement as ODM
from abc import *


class AbstractCSVExport(aCE):
	"""
	Class implementing the read and write functions to a CSV file
	"""

	PORTRAIT_HANDLER = 1
	LANDSCAPE_HANDLER = 2
	HANDLERS = [PORTRAIT_HANDLER, LANDSCAPE_HANDLER]

	HEADER_MIN_LENGTH = 2
	HEADER_OD = "OD"
	HEADER_CALIBRATED_OD = HEADER_OD
	HEADER_RAW_OD = "RA"
	HEADER_INTENSITY = "In"
	HEADER_BACKGROUND = "Ba"
	HEADER_PERCEIVED_INTENSITY = "PI"

	SETTINGS_OD_REPEATS = "od_repeats"
	SETTINGS_OD_LED = "od_led"

	_default_settings = aCE.mergeDictionary(
		aCE._default_settings,
		{
			"output_path" : "",
			"channel_count" : 8,
			"use_raw_od" : True,
			"od_repeats" : 3,
			"od_led" : 720,
			"delim" : ";",
		}, copy_target=True)

	def __init__(self, settings=None):
		aCE.__init__(self, settings=settings)
		# self.getChannelCount() = channels                       # number of channels to report on
		# self._od_repeats = od_repeats
		# self._od_led = od_led
		# self._delim = delimiter
		# self._use_raw_od = use_raw_od
		self._time_fmt = self.DT_FORMAT
		self._digit_test = self.DIGIT_TEST

	def __exit__(self, exc_type, exc_val, exc_tb):
		pass

	def getODRepeats(self):
		return self.retrieveSetting(name="od_repeats", default=self._default_settings)

	def getODLED(self):
		return self.retrieveSetting(name="od_led", default=self._default_settings)

	def getChannelCount(self):
		return self.retrieveSetting(name="channel_count", default=self._default_settings)

	def usesRAWOD(self):
		return self.retrieveSetting(name="use_raw_od", default=self._default_settings)

	def getDelimiter(self):
		return self.retrieveSetting(name="delim", default=self._default_settings)

	def getDestination(self):
		return self.getFilePath()

	@staticmethod
	def compareHeader(data_header, var_header):
		return data_header[:AbstractCSVExport.HEADER_MIN_LENGTH].lower() == \
		       var_header[:AbstractCSVExport.HEADER_MIN_LENGTH].lower()

	@staticmethod
	def getValueByHeader(values, headers, col_idx, var_header, default_value=None):
		result = default_value
		if AbstractCSVExport.compareHeader(headers[col_idx], var_header):
			result = values[col_idx]
		return result

	@staticmethod
	def parseValueByHeader(values, headers, col_idx, header, val_type=str, default_val=None, date_format=None,
	                       digit_test=None):
		result = AbstractCSVExport.getValueByHeader(values=values, headers=headers, col_idx=col_idx,
		                                                    var_header=header, default_value=default_val)
		return AbstractCSVExport.parseValue(value=result, v_type=val_type, default_value=default_val,
		                                            date_format=date_format, digit_test=digit_test)

	def setFilePath(self, path):
		self._settings["output_path"] = path
		return self.getFilePath()

	def getFilePath(self):
		"""
		Get the path to CSV file that is being used by this object
		:return: Path to CSV file
		:rtype: str
		"""
		return self.retrieveSetting(name="output_path", default=self._default_settings)

	@staticmethod
	def convertCSV(reader, writer):
		"""
		Is able to convert CSV files
		:param reader:
		:type reader: BasicCultivatorCSVHandler
		:param writer:
		:type writer: BasicCultivatorCSVHandler
		:return: Whether it was a success or not
		:rtype: bool
		"""
		result = False
		t_zero, times, m = reader.readMeasurements()
		writer.createHeader(t_zero)
		for time in sorted(times):
			result = writer.appendMeasurements( t_zero + td(minutes=time), m[time][0], m[time][1] )
		return result

	@staticmethod
	def getHandler( csv_type, settings=None):
		"""
		Retrieve a handler corresponding to the desired type
		:param csv_type: Either LANDSCAPE_HANDLER or PORTRAIT_HANDLER
		:type csv_type: int
		:return: Reference to object
		:rtype: BasicCultivatorCSVHandler, CultivatorLandscapeCSVHandler, CultivatorPortraitCSVHandler, None
		"""
		result = None
		if AbstractCSVExport.LANDSCAPE_HANDLER == csv_type:
			result = LandscapeCSVExport(settings=settings)
		if AbstractCSVExport.PORTRAIT_HANDLER == csv_type:
			result = PortraitCSVExport(settings=settings)
		return result

	def hasStorage(self):
		"""
		Will return True if the path exists
		:return:
		:rtype:
		"""
		self.checkPath(create=False)

	def checkPath(self, replace=False, create=False):
		"""
		Will check if the path leads to a file that already exists.
		If the path leads to something different than a file .csv is appended to it, and check that path
		If the path does not exist (yet), this method will create the file with a new header.
		:return: True on success, False on failure
		:rtype: bool
		"""
		result = not replace
		try:
			# the path we are looking is not a file: add .csv to path
			if path.exists(self.getFilePath()) and not path.isfile(self.getFilePath()):  # does exist but not a file!
				self.setFilePath(self.getFilePath() + ".csv")  # append csv
				result = self.checkPath(replace=replace, create=create)
			# path does not exist or we can replace it
			elif not path.exists(self.getFilePath()) or replace:
				# is writeable or create header
				result = not create or self.createStorage()
		except IOError:
			self.getLog().critical("Unable to open the CSV file at {}".format(self.getFilePath()))
		return result

	def getTimeZero(self):
		"""
		Retrieve the experiment's start date and time as datetime object from the CSV File
		:return: DateTime object representing the start date and time of the experiment
		:rtype: datetime.datetime
		"""
		try:
			with open(self.getFilePath()) as src:
				line = src.readline().split("\n")[0]  # get rid of the end of line
			t = line.split(self.getDelimiter())[0]  # remove delimiter
			self._t_zero = dt.strptime(t, self._time_fmt)
		except IOError as ioe:
			self.getLog().critical("Failed to get the t=0 from the CSV file at {}".format(self.getFilePath()))
		return self._t_zero

	def getTimeFormat(self):
		return self._time_fmt

	def setTimeZero(self, t_zero, write=False):
		"""
		:type t_zero: datetime.datetime
		:type write: bool
		:rtype: BasicCultivatorCSVHandler
		"""
		if t_zero is dt:
			aCE.setTimeZero(self, t_zero, write)
			if write and path.exists(self.getFilePath()):
				with open(self.getFilePath()) as src:
					with open(self.getFilePath(), "w") as dest:
						idx = 0
						for line in src:
							if idx == 0:
								# remove \n if present, skip first ; delimited field put fields back together
								line = ";".join(line.split("\n")[0].split(";")[1:])
								line = "{};{}".format(self._t_zero.strftime(self._time_fmt), line)
							dest.write(line)
							idx += 1
		return self._t_zero

	@abstractmethod
	def createStorage(self, destination=None, replace=False):
		"""
		Write the header.
		:return: True on succes, False on failure
		:rtype: bool
		"""
		raise NotImplementedError

	@abstractmethod
	def readODMeasurements(self, start_time=None, end_time=None):
		"""
		Read measurements and return them as ODMeasurement Objects
		:return: dict of ODMeasurement Objects { channel : {time : m}}
		:rtype: dict
		"""

	@abstractmethod
	def addODMeasurements(self, m):
		"""
		Add a list of ODMeasurements to the file
		Expected format: m[]
		:param m: List of OD Measurements to add
		:type m: ODMeasurement
		:rtype: bool
		"""

	@abstractmethod
	def readVariables(self):
		"""
		Reads the CSV as simple as possible to a dictionary of variables where each variable contains a tuple with
		a list of time points and a list of data points.
		:return: Dictionary of format { variable : ( [time], [data] )
		:rtype: dict
		"""

	def processVariables(self, data, headers, aggregations=None, selection_fun=None):
		"""
		processes the variable dictionary returned by readVariables() and shrinks the data dictionary to a dictionary
		with only the selected headers and with new variables created by aggregation described with a aggregations
		dictionary. To compare the headers from the data to the desired headers, the selection_fun is used. If the
		selection_fun is None, the standard compareHeader method is used.
		:param data: Dictionary with the data points, the key equals the CSV Header
		:type data: dict
		:param headers: List with the names of the desired variables
		:type headers: list
		:param aggregations: Dictionary with aggregations, where the key is the aggregation name and the value is a
		tuple containing a reference to a function as the first element, the second element is a list with names of the
		required variables. Data will be send to the function as a unpacked dictionary, where each element is a list
		of data points.
		:type aggregations: dict
		:param selection_fun: Reference to function to use when comparing the headers to the data headers. The
		function should take 2 arguments the data header and the variable name and return a bool.
		:return:
		:rtype:
		"""
		if selection_fun is None:
			selection_fun = self.compareHeader
		result = {}
		if isinstance(headers, list):
			for variable in headers:
				# Add name to the new dictionary
				result[variable] = []
				for name in data.keys():
					if selection_fun(name, variable):
						result[variable].append(data[name])
		# calculate aggregation
		for aggregation in aggregations.keys():
			variables = aggregations[aggregation][1]
			parameters = {}
			# collect the paramaters
			for variable in variables:
				parameters[variable] = []
				for name in data.keys():
					if selection_fun(name, variable):
						parameters[variable].append(data[name])
				if len(parameters[variable]) == 0:
					parameters.pop(variable)
			result[aggregation] = aggregations[aggregation][0](**parameters)

		# Check titles on data
		for variable in result.keys():
			if len(result[variable]) == 0:
				result.pop(variable)
				self.getLog().info("Removed {} from plot because it has no data".format(variable))
		return result

class LandscapeCSVExport(AbstractCSVExport):
	"""
	Class implementing read and write functionality for a column-wise CSV file. With variables on rows
	"""

	def __init__(self, settings=None):
		AbstractCSVExport.__init__(self, settings)

	def checkPath(self, replace=False, create=True):
		result = False
		try:
			# First check with the original routine
			result = AbstractCSVExport.checkPath(replace=replace, create=create)
			# If path is correct, check if file size >= 1 MB (1024 kb = 1024 * 1024 bytes) -> move to new place
			if create and result and path.getsize(self.getFilePath()) >= (1024 * 1024):
				# ok we need to move this file out of the way; add an increment (.x) and tar it.
				# store original t_zero
				t_zero = self.getTimeZero()
				# add an increment at the end, so we can archive
				idx = 1
				while path.exists("{}.{}".format(self.getFilePath(), idx)):
					idx += 1    # increase increment if path is already used
				# move file
				rename(self.getFilePath(), "{}.{}".format(self.getFilePath(), idx))
				# add to tar
				with tarfile.open("{}.{}.tar.gz".format(self.getFilePath(), idx), "w:gz") as tar:
					tar.add("{}.{}".format(self.getFilePath(), idx))
				remove("{}.{}".format(self.getFilePath(), idx))
				# create a new file with
				self.createStorage()   # create header with original t_zero
		except IOError as ioe:
			self.getLog().critical("IOError occurred during checkPath: {}".format(ioe))
		return result

	def createStorage(self, destination=None, replace=False):
		"""
		Write headers to first column, so data has to be appended in column-style
		:return: True on succes, False on failure
		:rtype: bool
		"""
		result = False
		with open(self.getFilePath(), 'w') as dest:
			dest.write("{}\n".format(self._t_zero.strftime(self._time_fmt)))
			# write OD
			for i in range(self.getChannelCount()):
				dest.write("OD_{}\n".format(i))
			if self.usesRAWOD():
				for i in range(self.getChannelCount()):
					dest.write("RAW_{}\n".format(i))
			for i in range(self.getChannelCount()):
				dest.write("Intensity_{}\n".format(i))
			for i in range(self.getChannelCount()):
				dest.write("Background_{}\n".format(i))
			result = True
		return result

	def readODMeasurements(self, start_time=None, end_time=None):
		raise NotImplementedError
		# t_zero = self.getTimeZero()
		# measurements = [ {} for i in range(self.getChannelCount()) ]  # measurements[channel] = [time : ODMeasurement]
		# with open(self.getFilePath()) as src:
		# 	# parse columns
		# 	lines = src.readlines()
		# 	line = lines[0].split("\n")[0]  # remove trailing \n if present
		# 	times = [int(float(time.replace(",", "."))) for time in line.split(";")[1:]]
		# 	nvars = 4 if self._use_perceived_intensity else 3
		# 	ch_count = (len(lines) - 1) / nvars  # len(lines) = 3*channels + 1 -> channels = (len(lines)-1)/3
		# 	for channel in range(1, ch_count + 1):
		# 		m_od = lines[channel].split("\n")[0].split(self._delim)[1:]
		# 		m_bg = lines[channel + ch_count].split("\n")[0].split(self._delim)[1:]
		# 		m_in = lines[channel + ch_count * 2].split("\n")[0].split(self._delim)[1:]
		# 		m_pint = None
		# 		if self._use_perceived_intensity:
		# 			m_pint = lines[channel + ch_count * 3].split("\n")[0].split(self._delim)[1:]
		# 		idx = 0
		# 		for time in times:
		# 			t = t_zero + td(minutes=time)
		# 			od = self.parseValue(m_od[idx], float, 0)
		# 			intensity = self.parseValue(m_in[idx], float, 0)
		# 			bgr = self.parseValue(m_bg[idx], float, 0)
		# 			led = ODM.OD_LED_720 if led == 720 else ODM.OD_LED_680
		# 			measurement = ODM( t, od, intensity, bgr, 0, repeats, led, t_zero )
		# 			if self._use_perceived_intensity:
		# 				measurement.setPerceivedIntensity(float(m_pint[idx].replace(",", ".")))
		# 			measurements[channel][time] = measurement
		# return measurements

	def addODMeasurements(self, measurements, writeRawOD=False):
		"""
		:param measurements: dict[int, uvaCultivator.uvaODMeasurement.ODMeasurement]
		"""
		result = False
		tmp = "{}{}".format(self.getFilePath(), ".bak")  # path to temp file
		with open(self.getFilePath()) as src:
			with open(tmp, 'w') as dest:
				line = src.readline().split("\n")[0]
				measurements[0].setTimeZero(self.getTimeZero())
				rel_time = measurements[0].getFormattedTimePoint(ODM.RELATIVE_TIMEPOINT)
				dest.write("{}{}{}\n".format(line,self.getDelimiter(), rel_time))
				for m in measurements:
					line = src.readline().split("\n")[0]    # strip \n
					if not self.isDigit(m.getCalibratedODValue()):
						OD = "NA"
					else:
						OD = "{:0.3f}".format(m.getCalibratedODValue())
					dest.write("{}{}{}\n".format(line, self.getDelimiter(), OD))
				for m in measurements:
					line = src.readline().split("\n")[0]
					intensity = ("NA" if not self.isDigit(m.getIntensity())
					             else "{:0.3f}".format(m.getIntensity()))
					dest.write("{}{}{}\n".format(line, self.getDelimiter(), intensity))
				for m in measurements:
					line = src.readline().split("\n")[0]  # strip \n
					bgr = "NA" if not self.isDigit(m.getBackground()) else "{:0.3f}".format(m.getBackground())
					dest.write("{}{}{}".format(line,self.getDelimiter(), bgr))
				if writeRawOD:
					for m in measurements:
						line = src.readline().split("\n")[0]  # strip \n
						pint = "NA" if not self.isDigit(m.getODValue()) else "{:0.3f}".format(
							m.getPerceivedIntensity()
						)
						dest.write("{}{}{}\n".format(line, self.getDelimiter(), pint))
				else:
					dest.write('\n')
		remove(self.getFilePath())
		rename(tmp, self.getFilePath())
		result = True
		return result

	def readVariables(self):
		"""
		@see: AbstractCSVExport.readVariables
		"""
		raise NotImplementedError
		# result = {}
		# with open(self.getFilePath()) as src:
		# 	lines = list(src)
		# 	# first cell at first row = start date
		# 	self._t_zero = dt.datetime.strptime(lines[0].split(";")[0], self.DT_FORMAT)
		# 	# rest is column headers -> timepoints
		# 	times = [float(i.replace(',', '.')) for i in lines[0][:-1].split(";")[1:]]
		# 	for line in range(1, len(lines)):
		# 		name = lines[line][:-1].split(";")[0]  # read name from first column
		# 		# read data as rest of line
		# 		data = [(float(i.replace(',', '.')) if self.isDigit(i) else 0)
		# 		        for i in lines[line][:-1].split(";")[1:]]
		# 		# add all measurement with corresponding time points
		# 		result[name] = (times, data)
		# return result


class PortraitCSVExport(AbstractCSVExport):
	"""
	Class implementing read and write functionality for a row-wise CSV file
	"""

	def __init__(self, settings=None):
		AbstractCSVExport.__init__(self, settings=settings)

	def createStorage(self, destination=None, replace=False):
		"""
		Write headers to first row, so data has to be appended in row-style
		:return: True on success, False on failure
		:rtype: bool
		"""
		result = False
		try:
			if destination is None:
				destination = self.getFilePath()
			if self._t_zero is None:
				self._t_zero = dt.now()
			if replace or not path.exists(destination):
				with open(destination, 'w') as dest:
					line = "{}".format(self._t_zero.strftime(self.DT_FORMAT))
					# write OD
					line += self.getDelimiter() + self.getDelimiter().join("OD_{}".format(i) for i in range(self.getChannelCount()))
					# write raw
					if self.usesRAWOD():
						line += self.getDelimiter() + self.getDelimiter().join("RAW_{}".format(i) for i in range(self.getChannelCount()))
					# write intensity header
					line += self.getDelimiter() + self.getDelimiter().join("Intensity_{}".format(i) for i in range(self.getChannelCount()))
					# write background header
					line += self.getDelimiter() + self.getDelimiter().join("Background_{}".format(i) for i in range(self.getChannelCount()))
					dest.write("{}\n".format(line))
					result = True
		except IOError:
			self.getLog().critical("Unable to create header or the CSV file at {}".format(self.getFilePath()))
		return result

	def readLine(self, line, headers, measurements, ch_count=8, start_time=None, end_time=None):
		# decompose line into columns, remove \n
		cols = line.split("\n")[0].split(self.getDelimiter())
		n_vars = len(headers) / ch_count
		# first is time -> create datetime object
		time = int(float(cols[0].replace(",", ".")))
		t_zero = self.getTimeZero()
		t = t_zero + td(minutes=time)
		if (start_time is None or start_time <= t) and (end_time is None or end_time >= t):
		# only include desired
			# move per channel
			for ch_idx in range(0, ch_count):
				# go over available variables: var_idx = ch_count* ch_idx
				bgr = 0
				intensity = 0
				raw_od = 0
				calib_od = 0
				p_intensity = 0
				for var_idx in range(0, n_vars):
					# col_idx = ch_count*var_idx + ch_idx
					col_idx = (ch_count * var_idx) + ch_idx
					calib_od = self.parseValueByHeader(header=self.HEADER_CALIBRATED_OD,
					                                   values=cols[1:], headers=headers, col_idx=col_idx,
					                                   val_type=float, default_val=calib_od)
					raw_od = self.parseValueByHeader(header=self.HEADER_RAW_OD,
					                                 values=cols[1:], headers=headers, col_idx=col_idx,
					                                 val_type=float, default_val=raw_od)
					bgr = self.parseValueByHeader(header=self.HEADER_BACKGROUND,
					                              values=cols[1:], headers=headers, col_idx=col_idx,
					                              val_type=float, default_val=bgr)
					intensity = self.parseValueByHeader(header=self.HEADER_INTENSITY,
					                                    values=cols[1:], headers=headers, col_idx=col_idx,
					                                    val_type=float, default_val=intensity)
					p_intensity = self.parseValueByHeader(header=self.HEADER_PERCEIVED_INTENSITY,
					                                      values=cols[1:], headers=headers, col_idx=col_idx,
					                                      val_type=float, default_val=p_intensity)
				od_led = ODM.OD_LED_720 if self.getODLED() == 720 else ODM.OD_LED_680
				measurement = ODM(t=t, OD=raw_od, intensity=intensity, background=bgr, flash=None,
				                  led=od_led, t_zero=t_zero)
				measurement.setCalibratedODValue(calib_od)
				measurement.setPerceivedIntensity(p_intensity)
				measurements[ch_idx][t] = measurement
		return measurements

	def readODMeasurements(self, start_time=None, end_time=None):
		measurements = []
		if self.checkPath(replace=False, create=False):
			with open(self.getFilePath()) as src:
				# parse columns, get types
				lines = src.readlines()
				headers = lines[0].split("\n")[0].split(self.getDelimiter())[1:]
				ch_count = 0
				# count the number of OD measurements = number of channels
				for header in headers:
					if header[:2] == "OD":
						ch_count += 1
				# len(headers) = channels*nvars -> nvars = len(headers)/channels
				measurements = [{} for i in range(ch_count)]  # measurements[channel] = [time : ODMeasurement]
				for line in lines[1:]:
					measurements = self.readLine(line, headers, measurements,
					                             ch_count=ch_count, start_time=start_time, end_time=end_time)
		return measurements

	def addODMeasurements(self, measurements):
		"""
		Append OD Measurements to a CSV File in Portrait (variables in columns, samples on rows) format. It will add
		as many lines as there are samples.
		:param measurements: A dictionary of measurements where channels are used as key.
		Each channel contains a list of measurements
		:type measurements: dict[int, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]
		:return: True on success, False on failure
		:rtype: bool
		"""
		result = False
		with open(self.getFilePath(), 'a') as dest:
			lines = len(measurements[measurements.keys()[0]])
			for line_idx in range(lines):
				# get the first of each line
				m = measurements[measurements.keys()[0]][line_idx]
				m.setTimeZero(self.getTimeZero())
				rel_time = m.getFomattedRelativeTimePoint()
				line = "{}".format(rel_time)
				values = []
				for c in sorted(measurements.keys()):
					m = measurements[c][line_idx]
					# OD
					values.append(
						"NA" if not self.isDigit(m.getCalibratedODValue()) else "{:0.4f}".format(
							m.getCalibratedODValue()))
				line += self.getDelimiter() + self.getDelimiter().join(values)
				if self.usesRAWOD():
					values = []
					for c in range(self.getChannelCount()):
						#
						m = measurements[c][line_idx]
						values.append("NA" if not self.isDigit(m.getODValue()) else "{:0.4f}".format(m.getODValue()))
					line += self.getDelimiter() + self.getDelimiter().join(values)
				values = []
				for c in range(self.getChannelCount()):
					# Intensity
					m = measurements[c][line_idx]
					values.append("NA" if not self.isDigit(m.getIntensity()) else "{:0.4f}".format(m.getIntensity()))
				line += self.getDelimiter() + self.getDelimiter().join(values)
				values = []
				for c in range(self.getChannelCount()):
					# Background
					m = measurements[c][line_idx]
					values.append("NA" if not self.isDigit(m.getBackground()) else "{:0.4f}".format(m.getBackground()))
				line += self.getDelimiter() + self.getDelimiter().join(values)
				dest.write(line + "\n")
			result = True
		return result

	def readVariables(self):
		"""
		@see: AbstractCSVExport.readVariables
		"""
		result = {}
		# result[ var ] = ( time , data )
		with open(self.getFilePath()) as src:
			lines = list(src)
			# first cell at first row = start date
			self._t_zero = dt.datetime.strptime(lines[0].split(";")[0], self.DT_FORMAT)
			# rest columns -> variable names
			names = lines[0][:-1].split(";")[1:]
			for name in names:
				result[name] = (np.zeros(len(lines) - 1), np.zeros(len(lines) - 1))  # init with empty list
			n = 0
			for line in range(1, len(lines)):
				# for each line: add data to the appropriate name
				line_data = lines[line][:-1].split(";")
				time = float(line_data[0].replace(',', '.'))
				# init data with zeros
				for idx in range(0, len(names)):
					# allocate the data
					name = names[idx]
					result[name][0][n] = time
					value = 0
					if len(line_data) > idx and self.isDigit(line_data[idx + 1]):
						value = float(line_data[idx + 1].replace(',', '.'))
					result[name][1][n] = value
				n += 1
		return result
