# coding=utf-8
#!/usr/bin/env python
"""
MongoDB Handler
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from uvaCultivatorExport import AbstractCultivatorExport as aCE
from datetime import datetime as dt, timedelta as td
import re, copy
import uvaMongoDBModel as uMDBM
#from uvaODMeasurement import ODMeasurement as ODM
from os import path as OP
from abc import *
import ming
from ming.odm import ThreadLocalODMSession

class AbstractMongoExport(aCE):

	# URL_TEST is a regex pattern that can process: [<protocol>://][<user>:<password>@]<server>[:<port>][/<database>]
	URL_TEST = re.compile('^(?:([^:/]+)://)?(?:([^:]+):([^@]+)@)?([^@:/?#]+)(?::([0-9]+))?/?(?:([^/]+))?')

	_default_settings = aCE.mergeDictionary(
		aCE._default_settings,
		{
		"project_id": "",
		"url" : None,
		"database" : "admin",
		"time_zero": None,
		"username" : "",
		"password" : "",
		})

	def __init__(self, settings=None):
		self._settings = settings
		self._session = None
		self._doc_session = None
		self._default_url_parts = [
			"mim", "", "", "localhost", "27017", self.retrieveSetting(name="database", default=self._default_settings)
		]
		self._url = self.retrieveSetting(name="url", default=self._default_settings)
		self._url, self._url_parts = self.parseURL(self._url)
		print "Will connect to {}".format(self._url)
		aCE.__init__(self, settings)
		# setup URL
		# self._config = None


	@abstractmethod
	def getTimeZero(self):
		raise NotImplementedError

	@abstractmethod
	def setTimeZero(self, t_zero, write=False):
		raise NotImplementedError

	@abstractmethod
	def addODMeasurements(self, m):
		raise NotImplementedError

	@abstractmethod
	def readODMeasurements(self, start_time=None, end_time=None):
		raise NotImplementedError

	def hasStorage(self):
		return self._session is not None

	def createStorage(self, destination=None, replace=False):
		"""
		Creates a MongoDB session and connects it to the database
		:return: True on success, False on failure
		:rtype: bool
		"""
		try:
			if destination is not None and self.isValidURL(destination):
				self._url, self._url_parts = self.parseURL(destination)
			config = {'ming.cultivator.uri': self._url}
			self._config = ming.configure(**config)
			self._doc_session = uMDBM.doc_session
			self._session = uMDBM.session
		except:
			self._doc_session = ming.Session.by_name('cultivator')
			self._session = ThreadLocalODMSession(doc_session=self._doc_session)
		return self._session

	def getSession(self):
		"""
		:return:
		:rtype: ming.odm.ThreadLocalODMSession
		"""
		return self._session

	def getDatabaseName(self):
		return self.retrieveSetting(name="database", default=self._default_settings)

	def isValidURL(self, url):
		return len(self.URL_TEST.findall(url)) > 0

	def parseURL(self, url, default_parts=None):
		if default_parts is None:
			default_parts = self._default_url_parts
		given_parts = []
		if url is not None:
			given_parts = self.URL_TEST.findall(url)
		url_parts = []
		if len(given_parts) > 0:
			for i in range(len(given_parts[0])):
				url_parts.append(default_parts[i] if given_parts[0][i] == "" else given_parts[0][i])
		else:
			url_parts = copy.copy(default_parts)
		url_auth = ""
		if "" not in [ url_parts[1], url_parts[2] ]:
			url_auth = "{}:{}@".format(url_parts[1], url_parts[2])
		url = "{}://{}{}:{}/{}".format(url_parts[0], url_auth, url_parts[3], url_parts[4], url_parts[5])
		return (url, url_parts)

	def getDatabaseURL(self):
		return self._url

	def getDatabaseProtocol(self):
		# self._url = "{}://{}:{}/{}".format(self._url_parts[0], self._url_parts[1], self._url_parts[2], self._db_name)
		return self._url_parts[0]

	def getDatabaseUser(self):
		return self._url_parts[1]

	def getDatabasePassword(self):
		return self._url_parts[2]

	def getDatabaseServer(self):
		return self._url_parts[3]

	def getDatabasePort(self):
		return self._url_parts[4]

	def getMeasurementCount(self):
		return uMDBM.Measurement.findByAttributes(m_type=False).count()

	def sync(self):
		result = False
		try:
			uMDBM.session.flush()
			uMDBM.session.clear()
			result = True
		except:
			self.getLog().info("Unable to flush data to MongoDB")
		return result

class SimpleMongoExport(AbstractMongoExport):
	"""
	Simple mongodb to store measurements. Requires a project id to function.
	"""

	_default_settings = aCE.mergeDictionary(
		aCE._default_settings,
		{
		"database": "mc_production",
		})

	def __init__(self, **kwargs):
		self._experiment = None
		AbstractMongoExport.__init__(self, **kwargs)

	def hasStorage(self):
		"""

		:return:
		:rtype:
		"""
		return self._experiment is not None or self.createStorage() is not None

	def createStorage(self, destination=None, replace=False):
		"""
		Loads an experiment from the database or creates if it does not already exist
		:param destination: url
		:return:
		:rtype: uvaCultivator.uvaCultivatorExport.uvaMongoDBModel.Experiment
		"""
		try:
			AbstractMongoExport.createStorage(self, destination, replace)
			self._experiment = uMDBM.Experiment.loadExperiment(project_id=self.getProjectId(),
			                                                   t_zero=self.getTimeZero())
		except:
			pass
		return self._experiment

	def importFromCSV(self, fp=None):
		"""
		Read a CSV file generated with uvaCultivatorCSVHandler, parse it into OD and Light measurements and store it
		in the mongodb.
		:type fp: str
		:return: True on success, False otherwise
		:rtype: bool
		"""
		cwd = OP.dirname(OP.realpath(__file__))
		uMDBM.session.flush()
		uMDBM.session.clear()
		if fp is not None:
			path = OP.realpath("{}/{}".format(cwd, fp) if fp[:1] != "/" else fp)
			if path is not None and OP.exists(path) and OP.isfile(path):
				len(self.readPortraitCSV(path)) > 0
			else:
				self.getLog().info("Unable to read {}".format(path))
		return self.sync()

	def readPortraitCSV(self, csv):
		"""
		Reads a Portrait formatted CSV file, parses the measurements into OD and light measurements
		:type csv: str
		:return:
		:rtype:
		"""
		result = []
		# result[ch] = {time: {"od": od, "light": light}}
		with open(csv) as src:
			lines = list(src)
			# first cell at first row = start date
			t_zero = dt.strptime(lines[0].split("\n")[0].split(";")[0], self.DT_FORMAT)
			self.getLog().info("CSV has a start date of: {}".format(t_zero.strftime(self.DT_FORMAT)))
			t_unit = 60 # seconds
			if not self.hasStorage():
				self.createStorage()
			# rest columns -> variable names
			names = lines[0][:-1].split(";")[1:]
			channel_count = 0
			for name in names:
				if name[:2] == "OD":
					channel_count += 1
			variable_count = len(names) / channel_count
			n = 0
			for line in range(1, len(lines)):
				time = float(lines[line][:-1].split(";")[0].replace(',', '.'))
				t_abs = t_zero + td(seconds=t_unit*time)
				data = [(float(i.replace(',', '.')) if self.isDigit(i) else 0)
				        for i in lines[line][:-1].split(";")[1:]]
				for ch in range(channel_count):
					result.append({})
					od_measurement = uMDBM.ODMeasurement.loadMeasurement(experiment=self._experiment, channel=ch,
					                                           time_point=t_abs)
					light_measurement = uMDBM.LightMeasurement.loadMeasurement(experiment=self._experiment, channel=ch,
					                                                time_point=t_abs)
					if None in [od_measurement, light_measurement]:
						if od_measurement is None:
							od_measurement = uMDBM.ODMeasurement(experiment=self._experiment, channel=ch,
							                                     time_point=t_abs)
						if light_measurement is None:
							light_measurement = uMDBM.LightMeasurement(experiment=self._experiment, channel=ch,
						                                           time_point=t_abs)
						for idx in range(variable_count):
							col = idx * ch + ch
							if names[col][:2].lower() == "OD".lower():
								od_measurement.setRawOD(data[col])
							if names[col][:2] == "In":
								light_measurement.setEmittedLight(data[col])
								if data[col] > 0:
									light_measurement.setLightState(True)
							if names[col][:2] == "Ba":
								od_measurement.setODBackground(data[col])
								light_measurement.setBackgroundLight(data[col])
							if names[col][:2] == "PI":
								light_measurement.setTotalLight(data[col])
							if names[col][:2] == "RA":
								od_measurement.setRawOD(data[col])
					result[ch][t_abs] = {"light": light_measurement, "od": od_measurement}
				# end of for ch
			# end of for line
		# end of with
		return result

	def getExperiment(self):
		"""
		:return:
		:rtype: uvaCultivator.uvaCultivatorExport.uvaMongoDBModel.Experiment
		"""
		return self._experiment

	def getProjectId(self):
		"""
		Return the currently used project_id
		:rtype: str
		"""
		return self.retrieveSetting(name="project_id", default=self._default_settings)

	def getTimeZero(self):
		"""
		Return the Time Zero of the currently used project id
		:rtype: datetime.datetime
		"""
		result = self.retrieveSetting(name="time_zero", default=self._default_settings)
		if self.hasStorage():
			result = self.getExperiment().getTimeZero()
		return result

	def setTimeZero(self, t_zero, write=False):
		"""
		Set the time zero of the currently used project id
		:param t_zero: The new t_zero
		:type t_zero: datetime.datetime
		:param write: Whether to store this change to the database
		:type write: bool
		:rtype: SimpleMongoDBHandler
		"""
		self.getExperiment().t_zero = t_zero
		if write:
			self.getExperiment().store(self._session)
			self.sync()
		return self

	def readODMeasurements(self, start_time=None, end_time=None):
		"""
		Read all measurements and return them as ODMeasurement Objects
		:return: dict of ODMeasurement Objects { channel : {time : m}}
		:rtype: dict
		"""

	def addODMeasurements(self, measurements):
		"""
		Add a list of ODMeasurements to the file
		Expected format: m[]
		:param measurements: Dictionary of channels with lists of OD Measurements to add
		:type measurements: dict[int, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]
		:rtype: bool
		"""
		result = {}
		for channel in measurements.keys():
			result[channel] = {}
			for measurement in measurements[channel]:
				t_abs = measurement.getTimePoint()
				od_measurement = uMDBM.ODMeasurement.loadMeasurement(experiment=self._experiment, channel=channel,
			                                                     time_point=t_abs)
				light_measurement = uMDBM.LightMeasurement.loadMeasurement(experiment=self._experiment, channel=channel,
			                                                           time_point=t_abs)
				if None in [od_measurement, light_measurement]:
					if od_measurement is None:
						od_measurement = uMDBM.ODMeasurement(experiment=self._experiment, channel=channel,
						                                     time_point=t_abs)
					if light_measurement is None:
						light_measurement = uMDBM.LightMeasurement(experiment=self._experiment, channel=channel,
						                                           time_point=t_abs)
					# set all od measurement parameters
					od_measurement.setRawOD(measurement.getODValue())
					od_measurement.setCalibratedOD(measurement.getCalibratedODValue())
					od_measurement.setODBackground(measurement.getBackground())
					od_measurement.setODLED(measurement.getLED())
					od_measurement.setODFlash(measurement.getFlash())
					# set all light parameters
					light_measurement.setBackgroundLight(measurement.getBackground())
					light_measurement.setEmittedLight(measurement.getIntensity())
					light_measurement.setLightState(measurement.getIntensity() == 0)
					result[channel][t_abs] = {"light": light_measurement, "od": od_measurement}
			# end of for measurement
		# end of for channel
		return self.sync()


	def getMeasurements(self, t_point=None, channel=None ):
		"""
		Get all measurments linked to the current Experiment and that satisfies the given criteria. When a criterium
		is set to None, it's omitted.
		:param t_point:
		:type t_point: datetime.datetime
		:param channel:
		:type channel: int
		:return:
		:rtype: uvaMongoDBModel.Measurement
		"""
		return self.getExperiment().retrieveByAttributes(self.getProjectId())

	def getODMeasurement(self, t_point, channel):
		"""

		:param t_point:
		:type t_point:
		:param channel:
		:type channel:
		:return:
		:rtype: uvaMongoDBModel.ODMeasurement
		"""
		return uMDBM.ODMeasurement.findByAttributes( self.getExperiment().getId(), t_point, channel).first()

	def getODMeasurementByTime(self, t_point, find_closest=False):
		"""
		Find the OD Measurement that
		:param t_point:
		:type t_point:
		:param find_closest: Tries to find a measurment that is closest to this measurement if there is no measurment
		:type find_closest: bool
		:return:
		:rtype: list
		"""
		if find_closest:
			# search in 5 minutes range: 2.5 minute before and 2.5 minute after
			measurements = uMDBM.ODMeasurement.retrieveByTime(experiment=self.getExperiment(), time_point=t_point,
			                                            max_distance=2.5*60)
			result = []
			min_dist = None
			for measurement in measurements:
				if t_point > measurement.getTime():
					distance = t_point - measurement.getTime()
				else:
					distance = measurement.getTime() - t_point
				if min_dist is None or min_dist > distance:
					min_dist = distance
					result = [measurement]
				elif min_dist == distance:
					result.append(measurement)
		else:
			result = [uMDBM.ODMeasurement.retrieveByTime(experiment=self.getExperiment(), time_point=t_point)]
		return result
