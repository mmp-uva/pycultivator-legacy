#!/usr/bin/env python
"""Module providing a sql platform"""

from uvaCultivatorExport import AbstractCultivatorExport
from uvaCultivator.uvaODMeasurement import ODMeasurement
from datetime import datetime as dt, timedelta as td
from abc import abstractmethod

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SQLCultivatorExport(AbstractCultivatorExport):
    """Base class for SQL exporters"""

    EXPERIMENT_COLUMNS = [
        "id", "project_id", "t_zero", "t_end" "state"
    ]

    MEASUREMENT_COLUMNS = [
        "id", "experiment_id", "channel_id", "time",
        "od_led", "od_value", "od_raw", "od_repeats",
        "intensity", "flash", "background", "temperature"
    ]

    LIGHT_COLUMNS = [
        'id', 'experiment_id', 'channel_id',
        "time", "intensity", "state"
    ]

    DECISION_COLUMNS = [
        "id", "experiment_id", "channel_id", "measurement_id",
        "time", "decision", "od_value"
    ]

    SQL_DT_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

    _default_settings = AbstractCultivatorExport.mergeDictionary(
        AbstractCultivatorExport._default_settings,
        {
            "project_id": "",
            "t_zero": None,
            "active": False,
            "database": "",
        })

    def __init__(self, settings=None):
        """Initialize the exporter"""
        AbstractCultivatorExport.__init__(self, settings, check_storage=False)
        # Connection to the database
        self._connection = None
        # experiment record
        self._experiment = None

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.isConnected():
            self.disconnect()

    def __del__(self):
        if self.isConnected():
            self.disconnect()

    # connection

    def createStorage(self, destination=None, replace=False):
        """Initializes the connection and creates the schema if necessary

        :type destination: str or None
        :type replace: bool
        :return: True on success, False on failure
        :rtype: bool
        """
        if self.connect():
            self.createSchema()
        return self.hasStorage()

    @abstractmethod
    def connect(self, path=None):
        """Connects to the database"""
        raise NotImplementedError

    @abstractmethod
    def disconnect(self):
        """Disconnects from the database"""
        raise NotImplementedError

    # Table generation

    def createSchema(self):
        """Creates all the tables

        :rtype: bool
        """
        return self.createExperimentTable() and self.createMeasurementTable()

    @abstractmethod
    def createExperimentTable(self):
        """Creates the experiment table

        :rtype: bool
        """
        raise NotImplementedError

    def createMeasurementTable(self):
        """Creates the measurement table

        :rtype: bool
        """
        raise NotImplementedError

    def createTurbidostatTable(self):
        """Creates the turbidostat table

        :rtype: bool
        """
        raise NotImplementedError

    def createLightTable(self):
        """Create the light intensity table

        :rtype: bool
        """
        raise NotImplementedError

    # storage

    def readODMeasurements(self, start_time=None, end_time=None, criteria=None):
        """Read measurements and return them as a dictionary of ODMeasurement Objects. By channel, then by time

        :return: Dictionary of ODMeasurement Objects { channel : {time : m}}
        :rtype: dict[int, dict[datetime.datetime, uvaCultivator.uvaODMeasurement.ODMeasurement]]
        """
        results = {}
        if not self.hasConnection():
            # connect to database
            self.connect()
            # create schema
            self.createSchema()
        if criteria is None:
            criteria = {}
        # get experiment (if not connected, will return None)
        experiment = self.getExperiment()
        if experiment is not None:
            criteria["experiment_id"] = experiment["id"]
            # make a list of criteria
            criteria_list = self.criteriaToList(**criteria)
            # add the period criteria
            criteria_list.extend(self.periodToCriteria(start_time=start_time, end_time=end_time))
            # get records
            records = self.findMeasurementRecords(criteria=criteria_list)
            if len(records) > 0:
                # process to OD Measurements
                for record in records:
                    m = self.recordToMeasurement(record=record)
                    channel_id = int(record["channel_id"])
                    if channel_id in results.keys():
                        if m.getTimePoint() in results[channel_id].keys():
                            results[channel_id][m.getTimePoint()].append(m)
                        else:
                            results[channel_id][m.getTimePoint()] = [m]
                    else:
                        results[channel_id] = {m.getTimePoint(): m}
                    # end for
                    # end if len
        return results

    def addODMeasurements(self, measurements):
        """Export a dictionary of new Measurements grouped by channel

        :param measurements: Dictionary of channels with lists of OD Measurements to add { channel : [m] }
        :type measurements: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        :rtype: bool
        """
        result = False
        if not self.hasConnection():
            # connect to database
            self.connect()
            # create schema
            self.createSchema()
        # gets the experiment, if not available will search for it and if not created creates one.
        experiment_record = self.getExperiment(force=True)
        if experiment_record is not None:
            experiment = self.experimentRecordToDict(experiment_record)
            result = True
            for measurement in measurements:
                channel = measurement["channel"]
                # create a dictionary from the measurement
                m = self.measurementToDict(measurement)
                # create the record
                result = self.createMeasurementRecord(
                    experiment_id=experiment["id"], channel_id=channel, **m
                ) is not None and result
        return result

    def readTurbidostatDecisions(self, start_time=None, end_time=None):
        pass

    def addTurbidostatDecisions(self, decisions):
        """Write decisions to the storage

        :type decisions: dict[int, dict[str, datetime.datetime, bool, float]]
        """
        result = False
        if not self.hasConnection():
            # connect to database
            self.connect()
            # create schema
            self.createSchema()
            # create turbidostat
            self.createTurbidostatTable()
        experiment_record = self.getExperiment(force=True)
        if experiment_record is not None:
            experiment = self.experimentRecordToDict(experiment_record)
            result = True
            for channel in decisions.keys():
                decision_record = decisions[channel]
                decision = {}
                for k, v in decision_record.items():
                    if k in self.DECISION_COLUMNS:
                        decision[k] = v
                # create the record
                result = self.createTurbidostatRecord(
                    experiment_id=experiment["id"], channel_id=channel, **decision
                ) is not None and result
        return result

    def readLightRecords(self, start_time=None, end_time=None):
        """Read light intensity records and return them as dictionary

        :return: Dictionary of tuples: { channel : (datetime, intensity, state) }
        :rtype: dict[int, tuple[datetime.datetime, float, bool]]
        """
        pass

    def addLightRecords(self, intensities):
        """Write intensities to the storage

        :param intensities: Dictionary of tuples: { channel : (datetime, intensity, state) }
        :type intensities: dict[int, tuple[datetime.datetime, float, bool]
        """
        result = False
        if not self.hasConnection():
            self.connect()
            self.createSchema()
            self.createLightTable()
        experiment_record = self.getExperiment(force=True)
        if experiment_record is not None:
            experiment = self.experimentRecordToDict(experiment_record)
            result = True
            for channel in intensities.keys():
                for intensity_record in intensities[channel]:
                    attributes = self.lightToDict(intensity_record)
                    # create the record
                    result = self.createLightRecord(
                        experiment_id=experiment["id"], channel_id=channel, **attributes
                    ) is not None and result
        return result

    # records

    @abstractmethod
    def createExperimentRecord(self, project_id=None, t_zero=None, state=None):
        """Creates an experiment record in the database and returns the record

        :rtype: object
        """
        raise NotImplementedError

    def updateExperimentRecord(self, experiment_id, project_id=None, t_zero=None, state=None):
        """Will update the experiment record or create it does not exist yet"""
        raise NotImplementedError

    def findExperimentRecords(self, project_id=None, t_zero=None, state=None, fill=False):
        """Finds all experiment records matching the given criteria.

        :param fill: Whether to fill the empty parameters with the known parameters from this object
        :type fill: bool
        :rtype: list
        """
        raise NotImplementedError

    @abstractmethod
    def findExperimentRecord(self, experiment_id=None, project_id=None, t_zero=None, state=None, fill=False):
        """Finds the first experiment record matching the given criteria. Result may be stored in _experiment.

        :param fill: Whether to fill the empty parameters with the known parameters from this object
        :type fill: bool
        :rtype: object
        """
        raise NotImplementedError

    @abstractmethod
    def createMeasurementRecord(self, experiment_id, channel_id, measurement=None, **attributes):
        """Creates a Measurement record"""
        raise NotImplementedError

    @abstractmethod
    def updateMeasurementRecord(self, measurement_id, experiment_id=None, channel_id=None, measurement=None,
                                **attributes):
        """Updates the measurement record in the database"""
        raise NotImplementedError

    @abstractmethod
    def findMeasurementRecords(self, measurement=None, criteria=None, **attributes):
        """Finds all measurement records matching the given criteria.

        :rtype: list
        """

    @abstractmethod
    def findMeasurementRecord(self, measurement=None, criteria=None, **attributes):
        """Finds the first measurement with the given criteria."""
        raise NotImplementedError

    @abstractmethod
    def countMeasurementRecords(self, measurement=None, criteria=None, **attributes):
        """Counts all the measurements with the given criteria"""
        raise NotImplementedError

    # turbidostat records

    @abstractmethod
    def createTurbidostatRecord(self, experiment_id, channel_id, decision=None, **attributes):
        """Creates a Measurement record"""
        raise NotImplementedError

    @abstractmethod
    def updateTurbidostatRecord(self, decision_id, experiment_id=None, channel_id=None, decision=None,
                                **attributes):
        """Updates the measurement record in the database"""
        raise NotImplementedError

    @abstractmethod
    def findTurbidostatRecords(self, decision=None, criteria=None, **attributes):
        """Finds all measurement records matching the given criteria.

        :rtype: list
        """

    @abstractmethod
    def findTurbidostatRecord(self, decision=None, criteria=None, **attributes):
        """Finds the first measurement with the given criteria."""
        raise NotImplementedError

    @abstractmethod
    def countTurbidostatRecords(self, decision=None, criteria=None, **attributes):
        """Counts all the measurements with the given criteria"""
        raise NotImplementedError

    # light intensity records

    @abstractmethod
    def createLightRecord(self, experiment_id, channel_id, **attributes):
        """Creates a Measurement record"""
        raise NotImplementedError

    @abstractmethod
    def updateLightRecord(self, record_id, experiment_id=None, channel_id=None, **attributes):
        """Updates the measurement record in the database"""
        raise NotImplementedError

    @abstractmethod
    def findLightRecords(self, light, criteria=None, **attributes):
        """Finds all measurement records matching the given criteria.

        :rtype: list
        """

    @abstractmethod
    def findLightRecord(self, light, criteria=None, **attributes):
        """Finds the first measurement with the given criteria."""
        raise NotImplementedError

    @abstractmethod
    def countLightRecords(self, light, criteria=None, **attributes):
        """Counts all the light intensity records matching the given criteria"""
        raise NotImplementedError

    # converting records

    @abstractmethod
    def recordToDict(self, record):
        raise NotImplementedError

    @abstractmethod
    def experimentRecordToDict(self, record):
        """Converts an experiment record to a dictionary

        :type record: object
        :rtype: dict[str, object]
        """
        raise NotImplementedError

    @abstractmethod
    def dictToExperimentRecord(self, **attributes):
        """Converts a dictionary to an experiment record

        :type attributes: dict
        :rtype: object
        """
        raise NotImplementedError

    def experimentToDict(self, experiment_id=None, project_id=None, t_zero=None, state=None, fill=False):
        """Creates an experiment dictionary from arguments and fills the gaps with """
        result = {}
        if experiment_id is not None:
            result["id"] = experiment_id
        if fill and project_id is None:
            project_id = self.getProjectId()
        if project_id is not None:
            result["project_id"] = project_id
        if fill and t_zero is None:
            t_zero = self.getTimeZero()
        if t_zero is not None:
            result["t_zero"] = t_zero
        if fill and state is None:
            state = self.isActive()
        if state is not None:
            result["state"] = state
        return result

    def measurementToDict(self, measurement):
        """Converts an OD Measurements to a dictionary

        :type measurement: uvaCultivator.uvaODMeasurement.ODMeasurement
        :rtype: dict
        """
        return {
            'time': measurement.getTimePoint(),
            'intensity': measurement.getIntensity(),
            'background': measurement.getBackground(),
            'flash': measurement.getFlash(),
            'od_led': measurement.getLED(asIndex=False),
            'od_repeats': measurement.getRepeats(),
            'od_value': measurement.getODValue(),
            'od_raw': measurement.getODRaw(),
            'temperature': measurement.getTemperature()
        }

    def dictToMeasurement(self, **attributes):
        """Converts a dictionary to an OD Measurement object (wrapper for ODMeasurement __init__)

        :type attributes: dict
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        for key in attributes:
            if key not in self.MEASUREMENT_COLUMNS:
                attributes.pop(key)
        return ODMeasurement(**attributes)

    @abstractmethod
    def measurementToRecord(self, measurement):
        """Converts an OD Measurement to a Record

        :type measurement: uvaCultivator.uvaODMeasurement.ODMeasurement
        :rtype: object
        """
        raise NotImplementedError

    @abstractmethod
    def recordToMeasurement(self, record):
        """Converts an record to OD Measurement

        :type record: object
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement
        """
        raise NotImplementedError

    def decisionToDict(self, d, **attributes):
        """Converts a decision object (plus any named attributes) to a dictionary

        :param decision:
        :type decision: tuple[ datetime, bool, float ]
        :return:
        :rtype: dict[str, object]
        """
        result = {'decision': 0}
        for variable in attributes.keys():
            if variable in self.DECISION_COLUMNS:
                result[variable] = attributes[variable]
        if d[0] is not None:
            result["measurement_id"] = d[0]
        if d[2] is not None:
            result["time"] = d[1]
        if d[3] is not None:
            result["decision"] = d[2]
        if d[4] is not None:
            result["od_value"] = d[3]
        return result

    def dictToDecision(self, **attributes):
        measurement_id = None
        if "measurement_id" in attributes.keys():
            measurement_id = attributes.get("measurement_id")
        t_point = None
        if "time" in attributes.keys():
            t_point = attributes.get("time")
        if "t_point" in attributes.keys():
            t_point = attributes.get("t_point")
        if isinstance(t_point, str):
            t_point = dt.strptime(t_point, self.SQL_DT_FORMAT)
        decision = attributes.get("decision", None)
        if isinstance(decision, str):
            decision = bool(decision)
        if isinstance(decision, int):
            decision = decision == 1
        od = attributes.get("od_value", None)
        return measurement_id, t_point, decision, od

    # convert turbidostat decisions

    @abstractmethod
    def decisionToRecord(self, decision):
        """Converts a Decision tuple to a Record

        :type decision: tuple[datetime.datetime, bool, float]
        """
        raise NotImplementedError

    @abstractmethod
    def recordToDecision(self, record):
        """Convert a turbidostat data record to a Decision tuple

        :param record: Record to be converted
        :rtype: tuple[datetime.datetime, bool, float]
        """
        raise NotImplementedError

    # convert light records

    def lightToDict(self, light, **attributes):
        """Convert a light intensity tuple to a dictionary

        :param light: Tuple with ( datetime, raw_intensity, intensity, state)
        :type light: tuple[datetime.datetime, float, float, bool]
        :rtype: dict[str, object]
        """
        result = { "state": 0}
        for variable in attributes.keys():
            if variable in self.LIGHT_COLUMNS:
                result[variable] = attributes[variable]
        if light[0] is not None:
            result["time"] = light[0]
        if light[1] is not None:
            result["intensity"] = light[1]
        if light[2] is not None:
            result["state"] = 1 if light[2] else 0
        # last check: if none use intensity
        if result["state"] is None:
            result["state"] = result["intensity"] > 0
        return result

    def dictToLight(self, **attributes):
        """Convert a dictionary to a light intensity tuple

        :return: Tuple of (time, intensity, state)
        :rtype: tuple[datetime.datetime, float, bool]
        """
        t_point = attributes.get("time", None)
        if isinstance(t_point, str):
            t_point = dt.strptime(t_point, self.SQL_DT_FORMAT)
        intensity = attributes.get("intensity", None)
        state = attributes.get("state", False)
        return t_point, intensity, state

    def lightToRecord(self, light):
        """Convert a light intensity tuple to a Record

        :type light: tuple[datetime.datetime, float, float, bool]
        """
        raise NotImplementedError

    def recordToLight(self, record):
        """Convert a record to a light intensity tuple

        :rtype: tuple[datetime.datetime, float, float, bool]
        """
        raise NotImplementedError

    # attributes

    def attributesToInsert(self, query, **attributes):
        """Extends an insert query with values from a dictionary of named attributes

        :rtype: str, dict[str, object]
        """
        fields, values = [], {}
        for name in attributes.keys():
            fields.append(name)
            values["i_{}".format(name)] = attributes[name]
        # base query: INSERT INTO table(id, project,...) VALUES (1,2,...)
        query = "{}({}) VALUES ({})".format(
            query, ",".join(fields), ",".join([":i_{}".format(field) for field in fields])
        )
        return query, values

    def attributesToUpdate(self, query, **attributes):
        """Adds the SET (column1=value1) clause to an UPDATE statement, does not add a WHERE clause!"""
        fields, values = [], {}
        for name in attributes.keys():
            fields.append(name)
            values["u_{}".format(name)] = attributes[name]
        # base query: UPDATE table SET (column1=value1, column2=value2, ...) WHERE criteria
        query = "{query} SET {fields}".format(**{
            "query": query, "fields": ",".join(
                ["{field}=:u_{field}".format(**{"field": field}) for field in fields]
            )
        })
        return query, values

    # criteria

    def criteriaToList(self, operators=None, operator="=", **criteria):
        """Creates a list of criteria by adding an operator to each key-value pair

        :param operators: A dictionary of operators, where each key corresponds with the key in criteria
        :param operator: The common operator to use for all criteria, or when no key is defined in operators
        :rtype: list[tuple[str, str, object]]
        """
        results = []
        if operators is None or not isinstance(operator, dict):
            operators = {key: operator for key in criteria.keys()}
        for idx, key in enumerate(criteria.keys()):
            o = operator
            if key in operators.keys():
                o = operators[key]
            if criteria[key] is not None:
                results.append((key, o, criteria[key]))
        return results

    def criteriaToWhere(self, query, criteria=None):
        """Extends a query with an where clause with the criteria

        Inspired by: http://stackoverflow.com/a/15496432

        Supported notations:
        * List or tuple, will translate [0][1][2] inot <field><operator><value>
        * Dictionary, assumes the existence of the keys: "field", "operator" and "value".

        :param query: The SQL Query that will be extended
        :param criteria: List of criteria
        :type criteria: list[ tuple | list | dict ]
        :return: The new query and a dictionary of all named variables
        :rtype: str, dict[str, object]
        """
        if criteria is None:
            criteria = []
        variables, values = [], {}
        for item in criteria:
            field, operator, value = None, None, None
            if isinstance(item, (tuple, list)):
                field = item[0]
                operator = item[1]
                value = item[2]
            elif isinstance(item, dict):
                if "field" in item.keys():
                    field = item["field"]
                if "operator" in item.keys():
                    operator = item["operator"]
                if "value" in item.keys():
                    value = item["value"]
            # add if we were able to parse it
            if None not in [field, operator, value]:
                variables.append("{} {} :c_{}".format(field, operator, field))
                values["c_{}".format(field)] = value
        if len(variables) > 0:
            query = '{} WHERE {}'.format(query, ' AND '.join(variables))
        return query, values

    @classmethod
    def periodToCriteria(cls, start_time=None, end_time=None):
        """Creates a list of criteria that filter for measurements, measured between start and end time.

        :param start_time:
        :type start_time: datetime.datetime or str
        :param end_time:
        :type end_time: datetime.datetime or str
        :return: A list of criteria in tuple notation
        :rtype: list
        """
        criteria = []
        if start_time is not None:
            t_start = start_time.strftime(cls.DT_FORMAT) if isinstance(start_time, dt) else start_time
            criteria.append(('time', '>=', t_start))
        if end_time is not None:
            t_end = end_time.strftime(cls.DT_FORMAT) if isinstance(end_time, dt) else end_time
            criteria.append(('time', '<=', t_end))
        return criteria

    # getters and setters

    def getExperimentRecord(self):
        """Returns the cached experiment record"""
        return self._experiment

    def hasExperimentRecord(self):
        """Returns whether the object has a cached experiment record"""
        return self._experiment is not None

    def getExperiment(self, force=False):
        """Returns the (cached) experiment record (associated with exporter via project_id, t_zero and state settings).

        If no experiment record is cached, the database is searched and the found experiment is stored in memory.
        :param force: If set the experiment record will be reloaded from the database (even when a cached version is
        available). If no experiment record could be found and was forced, create a new record.
        :type force: bool
        :return: The experiment record
        :rtype: sqlite3.Row
        """
        # if forced or if no cached record
        if force or not self.hasExperimentRecord():
            # search for record
            self._experiment = self.findExperimentRecord()
            # if forced and no record was found, create
            if force and not self.hasExperimentRecord():
                self._experiment = self.createExperimentRecord()
        return self.getExperimentRecord()

    def hasExperiment(self, force=False):
        """Returns whether this experiment has an experiment record associate.
        If no experiment record was found, it will try to load it.

        :param force: If set reloads (or creates) the experiment record from the database (passed to getExperiment)
        :type force: bool
        """
        return self.getExperiment(force) is not None

    def getProjectId(self):
        return self.retrieveSetting("project_id", default=self.getDefaultSettings())

    def setTimeZero(self, t_zero, write=False):
        """Set the Time zero of this experiment, if write will update the experiment record

        :param t_zero: The new t_zero
        :type t_zero: datetime.datetime
        :param write: Whether to store this change to the database
        :type write: bool
        :rtype: object
        """
        result = True
        if write:
            experiment_id = self.experimentRecordToDict(self.getExperiment(force=True)).get("id")
            if experiment_id is not None:
                self.updateExperimentRecord(experiment_id=experiment_id, t_zero=t_zero)
        if result:
            self.setSetting("t_zero", t_zero)
        return self.getTimeZero()

    def isActive(self):
        return self.retrieveSetting("active", default=self.getDefaultSettings()) is True

    def setActive(self, state):
        self.setSetting("active", state is True)
        return self

    @abstractmethod
    def getDestination(self):
        """Returns the destination (i.e. location) of the database"""
        raise NotImplementedError

    def getConnection(self):
        """Returns the connection object

        :rtype: sqlite3.Connection or None
        """
        return self._connection

    def hasConnection(self):
        return self.getConnection() is not None

    def isConnected(self):
        """Returns whether the exporter has a connection (same as hasConnection)"""
        return self.hasConnection()

    @abstractmethod
    def hasStorage(self):
        """Returns whether the given database location exists

        :rtype: bool
        """
        raise NotImplementedError
