#!/usr/bin/env python
"""Module implementing sqlite3 support"""

import os
import sqlite3
from datetime import datetime as dt

from sqlCultivatorExport import SQLCultivatorExport

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SQLiteCultivatorExport(SQLCultivatorExport):

    # SQLITE_MEMORY_STORAGE = 'memory'
    # SQLITE_FILE_STORAGE = 'file'

    _default_settings = SQLCultivatorExport.mergeDictionary(
        SQLCultivatorExport._default_settings,
        {
            "database": "",
            # "storage.type": SQLITE_FILE_STORAGE
        })

    def __init__(self, settings=None):
        """Initialize the exporter"""
        super(SQLiteCultivatorExport, self).__init__(settings=settings)

    # connection

    def connect(self, path=None):
        """Connects to the database"""
        if self.hasConnection():
            self.disconnect()
        if path is None:
            path = self.getDestination()
        if not self.hasConnection():
            self._connection = sqlite3.connect(path)
            self._connection.row_factory = sqlite3.Row
        return self.hasConnection()

    def createStorage(self, destination=None, replace=False):
        """Initializes the connection and creates the schema if necessary

        :type destination: str or None
        :type replace: bool
        :return: True on success, False on failure
        :rtype: bool
        """
        if self.hasConnection():
            self.createSchema()
        return self.hasStorage()

    def disconnect(self):
        """Disconnects from the database"""
        if self.hasConnection():
            self.getConnection().close()

    # Table generation

    def createLightTable(self):
        """Creates the light intensity table"""
        result = False
        cursor = self.getCursor()
        if cursor is not None:
            try:
                sql = """CREATE TABLE IF NOT EXISTS light(
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                experiment_id INTEGER NOT NULL,
                channel_id INTEGER NOT NULL,
                time TIMESTAMP NOT NULL,
                intensity REAL,
                state INTEGER NOT NULL
                )"""
                cursor.execute(sql)
                result = True
            except sqlite3.Error as e:
                self.getLog().critical("Unable to create Light Intensity Table: %s" % e)
        return result

    def createTurbidostatTable(self):
        """Creates the turbidostat table"""
        result = False
        cursor = self.getCursor()
        if cursor is not None:
            try:
                sql = """CREATE TABLE IF NOT EXISTS turbidostat(
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                experiment_id INTEGER NOT NULL,
                channel_id INTEGER NOT NULL,
                measurement_id INTEGER,
                time TIMESTAMP NOT NULL,
                decision INTEGER NOT NULL,
                od_value REAL
                )"""
                cursor.execute(sql)
                result = True
            except sqlite3.Error as e:
                self.getLog().critical("Unable to create Turbidostat Table: %s" % e)
        return result

    def createMeasurementTable(self):
        """Creates the measurement table"""
        result = False
        cursor = self.getCursor()
        if cursor is not None:
            try:
                sql = """CREATE TABLE IF NOT EXISTS measurement(
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                experiment_id INTEGER NOT NULL,
                channel_id INTEGER NOT NULL,
                time TIMESTAMP NOT NULL,
                od_led INTEGER NOT NULL,
                od_value REAL,
                od_raw REAL,
                od_repeats INTEGER,
                intensity REAL,
                flash REAL,
                background REAL,
                temperature REAL)"""
                cursor.execute(sql)
                result = True
            except sqlite3.Error as e:
                self.getLog().critical("Unable to create Measurement Table: %s" % e)
        return result

    def createExperimentTable(self):
        """Creates the experiment table"""
        result = False
        cursor = self.getCursor()
        if cursor is not None:
            try:
                sql = """CREATE TABLE IF NOT EXISTS experiment(
						id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
						project_id TEXT NOT NULL,
						t_zero TIMESTAMP NOT NULL,
						t_end TIMESTAMP,
						state INTEGER NOT NULL)"""
                cursor.execute(sql)
                result = True
            except sqlite3.Error as e:
                self.getLog().critical("Unable to create Measurement Table: %s" % e)
        return result

    # experiment records

    def createExperimentRecord(self, project_id=None, t_zero=None, state=None):
        """Creates an experiment record in the database"""
        result = None
        if project_id is None:
            project_id = self.getProjectId()
        if t_zero is None:
            t_zero = self.getTimeZero()
        if state is None:
            state = self.isActive()
        if self.hasConnection():
            # insert
            query, data = self.attributesToInsert(
                "INSERT INTO experiment", **{"project_id": project_id, "t_zero": t_zero, "state": 1 if state else 0}
            )
            self.getConnection().execute(query, data)
            self.getConnection().commit()
            result = self.findExperimentRecord(project_id=project_id, t_zero=t_zero, state=state)
        return result

    def updateExperimentRecord(self, experiment_id, project_id=None, t_zero=None, state=None):
        """Updates the experiment record"""
        result = None
        attributes = self.experimentToDict(
            experiment_id=experiment_id, project_id=project_id, t_zero=t_zero, state=state, fill=False
        )
        if self.hasConnection():
            # update
            query, update_values = self.attributesToUpdate(
                "UPDATE experiment", **attributes
            )
            query, where_values = self.criteriaToWhere(
                query, criteria=[("id", "=", experiment_id)]
            )
            self.getConnection().execute(query, update_values.update(where_values))
            self.getConnection().commit()
            result = self.findExperimentRecord(
                experiment_id=experiment_id, project_id=project_id, t_zero=t_zero, state=state
            )
        return result

    def findExperimentRecords(self, experiment_id=None, project_id=None, t_zero=None, state=None, fill=False):
        """Finds all experiment records matching the given criteria.

        :param fill: Whether to fill the empty parameters with the known parameters from this object
        :type fill: bool
        :rtype: list[sqlite3.Row]
        """
        results = []
        if self.hasConnection():
            if fill and project_id is None:
                project_id = self.getProjectId()
            if fill and t_zero is None:
                t_zero = self.getTimeZero()
            if fill and state is None:
                state = self.isActive()
            if state is not None:
                state = 1 if state else 0
            query, data = self.criteriaToWhere(
                "SELECT * FROM experiment", self.criteriaToList(
                    **{"id": experiment_id, "project_id": project_id, "t_zero": t_zero, "state": state}
                )
            )
            results = self.getConnection().execute(query, data).fetchall()
        return results

    def findExperimentRecord(self, experiment_id=None, project_id=None, t_zero=None, state=None, fill=False):
        """Finds the first experiment record matching the given criteria. Result may be stored in _experiment.

        :param fill: Whether to fill the empty parameters with the known parameters from this object
        :type fill: bool
        :rtype: object
        """
        results = self.findExperimentRecords(
            experiment_id=experiment_id, project_id=project_id, t_zero=t_zero, state=state, fill=fill
        )
        if len(results) > 0:
            self._experiment = results[0]
        return self._experiment

    # measurement records

    def createMeasurementRecord(self, experiment_id, channel_id, measurement=None, **attributes):
        """Creates a Measurement record"""
        result = None
        # add experiment id
        attributes["experiment_id"] = experiment_id
        attributes["channel_id"] = channel_id
        # add measurement criteria
        if measurement is not None:
            attributes.update(self.measurementToDict(measurement))
        query, data = self.attributesToInsert("INSERT INTO measurement", **attributes)
        if self.hasConnection():
            self.getConnection().execute(query, data)
            self.getConnection().commit()
            result = self.findMeasurementRecord(measurement=measurement, **attributes)
        return result

    def updateMeasurementRecord(self, measurement_id, experiment_id=None, channel_id=None, measurement=None,
                                **attributes):
        """Updates a Measurement record"""
        result = None
        if self.hasConnection():
            if measurement is not None:
                attributes.update(self.measurementToDict(measurement))
            attributes["experiment_id"] = experiment_id
            attributes["channel_id"] = channel_id
            # update
            query, update_values = self.attributesToUpdate(
                "UPDATE experiment", **attributes
            )
            query, where_values = self.criteriaToWhere(
                query, criteria=[("id", "=", measurement_id)]
            )
            self.getConnection().execute(query, update_values.update(where_values))
            self.getConnection().commit()
            result = self.findMeasurementRecord(
                measurement=measurement, id=measurement_id, experiment_id=experiment_id, channel_id=channel_id
            )
        return result

    def findMeasurementRecords(self, measurement=None, criteria=None, **attributes):
        """Finds all measurement records that match the given criteria

        :type measurement: uvaCultivator.uvaODMeasurement.ODMeasurement or None
        """
        results = []
        if self.hasConnection():
            if criteria is None:
                criteria = []
            # add measurement to attributes
            if measurement is not None:
                attributes.update(self.measurementToDict(measurement))
            # make a criteria list from the attributes and add them to the criteria
            criteria.extend(self.criteriaToList(**attributes))
            query, data = self.criteriaToWhere("SELECT * FROM measurement", criteria=criteria)
            results = self.getConnection().execute(query, data).fetchall()
        return results

    def findMeasurementRecord(self, measurement=None, criteria=None, **attributes):
        """Loads a Measurement record from the database

        :type measurement: uvaCultivator.uvaODMeasurement.ODMeasurement or None
        """
        result = None
        results = self.findMeasurementRecords(measurement=measurement, criteria=criteria, **attributes)
        if len(results) > 0:
            result = results[0]
        return result

    def countMeasurementRecords(self, measurement=None, criteria=None, **attributes):
        """Counts the number of records"""
        return len(self.findMeasurementRecords(measurement=measurement, criteria=criteria, **attributes))

    # turbidostat records

    def createTurbidostatRecord(self, experiment_id, channel_id, **attributes):
        """Creates a Turbidostat record"""
        result = None
        # add experiment id
        attributes["experiment_id"] = experiment_id
        attributes["channel_id"] = channel_id
        query, data = self.attributesToInsert("INSERT INTO turbidostat", **attributes)
        if self.hasConnection():
            result = self.getConnection().execute(query, data)
            self.getConnection().commit()
            result = True
            # result = self.findMeasurementRecord(measurement=measurement, **attributes)
        return result

    def updateTurbidostatRecord(
            self, measurement_id, experiment_id=None, channel_id=None, decision=None, **attributes
    ):
        pass

    def findTurbidostatRecords(self, decision=None, criteria=None, **attributes):
        """Finds all measurement records that match the given criteria

        :type decision: None or tuple[datetime.datetime, bool, float]
        """
        results = []
        if self.hasConnection():
            if criteria is None:
                criteria = []
            # add measurement to attributes
            if decision is not None:
                attributes.update(self.decisionToDict(decision))
            # make a criteria list from the attributes and add them to the criteria
            criteria.extend(self.criteriaToList(**attributes))
            query, data = self.criteriaToWhere("SELECT * FROM turbidostat", criteria=criteria)
            results = self.getConnection().execute(query, data).fetchall()
        return results

    def findTurbidostatRecord(self, decision=None, criteria=None, **attributes):
        """Loads a Measurement record from the database

        :type decision: None or tuple
        """
        result = None
        results = self.findTurbidostatRecords(decision=decision, criteria=criteria, **attributes)
        if len(results) > 0:
            result = results[0]
        return result

    def countTurbidostatRecords(self, decision=None, criteria=None, **attributes):
        """Counts the number of records"""
        return len(self.findTurbidostatRecords(decision=decision, criteria=criteria, **attributes))

    def createLightRecord(self, experiment_id, channel_id, **attributes):
        """Creates a light intensity record"""
        result = None
        # add experiment id
        attributes["experiment_id"] = experiment_id
        attributes["channel_id"] = channel_id
        query, data = self.attributesToInsert("INSERT INTO light", **attributes)
        if self.hasConnection():
            result = self.getConnection().execute(query, data)
            self.getConnection().commit()
            # result = self.findMeasurementRecord(measurement=measurement, **attributes)
        return result

    def updateLightRecord(self, record_id, experiment_id=None, channel_id=None, **attributes):
        """Updates the light record in the database"""
        pass

    def findLightRecords(self, light, criteria=None, **attributes):
        """Finds all measurement records matching the given criteria.

        :rtype: list
        """
        results = []
        if self.hasConnection():
            if criteria is None:
                criteria = []
            # add measurement to attributes
            if light is not None:
                attributes.update(self.lightToDict(light))
            # make a criteria list from the attributes and add them to the criteria
            criteria.extend(self.criteriaToList(**attributes))
            query, data = self.criteriaToWhere("SELECT * FROM light", criteria=criteria)
            results = self.getConnection().execute(query, data).fetchall()
        return results

    def findLightRecord(self, light, criteria=None, **attributes):
        """Finds the first measurement with the given criteria."""
        result = None
        results = self.findLightRecords(light=light, criteria=criteria, **attributes)
        if len(results) > 0:
            result = results[0]
        return result

    def countLightRecords(self, light, criteria=None, **attributes):
        """Counts all the light intensity records matching the given criteria"""
        return len(self.findLightRecords(light=light, criteria=criteria, **attributes))

    # conversion

    # experiment record

    def dictToExperimentRecord(self, **attributes):
        """Converts a dictionary (given as keyword arguments) to an experiment record (dictionary)"""
        for attribute in attributes.keys():
            if attribute not in self.EXPERIMENT_COLUMNS:
                attributes.pop(attribute)
        return attributes

    def experimentRecordToDict(self, record):
        """Converts an experiment record to a dictionary

        :param record: An SQLite Row record of the experiment table
        :type record: sqlite3.Row
        :return: An dictionary object with all the experiment properties
        :rtype: dict[str, object]
        """
        return self.dictToExperimentRecord(**dict(record))

    def recordToMeasurementDict(self, record):
        """Transforms a SQLite record to a dictionary understandable by ODMeasurement

        :type record: sqlite3.Row
        """
        return dict(record)

    def recordToMeasurement(self, record):
        d_old = self.recordToMeasurementDict(record)
        d_new = {}
        # cast to str
        for k, v in d_old.items():
            if isinstance(k, unicode):
                k = str(k)
            if isinstance(v, unicode):
                v = str(v)
            if k == "time":
                v = dt.strptime(v, "%Y-%m-%d %H:%M:%S.%f")
            d_new[k] = v
        del d_old
        return self.dictToMeasurement(**d_new)

    def measurementToRecord(self, measurement):
        return self.measurementToDict(measurement=measurement)

    def recordToDict(self, record):
        return dict(record)

    def decisionToRecord(self, decision):
        return self.decisionToDict(decision)

    def recordToDecision(self, record):
        d = self.recordToDict(record)
        return self.dictToDecision(**d)

    def lightToRecord(self, light):
        """Convert a light intensity tuple to a Record

        :type light: tuple[datetime.datetime, float, float, bool]
        """
        return self.lightToDict(light)

    def recordToLight(self, record):
        """Convert a record to a light intensity tuple

        :rtype: tuple[datetime.datetime, float, float, bool]
        """
        d = self.recordToDict(record)
        return self.dictToLight(**d)

    # getters and setters

    def getDestination(self):
        """Returns the destination (i.e. location) of the database"""
        return self.retrieveSetting("database", default=self.getDefaultSettings())

    def getCursor(self):
        """Returns the cursor of the connection or None

        :rtype: sqlite3.Cursor or None
        """
        result = None
        if self.hasConnection():
            result = self.getConnection().cursor()
        return result

    def hasStorage(self):
        """Returns whether the given database location exists

        :rtype: bool
        """
        has_destination = self.getDestination() is not None
        has_destination = has_destination and os.path.exists(
            os.path.dirname(self.getDestination())
        )
        return has_destination
