# coding=utf-8
"""
Simplified version of the uvaCultivator OrientDB Wrapper
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import unittest
from uvaCultivator.uvaCultivatorExport.uvaOrientModel import *
import pyorient


class uvaOrientModelTest(uvaObject, unittest.TestCase):

    def __init__(self, methodName='runTest'):
        uvaObject.__init__(self, settings=None)
        self.getLog().addStreamHandler(level="INFO")
        unittest.TestCase.__init__(self, methodName=methodName)

    @classmethod
    def setUpClass(cls):
        """
        Prepares the testSerial class to testSerial on orient database
        :param cls:
        :type cls:
        :return:
        :rtype:
        """
        # Create variables
        cls._db_url = "memory://sils_user:#MC$@localhost/mc_test"
        cls._db_settings = {
            "project_id": "mc0_test",
            "time_zero": dt.now()
        }
        # Setup DB
        client = pyorient.OrientDB()
        try:
            session = client.connect("sils_test", "AFN394GNBIR@F#3V")
            if not client.db_exists("mc_test", pyorient.STORAGE_TYPE_MEMORY):
                client.db_create("mc_test", pyorient.DB_TYPE_GRAPH, pyorient.STORAGE_TYPE_MEMORY)
        except Exception as e:
            cls.getLog().critical("Unable to create mc_test:\n {}".format(e))
        finally:
            client.db_close()

    def setUp(self):
        self._db = orientDatabase(settings=self._db_settings)

    @classmethod
    def tearDownClass(cls):
        client = pyorient.OrientDB()
        try:
            session = client.connect("sils_test", "#AFN394GNBIR@F#3V")
            if client.db_exists("mc_test", pyorient.STORAGE_TYPE_MEMORY):
                client.db_drop("mc_test")
        except Exception as e:
            cls.getLog().critical("Unable to drop mc_test:\n {}".format(e))
        finally:
            client.db_close()

    def test_init(self):
        self.assertEquals(self._db.getSettings(), {
            "database": "mc_production",
            "password": "#MC$",
            "port": '2424',
            "project_id": self._db_settings["project_id"],
            "protocol": "plocal",
            'server': "localhost",
            "time_zero": self._db_settings["time_zero"],
            "url": "plocal://sils_user:#MC$@localhost:2424/mc_production",
            "username": "sils_user"
        })
        self.assertIsNone(self._db.getClient(), msg="Client is not None")
        self.assertIsNone(self._db.getSchema(), msg="Schema is not None")
        self.assertIsNone(self._db.getSession(), msg="Session is not None")
        self.assertEquals(self._db.getConnectionState(), self._db.STATE_UNCONNECTED)

    def test_connectHost(self):
        pass

    def test_connectDatabase(self):
        pass

    def test_connect(self):
        pass

    def test_disconnect(self):
        pass

    def test_isValidURL(self):
        self.assertTrue(self._db.isValidURL(self._db_url))
        self.assertTrue(self._db.isValidURL("orientdb://user:password@host:80/database"))
        self.assertTrue(self._db.isValidURL("user:password@host:80/database"))
        self.assertTrue(self._db.isValidURL("orientdb://host:80/database"))
        self.assertTrue(self._db.isValidURL("host:80/database"))
        self.assertTrue(self._db.isValidURL("host:80"))
        self.assertTrue(self._db.isValidURL("host/database"))
        self.assertFalse(self._db.isValidURL("://host/database"))
        self.assertFalse(self._db.isValidURL("user@host/database"))
        self.assertFalse(self._db.isValidURL("user:password/database"))
