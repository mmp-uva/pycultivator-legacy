"""Module providing general infrastructure for data serialization"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from datetime import datetime as dt
from pycultivator_legacy.core import BaseObject
from pycultivator_legacy.config import XMLConfig


class AbstractCultivatorExport(BaseObject):
    """
    Basic interface to a measurement exporting class
    """

    EXPORT_TYPE = "abstract_exporter"

    _default_settings = BaseObject.mergeDefaultSettings(
        {
            "t_zero": None,
        }, namespace=""
    )

    def __init__(self, settings=None, check_storage=True):
        super(AbstractCultivatorExport, self).__init__(settings=settings)
        if check_storage:
            self._has_storage = self.hasStorage()

    def __enter__(self):
        """Return the object when used in a with statement

        :return:
        :rtype:
        """
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Close any open connections when exiting from a with statement"""
        raise NotImplementedError

    @classmethod
    def parseValue(cls, value, v_type, default_value=None, date_format=None, digit_test=None):
        """Parse the value to v_type and check if this is possible

        :param value:
        :type value: object
        :param v_type:
        :type v_type: object
        :param default_value:
        :type default_value: object
        :return: value with the type of v_type
        :rtype: object
        """
        if date_format is None:
            date_format = cls.DT_FORMAT
        if digit_test is None:
            digit_test = cls.DIGIT_TEST
        return cls.validateValue(value, v_type, default_value, date_format, digit_test)

    @classmethod
    def getExportType(cls):
        """Return the type of this exporting class

        :return:
        :rtype:
        """
        return cls.EXPORT_TYPE

    @staticmethod
    def getExportByType(type, **kwargs):
        """Retrieve a handler corresponding to the desired type

        :param csv_type: Either LANDSCAPE_HANDLER or PORTRAIT_HANDLER
        :type csv_type: int
        :param path: Path to file to handle
        :type path: str
        :return: Reference to object
        :rtype: BasicCultivatorCSVHandler, CultivatorLandscapeCSVHandler, CultivatorPortraitCSVHandler, None
        """
        result = None
        if type == AbstractCultivatorExport.EXPORT_TYPE:
            # This is an ABSTRACT class so we don't know how to create an instance of this
            raise NotImplementedError
        return result

    def getTimeFormat(self):
        return self.DT_FORMAT

    def setTimeZero(self, t_zero, write=False):
        """Set the Time Zero of this export

        :type t_zero: datetime.datetime
        :type write: bool
        :rtype: datetime.datetime
        """
        if t_zero is dt:
            self._settings["t_zero"] = t_zero
        return self.getTimeZero()

    def hasStorage(self):
        """Checks whether the storage exists.

        :rtype: bool
        """
        return False

    def createStorage(self, destination=None, replace=False):
        """Creates the storage.

        :param destination: Reference to the storage
        :type destination: str or None
        :return:
        :rtype:
        """
        raise NotImplementedError

    def readODMeasurements(self, start_time=None, end_time=None):
        """Read measurements and return them as a dictionary of ODMeasurement Objects. By channel, then by time

        :return: Dictionary of ODMeasurement Objects { channel : {time : m}}
        :rtype: dict[int, dict[datetime.datetime, uvaCultivator.uvaODMeasurement.ODMeasurement]]
        """
        raise NotImplementedError

    def addODMeasurements(self, measurements):
        """Export a dictionary of new Measurements grouped by channel

        :param measurements: Dictionary of channels with lists of OD Measurements to add { channel : [m] }
        :type measurements: dict[int, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]
        :rtype: bool
        """
        raise NotImplementedError

    def readTurbidostatDecisions(self, start_time=None, end_time=None):
        """Read turbidostat decisions and return them as dictionary

        :return: A dictionary of tuples { channel : { time : (decision, od) } }
        :rtype: dict[int, dict[datetime.datetime, tuple[bool, int]]
        """
        raise NotImplementedError

    def addTurbidostatDecisions(self, decisions):
        """Write decisions to the storage

        :param decisions: Dictionary of tuples: { channel : (datetime, decision, OD Measurement) }
        :type decisions: dict[int, tuple[datetime.datetime, bool, uvaCultivator.uvaODMeasurement.ODMeasurement]]
        """
        raise NotImplemented

    def readLightRecords(self, start_time=None, end_time=None):
        """Read light intensity records and return them as dictionary

        :rtype: dict[int, tuple[datetime.datetime, float, bool]]
        """

    def addLightRecords(self, intensities):
        """Write light intensity records to the storage

        :param intensities: Dictionary of tuples: { channel : (datetime, intensity, state) }
        :type intensities: dict[int, tuple[datetime.datetime, float, bool]
        """
        raise NotImplemented

    def calculateRelativeTime(self, t_now, t_zero=None):
        """Calculates the relative time since t_zero as defined by the file

        :param t_now: Time to calculate the distance in seconds to t=0
        :type t_now: datetime.datetime
        :param t_zero: Time from which to calculate the distance, if None the t_zero of this object is used.
        :type t_zero: datetime.datetime or None
        :return: Returns the time since t=0 in minutes
        :rtype: int or None
        """
        result = None
        if t_zero is None:
            t_zero = self.getTimeZero()
        if t_zero is not None:
            # round to int
            result = int("{:0.0f}".format((t_now - t_zero).total_seconds() / 60))
        return result

    def getTimeZero(self):
        """Retrieve the experiment's start date and time as datetime object from the CSV File

        :return: DateTime object representing the start date and time of the experiment
        :rtype: datetime.datetime
        """
        return self.retrieveSetting(name="t_zero", default=self.getDefaultSettings())

    def getDestination(self):
        """Returns the location where the data is stored

        :return: Location of the data
        :rtype: str
        """
        raise NotImplementedError
