# coding=utf-8
"""
Simplified version of the uvaCultivator OrientDB Wrapper
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import pyorient
from pyorient.exceptions import *
from pycultivator_legacy.core import uvaObject
import re


class orientDatabase(uvaObject):

	# URL_TEST is a regex pattern that can process: [<protocol>://][<user>:<password>@]<server>[:<port>][/<database>]
	URL_TEST = re.compile('^(?:([^:/]+)://)?(?:([^:]+):([^@]+)@)?([^@:/?#]+)(?::([0-9]+))?/?(?:([^/]+))?')
	URL_FMT = "{protocol}://{auth}{host}/{database}"

	STATE_UNCONNECTED = 0
	STATE_HOST_CONNECTED = 1
	STATE_DB_CONNECTED = 2

	ORIENT_STORAGE = 'orientdb'  # for legacy
	ORIENT_MEMORY_STORAGE = 'memory'
	ORIENT_PLOCAL_STORAGE = 'plocal'
	ORIENT_STORAGE_MAP = {
		ORIENT_MEMORY_STORAGE: pyorient.STORAGE_TYPE_MEMORY,
		ORIENT_PLOCAL_STORAGE: pyorient.STORAGE_TYPE_PLOCAL,
		ORIENT_STORAGE: pyorient.STORAGE_TYPE_PLOCAL
	}
	ORIENT_DEFAULT_STORAGE = ORIENT_PLOCAL_STORAGE

	_default_settings = {
		"project_id": "",
		"time_zero": None,
	}

	_default_url_settings = {
		"protocol": ORIENT_PLOCAL_STORAGE,
		"username": "sils_user",
		"password": "#MC$",
		"server": "localhost",
		"port": 2424,
		"database": "mc_production"
	}

	def __init__(self, settings=None):
		"""
		Initialize the class. Use the settings dictionary to create the connection.
		If an URL is defined in the dictionary it will overrule all the url settings
		:param settings:
		:type settings:
		:return:
		:rtype:
		"""
		uvaObject.__init__(self, settings=settings)
		if "url" in self._settings.keys():
			# URL overrides other settings
			self.setURL(self._settings["url"])
		else:
			# Create URL from settings
			self.setURL(self.createURL())
		self._state = self.STATE_UNCONNECTED
		self._client = None
		self._session = None
		self._schema = None

	def getConnectionState(self):
		"""
		:return: The current connection state of this object
		:rtype: int
		"""
		return self._state

	def isConnected(self, min_state=STATE_HOST_CONNECTED):
		"""
		Check if the connection state is at least of the given level.
		:param min_state: The minimal state this object should have
		:type min_state: int
		:return: Whether the current state is equal our "higher" than the given state
		:rtype: bool
		"""
		return self.getConnectionState() >= min_state

	def isInState(self, state=STATE_DB_CONNECTED):
		"""
		:param state: The state this object should have
		:type state: int
		:return: Whether the current state is equal to the given state.
		:rtype: bool
		"""
		return self.getConnectionState() == state

	def connectToHost(self, server=None, port=None, host_user=None, host_password=None):
		"""
		Connect to the Orient Database Host
		:return: True on success, False on failure
		:rtype: bool
		"""
		if server is None:
			server = self.getServer()
		if port is None:
			port = self.getPort()
		if host_user is None:
			host_user = self.getUser()
		if host_password is None:
			host_password = self.getPassword()
		try:
			self._client = pyorient.OrientDB(server, port)
			if self._client is not None:
				if host_user not in self.EMPTY_VALUE and host_password not in self.EMPTY_VALUE:
					self._session = self._client.connect(host_user, host_password)
				else:
					self._session = self._client.connect()
				if self._session is not None:
					self._state = self.STATE_HOST_CONNECTED
		except PyOrientConnectionException as pce:
			self._client = None
			self._session = None
			self.getLog().critical("[connectToHost] Unable to connect to {}:\n{}".format(server, pce))
		return None not in [self._client, self._session]

	def connectToDatabase(self, db=None, db_user=None, db_password=None):
		"""
		Connect to the Orient Database at the server
		:return: True on success, False on failure
		:rtype: bool
		"""
		if db is None:
			db = self.getDatabase()
		if db_user is None:
			db_user = self.getDatabaseUser()
		if db_password is None:
			db_password = self.getDatabasePassword()
		try:
			if self.getClient() is not None and db is not None:
				if self.hasDatabase():
					if db_user not in self.EMPTY_VALUE and db_password not in self.EMPTY_VALUE:
						self._schema = self.getClient().db_open(db, db_user, db_password)
					else:
						self._schema = self.getClient().db_open(db)
					if self._schema is not None:
						self._state = self.STATE_DB_CONNECTED
				else:
					raise Exception("[connectToDatabase] Database ({}) does not exist".format(db))
			else:
				if self.getClient() is None:
					raise Exception("[connectToDatabase] Please connect to host first")
				elif db is None:
					raise Exception("[connectToDatabase] Please set a proper Database Name")
		except PyOrientCommandException as pce:
			self._schema = []
			raise Exception("[connectToDatabase] Unable to connect to {}:\n{}".format(db, pce))
		return len(self._schema) > 0

	def connect(self, settings=None, desired_state=STATE_DB_CONNECTED, **kwargs):
		"""
		Connect to the database server
		:type settings: dict | None
		@kwargs: settings
		:return: Session token
		"""
		# merge settings with kwargs, kwargs replace settings
		settings = self.mergeDictionary(target=settings, source=kwargs)
		# create new URL use standard defaults
		new_url = self.createURL(settings=settings, default_url_settings=self._settings)
		# if url was given in the settings or kwargs; use that one
		if settings is not None and "url" in settings.keys():
			new_url = settings["url"]
		# proceed only when we have to change
		if new_url != self.getURL() or desired_state != self.getConnectionState():
			# check if proposed URL is valid
			if self.isValidURL(new_url):
				try:
					# if we need to connect to a new URL while a connection is still open
					if new_url != self.getURL() and self.isConnected():
						self.getLog().warning("There is still a connection to {}, so disconnect first..".format(
							self.getURL()))
						self.disconnect()
						self.setURL(new_url)
					# Disconnect if we need "downgrade" the connection state
					if self.getConnectionState() > desired_state:
						self.disconnect()
					# Connect if we are in a lower state and we need a higher state.
					if self.getConnectionState() < desired_state >= self.STATE_HOST_CONNECTED:
						self.connectToHost()
					# Connect if previous attempt was succesful
					if self.STATE_HOST_CONNECTED <= self.getConnectionState() < desired_state >= \
							self.STATE_DB_CONNECTED:
						self.connectToDatabase()
				except Exception as e:
					self.getLog().error(e)
			else:
				self.getLog().error("Unable to use the given URL to connect to the orientdb: {}".format(
					self.getURL())
				)
		return self._session

	def disconnect(self):
		if self.isConnected(min_state=self.STATE_DB_CONNECTED):
			self.getClient().db_close()
			self._state = self.STATE_HOST_CONNECTED
		if self.isConnected(min_state=self.STATE_HOST_CONNECTED):
			self._client = None
			self._state = self.STATE_UNCONNECTED

	def getSchema(self):
		"""
		:return: OrientDB Schema
		:rtype: pyorient.Information
		"""
		return self._schema

	def hasSchema(self):
		"""
		:return: Whether the schema of the database is know
		:rtype: bool
		"""
		return self.getSchema() is not None

	def getSession(self):
		"""
		:return: Session IDc
		:rtype: int
		"""
		return self._session

	def getClient(self):
		"""
		:return:
		:rtype: pyorient.OrientDB
		"""
		return self._client

	@classmethod
	def isValidURL(cls, url):
		"""
		Checks if a URL string is valid
		:param url: URl String to check
		:type url: str
		:return: Whether the URl string is valid
		:rtype: bool
		"""
		return len(cls.URL_TEST.findall(url)) > 0

	@classmethod
	def urlToSettings(cls, url, default_url_settings=None):
		"""
		Parse a url string to a settings dict
		"""
		## load defaults
		if default_url_settings is None:
			default_url_settings = cls._default_url_settings
		else:
			default_url_settings = cls.mergeDictionary(
				target=cls._default_url_settings,
				source=default_url_settings
			)
		## build url
		url_settings = {}
		if cls.isValidURL(url):
			## retrieve first matching element
			url_parts = cls.URL_TEST.findall(url)[0]
			url_settings = {
				"protocol" : url_parts[0],
				"username" : url_parts[1],
				"password": url_parts[2],
				"server" : url_parts[3],
				"port": url_parts[4],
				"database" : url_parts[5],
			}
		return cls.mergeDictionary(target=default_url_settings, source=url_settings)

	@classmethod
	def settingsToURL(cls, settings=None, default_url_settings=None, url_fmt=None, **kwargs):
		# load defaults for parameters
		if settings is None:
			settings = {}
		if default_url_settings is None:
			default_url_settings = cls.getDefaultURLSettings()
		if url_fmt is None:
			url_fmt = cls.URL_FMT
		# first replace defaults with given settings
		settings = cls.mergeDictionary(target=default_url_settings, source=settings)
		# then replace given settings with kwarg elements
		settings = cls.mergeDictionary(target=settings, source=kwargs)
		# build the auth part (if necessary)
		settings["auth"] = ""
		if settings["username"] not in cls.EMPTY_VALUE  and settings["password"] not in cls.EMPTY_VALUE:
			settings["auth"] = "{username}:{password}@".format(**settings)
		## build the host part
		settings["host"] = "{server}".format(**settings)
		if settings["port"] not in cls.EMPTY_VALUE:
			settings["host"] = "{server}:{port}".format(**settings)
		return url_fmt.format(**settings)

	def createURL(self, settings=None, default_url_settings=None, url_fmt=None, **kwargs):
		"""
		Create an URL String based on the elements in the settings (disregard the current URL).
		"""
		if settings is None:
			settings = self.getSettings()
		return self.settingsToURL(
			settings=settings,
			default_url_settings=default_url_settings,
			url_fmt=url_fmt,
			**kwargs
		)

	@classmethod
	def getDefaultURLSettings(cls):
		return cls._default_url_settings

	def getURL(self, force=False):
		if force:
			# create new url based on the current settings and store it in the settings dict
			self._settings["url"] = self.createURL()
		return self.retrieveSetting(name="url", default=self.getDefaultSettings())

	def setURL(self, url):
		"""
		If the given url is valid, update the settings dictionary to reflect the url
		:param url: URL String
		:type url: str
		:return: Return the new URL as stored in the settings dictionary.
		:rtype: str
		"""
		if self.isValidURL(url):
			# update the settings dict with the new url
			self._settings = self.urlToSettings(url, default_url_settings=self.getSettings())
		# return new url (and refresh it)
		return self.getURL(force=True)

	def getProtocol(self):
		return self.retrieveSetting(name="protocol", default=self.getDefaultSettings())

	def getStorage(self):
		result = "plocal"
		if self.getProtocol() in self.ORIENT_STORAGE_MAP.keys():
			result = self.ORIENT_STORAGE_MAP[self.getProtocol()]
		return result

	def getUser(self):
		return self.retrieveSetting(name="username", default=self.getDefaultSettings())

	def getPassword(self):
		return self.retrieveSetting(name="password", default=self.getDefaultSettings())

	def getServer(self):
		return self.retrieveSetting(name="server", default=self.getDefaultSettings())

	def getPort(self):
		port = self.retrieveSetting(name="port", default=self.getDefaultSettings())
		if isinstance(port, str):
			try:
				port = int(port)
			except ValueError as ve:
				port = 2424
		return port

	def getDatabase(self):
		return self.retrieveSetting(name="database", default=self._default_settings)

	def exists(self, db_name=None):
		"""
		Returns
		"""
		result = False
		if db_name is None:
			db_name = self.getDatabase()
		current_state = self.getConnectionState()
		self.connect(desired_state=self.STATE_HOST_CONNECTED)
		try:
			result = self.getClient().db_exists(db_name, self.getStorageType())
		except PyOrientCommandException as pce:
			self.getLog().error("Unable to check if {} exists at {}:\n{}".format(
				db_name,
				self.getDatabaseURL(),
				pce
			))
		if current_state > self.getConnectionState():
			self.connect(desired_state=current_state)
		return result

	def create(self, db_name=None):
		result = False
		if db_name is None:
			db_name = self.getDatabase()
		current_state = self.getConnectionState()
		self.connect(desired_state=self.STATE_HOST_CONNECTED)
		try:
			result = self.getClient().db_create(db_name, pyorient.DB_TYPE_GRAPH, self.getStorageType())
		except PyOrientCommandException as pce:
			self.getLog().error("Unable to create database {} at {}: \n{}".format(
				db_name,
				self.getDatabaseURL(),
				pce
			))
		if current_state > self.getConnectionState():
			self.connect(desired_state=current_state)
		return result

class Record(uvaObject, pyorient.OrientRecord):
	"""
	Basic record
	"""
	_schema = {
	}
	_class = "V"

	def __init__(self, db, settings=None, **properties):
		uvaObject.__init__(self, settings=settings)
		pyorient.OrientRecord.__init__(self, properties)
		self._db = db
		if not self.hasClass(db):
			self.defineClass(db)

	@classmethod
	def defineClass(cls, db):
		"""
		Defines the class in the database
		:param db:
		:type db: orientDatabase
		:return:
		:rtype: int
		"""
		try:
			db.getClient().batch("""create class V""")
		except pyorient.PyOrientCommandException as pce:
			cls.getLog().error("Unable to define {} in {}:\n {}".format(
				cls._class, db.getURL(), pce
			))

	@classmethod
	def hasClass(cls, db, name=None):
		"""
		Returns whether a class with the name is defined in the DB. If name is None it will use the name of this class.
		:param db: Database to use
		:type db: orientDatabase
		:return: Whether the class is defined in the DB
		:rtype: bool
		"""
		result = False
		if name is None:
			name = cls._class
		if db.isConnected():
			schema = db.getSchema().dataClusters
			for item in schema:
				result = "name" in item.keys() and name.lower() == item["name"].lower()
				if result:
					break
		return result

	@classmethod
	def getClass(cls):
		return cls._class

	@staticmethod
	def makeCriteria(**attributes):
		"""

		:param attributes:
		:type attributes:
		:return:
		:rtype: list
		"""
		criteria = []
		if "date_range" in attributes.keys():
			date_range = attributes.pop("date_range", {})
			start_date = date_range.pop("start", None)
			if start_date is not None:
				criteria.append("time >= '{}'".format(start_date.strftime(Vector.DT_FORMAT)))
			end_date = date_range.pop("end", None)
			if end_date is not None:
				criteria.append("time <= '{}'".format(end_date.strftime(Vector.DT_FORMAT)))
		for attribute in attributes.keys():
			if attributes[attribute] is not None:
				criteria.append("{}={}".format(attribute, attributes[attribute]))
		return criteria

	@classmethod
	def makeContent(cls, **attributes):
		"""
		Returns a dictionary that can be used with pyorient record_create and record_update
		:param attributes:
		:type attributes:
		:return:
		:rtype:
		"""
		return {"@{}".format(cls._class): attributes}

	@classmethod
	def parseProperties(cls, **attributes):
		"""
		Parses
		:param properties:
		:type properties: dict
		:return:
		:rtype:
		"""
		for key in cls._schema.keys():
			if key not in attributes.keys():
				attributes[key] = None
		return attributes

	@classmethod
	def getCluster(cls, db, name=None):
		"""

		:param db:
		:type db: orientDatabase
		:param name:
		:type name: str or None
		:return:
		:rtype:
		"""
		result = None
		if name is None:
			name = cls._class
		if db.isConnected():
			schema = db.getSchema()
			for cluster in schema:
				if "name" in cluster.keys() and name.lower() == cluster["name"].lower():
					result = cluster["id"]
					break
		return result

	@staticmethod
	def parseIndexes(indexes):
		"""
		Parses a list of indexes in to the bracket notation of OrientDB
		:param indexes: List of indexes or None of nothing is provided (for easy use in methods). If an element is a
		string, it will assume the string is of the form 'index=value' and only brackets are placed around it,
		if an element is a list or tuple, it assumes it has length 2 where the first is the index and the second is
		the value, so (index, value) -> "[index=value]".
		:type indexes: list or None
		:return: One string with all the indexes together in bracket notation; [index=value][index=value]
		:rtype: str
		"""
		if indexes is None:
			indexes = []
		if not isinstance(indexes, list):
			raise ValueError("[prepareIndexes] given object {} is not a list".format(indexes))
		# wrap each index in [] so that we can use it in queries
		results = []
		for index in indexes:
			if isinstance(index, (list, tuple)):
				results.append("[{}={}]".format(*index))
			if isinstance(index, str):
				results.append("[{}]".format(index))
		# create the string
		return "".join(results)


	def getDatabase(self):
		"""
		:return:
		:rtype: orientDatabase
		"""
		return self._db

	def getRID(self):
		return self._rid

	def getProperties(self):
		return self.oRecordData

	def getProperty(self, key):
		result = None
		if key in self.oRecordData.keys():
			result = self.oRecordData[key]
		return result

	def __getitem__(self, item):
		result = self.getProperty(item)
		if item == "rid":
			result = self.getRID()
		return result

	def __getattr__(self, item):
		return self.__getitem__(item)

	def __dict__(self):
		return self._properties

	def __json__(self):
		return str(dict(self))


class Vector(Record):

	def __init__(self, db, settings=None, **properties):
		Record.__init__(self, db, settings, **properties)

	@classmethod
	def create(cls, db, **properties):
		"""
		Creates a new record in the DB and returns the class instance wrapping it
		:param cls:
		:type cls:
		:param db:
		:type db:
		:return:
		:rtype:
		"""
		result = False
		rec = {'@{}'.format(cls.getClass()): properties}
		try:
			cluster = db.getCluster(cls._class)
			if cluster is not None:
				result = db.getClient().record_create(cluster, rec)
		# result = result[0] if len(result) > 0 else None
		except pyorient.PyOrientCommandException as pce:
			cls.getLog().critical("Unable to create {}:\n {}".format(cls._class, pce))
		return result

	@classmethod
	def load(self):
		"""
		Load the vector from the database
		:return:
		:rtype:
		"""
		raise NotImplementedError

	def exists(self):
		# TODO: Implement
		raise NotImplementedError

	def save(self, overwrite=False):
		# TODO: Implement
		raise NotImplementedError

	def countEdges(self, direction="out", name=None, indexes=None ):
		"""
		Counts the number of direct edges from or to this Vector. NOTE: Unable filter on where (yet).
		:param direction: Either 'out', 'in' or 'both'
		:param name: name of the edges to count
		:param indexes: List of indexes that we can use to filter the edges. NOTE: OrientDB only support equality
		checks on indexes.
		:return:
		:rtype:
		"""
		result = 0
		if direction not in ["out", "in", "both"]:
			raise ValueError("[countEdges] Given direction {} is not allowed".format(direction))
		indexes = self.parseIndexes(indexes)
		try:
			if self.getRID() is not None:
				query = "select sum({direction}(){indexes}.size()) as m_count from ".format(
					direction, self.getRID()
				)
				result = self.getClient().query(query, -1, "0:1")
			result = result[0].m_count if len(result) > 0 else 0
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to count Edges:\n{}".format(poe))
		return result


class Edge(Record):

	def __init__(self, db, settings=None, **properties):
		Record.__init__(self, db, settings, **properties)

	@classmethod
	def create(cls, db, fromV, toV, name):
		"""
		Creates an edge between two Nodes and will autmatically set the in and out properties of the edge.
		:param fromV: Record from which the edge originates
		:type fromV: Vector
		:param toV: Record to create the edge to
		:type toV: Vector
		:param name: Name of the edge
		:type name: str
		:return: Orient Record of the Edge or None if it failed
		:rtype: orientRecord or None
		"""
		result = None
		try:
			query = "create edge {name} from {fRID} to {tRID} SET out={fRID}, in={tRID}".format(
				name=name, fRID=fromV.getRID(), tRID=toV.getRID()
			)
			result = db.getClient().command(query)
			result = result[0] if len(result) > 0 else None
		except pyorient.PyOrientCommandException as pce:
			cls.getLog().critical("Unable to create edge {name} from {fRID} to {tRID}:\n{e}".format(
				name=name, fRID=fromV, tRID=toV, e=pce))
		return result
