# coding=utf-8
"""
Basic module layer that represents a Cultivator Light Regime.
"""

from pycultivator_legacy.core import BaseObject
from uvaPolynomialDependency import *
from uvaTimeDependency import TimeDependency
from uvaFluctuateDependency import FluctuateDependency
from datetime import datetime as dt
import copy

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class CultivatorLight(BaseObject):
    """
    A class representing the light panel in a Cultivator Channel.
    This light panel can be dependent on a sorts of variables described as Dependency classes
    """

    LIGHT_STATE = 0
    LIGHT_INTENSITY_VOLTAGE = 1
    LIGHT_INTENSITY_PERCENTAGE = 2
    LIGHT_INTENSITY_ABSOLUTE = 3

    def __init__(self, channel, intensity=None, state=True, settings=None):
        """A class describing a certain light regime

        :param channel: The owner of this light
        :type channel: None, uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :param intensity: How many photons per m2/s
        :type intensity: float
        :param state: whether the light panel is On or Off
        :type state: bool
        """
        super(CultivatorLight, self).__init__(settings=settings)
        self._channel = channel
        self._light_intensity = intensity  # = (voltage, percentage, absolute)
        self._configured_intensity = intensity
        if self._light_intensity is None:
            self._light_intensity = [0.0, 0.0, 0.0]
        self._light_state = state
        self._configured_state = state
        self._neighbour_dependency = {}
        self._default_left_neighbour_dependency = None
        self._default_right_neighbour_dependency = None
        self._fluctuate_dependency = None
        self._time_dependency = None
        self._continuous_time_dependency = None
        self._od_dependency = None
        self._photon_p_od = 0

    @staticmethod
    def loadDefault():
        return CultivatorLight(None)

    @classmethod
    def createFromXMLElement(cls, xml, config):
        """Create the light object from XML.

        :param xml: xml of channel
        :type xml: 	xml.etree._Element._Element
        :type config:  uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        :return:
        :rtype:
        """
        result = copy.deepcopy(config.loadDefaultLight())
        e_light = config.getLightElement(xml)
        if e_light is not None:
            result.setDefaultLeftNeighbourDependency(result.getLeftNeighbourDependency())
            result.setDefaultRightNeighbourDependency(result.getRightNeighbourDependency())
            result.setChannel(config.loadChannel(cls.validateValue(xml.get("id"), int, 0)))
            result.setConfiguredLightIntensity(
                cls.validateValue(e_light.get("intensity"), float, result.getConfiguredLightIntensity()),
                apply=False, simulate=True
            )
            result.setConfiguredLightState(
                cls.validateValue(e_light.get("state"), bool, result.getConfiguredLightState()),
                apply=True, simulate=True
            )
            dependencies = cls.createDependenciesFromXML(e_light, config)
            for dependency in dependencies:
                if dependency.isActive():
                    if dependency.getVariable().lower() == "time":
                        result.setTimeDependency(dependency)
                    if dependency.getVariable().lower() == "od":
                        result.setODDependency(dependency)
                    if dependency.getVariable().lower() == "intensity":
                        if dependency.getSource().lower() == "left":
                            result.setLeftNeighbourDependency(dependency)
                        if dependency.getSource().lower() == "right":
                            result.setRightNeighbourDependency(dependency)
                        if dependency.getSource().lower() == "self":
                            result.setFluctuateDependency(dependency)
        return result

    @classmethod
    def createDependenciesFromXML(cls, xml, config):
        results = []
        elements = config.getDependencyElements(xml)
        for element in elements:
            dependency = None
            if TimeDependency.canLoadXML(element):
                dependency = TimeDependency.createFromXMLElement(element, config=config)
            if ODDependency.canLoadXML(element):
                dependency = ODDependency.createFromXMLElement(element, config=config)
            if NeighbourDependency.canLoadXML(element):
                dependency = NeighbourDependency.createFromXMLElement(element, config=config)
            if FluctuateDependency.canLoadXML(element):
                dependency = FluctuateDependency.createFromXMLElement(element, config=config)
            if dependency is not None:
                results.append(dependency)
        return results

    @classmethod
    def createFromDefaultXML(cls, xml, config):
        """Creates a Light Object with the sole purpose to serve as the default template

        :param xml: light element
        :type xml:
        :param config:
        :type config:
        :return:
        :rtype:
        """
        result = cls.loadDefault()
        e_light = config.getLightElement(xml)
        if e_light is not None:
            result.setConfiguredLightIntensity(
                cls.validateValue(e_light.get("intensity"), float, result.getConfiguredLightIntensity()),
                apply=True, simulate=True
            )
            result.setConfiguredLightState(
                cls.validateValue(e_light.get("state"), bool, result.getConfiguredLightState()),
                apply=True, simulate=True
            )
            dependencies = cls.createDependenciesFromXML(e_light, config)
            for dependency in dependencies:
                if dependency.isActive():
                    if dependency.getVariable().lower() == "time":
                        result.setTimeDependency(dependency)
                    if dependency.getVariable().lower() == "od":
                        result.setODDependency(dependency)
                    if dependency.getVariable().lower() == "intensity":
                        if dependency.getSource().lower() == "left":
                            result.setLeftNeighbourDependency(dependency)
                        if dependency.getSource().lower() == "right":
                            result.setRightNeighbourDependency(dependency)
                        if dependency.getSource().lower() == "self":
                            result.setFluctuateDependency(dependency)
        return result

    def getChannel(self):
        """
        :rtype: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        """
        return self._channel

    def setChannel(self, channel):
        self._channel = channel
        return self

    def hasChannel(self):
        return self._channel is not None

    def getChannelIndex(self):
        result = 0
        if self.hasChannel():
            result = self._channel.getIndex()
        return result

    def getCultivator(self):
        """
        :rtype: uvaCultivator.uvaCultivator.Cultivator
        """
        result = None
        if self.hasChannel():
            result = self.getChannel().getCultivator()
        return result

    def hasCultivator(self):
        return self.hasChannel() and self.getChannel().getCultivator() is not None

    def getSerial(self):
        result = None
        if self.hasCultivator():
            result = self.getChannel().getCultivator().getSerial()
        return result

    def isFluctuateDependent(self):
        return self._fluctuate_dependency is not None and self._fluctuate_dependency.isActive()

    def getFluctuateDependency(self):
        """
        :rtype: uvaCultivator.uvaFluctuateDependency.FluctuateDependency
        """
        return self._fluctuate_dependency

    def setFluctuateDependency(self, dependency):
        if dependency is None or isinstance(dependency, FluctuateDependency):
            self._fluctuate_dependency = dependency
        else:
            self.getLog().error("Unable to set {} as fluctuate dependency on channel {}".format(
                type(dependency), self.getChannelIndex()
            ))
        return self

    def isTimeDependent(self):
        return self._time_dependency is not None and self._time_dependency.isActive()

    def getTimeDependency(self):
        """
        :rtype: uvaCultivator.uvaTimeDependency.TimeDependency
        """
        return self._time_dependency

    def setTimeDependency(self, dependency):
        if dependency is None or isinstance(dependency, TimeDependency):
            self._time_dependency = dependency
        else:
            self.getLog().error("Unable to set {} as time dependency on channel {}".format(
                type(dependency), self.getChannelIndex()
            ))
        return self

    def setODDependency(self, dependency):
        if dependency is None or isinstance(dependency, ODDependency):
            self._od_dependency = dependency
        else:
            self.getLog().error("Unable to set {} as OD dependency on channel {}".format(
                type(dependency), self.getChannelIndex()
            ))
        return self

    def isODDependent(self):
        return self._od_dependency is not None and self._od_dependency.isActive()

    def getODDependency(self):
        """
        :rtype: uvaCultivator.uvaPolynomialDependency.ODDependency
        """
        return self._od_dependency

    def isNeighbourDependent(self):
        return self._neighbour_dependency is not None and self._neighbour_dependency.isActive()

    def setNeighbourDependency(self, dependency, related_channel):
        """
        :type dependency: uvaCultivator.uvaDependency.NeighbourDependency
        :type related_channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel, int
        :rtype: CultivatorLight
        """
        from uvaCultivatorChannel import CultivatorChannel
        if isinstance(related_channel, CultivatorChannel):
            related_channel = related_channel.getIndex()
        if dependency is None or isinstance(dependency, NeighbourDependency):
            self._neighbour_dependency[related_channel] = dependency
        else:
            self.getLog().error("Unable to set {} as neighbour dependency on channel {}".format(
                type(dependency), self.getChannelIndex()
            ))
        return self

    def getNeighbourDependency(self, distance):
        """
        :type distance: int
        :rtype: uvaCultivator.uvaPolynomialDependency.NeighbourDependency
        """
        result = None
        if distance in self._neighbour_dependency:
            result = self._neighbour_dependency[distance]
        elif distance == -1:
            result = self._default_left_neighbour_dependency
        elif distance == 1:
            result = self._default_right_neighbour_dependency
        return result

    def hasNeighbourDependency(self, distance):
        return self.getNeighbourDependency(distance) is not None

    def setDefaultLeftNeighbourDependency(self, dependency):
        self._default_left_neighbour_dependency = dependency

    def setLeftNeighbourDependency(self, dependency):
        return self.setNeighbourDependency(dependency, -1)

    def getLeftNeighbourDependency(self):
        """
        :rtype: uvaCultivator.uvaPolynomialDependency.NeighbourDependency
        """
        result = self._default_left_neighbour_dependency
        if -1 in self._neighbour_dependency:
            result = self._neighbour_dependency[-1]
        return result

    def hasLeftNeighbourDependency(self):
        return (-1 in self._neighbour_dependency) or \
               self._default_left_neighbour_dependency is not None

    def setDefaultRightNeighbourDependency(self, dependency):
        self._default_right_neighbour_dependency = dependency

    def setRightNeighbourDependency(self, dependency):
        return self.setNeighbourDependency(dependency, 1)

    def getRightNeighbourDependency(self):
        """
        :rtype: uvaCultivator.uvaPolynomialDependency.NeighbourDependency
        """
        result = self._default_right_neighbour_dependency
        if 1 in self._neighbour_dependency:
            result = self._neighbour_dependency[1]
        return result

    def hasRightNeighbourDependency(self):
        return (1 in self._neighbour_dependency) or \
               self._default_left_neighbour_dependency is not None

    def isLightOn(self):
        return self._light_state is True

    def getLightState(self):
        return self._light_state

    def getLightIntensity(self, unit=LIGHT_INTENSITY_ABSOLUTE):
        """
        Retrieve the desired setting from the cultivator
        :param unit: Numerical representation of the desired unit.
        :type unit: int
        :return: The current setting for this light
        :rtype: None, bool, float
        """
        return self._light_intensity[unit - 1]

    def getGivenLight(self, unit=LIGHT_INTENSITY_ABSOLUTE):
        return self.getLightIntensity(unit) if self.getLightState() else 0

    def refreshLightSettings(self):
        """
        Retrieve all current settings from the cultivator
        :return: Whether the refresh was successful
        :rtype: bool
        """
        result = False
        if self.hasCultivator() and self.getChannel().isConnected():
            lis = self.getSerial().getLightSettings(self.getChannelIndex())
            # 0 = STATE / Voltage / Prc / Abs
            if lis and len(lis) == 4:
                self._light_state = lis[self.LIGHT_STATE]
                self._light_intensity = [
                    lis[self.LIGHT_INTENSITY_VOLTAGE],
                    lis[self.LIGHT_INTENSITY_PERCENTAGE],
                    lis[self.LIGHT_INTENSITY_ABSOLUTE]]
                result = True
        return result

    def setLightState(self, state, simulate=False):
        """
        Sets the light state of this channel
        :param state: True for light ON, False for light OFF
        :type state: bool
        :param simulate: If set to True the serial device is not set to the new state
        :type simulate: bool
        :return: True on Success, False otherwise
        :rtype: bool
        """
        lis = None
        if self.getSerial() is not None and not simulate:
            lis = self.getSerial().setLightState(self.getChannelIndex(), state)
        if simulate or lis is not None:
            self._light_state = state
            lis = True
        return lis is not None

    def setLightIntensity(self, value, unit=LIGHT_INTENSITY_ABSOLUTE, simulate=False):
        """
        Sets a particular light setting for this channel
        :param value: The intensity in given unit
        :type value: float
        :param unit: Unit of the value (uE/m2/s or percentage or Voltage)
        :type unit: int
        :param simulate: If set to True the serial device is not set to the new state
        :type simulate: bool
        :return: True on success, False otherwise
        :rtype: bool
        """
        lis = False
        if self.getSerial() is not None and not simulate:
            if unit == self.LIGHT_INTENSITY_ABSOLUTE:
                lis = self.getSerial().setLightAbsolute(self.getChannelIndex(), value)
            if unit == self.LIGHT_INTENSITY_PERCENTAGE:
                lis = self.getSerial().setLightPercentage(self.getChannelIndex(), value)
            if unit == self.LIGHT_INTENSITY_VOLTAGE:
                lis = self.getSerial().setLightVoltage(self.getChannelIndex(), value)
            if lis:
                self.refreshLightSettings()
        else:
            self._light_intensity[unit - 1] = value
            lis = True
        return lis

    def applyLightSettings(self, state=None, intensity=None, unit=LIGHT_INTENSITY_ABSOLUTE):
        """
        Apply the light settings to the serial device
        :param state: Light state or None when the internal stored value should be used
        :type state: bool, None
        :param intensity: Intensity or None when the internal stored value should be used
        :type intensity: float, None
        :param unit: Unit of the light intensity setting (uE/m2/s, percentage, voltage)
        :type unit: int
        :return: True on success, False otherwise
        :rtype: bool
        """
        if state is None:
            state = self.getLightState()
        if intensity is None:
            intensity = self.getLightIntensity(unit)
        t_start = dt.now()
        result = self.setLightIntensity(intensity, unit)
        self.getLog().debug(
            "It took me {:0.3f} seconds to set light intensity".format(
                (dt.now() - t_start).total_seconds()
            )
        )
        if result:
            result = self.setLightState(state)
        return result

    def getConfiguredLightState(self):
        return self._configured_state

    def setConfiguredLightState(self, state, apply=False, simulate=False):
        self._configured_state = state is True
        if apply:
            self.setLightState(state, simulate=simulate)
        return self

    def getDesiredLightState(self, t_now=None, default=None):
        """Returns the desired light state, derived from the provided regime or the default value.

        If default value is None, then we restore the light.

        :return: The desired state
        :rtype: bool
        """
        t_now = t_now if t_now else dt.now()
        # use current light state as default
        if default is None:
            default = self.isLightOn()
        result = default
        if self.isTimeDependent():
            dIntensity = self.getTimeDependency().calculate(t_now)
            if dIntensity is not None:
                result = True if dIntensity > 0 else False
        return result

    def getConfiguredLightIntensity(self):
        return self._configured_intensity

    def setConfiguredLightIntensity(self, value, apply=False, simulate=False):
        self._configured_intensity = value
        if apply:
            self.setLightIntensity(value, simulate=simulate)
        return self

    def getDesiredLightIntensity(self, od, t_now=None, default=None):
        """
        Will return the desired light intensity as dictated by this regime. If this regime is OD dependent it will
        return the calculated intensity from od and self.getPhotonPerOD(), otherwise it will use the default
        intensity of this object.
        :param od: The currently measured OD.
        :type od: float
        :return: The desired intensity
        :rtype: float
        """
        if default is None:
            default = self.getLightIntensity()
        result = None
        if self.isODDependent():
            result = self.getODDependency().calculate(od)
        if self.isTimeDependent():
            multiplier = self.getTimeDependency().calculate(t_now)
            if result is not None:
                result *= multiplier
            else:
                result = multiplier
        if result is None:
            result = default
        return result
