# coding=utf-8
"""
Initialize stub for uvaCultivatorProtocol package
"""

from uvaCultivator import uvaException, uvaLog
from pycultivator_legacy.core import BaseObject
from datetime import datetime as dt
import os
import sys

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

# connect to package to logger
uvaLog.getLogger(__name__).debug("Connected {} to logger".format(__name__))


class AbstractCultivatorProtocol(BaseObject):
    """Define a basic protocol class as foundation to all other protocol implementations"""

    _default_settings = BaseObject.mergeDefaultSettings(
        {
            "protocol.project_id": None,
            "protocol.time_zero": None,
            "protocol.force": False,
            "log.level": uvaLog.LEVELS["INFO"],
            "exporter.state": True,
        }, namespace=""
    )

    _name = "protocol_abstract"

    def __init__(self, settings=None):
        """Initialize the class"""
        super(AbstractCultivatorProtocol, self).__init__(settings=settings)
        self._cultivator = None
        self._connection = None

    @classmethod
    def getName(cls):
        """Returns the name of the protocol"""
        return cls._name

    def getProjectId(self):
        return self.retrieveSetting(name="project_id", namespace="protocol")

    def getTimeStart(self):
        return self.retrieveSetting(name="time_zero", namespace="protocol")

    def getLogLevel(self):
        return self.retrieveSetting(name="level", namespace="log")

    def start(self):
        """Starts the protocol, by first preparing, then executing and at last cleaning"""
        t_prepare = dt.now()
        result = self._prepare()
        self.inform(" === PREPARATION TIME: {:0.2f} seconds".format(
            (dt.now() - t_prepare).total_seconds())
        )
        try:
            result_react = True
            if result:
                t_measurement = dt.now()
                result = self._measure()
                self.inform(" === MEASUREMENT TIME: {:0.2f} seconds".format(
                    (dt.now() - t_measurement).total_seconds())
                )
                t_react = dt.now()
                result_react = result and self._react()
                self.inform(" === CONTROL TIME: {:0.2f} seconds".format(
                    (dt.now() - t_react).total_seconds())
                )
            if result and self.isReporting():
                t_report = dt.now()
                result = self._report()
                self.inform(" === REPORT TIME: {:0.2f} seconds".format(
                    (dt.now() - t_report).total_seconds())
                )
            result = result and result_react
        except KeyboardInterrupt:
            self.warn("Interrupted by user, quit gracefully")
        except Exception as e:
            self.error(
                "Unexpected {} while executing protocol: {}".format(
                    type(e), e
                ),
            )
        finally:
            t_clean = dt.now()
            result = self._clean() and result
            self.inform(" === CLEAN UP TIME: {:0.2f} seconds".format(
                (dt.now() - t_clean).total_seconds())
            )
            self.inform(" === PROTOCOL SUCCESS: {}".format(result))
        return result

    def _prepare(self):
        """Prepare for protocol execution (Config checking, connecting)

        :rtype: bool
        """
        return True     # overload

    def _measure(self):
        """Performs the actual protocol steps

        :return: True on success, False otherwise
        :rtype: bool
        """
        raise True      # overload

    def _react(self):
        """Reacts to measurements"""
        return True     # overload

    def _report(self):
        """Reports on the action"""
        return True     # overload

    def _clean(self):
        """Clean up after protocol execution

        :rtype: bool
        """
        raise True      # overload

    def forceRun(self, state):
        self._settings["force"] = state is True

    def isForced(self):
        return self.retrieveSetting(name="force", namespace="protocol")

    def isReporting(self):
        """Whether the obtained measurements will be reported (i.e. stored/exported)

        :rtype: bool
        """
        return self.retrieveSetting(name="state", namespace="exporter") is True

    def doReporting(self, state):
        """Sets whether the measurements obtained by this protocol will be stored/exported

        :type state: bool
        """
        self._settings["exporter.state"] = state is True
        return self

    @classmethod
    def inform(cls, msg, start="", end="\n"):
        """Informs user by writing messages to the console (normal) or log (parallel)"""
        log_level = cls.getLog().getEffectiveLevel()
        if log_level <= 20:
            cls.getLog().info(msg)
        else:
            sys.stdout.write("{}{}{}".format(start, msg, end))
            sys.stdout.flush()

    @classmethod
    def warn(cls, msg, start="", end="\n"):
        log_level = cls.getLog().getEffectiveLevel()
        if log_level <= 20:
            cls.getLog().warning(msg, exc_info=1)
        else:
            sys.stderr.write("{}{}{}".format(start, msg, end))
            sys.stderr.flush()

    @classmethod
    def error(cls, msg, start="", end="\n"):
        log_level = cls.getLog().getEffectiveLevel()
        if log_level <= 20:
            cls.getLog().error(msg, exc_info=1)
        else:
            sys.stderr.write("{}{}{}".format(start, msg, end))
            sys.stderr.flush()


class AbstractProtocolException(uvaException.UVAException):

    def __init__(self, msg):
        super(AbstractProtocolException, self).__init__(msg)


class AbstractParallelProtocol(AbstractCultivatorProtocol):
    """
    A class that is capable to run protocols in parallel to the main stub.
    Most of the work is done by connecting an additional logger to the rootlog which will capture all log messages
    written in the process.
    Implementation should use start instead of execute, start will call prepare which will do some preperations once
    this finished execute is called and at last stop is called to clean things up.
    """

    _default_settings = BaseObject.mergeDefaultSettings(
        {
            "log.file": None,
        }, namespace=""
    )

    _name = "batch_protocol_abstract"

    def __init__(self, settings=None):
        AbstractCultivatorProtocol.__init__(self, settings=settings)
        self._t_start = dt.now()

    # @classmethod
    # def getLog(cls):
    #     if cls._log is None:
    #         cls._log = uvaLog.getLogger(cls.__name__, cls.getRootLogger())
    #     return cls._log

    def getLogFile(self):
        return self.retrieveSetting(name="file", default=self.getDefaultSettings(), namespace="log")

    def connectLog(self):
        if self.getLogFile() is not None:
            self.connectFileHandler(self.getRootLogger(), self.getLogFile())
            # self.getRootLogger().propagate = False
        self._t_start = dt.now()
        self.getLog().info(" === PROTOCOL START AT: {} ===".format(self._t_start.strftime(self.DT_FORMAT)))

    def connectFileHandler(self, log, log_path=None, log_level=None, propagate=True):
        if log_level is None:
            log_level = self.getLogLevel()
        if log_path is None:
            log_path = self.getLogFile()
        if log_path is not None and os.path.exists(os.path.dirname(log_path)):
            uvaLog.connectFileHandler(log, log_path, level=log_level, propagate=propagate)

    def start(self):
        self.connectLog()
        super(AbstractParallelProtocol, self).start()
        t_end = dt.now()
        self.getLog().info(" === RUN TIME: {:.2f} seconds".format((t_end - self._t_start).total_seconds()))
        self.getLog().info(" === PROTOCOL END AT: {} === ".format(t_end.strftime(self.DT_FORMAT)))

    def _prepare(self):
        return super(AbstractParallelProtocol, self)._prepare()

    def _measure(self):
        raise NotImplementedError

    def _clean(self):
        result = super(AbstractParallelProtocol, self)._clean()
        return result

    # output methods

    @classmethod
    def inform(cls, msg, start="", end="\n"):
        cls.getLog().info(msg)

    @classmethod
    def warn(cls, msg, start="", end="\n"):
        cls.getLog().warning(msg)

    @classmethod
    def error(cls, msg, start="", end="\n"):
        cls.getLog().error(msg)
