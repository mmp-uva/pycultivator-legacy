
from schedulerProtocol import SchedulerProtocol
from uvaCultivator.uvaScheduler import DeviceJob
from datetime import datetime as dt, timedelta as td
from uvaCultivator.uvaScheduler import JobException

__author__ = "Max Guillaume <maxguillaume20@gmail.com>, Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class FluctuateProtocol (SchedulerProtocol):
    """A protocol that fluctuates light intensity between OD measurements"""

    _name = "fluctuate_protocol"

    _namespace = "protocol.fluctuate"
    _default_settings = SchedulerProtocol.mergeDefaultSettings(
        {
            "export": False
        }, namespace=_namespace
    )

    _measurement_interval = 5  # minutes
    _time_buffer = 5  # seconds
    _minimum_pause_length = 2  # seconds

    def __init__(self, config_file, settings=None):
        super(FluctuateProtocol, self).__init__(config_file=config_file, settings=settings)
        self._intensities = {}

    def willExportFluctuations(self):
        return self.retrieveSetting("export", namespace="protocol.fluctuate") is True

    def setExportFluctuations(self, state):
        self.setSetting("export", state is True, namespace="protocol.fluctuate")

    def _prepare(self):
        result = super(FluctuateProtocol, self)._prepare()
        device_name = self.getCultivator().getSerial().getPort()
        device = self.getCultivator().getSerial()
        # pre-register device, because we do not want to reload it
        self.getScheduler().registerDevice(name=device_name, device=device)
        return result

    def _measure(self):
        return super(FluctuateProtocol, self)._measure()

    def _react(self):
        result = super(FluctuateProtocol, self)._react()
        initial_time = dt.now()
        for channel in self.getCultivator().getChannelRange():
            light = self.getCultivator().getChannelLight(channel)
            if light.isFluctuateDependent():
                # get intensities
                incident_light = light.getLightIntensity()
                dependency = light.getFluctuateDependency()
                pause_length = dependency.getPause()
                if not isinstance(pause_length, (int, float)) or pause_length < self._minimum_pause_length:
                    self.getLog().warning(
                        "The parameter 'fluctuate.pause' for channel {0} has been inappropraitely set. Using the minimum value allowed: {1} seconds".format(channel, self._minimum_pause_length)
                    )
                    pause_length = self._minimum_pause_length
                fluctuation_count = self._calculateFluctuationCount(initial_time, pause_length)
                intensities = dependency.calculate(fluctuation_count=fluctuation_count, source_value=incident_light)
                # create and record light jobs
                start_time = initial_time
                if channel not in self._intensities.keys():
                    self._intensities[channel] = []
                for i in range(len(intensities)):
                    intensity = intensities[i]
                    self.schedule(start_time=start_time, channel=channel, intensity=intensity)
                    # record intensities for reporting: tuple with intensity, state
                    self._intensities[channel].append((start_time, intensity, intensity > 0))
                    # update start time for next interval
                    if i == 0:
                        # add time buffer after first intensity
                        start_time += td(seconds=self._time_buffer)
                    start_time += td(seconds=pause_length)
        return result

    def _calculateFluctuationCount (self, start_time, pause_length):
        # get the minute of the next measuring time
        next_measure_time = start_time.minute // self._measurement_interval * self._measurement_interval + self._measurement_interval
        # the time between now and when the fluctuations should stop
        delta_time = next_measure_time * 60 - (start_time.minute * 60 + start_time.second + self._time_buffer * 2)
        n_fluctuations = int(delta_time / pause_length)
        return n_fluctuations

    def _report(self, intensities=None):
        result = super(FluctuateProtocol, self)._report()
        light_result = False
        if intensities is None:
            intensities = self._intensities
        if self.willExportFluctuations() and len(intensities) > 0 and self.getExporterName() is not None:
            with self.createExporter() as exporter:
                dest = exporter.getDestination()
                self.getLog().debug("Write Light Intensities to {}".format(dest))
                if exporter.hasStorage():
                    light_result = exporter.addLightRecords(intensities)
                self.inform("Writing Light Intensities: {}".format(light_result))
        return result and light_result

    def getPSISerialControllerClass(self):
        if self.fakesConnection():
            from uvaSerial.psiFakeSerialController import psiFakeSerialController
            klass = psiFakeSerialController
        else:
            from uvaSerial.psiSerialController import psiSerialController
            klass = psiSerialController
        return klass

    def scheduleFluctuations(self, channel, intensity, start_time=None, settings=None, **kwargs):
        if settings is None:
            settings = {}
        settings.update(kwargs)
        # create job
        port = self.getCultivator().getSerial().getPort()
        settings["scheduler.job.fluctuate.serial.port"] = port
        settings["scheduler.job.fluctuate.class"] = self.getPSISerialControllerClass()
        settings["scheduler.job.fluctuate.channel"] = channel
        settings["scheduler.job.fluctuate.intensity"] = intensity
        job = FluctuateJob(t_schedule=start_time, parent=self.getScheduler(), settings=settings)
        return self.schedule(job)


class FluctuateJob(DeviceJob):
    """Abstract Fluctuation Job"""

    _name = "fluctuateLight"
    _namespace = "scheduler.job.fluctuate"
    _default_settings = DeviceJob.mergeDefaultSettings(
        {
            "serial.port": None,
            "class": None,
            "channel": None,
            "intensity": None
        }, namespace=_namespace
    )

    def __init__(self, t_schedule=None, parent=None, settings=None):
        """A light job is responsbible for changing the light intensity in all appropriate channels"""
        super(FluctuateJob, self).__init__(t_schedule=t_schedule, parent=parent, settings=settings)

    def getSerial(self):
        return self.getDevice()

    def getPort(self):
        return self.retrieveSetting('serial.port')

    def getDeviceName(self):
        return self.getPort()

    def getDeviceClass(self):
        return self.retrieveSetting('class')

    def getIntensity(self):
        return self.retrieveSetting('intensity')

    def getChannelId(self):
        return self.retrieveSetting('channel')

    def loadDevice(self):
        pass

    def execute(self):
        result = True
        if not self.getSerial().isConnected():
            self.getSerial().connect()
        result = result and self.getSerial().setLightAbsolute(channel=self.getChannelId(), value=self.getIntensity())
        self.getLog().info("Set light in Channel {} to {:.3f}: {}".format(
            self.getChannelId(), self.getIntensity(), result
        ))
        return result


class FluctuateJobException(JobException):
    def __init__(self, msg):
        super(FluctuateJobException, self).__init__(msg=msg)





