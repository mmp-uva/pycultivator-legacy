"""A general protocol that affect the light panel in the Multi-Cultivator"""

import simpleProtocol

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class LightProtocol(simpleProtocol.SimpleProtocol):
    """A protocol that interacts with the light panel"""

    _name = "light_protocol"

    _default_settings = simpleProtocol.SimpleProtocol.mergeDefaultSettings(
        {
            "allow_error": 1.0,  # in uE
            "force_configuration": True,
        }, namespace="protocol"
    )

    def __init__(self, config_file, settings=None):
        super(LightProtocol, self).__init__(config_file=config_file, settings=settings)

    def isConfigurationForced(self):
        """Returns whether the protocol is forced to set the intensity to the configured value instead of current"""
        return self.retrieveSetting("force_configuration", namespace="protocol") is True

    def getAllowedError(self):
        result = self.getDefaultSettings("allow_error", "protocol")
        error = self.retrieveSetting("allow_error", namespace="protocol")
        try:
            result = float(error)
        except ValueError:
            pass
        return result

    def isWithinError(self, lhs, rhs, error=None):
        """Returns whether the lhs is equal to the rhs given a certain error"""
        if error is None:
            error = self.getAllowedError()
        return abs(lhs - rhs) < error

    def _prepare(self):
        """Prepare for protocol, record current light states"""
        result = super(LightProtocol, self)._prepare()
        return result

    def _react(self):
        result = False
        self.getLog().debug("Light Protocol React")
        # adjust intensities but do not apply to panel
        channels = self.getCultivator().getChannels().values()
        for c in channels:
            result = self._adjust(c) and result
        return super(LightProtocol, self)._react() and result

    def _adjust(self, channel):
        """Adjust the light panel of this channel
        
        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        """
        state_result, intensity_result = True, True
        light = channel.getLight()
        # determine current intensity
        current_intensity = light.getLightIntensity()
        current_state = light.getLightState()
        # determine new intensity value
        new_intensity = self._adjust_intensity(channel)
        # determine new state value
        new_state = self._adjust_state(channel)
        # check if intensity has changed more than error range
        if not self.isWithinError(new_intensity, current_intensity):
            # apply
            intensity_result = light.setLightIntensity(new_intensity)
            self.inform(
                "Light Intensity @ channel {}, adjusted from {:0.2f} to {:0.2f}: {}".format(
                    channel.getIndex(), current_intensity, new_intensity, intensity_result
                )
            )
        # check if state is different
        if new_state != current_state:
            # apply
            state_result = light.setLightState(new_state)
            self.inform(
                "Light state @ channel {}, adjusted from {} to {}:".format(
                    channel.getIndex(),
                    "On" if current_state else "Off",
                    "On" if new_state else "Off",
                    state_result
                )
            )
        return intensity_result and state_result

    def _adjust_intensity(self, channel):
        """Adjusts the channel
        
        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :type start_intensity: float
        :rtype: float
        """
        light = channel.getLight()
        current = light.getLightIntensity()
        # if we need to force the configuration set the current state to this value
        if self.isConfigurationForced():
            current = light.getConfiguredLightIntensity()
        return current

    def _adjust_state(self, channel):
        """Adjust the state
        
        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :rtype: bool
        """
        light = channel.getLight()
        current = light.getLightState()
        # if we need to force the configuration set the current state to this value
        if self.isConfigurationForced():
            current = light.getConfiguredLightState()
        return current
