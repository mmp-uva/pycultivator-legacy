"""Protocol for measuring OD in a Multi-Cultivator"""

from uvaCultivator.uvaAggregation import ReadingAggregation
from uvaCultivator.uvaODMeasurement import ODMeasurement
from simpleProtocol import SimpleProtocol,SimpleProtocolException
from time import sleep
from datetime import datetime as dt

__author__ = "Joeri Jongbleots <j.a.jongbloets@uva.nl>"


class ODProtocol(SimpleProtocol):
    """Class that measures the OD and nothing else."""

    _name = "measure_od"

    _default_settings = SimpleProtocol.mergeDefaultSettings(
        {
            "gas_pause": 1
        }, namespace="protocol"
    )

    @classmethod
    def createMeasurement(cls, channel, wavelength, repeats, od_raw, **kwargs):
        ps_od = channel.getODCalibration(nm=wavelength).calculate(od_raw)
        if ps_od is None or ps_od < 0:
            cls.getLog().warning("Invalid OD value prediction ({}), predict 0 instead.".format(ps_od))
            ps_od = 0
        od_led = wavelength
        if od_led not in (680, 720):
            od_led = 680 if od_led == 0 else 720
        kwargs["channel"] = channel.getIndex()
        kwargs["t_point"] = dt.now()
        kwargs['od_repeats'] = repeats
        kwargs['od_led'] = od_led
        kwargs["od_value"] = ps_od
        kwargs["od_raw"] = od_raw
        return ODMeasurement(**kwargs)

    def __init__(self, config_file, settings=None):
        super(ODProtocol, self).__init__(config_file, settings=settings)
        self._light_states = {}

    def getGasPause(self):
        return self.retrieveSetting("gas_pause", namespace="protocol")

    def getODRepeats(self):
        return self.retrieveSetting("repeats", namespace="protocol.od")

    def getODWavelengths(self):
        return self.retrieveSetting("wavelengths", namespace="protocol.od")

    def getLightStates(self):
        """Return the light states as recorded before starting the protocol"""
        return self._light_states

    def hasLightState(self, channel):
        return channel in self.getLightStates().keys()

    def getLightState(self, channel):
        if not self.hasLightState(channel):
            raise KeyError("Invalid channel")
        return self.getLightStates()[channel]

    def recordLightStates(self, states):
        if not isinstance(states, dict):
            ValueError("Invalid light states; need a dictionary")
        self._light_states = states

    def recordLightState(self, channel, state):
        if not isinstance(channel, int):
            ValueError("Invalid channel; need an int")
        self._light_states[channel] = state is True

    def _prepare(self):
        """Prepare for the protocol: in this case make sure we are connected"""
        # first do normal preparation stuff
        result = super(ODProtocol, self)._prepare()
        # only try to connect if config is active or when forced
        connected = self.getConnection().isConnected()
        if result and not connected:
            connected = self.getConnection().connect()
            if not connected:
                self.error("Unable to configure or to connect to {}".format(
                    self.getConnection().getSerial().getPort()
                ))
        # if connected get state of led panel of all channels
        if connected:
            self.recordLightStates(self.getCultivator().getLightsState())
        # return success: we should be connected now
        self.inform("Connect to Multi-Cultivator: {}".format(result))
        return connected

    def _measure(self):
        """The actual protocol"""
        result = False
        try:
            # stop gas flow
            result = self._close_gas()
            self.inform("Stop gas flow: {}".format(result))
            if not result:
                raise ODProtocolException("Unable to stop gas flow")
            # disable light
            result = self._disable_lights()
            if not result:
                raise ODProtocolException("Unable to switch lights off")
            self.inform("Disable lights: {}".format(result))
            # do measure OD
            self.recordMeasurements(self._measure_od())
            result = True
        except KeyboardInterrupt:
            self.warn("Interrupted by user, quit gracefully")
        except ODProtocolException as ope:
            self.error(ope)
            # sleep a bit, to let system recover
            sleep(0.5)
        finally:
            if not self.getConnection().isConnected():
                self.warn("Controller was disconnected, reconnect")
                self.getConnection().connect()
            # ok restore light state and gas pump
            result_gas = self._open_gas()
            self.inform("Restore gas flow: {}".format(result_gas))
            result_light = self._enable_lights()
            self.inform("Restore lights: {}".format(result_light))
            result = result and result_gas and result_light
        # end of finally
        return result

    def _measure_od(self):
        """Do the OD Measurements"""
        results = []
        # TODO: move settings to protocol
        channels = self.getCultivator().getChannels().values()
        wavelengths = self.getCultivator().getMeasurementLeds()
        repeats = self.getCultivator().getRepeats()
        samples = self.getCultivator().getODSampleSize()
        method = self.getCultivator().getSampleAggregation()
        self.inform("Measure OD at {} wavelength(s) in {} channel(s)".format(
            len(wavelengths), len(channels)
        ))
        self.inform("Collect {} sample(s) of {} reading(s) per sample, aggregated by {}".format(
            samples, repeats, method
        ))
        for channel in channels:
            for wavelength in wavelengths:
                readings = self._collect_od(channel, wavelength=wavelength, repeats=repeats, samples=samples)
                reading = self._aggregate_od(readings, method=method)
                """:type: tuple[float, float, float]"""
                # reading = tuple[ flash, background, od]
                # make measurement
                kwargs = {
                    'wavelength': wavelength,
                    'repeats': repeats,
                    'flash': reading[0],
                    'background': reading[1],
                    'od_raw': reading[2],
                    'intensity': channel.getGivenLight(),
                    'temperature': self.getCultivator().getTemperature()
                }
                m = self.createMeasurement(channel, **kwargs)
                self._inform_od(channel, m)
                results.append(m)
        return results

    def _collect_od(self, channel, wavelength=720, repeats=3, samples=1, max_attemps=3):
        results = []
        attempts = 0
        while len(results) < samples and attempts < max_attemps:
            result = self._sample_od(channel, wavelength=wavelength, repeats=repeats)
            if result is not None:
                results.append(result)
                attempts = 0
            else:
                attempts += 1
        return results

    def _sample_od(self, channel, wavelength=720, repeats=3):
        """Measures one OD sample from the Multi-Cultivator

        :param channel:
        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :param wavelength: wavelength
        :type wavelength: int
        :param repeats: int
        :type repeats:
        :return:
        :rtype: uvaCultivator.uvaODMeasurement.ODMeasurement | None
        """
        od_led = wavelength
        if wavelength not in (1, 0):
            od_led = 0 if wavelength == 680 else 1
        return channel.getODReading(nm=od_led, repeats=repeats)

    def _aggregate_od(self, readings, method=None):
        """Aggregates multiple readings into on"""
        return ReadingAggregation.aggregate(method, readings)

    def _inform_od(self, channel, measurement):
        msg = "OD in {}@{} nm: MC OD = {:6.3f}, REF OD = {:6.3f}, BGR = {:6.3f}, LI = {:6.2f}".format(
            channel.getIndex(), measurement.getLED(asIndex=False),
            measurement.getODRaw(), measurement.getODValue(),
            measurement.getBackground(), measurement.getIntensity()
        )
        self.inform(msg)

    def _close_gas(self):
        # turn the gas flow off
        result = self.getConnection().setPumpState()
        if result:
            # TODO: Move setting to protocol
            gas_pause = self.getCultivator().getGasPause()
            self.inform("Have gas pause of {} seconds".format(gas_pause))
            # wait until all (or most) bubbles left the culture
            if gas_pause > 0:
                sleep(gas_pause)
        return result

    def _open_gas(self):
        return self.getConnection().setPumpState(True)

    def _disable_lights(self):
        # turn the light off
        return self.getCultivator().setLightsState()

    def _enable_lights(self):
        result = True
        current_states = self._light_states
        if len(current_states.keys()) == 0:
            self.warn("No previous state were recorded, so leave them OFF")
        for c in sorted(current_states.keys()):
            # write log message about light settings
            msg = "Light @ channel {}, is {}.".format(
                c, "switched back on" if current_states[c] else "left off"
            )
            self.getLog().debug(msg)
            # apply light setting
            if self.getCultivator().getChannel(c).hasLight() and current_states[c]:
                self.getCultivator().getChannel(c).setLightState(current_states[c])
        return result

    def _report(self, measurements=None):
        """Report the results of the protocol

        :param measurements: List with leds, each item is a dictionary with channels as key and each channels
        contains a list of measurements.
        :type measurements: list, None
        :return: True on success, otherwise False
        :rtype: bool
        """
        result = super(ODProtocol, self)._report()
        od_result = False
        if measurements is None:
            measurements = self.getMeasurements()
        if measurements is not None and len(measurements) > 0:
            od_result = True
            for wavelength in self.getCultivator().getMeasurementLeds(indices=True):
                records = self.selectMeasurements(wavelength=wavelength)
                od_result = self._report_wavelength(records, wavelength) and result
        return result and od_result

    def _report_wavelength(self, measurements, wavelength=720):
        """Reports the results obtained from a specific wavelength"""
        result = False
        if self.getExporterName() is not None:
            with self.createExporter(wavelengt=wavelength) as exporter:
                dest = exporter.getDestination()
                self.getLog().debug("Write OD Measurements at {}nm to {}".format(wavelength, dest))
                if exporter.hasStorage():
                    result = exporter.addODMeasurements(measurements)
                self.inform("Written OD Measurement, measured at {}nm: {}".format(
                        720 if wavelength == 1 else 680, result
                    )
                )
        return result

    def _clean(self):
        # check if we are connected
        connected = self.hasConnection() and self.getConnection().isConnected()
        # set connection state by either being not connected or by successful disconnecting
        connected = not connected or not self.getConnection().disconnect()
        self.inform("Disconnect from Multi-Cultivator: {}".format(not connected))
        # do normal clean stuff
        result = super(ODProtocol, self)._clean()
        return result and not connected


class ODProtocolException(SimpleProtocolException):

    def __init__(self, msg):
        super(ODProtocolException, self).__init__(msg)
