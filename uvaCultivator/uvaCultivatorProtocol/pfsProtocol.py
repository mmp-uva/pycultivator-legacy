"""
A script executing a protocol that measures OD and subsequently adjusts the light settings based on the OD.
"""

import lightProtocol

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PhotonFluxoStatProtocol(lightProtocol.LightProtocol):
    """A photonfluxostat protocol; adjusting the light to the OD"""

    _name = "pfs_protocol"
    _default_settings = lightProtocol.LightProtocol.mergeDefaultSettings(
        {
            "pfs.od_led": 720,
        }, namespace="protocol"
    )

    def __init__(self, config_file, settings=None):
        super(PhotonFluxoStatProtocol, self).__init__(config_file=config_file, settings=settings)

    def getWavelength(self):
        """Returns the wavelength used to set the light intensity"""
        result = self.retrieveSetting("od_led", namespace="protocol.pfs")
        return 680 if result in (0, 680) else 720

    def _adjust_intensity(self, channel):
        """Adjust the intensity

       :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
       :rtype: float
       """
        result = super(PhotonFluxoStatProtocol, self)._adjust_intensity(channel)
        light = channel.getLight()
        if not light.isODDependent():
            return result
        # set default od value
        current_od = 0.0
        # load wavelength that will be used to calculate the regime
        wavelength = self.getWavelength()
        # try to load last OD Measurement
        measurements = self.selectMeasurements(channel.getIndex(), wavelength)
        if len(measurements) > 0:
            m = measurements[0]
            current_od = m.getCalibratedODValue()
        # get dependency
        dependency = light.getODDependency()
        if not dependency.isActive():
            return result
        # calculate new intensity
        new_intensity = dependency.calculate(current_od)
        if not isinstance(new_intensity, (int, float)):
            self.inform("[PFS Protocol] Invalid value for new intensity: {}".format(new_intensity))
            return result
        # report
        self.inform(
            "[PFS Protocol] Adjust intensity in channel {} from {:.3f} to {:.3f}".format(
                channel.getIndex(), result, new_intensity
            )
        )
        return new_intensity
