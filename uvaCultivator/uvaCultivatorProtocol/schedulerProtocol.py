
from simpleProtocol import SimpleProtocol
from uvaCultivator.uvaScheduler import DeviceScheduler, DeviceJob
from time import sleep


class SchedulerProtocol(SimpleProtocol):
    """A basic protocol to schedule jobs for a device(s)"""
    _name = "scheduler_protocol"

    _default_settings = SimpleProtocol.mergeDefaultSettings(
        {
            # default settings
        }, namespace=""
    )

    def __init__(self, config_file, settings=None):
        super(SchedulerProtocol, self).__init__(config_file, settings)
        self._scheduler = None

    def getScheduler(self):
        """Return the pumpScheduler object

        :return:
        :rtype: uvaCultivator.uvaScheduler.DeviceScheduler
        """
        return self._scheduler

    def hasScheduler(self):
        return self.getScheduler() is not None

    def startScheduler(self):
        """Start separate thread that controls the pump"""
        if self._scheduler is None:
            self._scheduler = DeviceScheduler(log=self.getLog(), settings=self.getSettings())
        if not self.getScheduler().isAlive():
            self._scheduler.start()
        return self.getScheduler().isAlive()

    def stopScheduler(self, graceful=True):
        """Stop separate thread"""
        if self.hasScheduler() and self.getScheduler().isAlive():
            self.getScheduler().stop(graceful=graceful)
            if graceful:
                self.getLog().info("Waiting for Scheduler to exit")
                while self.getScheduler().isAlive():
                    try:
                        # block until scheduler is finished
                        sleep(1)
                    except KeyboardInterrupt:
                        self.getLog().warning("Received keyboard interrupt: force scheduler to stop")
                        break
                if self.getScheduler().isAlive():
                    # terminate
                    self.getScheduler().stop(False)
        return self._scheduler.isAlive()

    def _prepare(self):
        result = super(SchedulerProtocol, self)._prepare()
        success = self.startScheduler()
        self.getScheduler().keepDevices(True)
        self.getLog().info("Start Scheduler: {}".format(success))
        return result and success

    def _clean(self):
        result = super(SchedulerProtocol, self)._clean()
        # wait until scheduler is finished
        success = self.stopScheduler(graceful=True)
        self.getLog().info("Stop Scheduler: {}".format(success))
        success = success and self.getScheduler().removeDevices(ignore_users=True)
        return result and success

    def schedule(self, job):
        result = False
        if isinstance(job, DeviceJob):
            result = self.getScheduler().schedule(job)
        return result

