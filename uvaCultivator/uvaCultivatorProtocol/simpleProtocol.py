# coding=utf-8
"""
Define a measurement protocol, as a formal definition of all the step required for measuring
"""

from . import AbstractCultivatorProtocol, AbstractProtocolException
from pycultivator_legacy.config import XMLConfig
from datetime import datetime as dt
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SimpleProtocol(AbstractCultivatorProtocol):
    """
    A simple OD protocol that will measure the od for a given set of channels and nothing else.
    It will either use the build-in CSV Handler or use a user-defined function which accepts an array with leds,
    each led is a dictionary of channels where each channel references a list of measurements.
    When using the CSV Handler; this protocol will save the csv file in the current directory and the name has the
    following format"
    * <date in YYMMDD>_<project_id>_measureod_simple_<led>.csv
    """

    _name = "simple"
    _default_settings = AbstractCultivatorProtocol.mergeDefaultSettings(
        {
            "connection.fake": False,
            "exporter.name": "sqlite",
            "exporter.class": None,
            "exporter.func": None,
            "exporter.file.dir": "",
            "exporter.file.fmt": ["date", "project_id", "protocol", "led"],
            "exporter.file.sep": "_",
            "exporter.file.ext": "csv",
        }, namespace=""
    )

    def __init__(self, config_file, settings=None):
        """Initialise the protocol

        :type config_file: str
        :param settings: Dictionary with additional settings. Such as:
        - exporter.state -> to set the state of the exporter
        - exporter.name -> Name of the exporter class
        - exporter.class -> Reference to the exporter class
        - exporter.file.output_dir -> path to directory in which the csv will be stored
        - exporter.file.output_fmt -> String format template
        :type settings: dict
        :return:
        :rtype:
        """
        super(SimpleProtocol, self).__init__(settings=settings)
        self._config_file = config_file
        self._config = None
        self._cultivator = None
        self._connection = None
        self._measurements = []
        self._exporter_class = None
        self._exporter = None

    def fakesConnection(self):
        """Returns whether this protocol uses a fake connection controller"""
        return self.retrieveSetting("fake", default=self.getDefaultSettings(), namespace="connection") is True

    def fakeConnection(self, state):
        """Sets whether this protocol should use a fake connection controller"""
        self.setSetting("fake", state is True, "connection")

    def getExporterName(self):
        """Returns the name of the exporter that is used by this protocol

        :rtype: str
        """
        return self.retrieveSetting(name="name", default=self.getDefaultSettings(), namespace="exporter")

    def hasExporterClass(self):
        """Returns whether a reference to the exporter class has been loaded into memory

        :rtype: bool
        """
        return self._exporter_class is not None

    def getExporterClass(self, force=False):
        """Returns a reference to exporter class used by this protocol.

        If no exporter has been loaded yet it will be loaded. If force is set, the class is reloaded.
        """
        if not self.hasExporterClass() or force:
            self.loadExporterClass()
        return self._exporter_class

    def loadExporterClass(self):
        """Loads the reference to the exporter class in memory"""
        if self.hasConfig():
            self._exporter_class = self.getConfig().loadExporterClass()
        return self.hasExporterClass()

    def hasExporter(self):
        """Returns whether an exporter object has been created and loaded into memory

        :rtype: bool
        """
        return self._exporter is not None

    def getExporter(self, settings=None, force=False):
        """Returns the exporter object used by this class

        If one is not loaded yet it will be loaded. If force is set, the object is reloaded
        """
        if not self.hasExporter() or force:
            self.loadExporter(settings=settings)
        return self._exporter

    def loadExporter(self, settings=None):
        """Loads the exporter object into memory"""
        if self.hasConfig():
            self._exporter = self.getExporterClass()(settings=settings)
        return self.hasExporter()

    def setExporterFunc(self, func=None):
        """Sets the reference to the function that will be used to export the data

        :param func: Reference to a function that accepts 1 argument: a list of leds, where each item is a dictionary
        with channels as key and each key references a list of measurements
        :type func: list
        :return: True when the func is callable
        :rtype: bool
        """
        result = False
        if func is None or hasattr(func, "__call__"):
            self._settings["exporter.func"] = func
            result = True
        return result

    def getExporterFunc(self):
        return self.retrieveSetting(name="func", default=self.getDefaultSettings(), namespace="exporter")

    def setOutputFileDir(self, output_file_dir=None):
        """
        Set the path to the directory where the output will be written in CSV mode.
        :param output_file_dir: Path to the directory where the csv will be written in CSV mode.
        If None, the current working directory is used
        :type output_file_dir: str,  None
        :return:
        :rtype:
        """
        if output_file_dir is None:
            output_file_dir = os.getcwd()
        self._settings["exporter.file.dir"] = output_file_dir

    def getOutputFileDir(self):
        directory = self.retrieveSetting(name="dir", default=self.getDefaultSettings(), namespace="exporter.file")
        return os.path.abspath(directory)

    def getOutputFileFormat(self):
        return self.retrieveSetting(name="fmt", default=self.getDefaultSettings(), namespace="exporter.file")

    def getOutputFileSeparator(self):
        return self.retrieveSetting(name="sep", default=self.getDefaultSettings(), namespace="exporter.file")

    def getOutputFileExtension(self):
        return self.retrieveSetting(name="ext", default=self.getDefaultSettings(), namespace="exporter.file")

    def setOutputFileExtension(self, extension):
        return self.setSetting(name="ext", value=extension, namespace="exporter.file")

    def getOutputPath(self, directory=None, **kwargs):
        if directory is None:
            directory = self.getOutputFileDir()
        elements = []
        for name in self.getOutputFileFormat():
            if name in kwargs and kwargs[name] not in [None, ""]:
                elements.append(kwargs[name])
        fn = "{}.{}".format(self.getOutputFileSeparator().join(elements), self.getOutputFileExtension())
        return os.path.join(directory, fn)

    def getMeasurements(self):
        """Returns the measurements obtained by this protocol

        :return: List with leds, each item is a dictionary with channels as key and each channel
        contains a list of measurements.
        :rtype: list
        """
        return self._measurements

    def recordMeasurements(self, measurements):
        if not isinstance(measurements, (list)):
            measurements = list(measurements)
        self._measurements.extend(measurements)

    def filterMeasurementsByChannel(self, channel_idx, measurements=None):
        if measurements is None:
            measurements = self.getMeasurements()

        def f(m, idx):
            """

            :type m: uvaCultivator.uvaODMeasurement.ODMeasurement
            """
            return m["channel"] == idx
        return filter(lambda x: f(x, channel_idx), measurements)

    def filterMeasurementByWavelength(self, wavelength, measurements=None):
        if measurements is None:
            measurements = self.getMeasurements()

        def f(m, nm):
            """

            :type m: uvaCultivator.uvaODMeasurement.ODMeasurement
            """
            return m.isMeasuredAt(nm)
        return filter(lambda x: f(x, wavelength), measurements)

    def selectMeasurements(self, channel=None, wavelength=None, measurements=None):
        if measurements is None:
            measurements = self.getMeasurements()
        if channel is not None:
            measurements = self.filterMeasurementsByChannel(channel, measurements)
        if wavelength is not None:
            measurements = self.filterMeasurementByWavelength(wavelength, measurements)
        return measurements

    def sortMeasurementsByTime(self, measurements=None):
        if measurements is None:
            measurements = self.getMeasurements()
        return sorted(measurements, lambda x: x.getTimePoint())

    def getConfig(self):
        """Returns the currently used configuration object, or None if not present (loaded) yet.

        :return: Cultivator Config
        :rtype: uvaCultivator.uvaCultivatorConfig.uvaCultivatorConfig.AbstractCultivatorConfig[
        T<=uvaCultivator.uvaCultivatorConfig.uvaCultivatorConfig.AbstractCultivatorConfig]
        """
        if self._config is None:
            self.loadConfig()
        return self._config

    def hasConfig(self):
        """Returns whether there is configuration object present (loaded)."""
        return self.getConfig() is not None

    def loadConfig(self):
        if self.hasConfigFile():
            from pycultivator_legacy.config import xmlCultivatorConfig
            self._config = xmlCultivatorConfig.xmlCultivatorConfig.loadConfig(
                self.getConfigFile()
            )
        return self._config is not None

    def getConfigFile(self):
        """Returns the path to the currently used configuration file

        :rtype: str
        """
        return self._config_file

    def hasConfigFile(self):
        return self.getConfigFile() is not None and os.path.exists(self.getConfigFile())

    def getCultivator(self):
        """Returns the cultivator used by this protocol, if not defined loads it

        :return: Cultivator instance
        :rtype: uvaCultivator.uvaCultivator.Cultivator
        """
        if not self.hasCultivator():
            self.loadCultivator()
        return self._cultivator

    def loadCultivator(self, fake=None):
        """Loads the cultivator object from the configuration"""
        if fake is None:
            fake = self.fakesConnection()
        if self.hasConfig():
            self._cultivator = self.getConfig().loadCultivator(fake=fake)
        return self._cultivator is not None

    def hasCultivator(self):
        return self._cultivator is not None

    def getConnection(self):
        """Returns the connection to the cultivator

        :return:
        :rtype: uvaSerial.psiSerialController.psiSerialController
        """
        return self.getCultivator().getConnection()

    def hasConnection(self):
        return self.getCultivator().hasConnection()

    def createExporter(self, settings=None, **kwargs):
        result = None
        if settings is None:
            settings = self.mergeDictionary(self._settings, self.getConfig().getSettings())
        if self.getExporterName().upper() == "CSV":
            result = self.createCSVExporter(settings=settings, **kwargs)
        elif self.getExporterName().upper() == "MONGO":
            result = self.createMongoExporter(settings=settings, **kwargs)
        elif self.getExporterName().upper() == "SQLITE":
            result = self.createSQLiteExporter(settings=settings, **kwargs)
        elif self.getExporterName().upper() in ("ORIENT", "ORIENTDB"):
            result = self.createOrientExporter(settings=settings, **kwargs)
        return result

    def createCSVExporter(self, wavelength=720, settings=None, **kwargs):
        """Loads a CSV Exporter"""
        if settings is None:
            settings = self.mergeDictionary(self._settings, self.getConfig().getSettings())
        self.setOutputFileExtension("csv")
        # reduce exporter.file settings to baselevel
        settings = self.reduceNameSpace(settings, "exporter.file")
        # date_project-id_protocol_led.csv
        settings["output_path"] = self.getOutputPath(
            directory=self.getOutputFileDir(), date=dt.now().strftime(self.DT_FORMAT_YMD),
            project_id=self.getProjectId(), protocol=self._name,
            led="680" if wavelength == 0 or wavelength == 680 else "720"
        )
        return self.getExporter(settings=settings, force=True)

    def createMongoExporter(self, settings=None, **kwargs):
        """Loads a mongoDB exporter"""
        name = self.getExporterName().lower()
        if settings is None:
            settings = self.mergeDictionary(self._settings, self.getConfig().getSettings())
        # reduce the settings from the appropriate exporter namespace
        settings = self.reduceNameSpace(settings, "exporter.{}".format(name))
        return self.getExporter(settings=settings, force=True)

    def createSQLiteExporter(self, settings=None, **kwargs):
        """Loads a SQLite Exporter"""
        name = self.getExporterName().lower()
        if settings is None:
            settings = self.mergeDictionary(self._settings, self.getConfig().getSettings())
        self.setOutputFileExtension("db")
        # reduce the settings from the appropriate exporter namespace
        settings = self.reduceNameSpace(settings, "exporter.{}".format(name))
        # manually set the time zero
        settings["t_zero"] = self.getConfig().loadStartDate()
        # set output path
        settings["database"] = self.getOutputPath(
            path=self.getOutputFileDir(), project_id=self.getProjectId(),
            date=self.getConfig().loadStartDate().strftime(self.DT_FORMAT_YMD),
            protocol="measurements", led=None
        )
        return self.getExporter(settings=settings, force=True)

    def createOrientExporter(self, settings=None, **kwargs):
        """Loads a OrientDB Exporter"""
        name = self.getExporterName().lower()
        if settings is None:
            settings = self.mergeDictionary(self._settings, self.getConfig().getSettings())
        # reduce the settings from the appropriate exporter namespace
        settings = self.reduceNameSpace(settings, "exporter.{}".format(name))
        settings["time_zero"] = self.getConfig().loadStartDate()
        return self.getExporter(settings=settings, force=True)

    def _prepare(self):
        """Prepare for the execution of the protocol, by loading the protocol"""
        result = super(SimpleProtocol, self)._prepare()
        self._config = XMLConfig.loadConfig(self._config_file)
        if self.hasConfig():
            # load settings from the configuration
            self._settings = self.mergeDictionary(
                self._settings,
                self.getConfig().loadExperimentSettings(), source_namespace=""
            )
            # set project id
            self._settings["project_id"] = self._config.loadProjectId()
            # determine configuration state
            result = self.getConfig().loadState() or self.isForced()
            if self.isForced() and not self._config.loadState():
                self.inform("The Configuration file is inactive, but I'm forced to run this.")
        return result

    def _measure(self):
        """Execute the protocol.

        :return: Whether the execution of the protocol was a success.
        :rtype: bool
        """
        raise NotImplementedError

    def _report(self):
        return True

    def _clean(self):
        """Clean up after the execution of the protocol"""
        return True


class SimpleProtocolException(AbstractProtocolException):

    def __init__(self, msg):
        super(SimpleProtocolException, self).__init__(msg)
