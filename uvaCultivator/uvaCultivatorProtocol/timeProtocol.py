"""A protocol for adjusting the LED Panel state based on time"""

import lightProtocol

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TimeProtocol(lightProtocol.LightProtocol):

    _name = "time_protocol"

    _default_settings = lightProtocol.LightProtocol.mergeDefaultSettings(
        {
            # default settings
        }, namespace="protocol"
    )

    def __init__(self, config_file, settings=None):
        super(TimeProtocol, self).__init__(config_file=config_file, settings=settings)

    def _adjust_intensity(self, channel):
        """Adjust the intensity

        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :rtype: float
        """
        result = super(TimeProtocol, self)._adjust_intensity(channel)
        light = channel.getLight()
        if not light.isTimeDependent():
            return result
        # get dependency
        dependency = light.getTimeDependency()
        if not dependency.isActive():
            return result
        # calculate desired intensity
        adjust_factor = dependency.calculate()
        new_intensity = None
        if isinstance(adjust_factor, (int, float)):
            new_intensity = result * float(adjust_factor)
        if not isinstance(new_intensity, (int, float)):
            self.inform("[Time Protocol] Invalid value for new intensity: {}".format(new_intensity))
            return result
        self.inform(
            "[Time Protocol] Adjust intensity in channel {}, from {} to {}".format(
                channel.getIndex(), result, new_intensity
            )
        )
        return new_intensity

    def _adjust_state(self, channel):
        """Adjust the intensity

        :type channel: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
        :rtype: float
        """
        result = super(TimeProtocol, self)._adjust_state(channel)
        # implement a state affecting regime (not needed yet)
        return result


