# coding=utf-8
# !/usr/bin/env python
"""
Define a measurement protocol, as is a formal definition of what to do when measuring.
"""

from uvaCultivator.uvaScheduler import DeviceScheduler, DeviceJob
from . import AbstractParallelProtocol as aPP
from schedulerProtocol import SchedulerProtocol
from datetime import datetime as dt, timedelta as td
from time import sleep
import numpy as np

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TurbidostatProtocol(SchedulerProtocol):
    """A basic turbidostat protocol"""

    _name = "turbidostat"
    _default_settings = SchedulerProtocol.mergeDefaultSettings(
        {
            "protocol.use_fake": False,
            "od_led": 720,
            "cultivator_serial": None,
            "turbidostat.collect.time_span": 60,  # retrieve measurements of last hour
            "turbidostat.decision.threshold": 0.35,
            "turbidostat.decision.od_led": 720,
            "turbidostat.decision.breach_metric": "all",  # possible: all, mean, median, mode
            "turbidostat.decision.breach_size": 2,  # number of consecutive breaches need before deciding to dilute
            "turbidostat.pump.mapping": {
                # Pump setting of the form:
                # 0: {"in": "/dev/ttyRP0@1", "out": "/dev/ttyRP1@1"},
            },
            "turbidostat.pump.time": None,  # in seconds, takes precedence over flow - volume
            "turbidostat.pump.rate": 100,  # in RPM
            "turbidostat.pump.flow": 7.7,  # in mL/min
            "turbidostat.pump.volume": 2.4,  # 2.4, # in mL
            "turbidostat.pump.pause": 200,  # wait time in seconds between finishing pump in and pumping out
            "turbidostat.pump.out": True,
            "turbidostat.pump.out.margin": 30, # number of EXTRA seconds to run the pump out
            "reporter.name": "csv",
            #"exporter.file.dir": "",
            #"exporter.file.fmt": path.join("{path}", "{date}_{project_id}_{protocol}_{led}.csv"),
        },
        namespace=""
    )

    def __init__(self, config_file, settings=None):
        super(TurbidostatProtocol, self).__init__(config_file=config_file, settings=settings)
        # stores the collected data
        self._data = {}  # dict of channels with a tuple: ( OD , Decision )
        """ :type: dict[ int, tuple[uvaCultivator.uvaODMeasurement.ODMeasurement, bool]] """
        # importer object
        self._importer = None
        # Scheduler object
        # self._scheduler = DeviceScheduler(log=self.getLog(), settings=settings)

    def getPumpClass(self):
        if self.fakesConnection():
            from uvaSerial.regloFakeSerialController import regloFakeSerialController
            klass = regloFakeSerialController
        else:
            from uvaSerial.regloSerialController import regloSerialController
            klass = regloSerialController
        return klass

    def getThreshold(self):
        return self.retrieveSetting("threshold", namespace="turbidostat.decision")

    def getTimeSpan(self):
        return self.retrieveSetting("time_span", namespace="turbidostat.collect")

    def getBreachSize(self):
        return self.retrieveSetting("breach_size", namespace="turbidostat.decision")

    def getBreachMetric(self):
        return self.retrieveSetting(name="breach_metric", namespace="turbidostat.decision")

    def useWavelengthForDecision(self, led):
        if led in [0, 1]:
            led = [680, 720][led]
        self._settings["turbidostat.decision.od_led"] = led

    def usesWavelengthForDecision(self):
        return self.retrieveSetting(
            name="od_led", default=self.getDefaultSettings(), namespace="turbidostat.decision"
        )

    def getPumpMap(self):
        """Returns the mapping of MC Channels to Pumps

        :return: Dictionary
        :rtype: dict[ int, dict[ str, str]]
        """
        return self.retrieveSetting("mapping", namespace="turbidostat.pump")

    def getPumpPauseTime(self):
        return self.retrieveSetting(
            name="pause", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )

    def getPumpFlow(self):
        """Pump flow rate in ml/min"""
        result = self.retrieveSetting(
            name="flow", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )
        if result is None:
            volume = self.getPumpVolume()
            time = self.getPumpTime()
            if None not in (volume, time):
                result = volume / float(time)
        return result

    def getPumpTime(self):
        """How long to run the pump in seconds"""
        result = self.retrieveSetting(
            name="time", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )
        if result is None:
            volume = self.getPumpVolume()
            flow = self.getPumpFlow()
            if None not in (volume, flow):
                result = (volume / float(flow)) * 60.0
        return result

    def getPumpVolume(self):
        """How much to dispense in ml"""
        result = self.retrieveSetting(
            name="volume", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )
        if result is None:
            t = self.getPumpTime()
            flow = self.getPumpFlow()
            if None not in (t, flow):
                result = t * float(flow)
        return result

    def runsPumpOut(self):
        return self.retrieveSetting(
            name="out", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        ) is True

    def runPumpOut(self, state):
        self._settings['turbidostat.pump_out'] = state is True
        return self.runsPumpOut()

    def getPumpOutMargin(self):
        return self.retrieveSetting(
            name="margin", default=self.getDefaultSettings(), namespace="turbidostat.pump.out"
        )

    def getPumpInfo(self, mapping, channel, direction="in"):
        """Return pump information

        :type mapping: dict
        :type channel: int
        :type direction: str
        :return:
        :rtype: tuple[ None or str, None or int]
        """
        result = (None, None)
        if channel in mapping:
            if direction in mapping[channel]:
                pump_url = mapping[channel][direction]
                if pump_url is not None and "@" in pump_url:
                    result = pump_url.split("@")
                    result = (result[0], int(result[1]))
        return result

    def addDecision(self, channel, decision, od_value, t_point=None, **attributes):
        """Add a decision and accompanying od value for a given channel to the dataset.

        :type channel: int
        :type decision: bool
        :type od_value: float
        :type t_point: None or datetime.datetime
        :return: The stored decision
        :rtype: dict[str, datetime.datetime or bool or float] or None
        """
        if t_point is None:
            t_point = dt.now()
        data = {}
        data.update(attributes)
        data["time"] = t_point
        data["od_value"] = od_value
        data["decision"] = decision
        self._data[channel] = data
        return self.getDecision(channel)

    def getDecision(self, channel):
        """
        Return the decision for the given channel
        :param channel: The index of the channel
        :type channel: int
        :return: Tuple with the decision (True or False) on first position and OD value on second.
        :rtype: tuple[datetime.datetime, bool, float] or None
        """
        result = None
        if channel in self._data.keys():
            result = self._data[channel]
        return result

    # def getScheduler(self):
    #     """Return the pumpScheduler object
    #
    #     :return:
    #     :rtype: uvaCultivator.uvaScheduler.DeviceScheduler
    #     """
    #     return self._scheduler
    #
    # def hasScheduler(self):
    #     return self.getScheduler() is not None

    def getMeasurementFromDict(self, measurements, channel, key):
        result = None
        if channel in measurements:
            if key in measurements[channel]:
                result = measurements[channel][key]
        if isinstance(result, list):
            self.getLog().debug("Measurement is a list of size {}".format(len(result)))
            result = result[0]
        return result

    # def startScheduler(self):
    #     """Start separate thread that controls the pump"""
    #     if self._scheduler is None:
    #         self._scheduler = DeviceScheduler(log=self.getLog(), settings=self.getSettings())
    #     if not self.getScheduler().isAlive():
    #         self._scheduler.start()
    #     return self.getScheduler().isAlive()
    #
    # def stopScheduler(self, graceful=True):
    #     """Stop separate thread"""
    #     if self.hasScheduler() and self.getScheduler().isAlive():
    #         self.getScheduler().stop(graceful=graceful)
    #         if graceful:
    #             self.getLog().info("Waiting for the pump Scheduler to exit")
    #             while self.getScheduler().isAlive():
    #                 try:
    #                     # block until scheduler is finished
    #                     sleep(1)
    #                 except KeyboardInterrupt:
    #                     self.getLog().warning("Received keyboard interrupt: force scheduler to stop")
    #                     break
    #             if self.getScheduler().isAlive():
    #                 # terminate
    #                 self.getScheduler().stop(False)
    #     return self._scheduler.isAlive()

    # def _prepare(self):
    #     result = super(TurbidostatProtocol, self)._prepare()
    #     success = self.startScheduler()
    #     self.getScheduler().keepDevices(True)
    #     self.getLog().info("Start Scheduler: {}".format(success))
    #     return result and success

    def _measure(self):
        """Execute the measurement part of the protocol"""
        raise NotImplementedError

    def _react(self):
        """Execute the reaction"""
        measurements = self.collect()
        mapping = self.getPumpMap()
        for channel in mapping.keys():
            found = len(measurements[channel]) if channel in measurements.keys() else 0
            self.getLog().info(
                "Found {} measurements from channel {}".format(found, channel)
            )
            if found > 0:
                # we have measurements
                m = measurements[channel]
                decision = self.decide(m)
                self.getLog().info("Threshold was {decision}breached => Do {decision}run pump".format(
                    decision="" if decision["decision"] else "NOT "
                ))
                self.addDecision(channel, **decision)
                if decision["decision"]:
                    self.dilute(channel)
                else:
                    self.stopPump(channel)
        return True

    def _report(self, decisions=None):
        # we do not want to write turbidostat data
        result = False
        if decisions is None:
            decisions = self._data
        if len(decisions) > 0 and self.getExporterName() is not None:
            with self.createExporter() as exporter:
                dest = exporter.getDestination()
                self.getLog().debug("Write Turbidostat Decisions to {}".format(dest))
                if exporter.hasStorage():
                    result = exporter.addTurbidostatDecisions(decisions)
                self.inform("Writing Turbidostat Decisions: {}".format(result))
        return result

    def _clean(self):
        result = super(TurbidostatProtocol, self)._clean()
        # wait until scheduler is finished
        # success = self.stopScheduler(graceful=True)
        # self.getLog().info("Stop Scheduler: {}".format(success))
        # disconnect pump
        # success = self.getScheduler().removeDevices(ignore_users=True)
        # self.getLog().info("Disconnect active pumps: {}".format(result))
        return result

    def generateFakeMeasurement(self, t_point, od):
        from uvaCultivator.uvaODMeasurement import ODMeasurement
        attributes = {
            't_point': t_point,
            'od_value': 1,
            'intensity': 20,
        }
        result = ODMeasurement(**attributes)
        result.setCalibratedODValue(od)
        return result

    def collect(self):
        """
        Collects the measurements from a source
        :return:
        :rtype: dict[ int, dict[ datetime.datetime, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]]
        """
        result = {}
        # load data from CSV or orientDB
        # we use exporter for legacy as it is the data handler, might consider to change the name ;)
        self._importer = self.createExporter()
        self.getLog().info("Read measurements from {}".format(self._importer.getDestination()))
        # retrieve the latest data from a certain period
        t_now = dt.now()
        t_start = t_now - td(minutes=self.getTimeSpan())
        self.getLog().info("Collect measurements from the last {} minutes".format(self.getTimeSpan()))
        # read all measurements from the last hour
        if not self.fakesConnection() and self.isReporting():
            if self._importer.hasStorage():
                result = self._importer.readODMeasurements(
                    start_time=t_start, criteria={"od_led": self.usesWavelengthForDecision()}
                )
        else:
            result = {}
            for c in [0, 1, 3]:
                result[c] = {}
                t_points = range(0, self.getTimeSpan(), 5)
                for t_min in t_points:
                    t = dt.now() - td(minutes=t_min)
                    # generate a random factor between 0.8 and 1.2
                    f = round(np.random.normal(self.getThreshold(), 0.01), 3)
                    # f = round((np.random.randint(0, 10) * 0.04) + 0.8, 3)
                    result[c][t] = self.generateFakeMeasurement(t, f)
        return result

    def reduce(self, measurements, size=None):
        """Will the number of measurements to the number of measurements considered

        :type measurements: dict[datetime.datetime, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]
        :rtype: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        """
        results = []
        if size is None:
            size = self.getBreachSize()
        # order time points from high to low
        t_points = sorted(measurements.keys(), reverse=True)
        """:type: list[datetime.datetime]"""
        t_idx = 0
        while len(t_points) > 0 and len(results) < size:
            t_point = t_points.pop(0)
            ms = measurements[t_point]
            if isinstance(ms, (list, tuple)):
                for measurement in ms:
                    results.append(measurement)
                    t_idx += 1
            else:
                results.append(ms)
            del ms
            t_idx += 1
        return results

    def decide(self, measurements):
        raise NotImplementedError

    def dilute(self, channel):
        """
        Dilutes a channel; e.g. schedule adding media and schedule the removal
        """
        # schedule adding media
        pump_time = self.getPumpTime()
        t_in = dt.now()
        # schedule to start pump
        settings = {'scheduler.job.pump.time': pump_time}
        result = self.startPump(channel, start_time=t_in, settings=settings)
        # schedule to actively tell the pump to stop after it has finished
        # use 5 second margin
        self.stopPump(channel, start_time=t_in + td(seconds=pump_time + 5))
        # schedule media removal
        if result and self.runsPumpOut():
            # set out
            t_out = t_in + td(seconds=self.getPumpPauseTime())
            # get margin in running pump out
            pump_margin = self.getPumpOutMargin()
            # calculate time to run pump out
            pump_time += pump_margin
            settings = {"scheduler.job.pump.time": pump_time}
            result = self.startPump(channel, start_time=t_out, direction='out', settings=settings)
        return result

    def startPump(self, channel, start_time=None, settings=None, **kwargs):
        if start_time is None:
            start_time = dt.now()
        return self.schedulePumpJob(channel, start_time=start_time, mode='start', settings=settings, **kwargs)

    def stopPump(self, channel, start_time=None, settings=None, **kwargs):
        """Instruct pump to stop pumping"""
        if start_time is None:
            start_time = dt.now()
        return self.schedulePumpJob(channel, start_time=start_time, mode="stop", settings=settings, **kwargs)

    def schedulePumpJob(self, mc_channel, start_time=None, mode='start', direction="in", settings=None, **kwargs):
        if settings is None:
            settings = {}
        # add kwargs
        settings.update(kwargs)
        result = False
        # retrieve channel -> pump map
        port, pump_channel = self.getPumpInfo(self.getPumpMap(), mc_channel, direction)
        job = None
        settings["scheduler.job.pump.channel"] = pump_channel
        settings["scheduler.job.pump.class"] = self.getPumpClass()
        if mode.lower() == "start":
            settings["scheduler.job.pump.serial.port"] = port
            job = StartPumpJob(t_schedule=start_time, settings=settings)
        if mode.lower() == "stop":
            settings["scheduler.job.pump.serial.port"] = port
            job = StopPumpJob(t_schedule=start_time, settings=settings)
        # schedule the job
        return self.schedule(job)


class ParallelTurbidostatProtocol(TurbidostatProtocol, aPP):
    """
    This class will do OD Measurements and after that adjust the light. It's capabale
    """

    def __init__(self, config_file, **kwargs):
        aPP.__init__(self, **kwargs)
        TurbidostatProtocol.__init__(self, config_file=config_file, **kwargs)
        self._name = "batch_turbidostat"

    # No need to override the execute function to redirect, becasue it will automatically select the Cultivation execute
    # def execute(self):
    # 	CultivationProtocol.execute(self)


class PumpJob(DeviceJob):
    """Abstract Pump Job"""

    _namespace = "scheduler.job.pump"
    _default_settings = DeviceJob.mergeDefaultSettings(
        {
            "serial.port": None,
            "class": None,
            "channel": None,
            "flow": 7,     # in mL/min
            "volume": 7,   # in mL
            "time": None,  # if None; try to calculate from flow and volume
            "rate": 100,
        }, namespace=_namespace
    )

    def __init__(self, t_schedule=None, parent=None, settings=None):
        super(PumpJob, self).__init__(t_schedule=t_schedule, parent=parent, settings=settings)

    def getPumpPort(self):
        return self.retrieveSetting("serial.port")

    def hasPumpPort(self):
        return self.getPumpPort() is not None

    def getPumpClass(self):
        return self.retrieveSetting("class")

    def isConnected(self):
        return self.hasPump() and self.getPump().isConnected()

    def getPump(self):
        """Return handle to Pump Connection

        :rtype: uvaSerial.regloSerialController.regloSerialController
        """
        return self.getDevice()

    def hasPump(self):
        return self.getPump() is not None

    def loadDevice(self):
        """Create a new device (pump) instance"""
        result = None
        if self.hasPumpPort():
            klass = self.getPumpClass()
            settings = self.isolateNameSpace(self.getSettings(), self.getNameSpace())
            settings = self.isolateNameSpace(settings, 'serial')
            settings = self.prependNameSpace(settings, 'serial.controller')
            result = klass(settings=settings)
        return result

    def getDeviceName(self):
        return self.getPumpPort()

    def getPumpChannel(self):
        return self.retrieveSetting("channel")

    def getPumpRate(self):
        return self.retrieveSetting("rate")

    def getPumpFlow(self):
        return self.retrieveSetting("flow")

    def getPumpVolume(self):
        return self.retrieveSetting("volume")

    def getPumpTime(self, flow=None, volume=None):
        result = self.retrieveSetting(name="time", default=self.getDefaultSettings())
        if result is None:
            if flow is None:
                flow = self.getPumpFlow()
            if volume is None:
                volume = self.getPumpVolume()
            if None not in [volume, flow]:
                result = round((volume / flow) * 60, 3)
        return result

    def connect(self):
        result = self.isConnected()
        if not result:
            result = self.getPump().connect()
        return result

    def disconnect(self):
        result = True
        if self.isConnected():
            result = self.getPump().disconnect()
        return result

    def prepare(self):
        result = super(PumpJob, self).prepare()
        if result:
            # connect function will automatically check for an existing connection
            result = self.connect()
        return result

    def execute(self):
        raise NotImplementedError

    def clean(self):
        result = super(PumpJob, self).clean()
        # do not disconnect, we have scheduler take care of this
        # return self.disconnect(
        return result


class StartPumpJob(PumpJob):

    _name = "startPump"

    def execute(self):
        result = False
        if not self.isConnected():
            result = self.connect()
        if self.configurePump():
            # run pump
            c = self.getPumpChannel()
            result = self.getPump().startPump(c)
            self.getLog().debug("Instructed pump to run: {}".format(result))
        return result

    def configurePump(self):
        c = self.getPumpChannel()
        self.getLog().debug("Configure channel {} of pump {}".format(c, self.getPumpPort()))
        # dummy command so the connection is
        result = self.getPump().isRunning(c)
        self.getLog().debug("Is pump running? {}".format(result))
        result = self.getPump().stopPump(c)
        self.getLog().debug("Instructed pump to stop: {}".format(result))
        result = self.getPump().setPumpMode(c, self.getPump().PUMP_MODE_TIME)
        self.getLog().debug("Set pump mode to TIME: {}".format(result))
        result = self.getPump().setSpeedRPM(c, self.getPumpRate(), rate_mode=False)
        self.getLog().debug("Set pump rpm rate: {}".format(result))
        # self.getPump().setFlowMode(c, False)  # use RPM
        result = self.getPump().setRotationDirection(c)
        self.getLog().debug("Set pump direction: {}".format(result))
        t = self.getPumpTime()
        result = self.getPump().setDispenseTime(c, t)
        self.getLog().debug("Set pup dispense time to {}: {}".format(t, result))
        return result


class StopPumpJob(PumpJob):
    """Stop a pump"""

    _name = "stopPump"

    def execute(self):
        if not self.isConnected():
            self.connect()
        c = self.getPumpChannel()
        result = self.getPump().isRunning(c)
        self.getLog().debug("Is pump running? {}".format(result))
        # stop the pump
        result = self.getPump().stopPump(c)
        self.getLog().debug("Instructed pump to stop: {}".format(result))
        return result
