
from odProtocol import ODProtocol
from time import sleep

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class ValveProtocol (ODProtocol):
    """Class that measures the OD """

    _name = "measure_od_advanced"
    _namespace = "protocol.relay"
    _default_settings = ODProtocol.mergeDefaultSettings(
        {
            "state": False,
            "bus": None,
            "address": None,
            "valve.index": 3,
            "pause": 1.5
        }, namespace=_namespace
    )

    def __init__(self, config_file, settings=None):
        super(ValveProtocol, self).__init__(config_file=config_file, settings=settings)
        self._relay_device = None

    def usesValve(self):
        return self.retrieveSetting("state", namespace="protocol.relay") is True

    def setValveState(self, state):
        self.setSetting("state", state is True, namespace="protocol.relay")

    def getRelayBus(self):
        return self.retrieveSetting("bus", namespace="protocol.relay")

    def setRelayBus(self, bus):
        self.setSetting("bus", bus, namespace="protocol.relay")

    def getRelayAddress(self):
        return self.retrieveSetting("address", namespace="protocol.relay")

    def setRelayAddress(self, address):
        self.setSetting("address", address, namespace="protocol.relay")

    def getValveIndex(self):
        return self.retrieveSetting("valve.index", namespace="protocol.relay")

    def setValveIndex(self, index):
        self.setSetting("valve.index", index, namespace="protocol.relay")

    def getValvePause(self):
        return self.retrieveSetting("pause", namespace="protocol.relay")

    def setValvePause(self, pause):
        self.setSetting("pause", pause, namespace="protocol.relay")

    def createRelayDevice(self):
        # load package
        cp2104Connection = self._import_relay()
        # if package was loaded successful, we can continue
        if cp2104Connection is not None:
            klass = cp2104Connection.CP2104Connection
            if self.fakesConnection():
                self.getLog().info("Use FAKE Relay Connection")
                klass = cp2104Connection.FakeCP2104Connection
            self._relay_device = klass()
        return self._relay_device

    def getRelayDevice(self):
        """Get a connection handle to the relay

        :rtype: pycultivator_relay.connection.cp2104Connection.CP2104Connection
        """
        if self._relay_device is None:
            self.createRelayDevice()
        return self._relay_device

    @property
    def relay_device(self):
        return self.getRelayDevice()

    def hasRelayDevice(self):
        return self.getRelayDevice() is not None

    def _prepare(self):
        result = super(ValveProtocol, self)._prepare()
        # only continue of did not encounter problems and we need the valve
        if result and self.usesValve():
            # load the package
            cp2104Connection = self._import_relay()
            if cp2104Connection is None:
                result = False
            # if we were able to load
            if result and not self.relay_device.isConnected():
                valve_bus = self.getRelayBus()
                valve_address = self.getRelayAddress()
                try:
                    result = self.relay_device.connect(bus=valve_bus, address=valve_address)
                except cp2104Connection.USBConnectionException:
                    self.getLog().critical("Unable to connect to CP2104 Device. Setting 'use_valve' to False")
                    result = False
            # at this point result should be True, if not we were not able to connect!
            if not result:
                self.setValveState(False)
        # report whether we connect to the valve
        self.inform("Connect to Valve: {}".format(self.usesValve()))
        return True

    def _import_relay(self):
        try:
            from pycultivator_relay.connection import cp2104Connection
        except ImportError:
            cp2104Connection = None
            self.getLog().warning("Unable to load the pycultivator-relay package")
        return cp2104Connection

    def _close_gas(self):
        # stop gas bubbling
        state = self.getConnection().setPumpState()
        if not state:
            raise Exception("Unable to turn gas pump OFF")
        if self.usesValve():
            result = self._close_relay()
        else:
            gas_pause = self.getCultivator().getGasPause()
            self.inform("Have gas pause of {} seconds".format(gas_pause))
            # wait until all (or most) bubbles left the culture
            if gas_pause > 0:
                sleep(gas_pause)
            result = True
        return result

    def _close_relay(self):
        """The actual protocol"""
        result = True
        if self.usesValve():
            # switch valve open to release air pressure/stop bubbles
            relay_index = self.getValveIndex()
            result = self.relay_device.setLatchState(relay_index, 1)
            self.inform("Open Valve {}: {}".format(relay_index, result))
            # wait for pressure to release
            valve_pause = self.getValvePause()
            self.inform("Open air valve for {} seconds".format(valve_pause))
            sleep(valve_pause)
            # close valve
            result = self.relay_device.setLatchState(relay_index, 0)
            self.inform("Close Valve {}: {}".format(relay_index, result))
        return result

    def _clean(self):
        result = True
        if self.hasRelayDevice() and self.getRelayDevice().isConnected():
            result = self.relay_device.disconnect()
        self.inform("Disconnect from Valve: {}".format(result))
        result = super(ValveProtocol, self)._clean() and result
        return result
