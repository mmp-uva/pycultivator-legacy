# coding=utf-8
""" Module with the basic classes required for describing Dependencies between two things"""

from pycultivator_legacy.core import BaseObject

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Dependency(BaseObject):
    """An abstract dependency class"""

    def __init__(self, dependency_id, source, variable, target, state=False, settings=None):
        super(Dependency, self).__init__(settings=settings)
        self._id = dependency_id
        self._source = source
        self._variable = variable
        self._target = target
        self._state = state

    @classmethod
    def loadDefault(cls):
        """Returns the default object"""
        return cls(dependency_id=0, source="self", variable="od", target="intensity", state=False)


    @classmethod
    def canLoadXML(cls, xml):
        """Determines if this class is able to load this xml Element

        :type xml: lxml.etree._Element._Element
        :rtype: bool
        """
        return xml.tag == "{uvaCultivator}dependency"

    @classmethod
    def createFromXMLElement(cls, xml, config, default=None):
        """Creates the dependency object from the given xml element or returns the default (using loadDefault)

        <dependency id="1" source="left" variable="intensity" target="intensity" active="true">
            <rhythm>
            OR
            <polynomial
        </dependency>
        :type xml: lxml.etree._Element._Element
        :type default: Dependency
        :rtype : Dependency
        """
        from pycultivator_legacy.config import XMLConfig
        result = None
        if default is None:
            default = cls.loadDefault()
        dependency_id = XMLConfig.validateValue(xml.get("id"), int, default.getId())
        source = XMLConfig.validateValue(xml.get("source"), str, default.getSource())
        variable = XMLConfig.validateValue(xml.get("variable"), str, default.getVariable())
        target = XMLConfig.validateValue(xml.get("target"), str, default.getTarget())
        state = XMLConfig.validateValue(xml.get("active"), bool, default.isActive())
        if None not in [dependency_id, source, variable, target, state]:
            result = cls(dependency_id=dependency_id, source=source, variable=variable, target=target, state=state)
        return result

    def calculate(self, source_value=0.0):
        """Calculates a value based on the rules described by this class

        :param source_value: The value the insert in the polynomials
        :type source_value: float, int
        :return:
        :rtype: float
        """
        raise NotImplementedError

    def getId(self):
        return self._id

    def setId(self, identifier):
        self._id = identifier
        return self

    def getSource(self):
        return self._source

    def setSource(self, source):
        """ Set input device/instrument

        :param source:
        :type source: str | int
        :return:
        """
        self._source = source
        return self

    def getVariable(self):
        return self._variable

    def setVariable(self, variable):
        """ Set the input variable name

        :param variable:
        :type variable: str
        :return:
        """
        self._variable = variable
        return self

    def getTarget(self):
        return self._target

    def setTarget(self, target):
        """ Set the ouput variable name

        :param target:
        :type target: str
        :return:
        """
        self._target = target
        return self

    def isActive(self):
        return self._state is True

    def setActive(self, state):
        self._state = state is True
        return self

    # def __str__(self):
    #     """Returns a string representation of this dependency object"""
    #     return "Dependency (#%s, %sactive) %s depends on %s of %s" % (
    #         self.getId(), "" if self.isActive() else "in", self.getTarget(), self.getVariable(), self.getSource()
    #     )

    def copy(self, to=None):
        """Copies this dependency to the `to` object

        :type to: uvaCultivator.uvaDependency.Dependency
        :rtype: uvaCultivator.uvaDependency.Dependency[T < uvaCultivator.uvaDependency.Dependency]
        """
        if to is None:
            to = self.loadDefault()
        # copy
        to.setId(self.getId())
        to.setSource(self.getSource())
        to.setVariable(self.getVariable())
        to.setTarget(self.getTarget())
        to.setActive(self.isActive())
        return to
