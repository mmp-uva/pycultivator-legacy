"""Base Module implementing Exception Classes"""

__author__ = 'joeri jongbloets <j.a.jongbloets@uva.nl>'


class UVAException(Exception):

    def __init__(self, msg):
        super(UVAException, self).__init__()
        self._msg = msg

    def getName(self):
        """Returns the name of this exception class"""
        return self.__class__.__name__

    def getMessage(self):
        """Returns the message of this object"""
        return self._msg

    def __str__(self):
        return "[{}]: {}".format(self.getName(), self.getMessage())
