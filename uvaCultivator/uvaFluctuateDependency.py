"""
Basic module layer that implements rapid fluctuations in light intensity
"""
from uvaDependency import Dependency
from math import pi, sqrt, cos, sin
from random import randrange, random


class FluctuateDependency(Dependency):

    _default_settings = Dependency.mergeDefaultSettings(
        {
            "fluctuate.pause": 5,  # pause between fluctuations, in seconds
            "fluctuate.simulated.biomass_concentration": 0.148,  # in units of g L^-1
            "fluctuate.simulated.extinction_coefficient": 2.55,  # in units of L g^-1 cm^-1
            "fluctuate.simulated.vessel_radius": 5.0,  # in units of cm
            "fluctuate.simulated.sum_value": 16015.1109179 #
        }, namespace=""
    )

    _randomize_steps_multiplier = 3
    _max_randomize_fails = 3

    def __init__(self, dependency_id, source, variable, target, state=False, settings=None):
        Dependency.__init__(
            self, dependency_id=dependency_id, source=source, variable=variable, target=target, state=state,
            settings=settings
        )

    @classmethod
    def loadDefault(cls):
        result = super(FluctuateDependency, cls).loadDefault()
        result.setVariable("intensity")
        return result

    @classmethod
    def canLoadXML(cls, xml):
        """Determines if this class is able to load this xml Element

        :type xml: xml.etree._Element._Element
        :rtype: bool
        """
        return super(FluctuateDependency, cls).canLoadXML(xml) and xml.find("./{uvaCultivator}settings") is not None

    @classmethod
    def createFromXMLElement(cls, xml, config, default=None):
        """Create the dependency from an XML Element

        :param xml:
        :type xml: xml.etree._Element._Element
        :return:
        :rtype: FluctuateDependency
        """
        result = super(FluctuateDependency, cls).createFromXMLElement(xml, config=config, default=default)
        # parse settings
        if isinstance(result, FluctuateDependency):
            settings = config.parseSettings(source=xml)
            if settings is not None:
                result.mergeSettings(settings)
            else:
                result = None
        return result

    def getPause(self):
        return self.retrieveSetting("pause", namespace='fluctuate')

    def getMaxFails(self):
        return self._max_randomize_fails

    def getOD(self):
        return self.retrieveSetting("od", namespace='fluctuate.simulated')

    def getExtinctionCoefficient(self):
        return self.retrieveSetting("extinction_coefficient", namespace='fluctuate.simulated')

    def getVesselRadius(self):
        return self.retrieveSetting("vessel_radius", namespace='fluctuate.simulated')

    def getSumValue(self):
        return self.retrieveSetting("sum_value", namespace='fluctuate.simulated')

    def seedIntensities(self, fluctuation_count, incident_intensity, dtheta=pi / 90.0, dr=0.02):
        average_intensity = self.getSumValue()*incident_intensity/(pi*self.getVesselRadius()**2)*dr*dtheta
        initial_intensities = [average_intensity] * fluctuation_count
        return initial_intensities

    def calculate(self, fluctuation_count=0, source_value=0.0):
        initial_intensities = self.seedIntensities(fluctuation_count=fluctuation_count, incident_intensity=source_value)
        final_intensities = self.randomize(incident_intensity=source_value, initial_intensities=initial_intensities)
        return final_intensities

    def randomize(self, incident_intensity, initial_intensities):
        min_intenstiy = self.calculateMinimumIntensity(incident_intensity)
        final_intensities = list(initial_intensities)
        randomization_steps = len(initial_intensities) * self._randomize_steps_multiplier
        for j in range(randomization_steps):
            adjusted_intensities = False
            fail_count = 0
            while not adjusted_intensities and fail_count < self.getMaxFails():
                # ignore endpoints to allow for ease of stitching multiple randomizations together
                index_1 = randrange(1, len(final_intensities) - 1)
                intensity_1 = self.calculateIntensity(incident_intensity=incident_intensity,
                                                      radius=self.getVesselRadius() * random(), theta=2 * pi * random())
                delta_intensity = intensity_1 - final_intensities[index_1]
                index_2 = randrange(1, len(final_intensities) - 1)
                intensity_2 = final_intensities[index_2] - delta_intensity
                if min_intenstiy <= intensity_2 <= incident_intensity:
                    final_intensities[index_1] += delta_intensity
                    final_intensities[index_2] -= delta_intensity
                    adjusted_intensities = True
                fail_count += 1
        return final_intensities

    def calculateMinimumIntensity(self, incident_intensity):
        min_intenstiy = incident_intensity*10**(-self.getExtinctionCoefficient()*self.getOD()*2*self.getVesselRadius())
        return min_intenstiy

    def calculateIntensity (self, incident_intensity, radius, theta):
        '''Calculate intensity in simulated vessel given the incident light and a position within the vessel in polar coordinates'''
        intensity = incident_intensity*10**(-self.getExtinctionCoefficient()*self.getOD()*(sqrt(self.getVesselRadius()**2-(radius*cos(theta))**2)-radius*sin(theta)))
        return intensity

