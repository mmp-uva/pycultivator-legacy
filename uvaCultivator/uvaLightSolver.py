# coding=utf-8
"""
Basic module layer that represents a Cultivator Light Dependency.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from pycultivator_legacy.core import BaseObject
import numpy as np


class BasicLightSolver(BaseObject):
    """
    Basic Light Sovler class, provides interface
    """

    def __init__(self, cultivator, settings=None):
        """

        :type cultivator: uvaCultivator.uvaCultivator.Cultivator
        :return:
        :rtype:
        """
        super(BasicLightSolver, self).__init__(settings=settings)
        self._cultivator = cultivator

    def getCultivator(self):
        """
        :return:
        :rtype: uvaCultivator.uvaCultivator.Cultivator
        """
        return self._cultivator

    def getChannelCount(self):
        return self.getCultivator().getChannelCount()

    def getChannel(self, idx):
        return self.getCultivator().getChannel(idx)

    def solve(self, simulate=False):
        """
        :param simulate: Should the new light settings be applied to multicultivator, or only in memory
        :type simulate: bool
        :return: True on success, False otherwise
        :rtype: bool
        """
        raise NotImplementedError


class LinearLightSolver(BasicLightSolver):
    def __init__(self, cultivator, settings=None):
        BasicLightSolver.__init__(self, cultivator, settings=settings)

    def solve(self, simulate=False):
        # collect all polynomials from each channel
        # store contribution coefficient for each channel
        # coefficients[ subject , dependent ]
        coefficients = np.zeros((self.getChannelCount(), self.getChannelCount()))
        # store target intensity + offsets of each contribution polynomial
        ordinates = np.zeros(self.getChannelCount())
        for c in self.getCultivator().getChannelRange():
            subject = self.getCultivator().getChannel(c)
            ordinate = subject.getLightIntensity() * int(subject.getLightState())
            for dependent in self.getCultivator().getChannels():
                from  uvaPolynomialDependency import NeighbourDependency
                p = NeighbourDependency.loadDefault().getPolynomialByDomain()
                if subject.getLight().hasNeighbourDependency(dependent):
                    p = subject.getLight().getNeighbourDependency(dependent)
                # ok last coefficient
                ordinate += p.coeffs[-1]
                coefficients[subject.getIndex(), dependent.getIndex()] = 0
                if len(p.coeffs) > 1:
                    coefficients[subject.getIndex(), dependent.getIndex()] = p.coeffs[0]
            ordinates[subject.getIndex()] = ordinate
        # solve
        solution = np.linalg.solve(coefficients, ordinates)
        # set each channel to this intensity
        for channel in self.getCultivator().getChannels():
            self.getLog().info("Set Light intensity of channel {}, from desired {} to solution {}".format(
                channel.getIndex(),
                channel.getLightIntensity(),
                solution[channel.getIndex()]
            ))

            new_intensity = solution[channel.getIndex()]
            if new_intensity > 300:
                new_intensity = 300
            if new_intensity < 0:
                new_intensity = 0
            if 0 < new_intensity > 300:
                self.getLog().warning(
                    "Solution ({:0.3f}) of neighbour dependent intensity for channel {} is out of range "
                    "(0,300). Set the new intensity to {:0.3f} instead".format(
                        solution[channel.getIndex()], channel, new_intensity
                    ))
            if new_intensity > 0:
                channel.setLightIntensity(new_intensity, simulate=simulate)
            else:
                channel.setLightState(False, simulate)


class StrategicLightSolver(BasicLightSolver):
    def __init__(self, cultivator, settings=None):
        BasicLightSolver.__init__(self, cultivator, settings=settings)
        self._iteration = 0
        self._targets = None
        self._min_possible = None
        self._acceptance = None

    def loadTargets(self):
        if self._targets is None:
            self._targets = [0]
            # first calculate targets
            for c in self.getCultivator().getChannelRange():
                ls = float(self.getChannel(c).getLightState())
                # mi = self.getCultivator().getChannelLight(c).getLightIntensity() * ls
                ti = self.getChannel(c).getLightIntensity() * ls
                # if mi > ti:               # if min intensity > target intensity: use minimum
                # 	self._targets.append(mi)
                # else:                     # else use target intensity
                self._targets.append(ti)
            self._targets.append(0)
        elif self._acceptance is not None:
            for idx in range(len(self._targets)):
                if not self._acceptance[idx]:
                    self._targets[idx] = self._min_possible[idx]
        self.getLog().info("Targets: \n {}".format(self._targets))

    def runAlgorithm(self):
        self.loadTargets()
        # now we have 10 elements
        self._min_possible = [0]
        for idx in range(1, len(self._targets) - 1):  # iteratore over 1 .. 8
            l_contrib = 0
            r_contrib = 0
            if self.getCultivator().hasChannel(idx - 1):
                lr = self.getCultivator().getChannelLight(idx - 1)
                if lr.getLightState() and lr.getLightIntensity() > 0:
                    if lr.hasLeftNeighbourDependency():
                        l_contrib = lr.getLeftNeighbourDependency().calculate(self._targets[idx - 1])
                    if lr.hasRightNeighbourDependency():
                        r_contrib = lr.getRightNeighbourDependency().calculate(self._targets[idx + 1])
            self._min_possible.append(l_contrib + r_contrib)
        self._min_possible.append(0)
        self.getLog().info("Min possible: \n{}".format(self._min_possible))
        solutions = []
        for idx in range(len(self._targets)):
            solutions.append(self._targets[idx] - self._min_possible[idx])
        self.getLog().info("Solutions: \n{}".format(solutions))
        self._acceptance = []
        for idx in range(len(self._targets)):
            if self._targets[idx] - self._min_possible[idx] >= 0:
                self._acceptance.append(True)
            else:
                self._acceptance.append(False)
        self.getLog().debug("Acceptance: \n{}".format(self._acceptance))
        if self._iteration == 0 and False in self._acceptance:
            self.getLog().info("Recalculate!")
            self._iteration = 1
            solutions = self.runAlgorithm()
        compromised = False
        for idx in range(len(solutions)):
            if solutions[idx] < 0:
                compromised = True
                solutions[idx] = 0
        self.getLog().debug("Compromised: \n{}".format(compromised))
        self.getLog().debug("Solutions: \n{}".format(solutions))
        return solutions

    def solve(self, simulate=False):
        solutions = self.runAlgorithm()
        for c in self.getCultivator().getChannelRange():
            channel = self.getCultivator().getChannel(c)
            self.getLog().info("Set Light intensity of channel {}, from desired {} to solution {}".format(
                channel.getIndex(),
                channel.getLightIntensity(),
                solutions[channel.getIndex() + 1]
            ))
            new_intensity = solutions[channel.getIndex() + 1]
            if new_intensity > 300:
                new_intensity = 300
            if new_intensity < 0:
                new_intensity = 0
            if 0 < new_intensity > 300:
                self.getLog().warning(
                    "Solution ({:0.3f}) of neighbour dependent intensity for channel {} is out of range "
                    "(0,300). Set the new intensity to {:0.3f} instead".format(
                        solutions[channel.getIndex() + 1], channel, new_intensity
                    ))
            if new_intensity > 0:
                channel.setLightIntensity(new_intensity, simulate=simulate)
            else:
                channel.setLightState(False, simulate)
