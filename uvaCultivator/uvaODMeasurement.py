# coding=utf-8
"""
This module provides the class to abstract an OD Measurement
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from pycultivator_legacy.core import BaseObject
from datetime import datetime as dt


class ODMeasurement(BaseObject):
    """
    A class representing one OD Measurement.
    """

    DEFAULT_REPEATS = 3
    OD_LED_680 = 0
    OD_LED_720 = 1
    DEFAULT_LED = OD_LED_720
    TIME_SECOND = 1  # 1 second per second
    TIME_MINUTE = 60  # 60 seconds per minute
    TIME_HOUR = TIME_MINUTE * 60  # 60 minutes per hour
    TIME_DAY = TIME_HOUR * 24  # 24 hours per day
    DEFAULT_TIMEDELTA_UNIT = TIME_MINUTE
    DEFAULT_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
    RELATIVE_TIMEPOINT = 0
    ABSOLUTE_TIMEPOINT = 1
    DEFAULT_TIMEPOINT_FORMAT = ABSOLUTE_TIMEPOINT

    _default_attributes = {
        "t_zero": None,
        "t_point": None,
        "od_repeats": DEFAULT_REPEATS,
        "od_led": DEFAULT_LED,
        "od_raw": None,
        "od_value": None,
        "od_background": None,
        "od_flash": None,
        "light_intensity": None,
        "temperature": None,
    }

    _synonym_mapping = {
        "time_zero": "t_zero",
        "time_point": "t_point",
        "timepoint": "t_point",
        "time": "t_point",
        "repeats": "od_repeats",
        "led": "od_led",
        "OD": "od_value",
        "background": "od_background",
        "flash": "od_flash",
        "intensity": "light_intensity",
    }

    def __init__(self, settings=None, **attributes):
        """
        ( t, Repeats, LED, Intensity, OD, Flash, Background)
        :param settings: Settings dictionary of this object (should not contain attributes)
        :type settings: dict[str, object]
        :param attributes: Attributes given as key arguments. It is also possible to use the synonyms (see the
        ODMeasurement._synonym_mapping dictionary)
        :type attributes: dict[str, object]
        :return: The Measurement object
        :rtype: ODMeasurement
        """
        super(ODMeasurement, self).__init__(settings=settings)
        self._attributes = self.mergeDictionary(self._default_attributes, attributes, source_namespace="")
        # manually set synonyms
        for synonym in self._synonym_mapping.keys():
            if synonym in attributes.keys():
                attribute = self._synonym_mapping[synonym]
                self._attributes[attribute] = attributes[synonym]
        # OD = -( log(res/CALIB_COEFF) ) / log(10)
        # res = (flash-bgr)
        self._timedelta_unit = self.DEFAULT_TIMEDELTA_UNIT
        self._datetime_format = self.DEFAULT_DATETIME_FORMAT

    def getSynonymMapping(self):
        return self._synonym_mapping

    def getSynonyms(self):
        return self.getSynonymMapping().keys()

    def getSynonym(self, synonym):
        """
        Returns the translation of this synonym. If there is no translation defined the synonym is returned.
        :param synonym: The synonym to translate.
        :type synonym: str
        :return: The translation or the synonym (if no translation exists)
        :rtype: str
        """
        result = synonym
        if self.hasSynonym(synonym):
            result = self.getSynonymMapping()[synonym]
        return result

    def hasSynonym(self, synonym):
        return synonym in self.getSynonymMapping().keys()

    def setSynonym(self, synonym, name):
        """
        Will add/set a sysnonym translation of the form: synonym => name.
        :param synonym: Synonym name of the attribute
        :type synonym: str
        :param name: True name of the attribute
        :type name: str
        :return: Returns whether a new translation was added (True) or updated (False
        :rtype: bool
        """
        result = not self.hasSynonym(synonym)
        self.getSynonymMapping()[synonym] = name
        return result

    def getAttributes(self):
        """
        :return: The attribute dictionary
        :rtype: dict
        """
        return self._attributes

    def hasAttribute(self, key):
        """
        :param key: Name of the key
        :type key: str
        :return: Whether the key is in the attribute dictionary
        :rtype: bool
        """
        return key in self.getAttributes().keys()

    def getAttribute(self, key):
        """
        Returns the value of the key if present, the key is first processed with the mapping
        :param key: Name of the attribute, will be translated with the synonym mapping if there is a
        translation defined.
        :type key: str
        :return: The value of the attribute or None if not found
        :rtype: object or None
        """
        result = None
        key = self.getSynonym(key)
        if self.hasAttribute(key):
            result = self.getAttributes()[key]
        return result

    def __getitem__(self, item):
        return self.getAttribute(item)

    def setAttribute(self, key, value):
        """
        Sets the value of the attribute, with the name equal to key, equal to value.
        If there is a synonym mapping available, we will translate key.
        :param key: Name of the attribute, can be a synonym defined in synonym mapping
        :type key: str
        :param value: The value of the attribute to be set to.
        :type value: object
        :return: Indicates if an attribute was created (True) or updated (False)
        :rtype: bool
        """
        result = not self.hasAttribute(key)
        self.getAttributes()[key] = value
        return result

    def __setitem__(self, key, value):
        self.setAttribute(key, value)

    def getTimePoint(self):
        """
        Returns the datetime object representing the time point at which this measurement was made.
        Returns the absolute timepoint
        :rtype: datetime.datetime
        """
        return self.getAttribute("t_point")

    def getTimeZero(self):
        """
        The time zero of the experiment during which the measurement was performed.
        :return:
        :rtype: datetime.datetime or None
        """
        return self.getAttribute("t_zero")

    def setTimeZero(self, time_zero):
        if isinstance(time_zero, dt):
            self.setAttribute(key="t_zero", value=time_zero)
        return self

    def getRelativeTimePoint(self):
        return self.getTimePoint() - self.getTimeZero()

    def getFormattedTimePoint(self, fmt=DEFAULT_DATETIME_FORMAT):
        if fmt is None:
            fmt = self._datetime_format
        return self.getTimePoint().strftime(fmt)

    def getFomattedRelativeTimePoint(self, time_unit=None):
        if time_unit is None:
            time_unit = self._timedelta_unit
        # is timedelta object and time_zero is defined
        t = (self.getTimePoint() - self.getTimeZero()).total_seconds()
        return int("{:0.0f}".format(t / time_unit))

    def getTimeDeltaUnit(self):
        return self._timedelta_unit

    def setTimeDeltaUnit(self, unit):
        self._timedelta_unit = unit
        return self

    def getRepeats(self):
        return self.getAttribute("od_repeats")

    def getLED(self, asIndex=True):
        """
        Returns the LED that was used to measure the OD
        :param asIndex: Whether to return the LED as an index or as a wavelength (in nm)
        :rtype: int
        """
        result = self.getAttribute("od_led")
        if not asIndex:
            if self.measuredAt680():
                result = 680
            elif self.measuredAt720():
                result = 720
        return result

    def isMeasuredAt(self, nm):
        """
        Determines whether this measurement is measured at a given nm or led index.
        :type nm: int
        :rtype: bool
        """
        return (nm in (0, 680) and self.measuredAt680()) or (nm in (1, 720) and self.measuredAt720())

    def measuredAt680(self):
        return self.getAttribute("od_led") in (0, 680)

    def measuredAt720(self):
        return self.getAttribute("od_led") in (1, 720)

    def getIntensity(self):
        return self.getAttribute("intensity")

    def getBackground(self):
        return self.getAttribute("background")

    def getFlash(self):
        return self.getAttribute("flash")

    def setODRaw(self, value):
        self.setAttribute("od_raw", value)

    def getODRaw(self):
        return self.getAttribute("od_raw")

    def getODValue(self):
        return self.getAttribute("od_value")

    def getCalibratedODValue(self):
        return self.getODValue()

    def setCalibratedODValue(self, value):
        self.setAttribute("od_value", value)

    def getPerceivedIntensity(self):
        raise DeprecationWarning("Do not use perceived Intensity")

    def setPerceivedIntensity(self, perceived):
        raise DeprecationWarning("Do not use perceived Intensity")

    def getTemperature(self):
        return self.getAttribute("temperature")

    def setTemperature(self, t):
        self.setAttribute("temperature", t)
