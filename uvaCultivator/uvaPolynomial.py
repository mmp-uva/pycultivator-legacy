"""Implement Objects modelling polynomials

Two classes are defined:

* Domain: For modelling domain/interval notation
* Polynomial: For modelling polynomials

"""

from pycultivator_legacy.core import BaseObject
import re
import numpy as np
import copy


class Domain(BaseObject):
    """The Domain object implements a basic math domain/interval notation.

    See https://en.wikipedia.org/wiki/Interval_(mathematics) for definitions.

    """

    _domains = {
        # (tuple values): object
    }
    _domain_test = re.compile('(\(|\[) ?(-?\d*\.?\d*)? ?, ?(-?\d*\.?\d*)? ?(\)|\])')

    def __init__(self, start=None, includes_start=False, end=None, includes_end=False, settings=None):
        super(Domain, self).__init__(settings)
        self._start = start
        self._includes_start = includes_start is True
        self._end = end
        self._includes_end = includes_end is True

    @classmethod
    def createDomain(cls, start=None, includes_start=False, end=None, includes_end=False):
        """Factory method for domain objects"""
        result = None
        key = (start, includes_start, end, includes_end)
        # if not in dictionary, create
        if key not in cls._domains.keys():
            domain = cls(start=start, includes_start=includes_start, end=end, includes_end=includes_end)
            cls._domains[key] = domain
        result = cls._domains[key]
        return result

    @classmethod
    def createFromString(cls, domain):
        """Creates a domain object from a string, using the format:

        * 1 opening bracket: [ or (. [ means include start point
        * optional a number (0.0 or .0)
        * a comma
        * optional a number (0.8 or .8)
        * 1 closing bracket: ] or ). ) means don't include end point

        :param domain: Domain string
        :type domain: str
        :return: The desired object
        :raises ValueError: If given domain string is invalid
        :rtype: None or uvaCultivator.uvaPolynomial.Domain
        """
        result = None
        matches = cls._domain_test.findall(domain)
        if matches is None or len(matches) == 0:
            raise ValueError("Invalid domain - %s - given, no matches." % domain)
        # only consider first match
        m = matches[0]
        if len(m) < 4:
            raise ValueError("Invalid domain - %s - given, not enough captures." % domain)
        has_start = m[0] == "["
        start = None
        if m[1] != "" and cls.isDigit(m[1]):
            start = float(m[1])
        end = None
        if m[2] != "" and cls.isDigit(m[2]):
            end = float(m[2])
        has_end = m[3] == "]"
        return cls.createDomain(start=start, includes_start=has_start, end=end, includes_end=has_end)

    def adjustDomain(self, start=None, includes_start=None, end=None, includes_end=None):
        """Creates a new Domain based on the old"""
        if start is None:
            start = self.getStartValue()
        if includes_start is None:
            includes_start = self.includesStart()
        if end is None:
            end = self.getEndValue()
        if includes_end is None:
            includes_end = self.includesEnd()
        return self.createDomain(start=start, includes_start=includes_start, end=end, includes_end=includes_end)

    @property
    def start(self):
        return self._start

    def getStartValue(self):
        """Returns the start value of the domain, None if Infinite"""
        return self.start

    def includes_start(self):
        return self._includes_start is True

    def includesStart(self):
        """Returns whether the start value is included in the domain"""
        return self.includes_start()

    @property
    def end(self):
        return self._end

    def getEndValue(self):
        """Returns the end value of the domain, None if Infinite"""
        return self.end

    def includes_end(self):
        return self._includes_end is True

    def includesEnd(self):
        """Returns whether the end value is included in the domain"""
        return self.includes_end()

    def contains(self, x):
        """Returns whether the x-value is contained within this domain

        :type x: int
        :rtype: bool
        """
        result = False
        if isinstance(x, (int, float)):
            # domain start is None -> infinite
            start = self.getStartValue()
            has_start = self.includesStart()
            after_start = start is None or (has_start and start <= x) or (not has_start and start < x)
            # domain end is None -> infinite
            end = self.getEndValue()
            has_end = self.includesEnd()
            before_end = end is None or (has_end and end >= x) or (not has_end and end > x)
            result = after_start and before_end
        return result

    def __contains__(self, item):
        return self.contains(item)

    def __str__(self):
        """Returns a string representation of this object"""
        result = "[" if self.includesStart() else "("
        if self.getStartValue() is not None:
            result += "{:.8f}".format(self.getStartValue())
        result += ","
        if self.getEndValue() is not None:
            result += "{:.8f}".format(self.getEndValue())
        result += "]" if self.includesEnd() else ")"
        return result

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        """Compares this object to an `other` object

        :type other: uvaCultivator.uvaPolynomial.Domain
        :rtype: bool
        """
        result = True and isinstance(other, Domain)
        result = result and self.includesStart() is other.includesStart()
        result = result and self.getStartValue() == other.getStartValue()
        result = result and self.includesEnd() is other.includesEnd()
        result = result and self.getEndValue() == other.getEndValue()
        return result


class Polynomial(BaseObject):
    """Polynomial class to describe a numpy polynomial object.

    <polynomial domain="(,)">
        <coefficient order="0">1.720272765</coefficient>
        <coefficient order="1">0.060380645</coefficient>
    </polynomial>
    """

    _digit_test = re.compile('-?\d+(\.\d+)?')

    def __init__(self, domain=None, coefficients=None, settings=None):
        """ Initialise the polynomial

        >>> Polynomial().getCoefficients()
        [0, 0]
        >>> Polynomial(coefficients = [5]).getCoefficients()
        [5]
        >>> Polynomial(coefficients = 5).getCoefficients()
        [5]

        :param domain: The domain definition for this polynomial
        :type domain: None or str or uvaCultivator.uvaPolynomialDependency.Domain
        :param coefficients: A list with coefficients ascending in order (0 .. N)
        :type coefficients: None or list[float]
        :param settings: Extra settings for this object
        :type settings: dict
        """
        super(Polynomial, self).__init__(settings=settings)
        if coefficients is None:
            coefficients = [0, 0]
        if isinstance(coefficients, (tuple, set)):
            coefficients = list(coefficients)
        if not isinstance(coefficients, list):
            coefficients = [coefficients]
        self._coefficients = coefficients
        self._polynomial = None
        self._changed = True
        self._domain = Domain.createFromString("(,)")
        if domain is not None:
            if isinstance(domain, str):
                self._domain = Domain.createFromString(domain)
            if isinstance(domain, Domain):
                self._domain = domain

    @classmethod
    def createPolynomial(cls, domain=None, coefficients=None):
        """Create a polynomial based on the domain and coefficients.

        :param domain: The domain for which this polynomial is valid
        :type domain: str or uvaCultivator.uvaPolynomialDependency.Domain
        :param coefficients: List of coefficients in ascending order (0..N)
        :type coefficients: None or list[float]
        :return: The object, newly created or an already existing object
        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        return cls(domain=domain, coefficients=coefficients)

    @classmethod
    def createFromXMLElement(cls, xml):
        """Create a Polynomial Object from an XML Element

        :param xml: Polynomial xml
        :type xml: xml.etree.ElementTree.Element
        :return:
        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        from pycultivator_legacy.config import XMLConfig
        result = None
        domain = XMLConfig.validateValue(xml.get("domain"), str, "(,)")
        if domain is not None:
            coeff_e = xml.findall("./{uvaCultivator}coefficient")
            if len(coeff_e) > 0:
                coeffs = []
                for e in coeff_e:
                    order = XMLConfig.validateValue(e.get("order"), int, 0)
                    value = XMLConfig.validateValue(e.text, float, 0.0)
                    coeffs.insert(order, value)
                try:
                    result = cls.createPolynomial(domain=domain, coefficients=coeffs)
                except ValueError as ve:
                    cls.getLog().warning("Unable to create Polynomial: %s" % ve)
        return result

    @classmethod
    def createFromPolynomial(cls, domain, polynomial):
        """Create a Polynomial object from a numpy poly1d object.

        :param domain: The domain for which this polynomial is valid
        :type domain: str
        :param polynomial:
        :type polynomial: numpy.poly1d
        :return: The Polynomial object
        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        return cls.createPolynomial(domain, cls.polyToCoefficients(polynomial))

    @staticmethod
    def coefficientsToString(coefficients):
        """Creates a string representation of a list of coefficients

         Writes a formula of the form: y = sum(a_i * x^i)

        :param coefficients: List of coefficients
        :type coefficients: list[int or float]
        :return: String representing a list of coefficients
        :rtype: str
        """
        result = []
        for order, coefficient in enumerate(coefficients):
            plus = ""
            if len(result) > 0:
                plus = "+" if coefficient >= 0 else "-"
            value_str = "%s %s%s%s" % (
                plus,
                "{:.6f}".format(coefficient if order == 0 or coefficient >= 0 else -1 * coefficient),
                ("x" if order >= 1 else ""),
                ("" if order <= 1 else "^%s" % order)
            )
            result.append(value_str)
        return " ".join(result)

    @staticmethod
    def coefficientsToPoly(coefficients):
        """Use a list of coefficients to create numpy poly1d object.

        Elements in the list represent coefficients from high to low orders.
        First element is the highest order coefficient.
        :param coefficients: List of coefficients
        :type coefficients: list[float]
        :returns: Numpy Polynomial
        :rtype: numpy.poly1d
        """
        result = copy.copy(coefficients)
        # first reverse
        result.reverse()
        return np.poly1d(result)

    @staticmethod
    def polyToCoefficients(poly):
        """Retrieve a list of coefficients from a numpy poly1d object

        :param poly: Numpy Polynomial
        :type poly: numpy.poly1d
        :return: List of coefficients
        :rtype: list[float]
        """
        result = copy.copy(list(poly.coeffs))
        # first reverse
        result.reverse()
        return result

    def getCoefficients(self):
        """Returns the list of coefficients defined for this polynomial"""
        return self._coefficients

    def setCoefficients(self, coefficients):
        """Replaces the coefficients of this polynomial with new coefficients"""
        if not isinstance(coefficients, list):
            raise ValueError("Coefficients are not a list: %s" % coefficients)
        self._coefficients = coefficients
        self._changed = True
        return self

    def setCoefficient(self, coefficient, order):
        if not self.hasCoefficient(order):
            raise IndexError("Invalid index - %s - given." % order)
        self._coefficients[order] = coefficient
        self._changed = True
        return self

    def countCoefficients(self):
        """Returns the number of coefficients defined for this polynomial"""
        return len(self.getCoefficients())

    def hasCoefficient(self, order):
        """Returns whether a coefficient has been defined for the `order`th order"""
        return 0 <= order < self.countCoefficients()

    def resetCoefficients(self):
        """Resets the coefficients of this polynomial to 0,0 (i.e. always 0)"""
        self._coefficients = [0, 0]
        self._changed = True
        return self

    def getCoefficient(self, order):
        """Returns the coefficient defined for the `order`th order

        :param order:
        :type order:  int
        :return: Coefficient
        :raises IndexError: If no coefficient is defined for the given order
        :rtype: int
        """
        if not self.hasCoefficient(order):
            raise IndexError("Invalid index - %s - given." % order)
        result = self.getCoefficients()
        return result[order]

    def addCoefficient(self, coefficient, order=None):
        """

        >>> Polynomial(coefficients=[]).addCoefficient(5) == Polynomial(coefficients = [5])
        True
        >>> Polynomial(coefficients=[]).addCoefficient(5, 2) == Polynomial(coefficients = [0, 0, 5])
        True

        :type coefficient: float | int
        :type order: int
        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        if order is not None:
            while order > len(self._coefficients):
                self._coefficients.append(0)
            self._coefficients.insert(order, coefficient)
        else:
            self._coefficients.append(coefficient)
        self._changed = True
        return self

    def getDomain(self):
        """Returns the domain of this Polynomial

        :rtype: uvaCultivator.uvaPolynomial.Domain
        """
        return self._domain

    def setDomain(self, domain):
        if isinstance(domain, str):
            domain = Domain.createFromString(domain=domain)
        if not isinstance(domain, Domain):
            raise ValueError("Invalid domain - %s - given" % domain)
        self._domain = domain
        self._changed = True
        return self

    def hasChanged(self):
        return self._changed

    def getPolynomial(self):
        """Returns the actual numpy polynomial object

        :rtype: numpy.poly1d
        """
        if self.hasChanged():
            self._polynomial = self.coefficientsToPoly(self._coefficients)
        return self._polynomial

    def setPolynomial(self, poly):
        """Replaces the current polynomial with a new numpy polynomial"""
        return self.setCoefficients(self.polyToCoefficients(poly))

    def predict(self, x):
        """Predicts the y-value by using the defined polynomial, if the x value is within the defined domain"""
        result = None
        if self.contains(x):
            result = self.getPolynomial()(x)
        return result

    def contains(self, x):
        return self.getDomain().contains(x)

    # def __str__(self):
    #     return "%s : %s" % (self.getDomain(), self.coefficientsToString(self.getCoefficients()))

    def __eq__(self, other):
        """Returns whether this Polynomial object is equal to the `other` Polynomial by comparing the domain
        and the polynomial object

        :type other: Polynomial
        :rtype: bool
        """
        result = self.getDomain() == other.getDomain() and self.getPolynomial() == other.getPolynomial()
        return result

    def copy(self, to=None):
        """Copies this polynomial to the `to` polynomial

        :param to: The object to copy the information of this object to
        :type to: None or uvaCultivator.uvaPolynomial.Polynomial
        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        if to is None:
            to = self.createPolynomial()
        to.setDomain(self.getDomain())
        to.setPolynomial(self.getPolynomial())
        return to
