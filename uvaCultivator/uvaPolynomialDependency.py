# coding=utf-8
"""
Basic module layer that implements a dependency of polynomials
"""

from uvaDependency import Dependency
from uvaPolynomial import Polynomial

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PolynomialDependency(Dependency):
    """
    A dependency described by one - or more - polynomials.
    """

    def __init__(self, dependency_id, source, variable, target, state=False, settings=None):
        super(PolynomialDependency, self).__init__(
            dependency_id=dependency_id, source=source, variable=variable, target=target,
            state=state, settings=settings
        )
        self._polynomials = []  # self._polynomials[index] = Polynomial

    @classmethod
    def loadDefault(cls):
        """Returns the default PolynomialDependency"""
        result = super(PolynomialDependency, cls).loadDefault()
        result.setVariable("od")
        result.setTarget("intensity")
        result.addPolynomial(
            Polynomial(coefficients=[0, 1])
        )
        return result

    @classmethod
    def canLoadXML(cls, xml):
        """Determines if a PolynomialDependency can be created from the given xml element

        :type xml: lxml.etree._Element._Element
        :rtype: bool
        """
        return len(xml.findall("./{uvaCultivator}polynomial")) > 0

    @classmethod
    def createFromXMLElement(cls, xml, config, default=None):
        """Creates an Polynomial Dependency object from the given xml element

        :param xml:
        :type xml: lxml.etree._Element._Element
        :param default:
        :type default: uvaCultivator.uvaPolynomialDependency.PolynomialDependency
        :return:
        :rtype: uvaCultivator.uvaPolynomialDependency.PolynomialDependency
        """
        result = super(PolynomialDependency, cls).createFromXMLElement(xml, config=config, default=default)
        """:type: uvaCultivator.uvaPolynomialDependency.PolynomialDependency"""
        #  now load polynomials
        polies = xml.findall("./{uvaCultivator}polynomial")
        if len(polies) > 0:
            result.clearPolynomials()
            for poly_e in polies:
                poly = Polynomial.createFromXMLElement(poly_e)
                if isinstance(result, PolynomialDependency) and poly is not None:
                    result.addPolynomial(poly)
        else:
            # load the default Polynomials
            for poly in default.getPolynomials():
                result.addPolynomial(poly.copy())
        return result

    def clearPolynomials(self):
        """Removes all Polynomials included in this dependency"""
        self._polynomials = []

    def getPolynomials(self):
        """Return a list of all Polynomials included in this dependency

        :return:
        :rtype: list[uvaCultivator.uvaPolynomialDependency.Polynomial]
        """
        return self._polynomials

    def countPolynomials(self):
        return len(self.getPolynomials())

    def hasPolynomialIndex(self, index):
        return -self.countPolynomials() <= index < self.countPolynomials()

    def addPolynomial(self, polynomial):
        """Add a polynomial to the dependency

        :param polynomial:
        :type polynomial: uvaCultivator.uvaPolynomialDependency.Polynomial
        :return:
        :rtype:
        """
        if polynomial not in self._polynomials:
            self._polynomials.append(polynomial)
        return self

    def getPolynomialByIndex(self, index):
        """Retireve a polynomial by it's index

        :param index: Index of the polynomial
        :type index: int
        :rtype: uvaCultivator.uvaPolynomialDependency.Polynomial
        """
        result = None
        if self.hasPolynomialIndex(index=index):
            result = self.getPolynomials()[index]
        return result

    def getPolynomialByDomain(self, x):
        """Get the polynomial that has a domain that contains x

        :param x:
        :type x: float, int
        :return:
        :rtype: uvaCultivator.uvaPolynomialDependency.Polynomial
        """
        # by default return the standard polynomial
        result = self.getPolynomials()[0]
        for idx in range(self.countPolynomials()):
            if self.getPolynomials()[idx].contains(x):
                result = self.getPolynomials()[idx]
                break
        return result

    def calculate(self, source_value=0.0):
        """Calculates the predicted value using the polynomial valid for this range

        :param source_value:
        :type source_value: float
        :return:
        :rtype: float
        """
        result = None
        polynomial = self.getPolynomialByDomain(source_value)
        if self.isActive() and polynomial is not None:
            result = polynomial.predict(source_value)
        return result

    def copy(self, to=None):
        """Copies this dependency to the `to` object

        :type to: uvaCultivator.uvaPolynomialDependency.PolynomialDependency
        :rtype: uvaCultivator.uvaPolynomialDependency.PolynomialDependency
        """
        # first do the basic stuff
        to = super(PolynomialDependency, self).copy(to=to)
        # now add all the polynomials
        to.clearPolynomials()
        for poly in self.getPolynomials():
            to.addPolynomial(poly.copy())
        return to


class ODDependency(PolynomialDependency):

    @classmethod
    def canLoadXML(cls, xml):
        """Determines if this class is able to load this xml Element

        :type xml: lxml.etree._Element._Element
        :rtype: bool
        """
        result = super(ODDependency, cls).canLoadXML(xml)

        result = result and xml.tag == '{uvaCultivator}dependency'
        result = result and cls.validateValue(xml.get("source"), str) == "self"
        result = result and cls.validateValue(xml.get("variable"), str) == "od"
        return result


class CalibrationDependency(PolynomialDependency):

    @classmethod
    def loadDefault(cls):
        result = super(CalibrationDependency, cls).loadDefault()
        result.setSource("self")
        result.setVariable("intensity")
        result.setTarget("intensity")
        return result

    @classmethod
    def canLoadXML(cls, xml):
        """Determines if this class is able to load this xml Element

        :type xml: lxml.etree._Element._Element
        :rtype: bool
        """
        return super(CalibrationDependency, cls).canLoadXML(xml) and xml.tag == "{uvaCultivator}calibration"

    def adjustIntercepts(self, correction):
        """Will adjust the intercepts of all models using the correction"""
        for poly in self.getPolynomials():
            poly.setCoefficient(poly.getCoefficient(0) + correction, 0)
        # done


class ODCalibrationDependency(CalibrationDependency):

    @classmethod
    def loadDefault(cls):
        """ Returns the default calibration

        :return:
        :rtype: uvaCultivator.uvaPolynomialDependency.ODCalibrationDependency
        """
        result = super(ODCalibrationDependency, cls).loadDefault()
        result.setVariable("od")
        result.setTarget("od")
        return result

    @classmethod
    def canLoadXML(cls, xml):
        """Determines if this class is able to load this xml Element

        :type xml: lxml.etree._Element._Element
        :rtype: bool
        """
        from pycultivator_legacy.config.xmlCultivatorConfig import xmlCultivatorConfig as xCC
        return super(ODCalibrationDependency, cls).canLoadXML(xml) and \
               xCC.validateValue(xml.get("variable"), str) == "od"


class NeighbourDependency(PolynomialDependency):

    @classmethod
    def loadDefault(cls):
        result = super(NeighbourDependency, cls).loadDefault()
        result.setId(0)
        result.setSource("left")
        result.setVariable("intensity")
        result.setTarget("intensity")
        return result

    @classmethod
    def canLoadXML(cls, xml):
        """Determines if this class is able to load this xml Element

        :type xml: lxml.etree._Element._Element
        :rtype: bool
        """
        return super(NeighbourDependency, cls).canLoadXML(xml) and \
               (cls.validateValue(xml.get('source'), str) == "left" or
                cls.validateValue(xml.get("source"), str) == "right")

    # def getPolynomialByChannel(self, subject, dependent_on ):
    # 	"""
    # 	:type subject: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
    # 	:type dependent_on: uvaCultivator.uvaCultivatorChannel.CultivatorChannel
    # 	:rtype: DependencyPolynomial
    # 	"""
    # 	result = DependencyPolynomial("none")
    # 	from uvaCultivatorChannel import CultivatorChannel as CC
    # 	if isinstance(dependent_on, CC):
    # 		if subject.getChannel() == dependent_on.getChannel():
    # 			result = DependencyPolynomial.createFromCoefficients("self", [1, 0])
    # 		if subject.getChannel() - 1 == dependent_on.getChannel():
    # 			result = self.getLeftDependency()
    # 		if subject.getChannel() + 1 == dependent_on.getChannel():
    # 			result = self.getRightDependency()
    # 	return result