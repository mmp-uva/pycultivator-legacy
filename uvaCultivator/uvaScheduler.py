"""Module for scheduling jobs

Logic:

- A Scheduler; separate thread to execute jobs

"""

from pycultivator_legacy.core import BaseObject
import uvaException
import sys
import threading
from datetime import datetime as dt
# import version dependent modules
if sys.version_info[0] < 3:
    import Queue
else:
    from queue import Queue

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class Job(BaseObject):
    """A job that can be scheduled and executed in a separate thread"""

    _namespace = "job.protocol"
    _name = "Job"

    def __init__(self, t_schedule=None, parent=None, settings=None):
        """Initialize a job

        :type t_start: Time at which this job should start
        """
        if settings is None:
            settings = {}
        self.isolateNameSpace(settings, self.getNameSpace())
        super(Job, self).__init__(settings)
        if t_schedule is None:
            t_schedule = dt.now()
        self._t_schedule = t_schedule
        self._t_start = None
        self._t_finish = None
        self._parent = parent

    @classmethod
    def getName(cls):
        return cls._name

    def getTimeStart(self):
        """The time that this job has started"""
        return self._t_start

    def setTimeStart(self, t):
        if not isinstance(t, dt):
            raise ValueError("Invalid value for start time")
        self._t_start = t

    def getTimeFinished(self):
        return self._t_finish

    def setTimeFinish(self, t):
        if not isinstance(t, dt):
            raise ValueError("Invalid value for finish time")
        self._t_finish = t

    def getTimeScheduled(self):
        """The time that this job is scheduled to start"""
        return self._t_schedule

    def setTimeScheduled(self, t):
        if not isinstance(t, dt):
            raise ValueError("Invalid value for schedule time")
        self._t_schedule = t

    def shouldRun(self, t_now=None):
        if t_now is None:
            t_now = dt.now()
        return t_now >= self.getTimeScheduled()

    def getParent(self):
        """Scheduler that will be executing this job

        :rtype: uvaCultivator.uvaScheduler.DeviceScheduler
        """
        return self._parent

    def hasParent(self):
        """Whether the job has a scheduler to execute this job"""
        return self.getParent() is not None

    @property
    def parent(self):
        return self.getParent()

    def setParent(self, p):
        if not isinstance(p, Scheduler):
            raise ValueError("Invalid parent object")
        self._parent = p
        return self

    @parent.setter
    def parent(self, p):
        self.setParent(p)

    def start(self):
        """Run the job"""
        # register start time
        self.setTimeStart(dt.now())
        result = self.prepare()
        if result:
            result = self.execute()
        result = self.clean() and result
        # register finish time
        self.setTimeFinish(dt.now())
        return result

    def prepare(self):
        """Prepare the job for execution"""
        return True  # overload

    def execute(self):
        """Execute the job"""
        raise NotImplementedError

    def clean(self):
        """Finish and clean"""
        return True  # overload


class JobException(uvaException.UVAException):
    """Exception raised by jobs"""

    def __init__(self, msg):
        super(JobException, self).__init__(msg=msg)


class Scheduler(BaseObject, threading.Thread):
    """Executes jobs in a separate thread"""

    def __init__(self, log=None, settings=None):
        BaseObject.__init__(self, settings=settings)
        if log is not None:
            self._log = log
        threading.Thread.__init__(self)
        # create locks and queues
        self._update = threading.Event()
        # signals we should exit
        self._exit = threading.Event()
        # signals we must exit
        self._terminate = threading.Event()
        self._queue = Queue.Queue()
        self._schedule = []
        """ :type: list[uvaCultivator.uvaScheduler.Job] """
        self._schedule_lock = threading.RLock()

    def run(self):
        """ Run the scheduler thread

        :return:
        :rtype: bool
        """
        result = self.prepare()
        if result:
            result = self.execute()
        result = self.clean() and result
        return result

    def prepare(self):
        """Prepare for scheduling

        :rtype: bool
        """
        self._terminate.clear()
        self._exit.clear()
        return True

    def execute(self):
        """Execute scheduling tasks"""
        while self.canRun():
            try:
                # reset update flag
                self._update.clear()
                # process new jobs
                self.processQueue()
                # start jobs scheduled to start
                self.processSchedule()
                if not self.hasUpdate():
                    self._update.wait(0.1)
            except KeyboardInterrupt:
                self.terminate()
                raise
            except JobException as je:
                self.getLog().critical("Unexpected Exception raised by Job", exc_info=1)
        return True

    def clean(self):
        """Clean up"""
        if self.shouldExit() and self.canExit():
            self.getLog().info("Could exit so I did")
        if self.mustExit():
            self.getLog().info("Received terminate signal: exiting")
        return True

    def hasUpdate(self):
        """Whether an the update signal has been set"""
        return self._update.isSet()

    def mustExit(self):
        """Whether the scheduler is forced to exit"""
        return self._terminate.isSet()

    def shouldExit(self):
        """Whether the scheduler is signalled it is allowed to exit"""
        return self._exit.isSet()

    def canRun(self):
        """Whether the scheduler can spent more time in the while loop"""
        return not self.mustExit() and not (self.shouldExit() and self.canExit())

    def canExit(self):
        return not self.hasJobsWaiting() and not self.hasJobsScheduled()

    def hasJobsScheduled(self):
        result = False
        with self._schedule_lock:
            result = len(self._schedule) > 0
        return result

    def hasJobsWaiting(self):
        return self._queue.unfinished_tasks > 0

    def update(self):
        """Notify scheduler about a change"""
        self._update.set()

    def stop(self, graceful=True):
        """Tell scheduler to exit (graceful)"""
        if graceful:
            self.exit()
        else:
            self.terminate()
        # notify about change
        self.update()

    def exit(self):
        """Tell scheduler it should exit when it is ready"""
        self._exit.set()

    def terminate(self):
        """Tell scheduler it should terminate as soon as possible"""
        self._terminate.set()

    def processQueue(self):
        """ Reads the queue and puts the job in the schedule

        :return:
        """
        while self._queue.unfinished_tasks:
            job = self.readJob()
            if job is not None:
                self.scheduleJob(job)

    def processSchedule(self):
        """Reads the schedule and tries to start jobs"""
        new_schedule = []
        with self._schedule_lock:
            for job in self._schedule:
                if job.shouldRun():
                    self.runJob(job)
                else:
                    new_schedule.append(job)
            self._schedule = new_schedule

    def readJob(self):
        """ Reads a Job from the queue

        :rtype: None or pumpJob
        """
        result = None
        try:
            result = self._queue.get(block=False)
            self._queue.task_done()
        except Queue.Empty():
            pass
        return result

    def queueJob(self, job):
        """Queue a job for scheduling"""
        result = False
        if job is not None:
            self._queue.put(job)
            result = True
        return result

    def scheduleJob(self, job):
        """Put a job into the schedule

        :type job: uvaCultivator.uvaScheduler.Job
        """
        # register this scheduler as parent
        job.setParent(self)
        with self._schedule_lock:
            self._schedule.append(job)

    def runJob(self, job):
        """Run a job in this thread

        :param job:
        :type job: uvaCultivator.uvaScheduler.Job
        :return: Whether the job finished successfully
        :rtype: bool
        """
        self.getLog().debug(
            "Run {} job (Scheduled for: {}, Started at {})".format(
                job.getName(),
                job.getTimeScheduled().strftime(self.DT_FORMAT),
                dt.now().strftime(self.DT_FORMAT)
        ))
        return job.start()

    def notify(self):
        """Notify the scheduler about a change"""
        self._update.set()

    def schedule(self, job):
        """Schedule a job to thes

        :param job: The job to schedule
        :type job: uvaCultivator.uvaScheduler.Job
        :return:
        :rtype:
        """
        result = False
        try:
            if not isinstance(job, Job):
                raise SchedulerException("Invalid job object. Cannot be: {}".format(job))
            self.queueJob(job)
            # notify!
            self.notify()
            # succes!
            result = True
        except SchedulerException as se:
            self.getLog().error("Unable to queue job: {}".format(se))
        return result


class SchedulerException(uvaException.UVAException):
    """Exception raised by a Scheduler"""

    def __init__(self, msg):
        super(SchedulerException, self).__init__(msg)


class DeviceJob(Job):
    """A Job that acts on a device"""

    _name = "Device Job"

    def __init__(self, t_schedule=None, parent=None, settings=None):
        super(DeviceJob, self).__init__(t_schedule=t_schedule, parent=parent, settings=settings)
        self._device_class = None
        self._device_settings = {}

    def getParent(self):
        """Return scheduler

        :rtype: uvaCultivator.uvaScheduler.DeviceScheduler
        """
        return super(DeviceJob, self).getParent()

    def getDeviceName(self):
        """Return the name of the device that should be reserved for this job"""
        raise NotImplementedError

    def getDevice(self):
        result = None
        name = self.getDeviceName()
        if self.hasParent():
            if self.getParent().hasDevice(name):
                result = self.getParent().getDevice(name)
        if result is None:
            result = self.loadDevice()
            if result is not None and self.hasParent():
                if self.getParent().hasDevice(name):
                    self.getParent().setDevice(name, result)
                else:
                    self.getParent().registerDevice(name, result)
        return result

    def hasDevice(self):
        return self.getDevice() is not None

    def getDeviceClass(self):
        return self._device_class

    def setDeviceClass(self, klass):
        self._device_class = klass

    def getDeviceSettings(self):
        return self._device_settings

    def setDeviceSettings(self, settings):
        self._device_settings = settings

    def loadDevice(self):
        """Create a new device instance"""
        raise NotImplementedError

    def execute(self):
        raise NotImplementedError


class DeviceScheduler(Scheduler):
    """A Scheduler that manages a pool of devices and shares it between jobs"""

    _device_lock = threading.RLock()

    def __init__(self, log=None, settings=None):
        super(DeviceScheduler, self).__init__(log=log, settings=settings)
        self._keep_devices = True
        self._devices = {
            # name: tuple( device, users )
        }

    def keepsDevices(self):
        result = False
        with self._device_lock:
            result = self._keep_devices
        return result

    def keepDevices(self, state):
        with self._device_lock:
            self._keep_devices = state is True
        return self

    def getDevices(self):
        """Registry of devices

        :rtype: dict[str, list[int, object]]
        """
        return self._devices

    @property
    def devices(self):
        return self.getDevices()

    def hasDevices(self):
        return len(self.devices.keys()) > 0

    def hasDevice(self, name):
        result = False
        with self._device_lock:
            result = name in self.getDevices().keys()
        return result

    def registerDevice(self, name, device):
        with self._device_lock:
            if self.hasDevice(name):
                raise KeyError("Another device is already registered under: {}".format(name))
            self.devices[name] = [1, device]

    def getDevice(self, name):
        result = None
        with self._device_lock:
            if not self.hasDevice(name):
                raise KeyError("No device registered under: {}".format(name))
            result = self.devices[name][1]
        return result

    def getDeviceUsers(self, name):
        """Get the number of users registered to use the device"""
        result = 0
        with self._device_lock:
            if not self.hasDevice(name):
                raise KeyError("No device registered under: {}".format(name))
            result = self.devices[name][0]
        return result

    def setDevice(self, name, device):
        result = None
        with self._device_lock:
            if not self.hasDevice(name):
                raise KeyError("No device registered under: {}".format(name))
            self.devices[name][1] = device
            result = True
        return result

    def removeDevice(self, name, ignore_users=False):
        result = None
        with self._device_lock:
            if not self.hasDevice(name):
                raise KeyError("No device registered under: {}".format(name))
            if not ignore_users and self.getDeviceUsers(name) > 0:
                raise SchedulerException("Cannot remove device; somebody is using it")
            device = self.getDevice(name)
            from uvaSerial.uvaSerialController import SerialController
            if isinstance(device, SerialController):
                if device.isConnected():
                    device.disconnect()
            result = self.devices.pop(name)
        return result

    def removeDevices(self, ignore_users=False):
        self.cleanDevices()
        with self._device_lock:
            while len(self.devices.keys()) > 0:
                name = self.devices.keys()[0]
                self.removeDevice(name, ignore_users=ignore_users)
        return not self.hasDevices()

    def cleanDevices(self):
        with self._device_lock:
            idx = 0
            while idx < len(self.devices.keys()):
                name = self.devices.keys()[idx]
                users = self.getDeviceUsers(name)
                if users == 0:
                    self.getLog().info("Disconnect device {}, as it is no longer used.".format(name))
                    self.removeDevice(name)
                else:
                    idx += 1
        # done

    def reserveDevice(self, name):
        """Increase user count for the device name"""
        with self._device_lock:
            if not self.hasDevice(name):
                self.registerDevice(name, None)
            else:
                self.devices[name][0] += 1

    def finishDevice(self, name):
        """Decrease user count for the device"""
        with self._device_lock:
            if not self.hasDevice(name):
                raise KeyError("No device registered under: {}".format(name))
            if self.getDeviceUsers(name) == 0:
                raise SchedulerException("Cannot lower user count, count already 0")
            self.devices[name][0] -= 1

    def scheduleJob(self, job):
        """Queue the job for execution

        :param job:
        :type job: uvaCultivator.uvaScheduler.DeviceJob
        :return:
        :rtype:
        """
        # register job as new user of device
        # device = job.getDeviceName()
        # self.reserveDevice(device)
        return super(DeviceScheduler, self).scheduleJob(job)

    def queueJob(self, job):
        """Queue job for scheduler"""
        # register job as new user of device
        device = job.getDeviceName()
        self.reserveDevice(device)
        return super(DeviceScheduler, self).queueJob(job)

    def runJob(self, job):
        result = super(DeviceScheduler, self).runJob(job)
        # remove reservation
        self.finishDevice(job.getDeviceName())
        return result

    def processSchedule(self):
        result = super(DeviceScheduler, self).processSchedule()
        # clean devices that are no longer needed
        if not self.keepsDevices():
            self.cleanDevices()
        return result

    def prepare(self):
        self.keepDevices(True)
        return super(DeviceScheduler, self).prepare()

    def exit(self):
        # no longer retain devices if exit flag is set
        self.keepDevices(False)
        super(DeviceScheduler, self).exit()
