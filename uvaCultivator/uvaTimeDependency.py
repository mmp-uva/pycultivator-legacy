# coding=utf-8
"""
Basic module layer that implements a dependency on time
"""

from pycultivator_legacy.core import BaseObject
from uvaDependency import Dependency
from datetime import datetime as dt
from math import sin, pi

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TimeDependency(Dependency):
    """A Dependency object that is dependent on time"""

    def __init__(self, dependency_id, source, variable, target, state=False, settings=None):
        Dependency.__init__(
            self, dependency_id=dependency_id, source=source, variable=variable, target=target, state=state,
            settings=settings
        )
        self._rhythm = None

    @classmethod
    def loadDefault(cls):
        result = super(TimeDependency, cls).loadDefault()
        result.setVariable("time")
        result.setTarget("intensity")
        return result

    @classmethod
    def canLoadXML(cls, xml):
        """Determines if this class is able to load this xml Element

        :type xml: xml.etree._Element._Element
        :rtype: bool
        """
        return super(TimeDependency, cls).canLoadXML(xml) and xml.find("./{uvaCultivator}rhythm") is not None

    @classmethod
    def createFromXMLElement(cls, xml, config, default=None):
        """Create the dependency from an XML Element

        :param xml:
        :type xml: xml.etree._Element._Element
        :return:
        :rtype: TimeDependency
        """
        result = super(TimeDependency, cls).createFromXMLElement(xml, config=config, default=default)
        # parse the rhythm
        rhythm_e = xml.find("./{uvaCultivator}rhythm")
        if isinstance(result, TimeDependency) and rhythm_e is not None:
            rhythm = Rhythm.createFromXMLElement(rhythm_e, config=config)
            if rhythm is not None:
                result.setRhythm(rhythm)
            else:
                result = None
        return result

    def setRhythm(self, rhythm):
        self._rhythm = rhythm
        if rhythm is not None:
            self._rhythm.setDependecy(self)

    def getRhythm(self):
        """
        :rtype: Rhythm
        """
        return self._rhythm

    def hasRhythm(self):
        return self._rhythm is not None

    def calculate(self, source_value=None):
        """
        :param source_value:
        :type source_value: datetime.datetime
        :return:
        :rtype: bool
        """
        result = None
        if source_value is None or not isinstance(source_value, dt):
            source_value = dt.now()
        self.getLog().debug("The time dependency is {}active".format("" if self.isActive() else "not "))
        if self.isActive() and self.hasRhythm() and self.getRhythm().isActive():
            result = self.getRhythm().calculate(source_value)
        return result


class Rhythm(BaseObject):
    """ An object that describes a time rhythm

    """

    DT_FORMAT = "%Y-%m-%dT%H:%M:%S"

    def __init__(self, start=None, stop=None, dependency=None, settings=None):
        super(Rhythm, self).__init__(settings=settings)
        self._start = start
        if self._start is None:
            self._start = dt.now()
        self._stop = stop
        self._dependency = dependency

    @staticmethod
    def loadDefault():
        return Rhythm()

    @staticmethod
    def canLoadXML(xml):
        """
        Determines if this class is able to load this xml Element
        :type xml: xml.etree.ElementTree.Element
        :rtype: bool
        """
        return xml.tag == "{uvaCultivator}rhythm" and (xml.find("{uvaCultivator}stepwise") is not None or xml.find("{uvaCultivator}sinusoidal") is not None)

    @classmethod
    def createFromXMLElement(cls, xml, config, default=None):
        """Create new time dependency from XML

        :param xml: Rhythm Element
        :type xml: xml.etree.ElementTree.Element
        :type default: Rhythm
        :return:
        :rtype: Rhythm
        """
        result = None
        if default is None:
            default = cls.loadDefault()
        if cls.canLoadXML(xml):
            start_date_time = cls.validateValue(xml.get("start"), dt, default.getStartDateTime())
            stop_date_tme = cls.validateValue(xml.get("stop"), dt, default.getStopDateTime())

            for child in xml:
                if StepWise.canLoadXML(child):
                    result = StepWise.createFromXMLElement(child, config=config)
                elif Sinusoidal.canLoadXML(child):
                    result = Sinusoidal.createFromXMLElement(child, config=config)
                if result is not None:
                    result.setStartDateTime(start_date_time)
                    result.setStopDateTime(stop_date_tme)
                    break

        return result

    def getStartDateTime(self):
        """
        :rtype: datetime.datetime
        """
        return self._start

    def setStartDateTime(self, value):
        """
        :param value: datetime.dateime
        """
        self._start = value
        if self._start is None:
            self._start = dt.now()

    def getStopDateTime(self):
        """
        :rtype: datetime.datetime
        """
        return self._stop

    def setStopDateTime(self, value):
        """
        :param value: datetime.dateime
        """
        self._stop = value

    def getDependency(self):
        """

        :rtype: TimeDependency
        """
        return self._dependency

    def setDependecy(self, dependency):
        """

        :param dependency: TimeDependency
        """
        self._dependency = dependency

    def hasDependency(self):
        """

        :rtype: boolean
        """
        return self._dependency is not None

    def isActive(self, t_now=None):
        if not isinstance(t_now, dt):
            t_now = dt.now()
        return self._start <= t_now and (self._stop is None or t_now <= self._stop)

    def calculate(self, t_now):
        raise NotImplementedError


class StepWise (Rhythm):

    def __init__(self, start=None, stop=None, periods=None, settings=None):
        super(StepWise, self).__init__(start=start, stop=stop, settings=settings)
        self._periods = periods
        if self._periods is None:
            self._periods = {0: (1, 0)}
        if self.getCycleLength() == 0:
            self.getLog().warning("I don't allow rhythm with no length; so this is now continuous light")
            self._periods[0] = (1, 0)

    @classmethod
    def createFromXMLElement(cls, xml, config, default=None):
        """Create a new step wise object from xml

        :param xml: Stepwise Element
        :type xml: xml.etree.ElementTree.Element
        :type default: Stepwise
        :return:
        :rtype: StepWise
        """
        result = None
        if cls.canLoadXML(xml):
            periods_e = xml.findall("./{uvaCultivator}period")
            periods = {}
            for period_e in periods_e:
                id = cls.validateValue(period_e.get("id"), int, 0)
                length = cls.validateValue(period_e.get("length"), int, 0)
                intensity = cls.validateValue(period_e.text, float, 0.0)
                periods[id] = (length, intensity)
            result = StepWise(periods=periods)
        return result

    @staticmethod
    def canLoadXML(xml):
        """
        Determines if this class is able to load this xml Element
        :type xml: xml.etree.ElementTree.Element
        :rtype: bool
        """
        return xml.tag == "{uvaCultivator}stepwise"

    def getPeriod(self, idx):
        result = None
        if idx <= len(self._periods):
            result = self._periods[idx]
        return result

    def getPeriods(self):
        """
        :rtype: dict
        """
        return self._periods

    def getCycleLength(self):
        result = 0
        for period in self.getPeriods().keys():
            # period = ( duration , intensity )
            result += self.getPeriod(period)[0]
        return result

    def calculate(self, t_now):
        """
        Will return the desired light intensity as dictated by this regime
        :rtype: None, float
        """
        dIntensity = None
        # testSerial if we are active
        if self.isActive(t_now):
            # First we need to calculate where we are in a cycle
            # calculate remainder as int -> number of seconds in cycle
            distance = int((t_now - self.getStartDateTime()).total_seconds())
            cycle_length = self.getCycleLength()
            cycle_time = distance % cycle_length
            idx = 0
            keys = sorted(self._periods.keys())
            while cycle_time >= 0 and idx < len(keys):
                cycle_time -= self._periods[keys[idx]][0]
                if cycle_time <= 0:
                    dIntensity = self._periods[keys[idx]][1]
                idx += 1
            self.getLog().debug(
                """We have a cycle of {0} seconds. After {1} cycles we are now {2} seconds in a cycle.
                This means that the desired light intensity is {3}.""".format(
                    self.getCycleLength(),
                    int((t_now - self.getStartDateTime()).total_seconds() / self.getCycleLength()),
                    int((t_now - self.getStartDateTime()).total_seconds() % self.getCycleLength()),
                    dIntensity
                ))
        return dIntensity


class Sinusoidal(Rhythm):

    def __init__(self, start=None, stop=None, amplitude=None, period=None, x_offset=None, y_offset=None, settings=None):
        super(Sinusoidal, self).__init__(start=start, stop=stop, settings=settings)
        self._amplitude = amplitude
        self._period = period
        self._x_offset = x_offset
        self._y_offset = y_offset

    @classmethod
    def createFromXMLElement(cls, xml, config, default=None):
        """
        :param xml: Sinusoidal Element
        :type xml: xml.etree.ElementTree.Element
        :type default: Sinusoidal
        :return:
        :rtype: Sinusoidal
        """
        result = None
        if cls.canLoadXML(xml):
            amplitude = cls.validateValue(xml.get("amplitude"), float, 0.0)
            period = cls.validateValue(xml.get("period"), float, 0.0)
            x_offset = cls.validateValue(xml.get("x_offset"), float, 0.0)
            y_offset = cls.validateValue(xml.get("y_offset"), float, 0.0)
            result = Sinusoidal(amplitude=amplitude, period=period, x_offset=x_offset, y_offset=y_offset)
        return result

    @staticmethod
    def canLoadXML(xml):
        """
        Determines if this class is able to load this xml Element
        :type xml: xml.etree.ElementTree.Element
        :rtype: bool
        """
        return xml.tag == "{uvaCultivator}sinusoidal"

    def getAmplitude(self):
        """

        :rtype: float
        """
        return self._amplitude

    def getPeriod(self):
        """

        :rtype: float
        """
        return self._period

    def getXOffset(self):
        """

        :rtype: float
        """
        return self._x_offset

    def getYOffset(self):
        """

        :rtype: float
        """
        return self._y_offset

    def calculate(self, t_now=None):
        """
       Will return the desired light intensity as dictated by this regime
       :rtype: None, float
       """
        dIntensity = None
        # test if we are active
        if self.isActive(t_now):
            distance = float((t_now - self.getStartDateTime()).total_seconds())
            dIntensity = self.getAmplitude() * sin(2 * pi * distance / self.getPeriod() + self.getXOffset()) + self.getYOffset()
            if dIntensity < 0.0:
                dIntensity = 0.0
        return dIntensity

