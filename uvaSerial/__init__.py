# coding=utf-8
"""
Initialize stub for uvaSerial package
"""

from uvaCultivator import uvaLog
import uvaSerialController
import psiSerialController
import psiFakeSerialController
import regloSerialController
import regloFakeSerialController

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# connect to logger
uvaLog.getLogger(__name__).debug("Connected {} to logger".format(__name__))

SERIAL_PSIMC_CONTROLLER = "psimc"
FAKE_SERIAL_PSIMC_CONTROLLER = "fake-psimc"
SERIAL_REGLO_CONTROLLER = "reglo"
FAKE_SERIAL_REGLO_CONTROLLER = "fake-reglo"

_registry = {
    SERIAL_PSIMC_CONTROLLER: psiSerialController.psiSerialController,
    FAKE_SERIAL_PSIMC_CONTROLLER: psiFakeSerialController.psiFakeSerialController,
    SERIAL_REGLO_CONTROLLER: regloSerialController.regloSerialController,
    FAKE_SERIAL_REGLO_CONTROLLER: regloFakeSerialController.regloFakeSerialController
}
""":type: dict[str, type[T <= uvaSerialController]]"""


def loadController(name, settings=None, **kwargs):
    """Loads a Controller from the registry

    :type name: str
    :type settings: None or dict[str, object]
    :type kwargs:
    :rtype: type[T <= uvaSerial.uvaSerialController]
    """
    result = None
    if name in _registry.keys():
        controller = _registry[name]
        result = controller(settings, **kwargs)
    return result
