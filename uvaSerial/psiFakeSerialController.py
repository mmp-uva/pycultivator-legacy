# coding=utf-8
"""
Responsible for translating function to specific commands and writeing those to psi MC.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from uvaSerialController import SerialController
from uvaSerialException import SerialException


class psiFakeSerialController(SerialController):
    """
        Class SIMULATING the serial communication with PSI's MultiCultivator
    """

    LIGHT_STATE = 0
    LIGHT_INTENSITY_VOLTAGE = 1
    LIGHT_INTENSITY_PERCENTAGE = 2
    LIGHT_INTENSITY_ABSOLUTE = 3

    _default_settings = SerialController.mergeDictionary(
        SerialController._default_settings,
        {
            "channel_count": 8,
            "od.mean": 1,
            "od.sd": 0.02,
            "light.intensity.absolute": 120,
            "light.intensity.percentage": 100,
            "light.intensity.voltage": 100,
            "light.state": True,
            "temperature.target": 30.0,
            "pump.state": True,
            "protocol.max_retry": 2
        }
    )

    def __init__(self, settings=None):
        super(psiFakeSerialController, self).__init__(settings=settings)
        self._retry_max = 2
        self._pumpState = True
        self._targetTemp = 30.0
        self._controlTemp = True
        self._light_settings = []
        for c in range(self.getChannelCount()):
            # light_settings[channel] = [ state, volt, perc, abs ]
            self._light_settings.append([True, 100, 100, 120])

    def connect(self):
        self._isOpen = True
        self.getLog().info("FakeSerialController is now connected")
        return self._isOpen

    def isConnected(self):
        return self._isOpen is True

    def configure(self, port=None, **kwargs):
        self._settings = kwargs
        self._configured = True
        return self._configured

    def disconnect(self):
        self._isOpen = False
        return not self._isOpen

    def testRates(self, speeds=None, testAll=True):
        return 115200

    def receive(self, expLen=0, dataFormat=""):
        return None

    def send(self, packet):
        return True

    def getChannelCount(self):
        return self.retrieveSetting("channel_count", default=self.getDefaultSettings())

    def generateOD(self, mean=None, std=None):
        import numpy as np
        if mean is None:
            mean = self.retrieveSetting("od.mean", default=self.getDefaultSettings())
        if std is None:
            std = self.retrieveSetting("od.sd", default=self.getDefaultSettings())
        return round(np.random.normal(mean, std), 6)

    #
    # Protocol commands
    #

    def sendPing(self):
        """
        Send a ping command and test if the response is OK
        :return: Whether the repsonds is OK
        :rtype: bool
        """
        return True

    def startCommunication(self):
        """
        Starts the connection with 5 start-up commands and then sets the speed to 9600
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        return True

    def getODReading(self, channel=0, led=0, repeats=1, mean=None, std=None):
        """
        Directly measure the OD. This will not mess with the pumps, use measureOD instead
        :param channel: The channel or vessel to measure the OD <0,7>
        :type channel: int
        :param led: The led to use [ 0=680nm, 1=720nm ]
        :type led: int
        :param repeats: Number of measurements to make and calculate the average over <1,N>
        :type repeats: int
        :return: tuple with (signal intensity, background, OD)
        :rtype: tuple
        """
        # BEGIN_CLASS( getODReadingf, 1092, "uuU" )
        # uint8_t light;      byte 1
        #           uint8_t led;        byte 1 (2
        #           uint16_t repeats;   short 2 (4
        #       END_CLASS
        #
        #       BEGIN_CLASS( ODReadingf, 2092, "UUf" )
        #           uint16_t flash;     short 2
        #           uint16_t bgr;       short 2 (4
        #           float od;           float 4 (8
        #       END_CLASS
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        result = (None, None, None)
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        if 0 > led > 1: raise ValueError('The value of led is out of range: {}. ' +
                                         'Allowed range is <0,1>'.format(led))
        if 0 > repeats: raise ValueError('The value of repeats is out of range: {}. ' +
                                         'Allowed range is <0,7>'.format(repeats))
        od = self.generateOD(mean, std)
        if led == 0:
            od /= 2.0
        return self._light_settings[channel][self.LIGHT_INTENSITY_ABSOLUTE], 13.0, od

    def getODDataSize(self):
        """
        Ask the multicultivator for the current Data Size.
        :return: Data Size of all OD measurements stored in the multicultivator
        :rtype: int
        """
        result = -1
        if not self._isOpen: raise SerialException(SerialException.SERIAL_ERROR, "No connection")
        return 1024

    def getDeviceVersion(self):
        """
        Get the device version
        :return: string
        :rtype:
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_ERROR, "No connection")
        return "Fake Version 1.0"

    def getLightSettings(self, channel=0):
        """
        Get the current light settings for a given channel
        :param channel: Vessel for which to get the light settings <0,7>
        :type channel: int
        :return: A tuple with 4 elements ( ON/OFF, Voltage, Eprc, Eabs )
        :rtype: tuple
        CLASS_CREATION(getLightSettings, 1021, "u")
        BEGIN_CLASS(lightSettings, 2021, "ufff")
             uint8_t on;         # byte 1
            float	voltage;    # float 4 (5
            float	uEprc;      # float 4 (9
            float	uEabs;      # float 4 (13
        END_CLASS
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        return self._light_settings[channel]

    def setAllLightStates(self, state=False):
        """
        Sets the state of all lights to the given state.
        :param state: New state for all lights (ON = True, OFF = False)
        :type state: bool
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1022, "uu")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        for c in range(len(self._light_settings)):
            self._light_settings[c][self.LIGHT_STATE] = state
        return True

    def setLightState(self, channel=0, state=False):
        """
        Set the light state of a given channel. State is ON (True) or OFF (False)
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param state: The state to set the light to (ON = True, OFF = False)
        :type state: bool
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1022, "uu")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        self._light_settings[channel][self.LIGHT_STATE] = state
        return True

    def setAllLightsPercentage(self, value=0):
        """
        Set the light to a percentage of maximum E/m2/s
        :param value: The percentage [0,100]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1023, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if 0 > value > 100: raise ValueError('The value of perc is out of range: {}' +
                                             'Allowed range is [0,100]'.format(value))
        for c in range(len(self._light_settings)):
            self._light_settings[c][self.LIGHT_INTENSITY_PERCENTAGE] = value
        return True

    def setLightPercentage(self, channel=0, value=0):
        """
        Set the light to a percentage of maximum E/m2/s
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param value: The percentage <0,100>
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1023, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        if 0 > value: raise ValueError('The value of perc is out of range: {}' +
                                       'Allowed range is <0,N>'.format(value))
        self._light_settings[channel][self.LIGHT_INTENSITY_PERCENTAGE] = value
        return True

    def setAllLightsAbsolute(self, value=0):
        """
        Set the all lights to a absolute value of E/m2/s
        :param value: The light intensity [0,N]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightuEabs, 1024, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if 0 > value: raise ValueError('The value of value is out of range: {}. ' +
                                       'Allowed range is [0,N]'.format(value))
        for c in range(len(self._light_settings)):
            self._light_settings[c][self.LIGHT_INTENSITY_ABSOLUTE] = value
        return True

    def setLightAbsolute(self, channel=0, value=0):
        """
        Set the light to a absolute value of E/m2/s
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param value: The light intensity [0,N]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightuEabs, 1024, "uf")
        # light_settings[channel] = [ state , voltage, perc, abs ]
        self._light_settings[channel][self.LIGHT_INTENSITY_ABSOLUTE] = value
        return True

    def setLightVoltage(self, channel=0, value=0):
        """
        Set the light to a voltage value
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param value: The voltage [0,100]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightuEabs, 1024, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        if 0 > value > 100: raise ValueError('The value of value is out of range: {}. ' +
                                             'Allowed range is [0,100]'.format(value))
        self._light_settings[channel][self.LIGHT_INTENSITY_VOLTAGE] = value
        return True

    def getTempSetting(self):
        """
        Get the current temperature settings
        :return: tuple with 2 elements: ( state, target_temperature)
        :rtype: tuple
        """
        # CLASS_CREATION(getTempSettings, 1000, "")
        # BEGIN_CLASS( tempSettings, 2000, "uf" )
        #   uint8_t on;
        #   float targetT;
        # END_CLASS
        return (self._controlTemp, self._targetTemp)

    def setTempState(self, state=False):
        """
        Set the temperature control state
        :param state: State of temperature control. <True = on, False =off>
        :type state: bool
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setTempState_cmd, 1001, "u")
        self._controlTemp = state
        return True

    def setTargetTemperature(self, state=False, temp=0.0):
        """
        Set the target temperature for the MC
        :param state: State of temperature control. <True = on, False =off>
        :type state: bool
        :param temp: Target temperature <0,N>
        :type temp: float
        :return: Return True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION( setTempTarget, 1002, "uf" )
        self._targetTemp = temp
        self._controlTemp = state
        return True

    def getTemperature(self):
        """
        Get the current temperature of the water bath
        :return: The current temperature or None if not available
        :rtype: float, None
        """
        # CLASS_CREATION(getTemp, 1003, "")
        return self._targetTemp

    def checkWaterLevel(self):
        """
        Ask the MC if the water level is OK.
        BEGIN_CLASS(getWaterLvl, 1110, "")
        END_CLASS
        :return: True if water level is OK else False
        :rtype: bool
        """
        return True

    def setPumpState(self, state=False):
        """
        Turn the pump on (state=True) or off (state=False)
        BEGIN_CLASS(setAirPumpState, 1121, "u")
            uint8_t state;
        END_CLASS
        :param state: Toggles the pump
        :type state: bool
        :return: True on success, False on failure
        :rtype: bool
        """
        self._pumpState = state
        return True

    def getPumpState(self):
        """
        Ask the MC about the GAS pump state
        BEGIN_CLASS(getAirPumpState, 1120, "")
        :return: Whehter the pump is on (true) of off (false)
        :rtype: bool
        """
        return self._pumpState

    def calibrateOD(self):
        """
        Will start the calibration process for the OD measurements
        BEGIN_CLASS(calibrateOD, 1093, "")
        END_CLASS
        :return: Whether the calibration was performed with success (True) otherwise False
        :rtype: bool
        """
        return True
