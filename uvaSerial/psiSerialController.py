# coding=utf-8
"""
Responsible for translating function to specific commands and writeing those to psi MC.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from psiSerialPacket import psiSerialPacket, psiSerialPacketException
from uvaSerialController import SerialController
from uvaSerialException import SerialException
import struct


class psiSerialController(SerialController):
    """
    Class implementing the serial communication with PSI's MultiCultivator
    """

    def __init__(self, settings=None):
        """
        """
        super(psiSerialController, self).__init__(settings)
        self._retry_max = 2

    def testRates(self, speeds=None, testAll=True):
        """
        Test which BAUDrates work with the serial device
        :param speeds: list with speeds to test (even when only one rate is given)
        :type speeds: list
        :param testAll: whether to test all speeds, or stop after one working speed is found
        :type testAll: bool
        :return: list with working rates (even when only one rate is returned)
        :rtype: list
        """
        rates = []
        # use default list
        if not speeds:
            speeds = [9600] #[115200, 9600]
        # if already open; or if we succeed opening it manually
        wasOpen = self._isOpen
        for rate in speeds:
            self.getLog().debug("Test communcation at rate: {}".format(rate))
            # set rate
            try:
                self._serial.configure(rate=rate, readTimeout=20)
                # if not already open, open it
                if not self._isOpen: self._isOpen = self._serial.open()
                # if open (already open or succeeded opening it) test it
                for retry in range(self._retry_max):
                    if self._isOpen:
                        self.reset()
                        # write ping
                        p = psiSerialPacket.createPingPacket(self.takeSeqNum())
                        if self.sendWAnswerOk(p):
                            rates.append(rate)
                            break
                if len(rates) >= 1 and not testAll:
                    break  # found a rate; so break
            except OSError as os:
                pass  # ignore
            except SerialException as se:
                pass  # ignore
        # we opened the serial for test purpose; now close it
        if not wasOpen: self._isOpen = not self._serial.close()
        return rates

    def connect(self):
        result = super(psiSerialController, self).connect()
        if result:
            # configure and start communication
            self.startCommunication()
        return self._isOpen

    def receive(self, expLen=0, dataFormat=""):
        """Receive data of a certain length

        :param expLen: Expected DATA length (excluding command code size)
        :type expLen: int
        :param dataFormat: Struct Format string, specifying how to interpret the data
        :type dataFormat: str
        :return: The received packet or None if nothing was received or on failure
        :rtype: SerialPacket or None
        """
        result, data = None, bytearray()
        try:
            # data = self._serial.read_after(70, expLen + 11)
            # if len(data) > 0:
            #    result = psiSerialPacket.parseByteArray(data, dataFormat)
            result = self.receivePacket(dataFormat=dataFormat)
        except psiSerialPacketException:
            self._serial.logData(
                data, msg="Unable parse format {} using data:".format(dataFormat),
                level=self.getLogLevels()["WARNING"]
            )
        return result

    def receivePacket(self, splitSignal=None, dataFormat="", max_attempts=5):
        result = None
        packets = self.receivePackets(
            splitSignal=splitSignal, dataFormat=dataFormat,
            max_attempts=max_attempts
        )
        # return one packet
        if len(packets) > 1:
            self.getLog().warning("Received more than one valid packets, take first")
        if len(packets) > 0:
            result = packets[0]
        else:
            self.getLog().warning("No valid packets received")
        return result

    def receivePackets(self, splitSignal=None, dataFormat="", max_attempts=5):
        if splitSignal is None:
            splitSignal = bytearray(struct.pack("4s", "FMTP"))
        packets = []
        data = bytearray()
        attempts = 0
        while len(packets) == 0 and attempts < max_attempts:
            new_data = bytearray()
            try:
                new_data = self._serial.read_all(min_length=0, min_no_data=3)
            except SerialException:
                pass
            # add new_data to data
            data.extend(new_data)
            # split into packages
            arrays = data.split(splitSignal)
            # parse packets
            for array in arrays:
                p = None
                ba = splitSignal + array
                if len(array) > 0:
                    p = psiSerialPacket.parseByteArray(ba, dataFormat)
                if p is not None and p.getSequenceNumber() == self.getSeqNum():
                    packets.append(p)
            attempts += 1
        return packets

    def splitData(self, data, startSignal=None, endSignal=None):
        packets = []
        if startSignal is None:
            startSignal = struct.pack("4s", "FMTP")
        if endSignal is None:
            endSignal = struct.pack("4s", "FMTP")
        pointer, selection, packet = 0, bytearray(), bytearray()
        state = 0  # 0 : nothing , 1 = has start
        while len(data) > 0:
            desired = len(startSignal) if state == 0 else len(endSignal)
            # add data first byte to selection
            selection.append(data.pop(0))
            if len(selection) == desired:
                if state == 1:
                    if endSignal == selection:
                        self.getLog().log(2, "Found end signal")
                        state = 0
                        packets.append(packet)
                        packet = bytearray()
                    else:
                        packet.append(selection.pop(0))
                if state == 0:
                    # looking for start
                    if selection.startswith(startSignal):
                        self.getLog().log(2, "Found start signal")
                        state += 1
                        # add to packet and reset selection
                        packet.extend(selection)
                        selection = bytearray()
                    else:
                        # remove first character
                        selection.pop(0)
        if len(selection) > 0:
            self.getLog().log(2, "Selection in pipeline, but reach EOL")
            packet.extend(selection)
        if len(packet) > 0:
            self.getLog().log(2, "Packet in pipeline reached EOL without end signal")
            packets.append(packet)
        return packets

    def receiveString(self, expLen=0):
        """Try to receive a String if expLen is zero; use variable length approach

        :param expLen: The size of the expected string
        :type expLen: int
        :return: The received packet or None if nothing was received or on failure
        :rtype: psiSerialPacket, None
        """
        return self.receiveAndCheck(expLen=-11, expCmd=101, dataFormat="s")

    #
    # Protocol commands
    #

    def sendPing(self):
        """
        Send a ping command and test if the response is OK
        :return: Whether the repsonds is OK
        :rtype: bool
        """
        return self.sendWAnswerOk(psiSerialPacket.createPingPacket(self.takeSeqNum()))

    def startCommunication(self):
        """Starts the connection with 5 start-up commands and then sets the speed to 9600

        :return: Returns True on success and False on failure
        :rtype: bool
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        res = False
        # write 5 packets
        for i in range(5):
            res = res and self.sendPing()
        self._serial.flush()
        if res:
            rate = self.getSerialSpeed()
            if rate != self.getRate():
                res = self.setSerialSpeed(self.getRate())
                if not res:
                    self.getLog().warning(
                        "Could not change serial rate on device, from {} to {}".format(
                            rate, self.getRate()
                        )
                    )
        return res

    def getSerialSpeed(self):
        """Determines the serial speed that the device is configured to use


        :return: The serial speed as reported by the device
        :rtype: int or None
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        res = None
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=17, data_length=0)
        self.getLog().debug("Determine serial speed of device")
        # send command
        if self.send(senp):
            # try to receive
            recp = self.receiveAndCheck(expLen=2, expCmd=111, dataFormat="H")
            if recp:
                # read result
                res = int(recp.getDataFieldValue(0))
        return res

    def setSerialSpeed(self, speed):
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=20, data_length=6)
        if 9600 >= speed >= 115200:
            raise ValueError(
                'The value of channel is out of range: {}. ' +
                'Allowed range is <0,7>'.format(speed)
            )
        # speed
        senp.addDataField(psiSerialPacket.FORMAT_UINT, speed)
        # timeout
        senp.addDataField(psiSerialPacket.FORMAT_USHORT, 5000)
        self.getLog().debug("Set the serial speed of device to {}".format(speed))
        return self.sendWAnswerOk(senp)

    def getODReading(self, channel=0, led=0, repeats=1):
        """
        Directly measure the OD. This will not mess with the pumps, use measureOD instead
        :param channel: The channel or vessel to measure the OD <0,7>
        :type channel: int
        :param led: The led to use [ 0=680nm, 1=720nm ]
        :type led: int
        :param repeats: Number of measurements to make and calculate the average over <1,N>
        :type repeats: int
        :return: tuple with (signal intensity, background, OD)
        :rtype: tuple
        """
        # BEGIN_CLASS( getODReadingf, 1092, "uuU" )
        # uint8_t light;      byte 1
        #           uint8_t led;        byte 1 (2
        #           uint16_t repeats;   short 2 (4
        #       END_CLASS
        #
        #       BEGIN_CLASS( ODReadingf, 2092, "UUf" )
        #           uint16_t flash;     short 2
        #           uint16_t bgr;       short 2 (4
        #           float od;           float 4 (8
        #       END_CLASS
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        result = (None, None, None)
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1092, data_length=4)
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, channel)
        if 0 > led > 1: raise ValueError('The value of led is out of range: {}. ' +
                                         'Allowed range is <0,1>'.format(led))
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, led)
        if 0 > repeats: raise ValueError('The value of repeats is out of range: {}. ' +
                                         'Allowed range is <0,7>'.format(repeats))
        senp.addDataField(psiSerialPacket.FORMAT_USHORT, repeats)
        self.getLog().debug("Read OD of {} at led {} using {} repeats".format(channel, led, repeats))
        if self.send(senp):
            recp = self.receiveAndCheck(expLen=8, expCmd=2092, dataFormat="HHf")
            if recp: result = (recp.getDataFieldValue(0), recp.getDataFieldValue(1), recp.getDataFieldValue(2))
        return result

    def getODDataSize(self):
        """
        Ask the multicultivator for the current Data Size.
        :return: Data Size of all OD measurements stored in the multicultivator
        :rtype: int
        """
        result = -1
        if not self._isOpen: raise SerialException(SerialException.SERIAL_ERROR, "No connection")
        self.getLog().debug("Read Data Size of OD Measurements in device")
        if self.send(psiSerialPacket.createCommandPacket(self.takeSeqNum(), 1091)):
            p = self.receiveAndCheck(4, 112, psiSerialPacket.FORMAT_UINT)
            if p: result = int(p.getDataFieldValue(0))
        return result

    def getDeviceVersion(self):
        """
        Get the device version
        :return: string
        :rtype:
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_ERROR, "No connection")
        recp = None
        senp = psiSerialPacket.createCommandPacket(self.takeSeqNum(), 4)
        self.getLog().debug("Read Device Version")
        if self.send(senp):
            recp = self.receiveString()
        return recp

    def getLightSettings(self, channel=0):
        """
        Get the current light settings for a given channel
        :param channel: Vessel for which to get the light settings <0,7>
        :type channel: int
        :return: A tuple with 4 elements ( ON/OFF, Voltage, Eprc, Eabs )
        :rtype: tuple[bool, float, float, float]

        CLASS_CREATION(getLightSettings, 1021, "u")
        BEGIN_CLASS(lightSettings, 2021, "ufff")
             uint8_t on;         # byte 1
            float	voltage;    # float 4 (5
            float	uEprc;      # float 4 (9
            float	uEabs;      # float 4 (13
        END_CLASS
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        result = None
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1021, data_length=1)
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, channel)
        self.getLog().debug("Read Light Settings of channel {}".format(channel))
        if self.send(senp):
            recp = self.receiveAndCheck(expLen=13, expCmd=2021, dataFormat="bfff")
            if recp:
                # parse result into tuple
                result = (recp.getDataFieldValue(0) == 1, float(recp.getDataFieldValue(1)),
                          float(recp.getDataFieldValue(2)), float(recp.getDataFieldValue(3)))
        return result

    def setAllLightStates(self, state=False):
        """
        Sets the state of all lights to the given state.
        :param state: New state for all lights (ON = True, OFF = False)
        :type state: bool
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1022, "uu")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1022, data_length=2)
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 255)
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 1 if state else 0)  # use if to set bit value
        self.getLog().debug("Set all Lights to {}".format(state))
        return self.sendWAnswerOk(senp)

    def _setAllLightStatesSafe(self, state=False):
        result = True
        for i in range(8):
            success = self.setLightState(i, state)
            if not success:
                self.getLog().warning(
                    "Failed to set Light of channel {} to {}".format(i, state)
                )
            result = success and result
        return result

    def setLightState(self, channel=0, state=False):
        """
        Set the light state of a given channel. State is ON (True) or OFF (False)
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param state: The state to set the light to (ON = True, OFF = False)
        :type state: bool
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1022, "uu")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1022, data_length=2)
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, channel)
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 1 if state else 0)  # use if to set bit value
        self.getLog().debug("Set Light in channel {} to {}".format(channel, state))
        return self.sendWAnswerOk(senp)

    def setAllLightsPercentage(self, value=0):
        """
        Set the light to a percentage of maximum E/m2/s
        :param value: The percentage [0,100]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1023, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1023, data_length=5)
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 255)
        if 0 > value > 100: raise ValueError('The value of perc is out of range: {}' +
                                             'Allowed range is [0,100]'.format(value))
        senp.addDataField(psiSerialPacket.FORMAT_FLOAT, value)
        self.getLog().debug("Set ALL Lights to {}%".format(value))
        return self.sendWAnswerOk(senp)

    def setLightPercentage(self, channel=0, value=0):
        """
        Set the light to a percentage of maximum E/m2/s
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param value: The percentage <0,100>
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightState_cmd, 1023, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1023, data_length=5)
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, channel)
        if 0 > value: raise ValueError('The value of perc is out of range: {}' +
                                       'Allowed range is <0,N>'.format(value))
        senp.addDataField(psiSerialPacket.FORMAT_FLOAT, value)
        self.getLog().debug("Set Light in channel {} to {}%".format(channel, value))
        return self.sendWAnswerOk(senp)

    def setAllLightsAbsolute(self, value=0):
        """
        Set the all lights to a absolute value of E/m2/s
        :param value: The light intensity [0,N]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightuEabs, 1024, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1024, data_length=5)
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 255)
        if 0 > value: raise ValueError('The value of value is out of range: {}. ' +
                                       'Allowed range is [0,N]'.format(value))
        senp.addDataField(psiSerialPacket.FORMAT_FLOAT, value)
        self.getLog().debug("Set ALL Lights to {}".format(value))
        return self.sendWAnswerOk(senp)

    def setLightAbsolute(self, channel=0, value=0):
        """
        Set the light to a absolute value of E/m2/s
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param value: The light intensity [0,N]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightuEabs, 1024, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1024, data_length=5)
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, channel)
        if 0 > value: raise ValueError('The value of value is out of range: {}. ' +
                                       'Allowed range is [0,N]'.format(value))
        senp.addDataField(psiSerialPacket.FORMAT_FLOAT, value)
        self.getLog().debug("Set Light in channel {} to {}".format(channel, value))
        return self.sendWAnswerOk(senp)

    def setLightVoltage(self, channel=0, value=0):
        """
        Set the light to a voltage value
        :param channel: The channel or vessel of which to set the light [0,7]
        :type channel: int
        :param value: The voltage [0,100]
        :type value: float
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setLightuEabs, 1024, "uf")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1025, data_length=5)
        if 0 > channel > 7: raise ValueError('The value of channel is out of range: {}. ' +
                                             'Allowed range is <0,7>'.format(channel))
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, channel)
        if 0 > value > 100: raise ValueError('The value of value is out of range: {}. ' +
                                             'Allowed range is [0,100]'.format(value))
        senp.addDataField(psiSerialPacket.FORMAT_FLOAT, value)
        self.getLog().debug("Set Light in channel {} to {}V".format(channel, value))
        return self.sendWAnswerOk(senp)

    def getTempSetting(self):
        """Get the current temperature settings

        :return: tuple with 2 elements: ( state, target_temperature)
        :rtype: tuple
        """
        # CLASS_CREATION(getTempSettings, 1000, "")
        # BEGIN_CLASS( tempSettings, 2000, "uf" )
        #   uint8_t on;
        #   float targetT;
        # END_CLASS
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        result = None
        self.getLog().debug("Read temperature settings")
        if self.send(psiSerialPacket.createCommandPacket(self.takeSeqNum(), 1000)):
            recp = self.receiveAndCheck(6, 2000, "Bf")
            if recp:
                result = (recp.getDataFieldValue(0) == 1, recp.getDataFieldValue(1))
        return result

    def setTempState(self, state=False):
        """Set the temperature control state

        :param state: State of temperature control. <True = on, False =off>
        :type state: bool
        :return: Returns True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION(setTempState_cmd, 1001, "u")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1001, data_length=1)
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 1 if state else 0)
        self.getLog().debug("Set Temperature Control to {}".format(state))
        return self.sendWAnswerOk(senp)

    def setTargetTemperature(self, state=False, temp=0):
        """Set the target temperature for the MC

        :param state: State of temperature control. <True = on, False =off>
        :type state: bool
        :param temp: Target temperature <0,N>
        :type temp: float
        :return: Return True on success and False on failure
        :rtype: bool
        """
        # CLASS_CREATION( setTempTarget, 1002, "uf" )
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1002, data_length=5)
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 1 if state else 0)
        # TODO: Implement range check <0,N>
        senp.addDataField(psiSerialPacket.FORMAT_FLOAT, temp)
        self.getLog().debug("Set Temperature Control to {} at {}C".format(state, temp))
        return self.sendWAnswerOk(senp)

    def getTemperature(self):
        """Get the current temperature of the water bath

        :return: The current temperature or None if not available
        :rtype: float, None
        """
        # CLASS_CREATION(getTemp, 1003, "")
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        self.send(psiSerialPacket.createCommandPacket(self.takeSeqNum(), 1003))
        self.getLog().debug("Get current Temperature")
        p = self.receiveAndCheck(4, 116, psiSerialPacket.FORMAT_FLOAT)
        return float(p.getDataFieldValue(0)) if p else None

    def checkWaterLevel(self):
        """Ask the MC if the water level is OK.

        BEGIN_CLASS(getWaterLvl, 1110, "")
        END_CLASS
        :return: True if water level is OK else False
        :rtype: bool
        """
        result = False
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        self.getLog().debug("Check water level")
        if self.send(psiSerialPacket.createCommandPacket(self.takeSeqNum(), 1110)):
            recp = self.receiveAndCheck(1, 110, "B")
            if recp:
                result = recp.getDataFieldValue(0) == 1
        return result

    def setPumpState(self, state=False):
        """Turn the pump on (state=True) or off (state=False)

        BEGIN_CLASS(setAirPumpState, 1121, "u")
            uint8_t state;
        END_CLASS
        :param state: Toggles the pump
        :type state: bool
        :return: True on success, False on failure
        :rtype: bool
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        senp = psiSerialPacket(seq_nr=self.takeSeqNum(), cmd=1121, data_length=1)
        # parse state bool to int
        senp.addDataField(psiSerialPacket.FORMAT_UBYTE, 1 if state else 0)
        self.getLog().debug("Set AIR Pump to {}".format(state))
        return self.sendWAnswerOk(senp)

    def getPumpState(self):
        """
        Ask the MC about the GAS pump state
        BEGIN_CLASS(getAirPumpState, 1120, "")
        :return: Whehter the pump is on (true) of off (false)
        :rtype: bool
        """
        result = False
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        self.getLog().debug("Determine AIR Pump state")
        if self.send(psiSerialPacket.createCommandPacket(self.takeSeqNum(), 1120)):
            recp = self.receiveAndCheck(6, 2120, psiSerialPacket.FORMAT_UBYTE)
            if recp:
                result = recp.getDataFieldValue(0) == 1
        return result

    def calibrateOD(self):
        """Will start the calibration process for the OD measurements

        BEGIN_CLASS(calibrateOD, 1093, "")
        END_CLASS
        :return: Whether the calibration was performed with success (True) otherwise False
        :rtype: bool
        """
        if not self._isOpen: raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        self.getLog().debug("Execute OD Blanking program")
        return self.sendWAnswerOk(psiSerialPacket.createCommandPacket(self.takeSeqNum(), 1093))
