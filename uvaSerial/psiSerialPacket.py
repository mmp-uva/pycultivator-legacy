# coding=utf-8
"""
Module that implements the parsing of bytearrays into objects (psiSerialPacket2)
"""

from uvaSerialPacket import SerialPacket
import uvaSerialException
import struct
import re

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'



class psiSerialPacket(SerialPacket):
    """
    Responsible for parsing and creating FMTP packets
    Maybe overloaded to model specific FMTP packets
    The corresponding ByteArray on request if the properties of the object were changed since previous request
    Users are responsible for setting the correct length
    - header -- -sq#- -len- -cmd- <-data-> cs
    46 4d 54 50 01 00 02 00 04 00          08
    :param seq_nr: Sequence number of this packet
    :type seq_nr: int
    :param cmd: Command code of this packet
    :type cmd: int
    :param data_length: data length of this packet (EXcluding cmd code size). Default=0
    :type data_length: int
    """

    # Constants
    FORMAT_HEADER = "4c"
    HEADER_LENGTH = 4

    def __init__(self, seq_nr=0, cmd=0, data_length=0):
        # call super with
        # with a header of "FMTP"
        # with no footer
        # a overhead length of 9 (4 header + 2 seq nr + 2 length +1 crc)
        super(psiSerialPacket, self).__init__(b'FMTP', None, seq_nr, cmd, data_length + 2, 9, True)

    @classmethod
    def createCommandPacket(cls, seq_nr, cmd):
        """
        create a psiSerialPacket2 with a given cmd and seqNr.
        :type seq_nr: int
        :param seq_nr: number of this packet in protocol. <0,>
        :type cmd: int
        :param cmd: Command code of this packet
        :rtype: psiSerialPacket2
        :return: psiSerialPacket2 object with command code and sequence, without data
        """
        return cls(seq_nr=seq_nr, cmd=cmd)

    @classmethod
    def createPingPacket(cls, seqNr):
        """
        create a psiSerialPacket2 with command code 1 and given seqNr.
        :type seqNr: int
        :param seqNr: number of this packet in protocol. <0,>
        :rtype: psiSerialPacket2
        :return: object with the properties of a ping packet
        """
        return cls.createCommandPacket(seqNr, 1)

    @classmethod
    def parseByteArrayValue(cls, fmt, array, offset=0):
        """Unpacks a value from a bytearray using the given format

        :rtype: tuple[list[int,str,float],int]
        :return: The values and a new offset
        """
        fmt_size = struct.calcsize(fmt)
        if len(array) < fmt_size:
            raise psiSerialPacketException(
                None, "Not enough data to read format ({} < {})".format(
                    len(array), fmt_size)
            )
        try:
            values = struct.unpack_from(fmt, array, offset)
            offset += fmt_size
        except struct.error as e:
            raise psiSerialPacketException(
                None, "Failed to read format: {}".format(e)
            )
        return values, offset

    @classmethod
    def parseByteArrayHeader(cls, array, packet=None, offset=0):
        """
        Parses the header of a given bytearray into a given psiSerialPacket2 object or a new instance
        :param array: Bytearray to parse
        :type array: bytearray
        :param packet: Packet to parse the bytearray to
        :type packet: psiSerialPacket
        :return: The updated packet object
        :rtype: psiSerialPacket
        """
        if packet is None:
            packet = cls()
        # read header without storing
        header, offset = cls.parseByteArrayValue(cls.FORMAT_HEADER, array, offset)
        header = ''.join(header)
	if header != packet._header:
	    raise psiSerialPacketException(None, "Invalid header: {} != {}".format(header, packet._header))
        # read sequence number
        seqnr, offset = cls.parseByteArrayValue(cls.FORMAT_USHORT, array, offset)
        packet.setSequenceNumber(int(seqnr[0]))
        # read data length
        payload, offset = cls.parseByteArrayValue(cls.FORMAT_USHORT, array, offset)
        packet.setDataLength(int(payload[0]) - 2)
        # read command code
        cmd, offset = cls.parseByteArrayValue(cls.FORMAT_USHORT, array, offset)
        packet.setCommandCode(int(cmd[0]))
        return packet, offset

    @classmethod
    def parseByteArray(cls, array, fmts="", check_crc=False):
        """
        parse a ByteArray
        :param array: ByteArray - Packet in the form of bytearray
        :type array: bytearray
        :param f: A Python struct format string, which specifies how to parse the data part
        :type f: str
        :return: new instance with data of array or None on failure
        :rtype: psiSerialPacket
        """
        p = None
        if not check_crc or cls.check_crc(array):
            try:
                p = cls()
                p, offset = cls.parseByteArrayHeader(array=array, packet=p)
		if fmts != "":
	                p, offset = cls.parseByteArrayData(fmts=fmts, array=array, packet=p, offset=offset)
            except psiSerialPacketException as pse:
                cls.ByteArrayToLog(
                    cls.getLog(), array, "Unable to parse:\n{}\nData:".format(pse)
                )
            except struct.error as se:
                cls.ByteArrayToLog(
                    cls.getLog(), array, "Unable to parse:\n{}\nData:".format(se)
                )
        return p

    @classmethod
    def parseByteArrayData(cls, fmts, array, packet=None, offset=0):
        if packet is None:
            packet = cls()
        #if packet.getDataLength() > 0:
        #    raise psiSerialPacketException(None, "Packet already contains data")
        if fmts == "":
            raise psiSerialPacketException(None, "Invalid format given")
        # separate struct format strings into a list, so we can parse them individually
        fmts = re.findall(r"[0-9]*[A-Za-z]", fmts)
        for fmt in fmts:
            data = None
            try:
                fType = fmt[-1]  # read the "class"
                if fType == cls.FORMAT_STRING:
                    fmt = "{}s".format(packet.getDataLength())
                values, offset = cls.parseByteArrayValue(fmt, array, offset)
                if fType == cls.FORMAT_CHAR:
                    data = ''.join(values)
                else:
                    data = values[0]
            except psiSerialPacketException as pse:
                cls.ByteArrayToLog(
                    cls.getLog(), array, "Unable to parse:\n{}\nData:".format(pse)
                )
            if data is not None:
                packet.addDataField(fmt, data)
        return packet, offset

    def createByteArray(self):
        """
        Create a bytearray representation of this object. Format of the array is as follows:
        --4 bytes-- 2 byt 2 byt 2 byt < len-2> 1b
        ---header-- -sq#- -len- -cmd- <-data-> cs
        46 4d 54 50 01 00 02 00 04 00          08
        :rtype: bytearray
        :return: Bytearray representation of this object.
        """
        p = bytearray()
        # first create header
        p.extend(struct.pack(self.FORMAT_HEADER, *list(self._header)))
        p.extend(struct.pack(self.FORMAT_USHORT, self._seqNr))
        p.extend(struct.pack(self.FORMAT_USHORT, self._payloadLength))
        p.extend(struct.pack(self.FORMAT_USHORT, self._cmd))
        # then load data field
        for f in self._dataFields:
            # strings should be parsed as 1, combinations of bytes as seperate
            if len(f[0]) > 1 and f[0][-1] != self.FORMAT_STRING:
                p.extend(struct.pack(f[0], *f[1]))
            else:
                p.extend(struct.pack(f[0], f[1]))
        # if crc is enabled: calculate the crc and add it to the packet!
        if self._crc: p.extend(struct.pack(self.FORMAT_UBYTE, self.calculate_crc(p)))  # 1 byte crc (11)
        self._bytearray = p
        self._changed = False
        return p

    def setDataLength(self, length):
        """
        Set the length of the data part of the packet excluding the command code bytes
        :param length: Length in bytes of the data part, excluding command code bytes
        :type length: int
        :rtype: int
        """
        self._payloadLength = length + 2  # add 2 command code bytes
        self._changed = True
        return length

    def setPacketLength(self, length):
        """
        Set the TOTAL length of the packet including the command code, header, seqNr, length and CRC bytes
        :type length: int
        :param length: TOTAL length of the packet. Domain=<11,>
        """
        self._payloadLength = length - self._overheadLength  # only store cmd code + data length <2,>
        self._changed = True
        return length

    def getDataLength(self):
        """
        Get the length of the data part of the packet excluding the length of the command code bytes
        :rtype: int
        :return: Data length of the packet excluding the command code bytes. Domain=<0,>
        """
        return self._payloadLength - 2  # remove 2 command code bytes


class psiSerialPacketException(uvaSerialException.SerialException):

    def __init__(self, code, msg):
        super(psiSerialPacketException, self).__init__(code, msg)
