# coding=utf-8
"""
Responsible for translating function to specific commands and send/receive those to/from the reglo ISMATEC PUMP.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from regloSerialPacket import regloSerialPacket
from regloSerialController import regloSerialController
from uvaSerialException import SerialException
import re


class regloFakeSerialController(regloSerialController):
    """
    Class implementing the serial communication with PSI's MultiCultivator
    """

    def __init__(self, settings):
        """

        """
        super(regloSerialController, self).__init__(settings=settings)
        # store the state of each channel
        self._states = {
            # 0 : (True, RPM, MODE, VALUE)
        }
        self._remote = True
        self._channels = 4
        self._rollers = 8

    def connect(self):
        port = self.getPort()
        if port is not None:
            self._isOpen = True
            self.getLog().info("[regloFakeSerialController] Open Serial connection to {}".format(
                self.getPort()
            ))
        return self._isOpen

    def disconnect(self):
        self._isOpen = False
        self.getLog().info("[regloFakeSerialController] Close Serial connection to {}".format(
            self.getPort()
        ))
        return self._isOpen

    def configure(self, port=None, **kwargs):
        """
        Configures the serial link to the correct settings
        :param port: Optional set the port to a device. Default: use port given at object creation
        :type port: str
        :return:
        :rtype:
        """
        self._configured = False
        if port is not None:
            self._settings["port"] = port
        self.getLog().info("Configure serial connection to pump at {}".format(self.getPort()))
        if self.getPort():
            self._configured = True
        return self._configured

    def testRates(self, speeds=None, testAll=True):
        """
        @deprecated REGLO ONLY SUPPORTS 9600. This will always return a list with 1 element (9600)
        :param speeds: list with speeds to test (even when only one rate is given)
        :type speeds: list
        :param testAll: whether to test all speeds, or stop after one working speed is found
        :type testAll: bool
        :return: list with working rates (even when only one rate is returned)
        :rtype: list
        """
        return [9600]

    def receive(self, expLen=0, dataFormat=""):
        """
        Receive a packet. NOTE: The Fake Controller always returns None
        :param expLen: Expected length of the complete package! (including footer). Domain=<0,N>
        :type expLen: int
        :param dataFormat: Python struct Format string without length
        :type dataFormat: str
        :return: The received packet or None if nothing was received or on failure
        :rtype: SerialPacket or None
        """
        return None

    def receiveAndCheck(self, expLen, expCmd, dataFormat=""):
        """
        Try to receive a packet of given length, with a given command code and an optional data format.
        Will check if length and command code are correct.
        :param expLen: Expected length of the complete package (including footer). Domain=<0,N>
        :type expLen: int
        :param expCmd: Expected command code
        :type expCmd: str
        :param dataFormat: struct format string, specifying how to interpret the data
        :type dataFormat: str
        :return: The received packet or None if nothing was received or on failure
        :rtype: SerialPacket, None
        """
        return None

    def receiveOK(self):
        """
        Try to receive an OK packet
        :return: True on success and False on incorrect response
        :rtype: bool
        """
        return self.receiveAndCheck(1, "*") is not None

    def receiveYes(self):
        """
        Try to receive an OK packet
        :return: True on success and False on incorrect response
        :rtype: bool
        """
        return self.receiveAndCheck(1, "+") is not None

    def receiveNo(self):
        """
        Try to receive an OK packet
        :return: True on success and False on incorrect response
        :rtype: bool
        """
        return self.receiveAndCheck(1, "-") is not None

    def receiveString(self, expLen=0):
        """
        Try to receive a String if expLen is zero; use variable length approach
        :param expLen: The size of the expected string
        :type expLen: int
        :return: The received packet or None if nothing was received or on failure
        :rtype: psiSerialPacket, None
        """
        return None

    def testRequirements(self, testSerial=True, testPump=None):
        """
        Will test whether all requirements are met. Can test for serial connection status and pump ID
        Will raise an exception if a requirement is not met
        :param testSerial: Whether to test if the the connection to the serial device is open
        :type testSerial: bool
        :param testPump: Whether to test if the pump ID is in range (None to omit testing)
        :type testPump: None, int
        """
        if testSerial and not self._isOpen:
            raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if testPump not in range(1, 9):
            # TODO: implement value error in Serial Error
            raise SerialException(SerialException.PROTOCOL_ERROR, "Pump ID ({}) is not valid.".format(testPump))

    def storeState(self, pump, **kwargs):
        if pump in self._states.keys():
            self.mergeDictionary(self._states[pump], kwargs, copy_target=False)
        else:
            self._states[pump] = kwargs
        return self._states[pump]

    def getState(self, pump, key, default=None):
        result = default
        if pump in self._states.keys():
            if key in self._states[pump].keys():
                result = self._states[pump][key]
        return result

    #
    #   Protocol commands
    #

    def saveConfiguration(self, pump):
        """
        Save the configuration to non-volatile memory, so it will be restored after shutdown
        :param pump: ID of the pump. <1,8>
        :type pump: int
        :return: True on succes, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        return True

    def startPump(self, pump):
        """
        Start the pump
        :param pump: ID of the pump. <1,8>
        :type pump: int
        :return: True on succes, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        self.storeState(pump=pump, state=True)
        return True

    def stopPump(self, pump):
        """
        Stop the pump
        :param pump: ID of the pump. <1,8>
        :type pump: int
        :return: True on succes, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        self.storeState(pump=pump, state=False)
        return True

    def setRotationDirection(self, pump, direction=None):
        """

        :param pump: ID of the pump. <1,8>
        :type pump: int
        :param direction:
        :type direction:
        :return:
        :rtype:
        """
        if direction is None:
            direction = self.PUMP_ROTATE_CLOCKWISE
        self.testRequirements(testPump=pump)
        if not direction in self.PUMP_ROTATE_DIRECTIONS:
            raise SerialException(SerialException.PROTOCOL_ERROR, "Invalid pump direction")
        self.storeState(pump=pump, direction=direction)
        return True

    def setRemoteControlModus(self, pump, state=False):
        """

        :param pump: ID of the pump. <1,8>
        :type pump: int
        :param state:
        :type state:
        :return:
        :rtype:
        """
        self.testRequirements(testPump=pump)
        self._remote = state is True
        return True

    def isRunning(self, pump):
        """
        Will retrieve the current state of the pump identified by the given pump ID
        :param pump: ID of the pump of which to get the state. <1,8>
        :type pump: int
        :return: True if running, False if not
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        return self.getState(pump, "state")

    def setPumpMode(self, pump, mode):
        """
        Set the pump to a certain mode (how the speed is defined)
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: True on success, False on false
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        if not mode in self.PUMP_MODES:
            raise SerialException(SerialException.PROTOCOL_ERROR, "Invalid mode: {}".format(mode))
        self.storeState(pump=pump, mode=mode)
        return True

    def getPumpHeadType(self, pump):
        """
        Returns a tuple with information on the pump head: ( #channels, #rollers)
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: tuple (#channels, #rollers)
        :rtype: tuple, None
        """
        self.testRequirements(testPump=pump)
        return (self._channels, self._rollers)

    def setPumpHeadType(self, pump, channels=4, rollers=8):
        """
        Set the used pump head type; which is a combination of channels and rollers
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param channels: Number of available channels. <0,9>
        :type channels: int
        :param rollers: Number of installed installed. <0,99>
        :type rollers: int
        :return: True on success otherwise False
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        self._channels = channels
        self._rollers = rollers
        return True

    def getPumpSoftwareVersion(self, pump):
        """
        Returns the installed software version
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: Software version
        :rtype: int, None
        """
        self.testRequirements(testPump=pump)
        return self.__version__

    def getTubeID(self, pump):
        """
        Get the inner diameter of the used tubing, as known by the pump
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: Inner Diameter of the tubing
        :rtype: float
        """
        self.testRequirements(testPump=pump)
        return self.getState(pump, "tube_id", default=1.22)

    def setTubeID(self, pump, diameter):
        """
        Stores the Inner Diameter of the used tubing in the pump (used for calculations and calibration)
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param diameter: The used Inner Diameter of the tubing in mm (!!)
        :type diameter: float
        :return: True on success, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        self.storeState(pump, tube_id=diameter)
        return True

    def getSpeedRPM(self, pump):
        """
        Get the current speed setting in RPM
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: The RPM or None on failure
        :rtype: float, None
        """
        self.testRequirements(testPump=pump)
        return self.getState(pump, "rate")

    def setSpeedRPM(self, pump, rpm, rate_mode=True):
        """
        Set the speed at which the pump will run
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param rpm: RPM to run at <0.00,100.00>
        :type rpm: float
        :return: True on success, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        self.storeState(pump, rate=rpm)
        return True

    def getFlowRate(self, pump):
        """
        Get the current speed setting in ml/min
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: The flow rate in ml/min or None on failure
        :rtype: float, None
        """
        self.testRequirements(testPump=pump)
        return self.getState(pump, "flow")

    def setFlowRate(self, pump, rate):
        """
        Set the current speed setting in ml/min
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param rate: flow rate to set
        :type rate: float
        :return: Flow rate that was set
        :rtype: float, None
        """
        self.testRequirements(testPump=pump)
        self.storeState(pump, flow=rate)
        return True

    def setDispenseTime(self, pump, time):
        """
        Set how long the pump will work in seconds
        :param pump: ID of the pump to set the mode of <0,4>
        :type pump: int
        :param time: In seconds.
        :type time: float
        :return:
        :rtype:
        """
        self.testRequirements(testPump=pump)
        self.storeState(pump, time=time)
        return True

    def setDispenseVolume(self, pump, volume):
        """
        Let the pump, dispense a given amount and then stop
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param volume: amount to dispense in ml
        :type volume: float
        :return: Dispense volume that was set
        :rtype: float, None
        """
        self.testRequirements(testPump=pump)
        self.storeState(pump, volume=volume)
        return True

    def getTotalVolumePumped(self, pump):
        """
        Get the total volume pumped since last time
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: Tuple with amount and unit ( amount, unit ). for example: (100, "ml") or None when failed
        :rtype: tuple, None
        """
        self.testRequirements(testPump=pump)
        return 100000

    def resetTotalVolumePumped(self, pump):
        """
        Resets the total volume pumped to 0
        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: True on success, False otherwise
        :rtype: bool
        """
        return True
