# coding=utf-8
"""
Responsible for translating function to specific commands and send/receive those to/from the reglo ISMATEC PUMP.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from regloSerialPacket import regloSerialPacket
from uvaSerialController import SerialController
from uvaSerialException import SerialException
import re


class regloSerialController(SerialController):
    """Class implementing the serial communication with PSI's MultiCultivator"""

    PUMP_ROTATE_CLOCKWISE = 1
    PUMP_ROTATE_COUNTERCLOCKWISE = 0
    PUMP_ROTATE_DIRECTIONS = [PUMP_ROTATE_CLOCKWISE, PUMP_ROTATE_COUNTERCLOCKWISE]

    PUMP_MODE_RPM = "L"
    PUMP_MODE_FLOWRATE = "M"
    PUMP_MODE_TIME = "N"
    PUMP_MODE_VOLUME_RATE = "O"
    PUMP_MODE_PAUSE = "]"
    PUMP_MODE_TIME_PAUSE = "P"
    PUMP_MODE_VOLUME_PAUSE = "Q"
    PUMP_MODE_VOLUME_TIME = "G"
    PUMP_MODES = [PUMP_MODE_RPM, PUMP_MODE_FLOWRATE, PUMP_MODE_TIME, PUMP_MODE_VOLUME_RATE,
                  PUMP_MODE_PAUSE, PUMP_MODE_TIME_PAUSE, PUMP_MODE_VOLUME_PAUSE, PUMP_MODE_VOLUME_TIME]

    def __init__(self, settings):
        """

        """
        super(regloSerialController, self).__init__(settings=settings)

    def configure(self, port=None, **kwargs):
        """
        Configures the serial link to the correct settings
        :param port: Optional set the port to a device. Default: use port given at object creation
        :type port: str
        :return:
        :rtype:
        """
        self._configured = False
        if port is not None: self._settings["port"] = port
        self.getLog().info("Will configure to use pump at {}".format(self.getPort()))
        if self.getPort():
            self._serial.configure(port=self.getPort(), rate=9600)
            self._configured = True
        return self._configured

    def testRates(self, speeds=None, testAll=True):
        """
        @deprecated REGLO ONLY SUPPORTS 9600. This will always return a list with 1 element (9600)
        :param speeds: list with speeds to test (even when only one rate is given)
        :type speeds: list
        :param testAll: whether to test all speeds, or stop after one working speed is found
        :type testAll: bool
        :return: list with working rates (even when only one rate is returned)
        :rtype: list
        """
        return [9600]

    def receive(self, expLen=0, dataFormat=""):
        """Receive a packet

        :param expLen: Expected length of the complete package! (including footer). Domain=<0,N>
        :type expLen: int
        :param dataFormat: Python struct Format string without length
        :type dataFormat: str
        :return: The received packet or None if nothing was received or on failure
        :rtype: SerialPacket or None
        """
        data = bytearray()
        try:
            data = self._serial.read(expLen)
        except SerialException:
            pass
        return regloSerialPacket.parseByteArray(data, dataFormat) if data else None

    def receiveAndCheck(self, expLen, expCmd, dataFormat=""):
        """Receive a packet and check if the response is correct.

        :param expLen: Expected length of the complete package (including footer). Domain=<0,N>
        :type expLen: int
        :param expCmd: Expected command code
        :type expCmd: str
        :param dataFormat: struct format string, specifying how to interpret the data
        :type dataFormat: str
        :return: The received packet or None if nothing was received or on failure
        :rtype: SerialPacket, None
        """
        result = None
        p = self.receive(expLen, dataFormat)
        if p is not None:
            # next check if we have data at all, correct length and correct cmd code
            rec_length = p.getPacketLength()
            rec_cmd = p.getCommandCode()
            if p.getPacketLength() == expLen and p.getCommandCode() == expCmd:
                result = p
        return result

    def receiveOK(self):
        """Try to receive an OK packet

        :return: True on success and False on incorrect response
        :rtype: bool
        """
        return self.receiveAndCheck(1, "*") is not None

    def receiveYes(self):
        """Try to receive an OK packet

        :return: True on success and False on incorrect response
        :rtype: bool
        """
        return self.receiveAndCheck(1, "+") is not None

    def receiveNo(self):
        """Try to receive an OK packet

        :return: True on success and False on incorrect response
        :rtype: bool
        """
        return self.receiveAndCheck(1, "-") is not None

    def receiveString(self, expLen=0):
        """Try to receive a String if expLen is zero; use variable length approach

        :param expLen: The size of the expected string
        :type expLen: int
        :return: The received packet or None if nothing was received or on failure
        :rtype: psiSerialPacket, None
        """
        recp = self.receive(dataFormat="s")
        return recp if recp and recp.getCommandCode() != "#" else None

    def testRequirements(self, testSerial=True, testPump=None):
        """Will test whether all requirements are met. Can test for serial connection status and pump ID

        Raises an exception if a requirement is not met

        :param testSerial: Whether to test if the the connection to the serial device is open
        :type testSerial: bool
        :param testPump: Whether to test if the pump ID is in range (None to omit testing)
        :type testPump: None, int
        """
        if testSerial and not self._isOpen:
            raise SerialException(SerialException.SERIAL_NOT_OPEN, "No connection")
        if not testPump in range(1, 9):
            # TODO: implement value error in Serial Error
            raise SerialException(SerialException.PROTOCOL_ERROR, "Pump ID ({}) is not valid.".format(testPump))
        #
        #   Protocol commands
        #

    def saveConfiguration(self, pump):
        """Save the configuration to non-volatile memory, so it will be restored after shutdown

        :param pump: ID of the pump. <1,8>
        :type pump: int
        :return: True on succes, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="*")
        return self.send(senp) and self.receiveOK()

    def startPump(self, pump):
        """Tell one pump channel to run

        :param pump: ID of the pump. <1,8>
        :type pump: int
        :return: True on succes, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="H")
        self.getLog().log(1, "Instruct Pump {} to start".format(pump))
        return self.send(senp) and self.receiveOK()

    def stopPump(self, pump):
        """Tell one pump channel to stop

        :param pump: ID of the pump. <1,8>
        :type pump: int
        :return: True on succes, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="I")
        self.getLog().log(1, "Instruct Pump {} to stop".format(pump))
        return self.send(senp) and self.receiveOK()

    def setRotationDirection(self, pump, direction=None):
        """Tell one pump channel to pump in a direction

        :param pump: ID of the pump. <1,8>
        :type pump: int
        :param direction:
        :type direction:
        :return:
        :rtype:
        """
        self.testRequirements(testPump=pump)
        if direction is None:
            direction = self.PUMP_ROTATE_CLOCKWISE
        if direction not in self.PUMP_ROTATE_DIRECTIONS:
            raise SerialException(SerialException.PROTOCOL_ERROR, "Invalid pump direction")
        cmd = "J" if direction == self.PUMP_ROTATE_CLOCKWISE else "K"
        senp = regloSerialPacket(pump=pump, cmd=cmd)
        self.getLog().log(1, "Set Pump {} in direction {}".format(pump, direction))
        return self.send(senp) and self.receiveOK()

    def setRemoteControlModus(self, pump, state=False):
        """Tell one pump channel to be in remote control state

        :param pump: ID of the pump. <1,8>
        :type pump: int
        :param state:
        :type state:
        :return:
        :rtype:
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="B" if state else "A")
        return self.send(senp) and self.receiveOK()

    def isRunning(self, pump):
        """Determine if the pump channel is running

        :param pump: ID of the pump of which to get the state. <1,8>
        :type pump: int
        :return: True if running, False if not
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="E")
        return self.send(senp) and self.receiveYes()

    def setPumpMode(self, pump, mode):
        """Set the pump to a certain mode.

        Modes define how the RPM, pump time and/or volume are determined.

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: True on success, False on false
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        if mode not in self.PUMP_MODES:
            raise SerialException(SerialException.PROTOCOL_ERROR, "Invalid mode: {}".format(mode))
        senp = regloSerialPacket(pump=pump, cmd=mode)
        self.getLog().log(1, "Use Pump {} in {} MODE".format(pump, mode))
        return self.send(senp) and self.receiveOK()

    def getPumpHeadType(self, pump):
        """Returns a tuple with information on the pump head: (#channels, #rollers)

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: tuple (#channels, #rollers)
        :rtype: tuple, None
        """
        result = None
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd=")")
        if self.send(senp):
            recp = self.receive(5, "s")
            if recp:
                # parse result
                result = (int(recp.getDataFieldValue(0)[:1]), int(recp.getDataFieldValue(0)[1:]))
        return result

    def setPumpHeadType(self, pump, channels=4, rollers=8):
        """Set the used pump head type; which is a combination of channels and rollers

        WARNING: NOT WORKING YET

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param channels: Number of available channels. <0,9>
        :type channels: int
        :param rollers: Number of installed installed. <0,99>
        :type rollers: int
        :return: True on success otherwise False
        :rtype: bool
        """
        # FIXME: Pump responds with # (failure)
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd=")", data_length=5)
        type = "0{:1d}{:02d}".format(channels, rollers)
        senp.addDataField("4s", type)
        print(senp.getByteArray())
        return self.send(senp) and self.receiveOK()

    def getPumpSoftwareVersion(self, pump):
        """Returns the installed software version

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: Software version
        :rtype: int, None
        """
        result = None
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="(")
        if self.send(senp):
            recp = self.receive(6, "s")
            if recp:
                result = int(recp.getDataFieldValue(0))
        return result

    def getTubeID(self, pump):
        """Get the inner diameter of the used tubing, as known by the pump

        This setting is used to determine the volume that is pumped.

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: Inner Diameter of the tubing
        :rtype: float
        """
        result = None
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="+")
        if self.send(senp):
            recp = self.receiveString()
            if recp:
                # parse result into a float, allow for numbers with 2 decimals
                result = float(re.findall("[0-9]+.[0-9]{2}", recp.getDataFieldValue(0))[0])
        return result

    def setTubeID(self, pump, diameter):
        """Stores the Inner Diameter of the tubing placed in the channel

        This setting is used to determine the volume that is pumped.

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param diameter: The used Inner Diameter of the tubing in mm (!!)
        :type diameter: float
        :return: True on success, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="+", data_length=6)
        diameter = "{:04.0f}".format(diameter * 100)
        senp.addDataField("4s", diameter)
        self.getLog().debug("Use Pump {} with {} mm ID".format(pump, diameter))
        return self.send(senp) and self.receiveOK()

    def setFlowMode(self, pump, flow_mode=True):
        """Sets the rate mode of the pump when not in RPM or Flow Rate Mode

        Pump can be set to RPM Mode or Flow rate mode (ml/min)

        :param pump: ID of the pump <1,8>
        :param flow_mode: Whether to set the pump to Flow (=True) or RPM (=False) mode
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="xf", data_length=6)
        mode = "{:06.0f}".format(1 if flow_mode else 0)
        senp.addDataField("6s", mode)
        result = self.send(senp) and self.receiveOK()
        self.getLog().debug("Pump {} {} to run in {} Mode".format(
            pump, "ACCEPTED" if result else "REJECTED", "flow" if flow_mode else "RPM"
        ))
        return result

    def getSpeedRPM(self, pump):
        """Get the current speed setting in RPM

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: The RPM or None on failure
        :rtype: float, None
        """
        result = None
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="S")
        if self.send(senp):
            recp = self.receiveString()
            if recp:
                result = float(recp.getDataFieldValue(0))
        return result

    def setSpeedRPM(self, pump, rpm, rate_mode=True):
        """Set the speed at which the pump will run

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param rpm: RPM to run at <0.00,100.00>
        :type rpm: float
        :return: True on success, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        if 0 > rpm > 100:
            raise SerialException(
                SerialException.PROTOCOL_ERROR,
                "RPM is out of range <0,100>: {}".format(rpm)
            )
        cmd = "S" # if rate_mode else "xf"
        senp = regloSerialPacket(pump=pump, cmd=cmd, data_length=6)
        senp.addDataField("6s", ''.join('{:06.0f}'.format(rpm * 100)))
        result = self.send(senp) and self.receiveOK()
        self.getLog().debug("Pump {} {} to run at {} RPM".format(
            pump, "ACCEPTED" if result else "REJECTED", rpm)
        )
        return result

    def getFlowRate(self, pump):
        """Get the current speed setting in ml/min

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: The flow rate in ml/min or None on failure
        :rtype: float, None
        """
        result = None
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="f")
        if self.send(senp):
            recp = self.receiveString()
            if recp:
                result = float(recp.getDataFieldValue(0)) / 1000
        return result

    def setFlowRate(self, pump, rate):
        """Set the current speed setting in ml/min

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param rate: flow rate to set
        :type rate: float
        :return: Flow rate that was set
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="f", data_length=6)
        F = "{:06.3E}".format(rate).split("E")
        F = "{:4.0f}{:02.0f}".format(float(F[0]) * 1000, float(F[1]))
        senp.addDataField("6s", F)
        result = self.send(senp) and self.receiveString()
        self.getLog().debug("Pump {} {} to run at {} ml/min".format(
            pump, "ACCEPTED" if result else "REJECTED", rate)
        )
        return result

    def setDispenseTime(self, pump, time):
        """Set how long the pump will work in seconds

        :param pump: ID of the pump to set the mode of <0,4>
        :type pump: int
        :param time: In seconds.
        :type time: float
        :return:
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        cmd = "xT"
        T = "{:08.0f}".format(float(time)*10)
        senp = regloSerialPacket(pump=pump, cmd=cmd, data_length=8)
        senp.addDataField("8s", T)
        result = self.send(senp) and self.receiveOK()
        self.getLog().debug("Pump {} {} to run for {:6.1f} sec".format(
            pump, "ACCEPTED" if result else "REJECTED", time)
        )
        return result

    def setDispenseVolume(self, pump, volume):
        """Let the pump, dispense a given amount and then stop

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :param volume: amount to dispense in ml
        :type volume: float
        :return: Dispense volume that was set
        :rtype: float, None
        """
        result = None
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="v", data_length=6)
        V = "{:06.3E}".format(volume).split("E")
        V = "{:4.0f}{:02.0f}".format(float(V[0]) * 1000, float(V[1]))
        senp.addDataField("6s", V)
        result = self.send(senp) and self.receiveString()
        self.getLog().debug("Pump {} {} to dispense {:6.3f} ml".format(
            pump, "ACCEPTED" if result else "REJECTED", volume)
        )
        return result

    def getTotalVolumePumped(self, pump):
        """Get the total volume pumped since last time

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: Tuple with amount and unit ( amount, unit ). for example: (100, "ml") or None when failed
        :rtype: tuple, None
        """
        result = None
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd=":")
        if self.send(senp):
            recp = self.receiveString()
            result = (float(str(recp.getDataFieldValue(0)).split(" ")[0]),
                      str(recp.getDataFieldValue(0)).split(" ")[1])
        return result

    def resetTotalVolumePumped(self, pump):
        """Resets the total volume pumped to 0

        :param pump: ID of the pump to set the mode of <1,8>
        :type pump: int
        :return: True on success, False otherwise
        :rtype: bool
        """
        self.testRequirements(testPump=pump)
        senp = regloSerialPacket(pump=pump, cmd="W")
        return self.send(senp) and self.receiveOK()
