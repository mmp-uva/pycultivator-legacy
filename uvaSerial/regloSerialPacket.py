# coding=utf-8
"""
Module that implements the parsing of byte arrays received from the Ismatic Reglo pump into Objects
"""

from uvaSerialPacket import SerialPacket

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import struct


class regloSerialPacket(SerialPacket):
    """
    Responsible for parsing and creating serial packets to send an receive to/from a REGLO pump
    The corresponding ByteArray is created on request when the properties of the object were changed since previous
    request. The packet length is not calculated automatically.
    Packets are either data + a footer ( 1310 ) or a 1 char response ( */# or +/-)
    :param pump: Sequence number of this packet
    :type pump: int
    :param cmd: Command code of this packet
    :type cmd: int
    :param data_length: data length of this packet (EXcluding cmd code size). Default=0
    :type data_length: int
    """

    # Constants
    FORMAT_HEADER = ""
    HEADER_LENGTH = 0
    FORMAT_FOOTER = "2s"

    def __init__(self, pump=0, cmd=0, data_length=1):
        """

        :param pump:
        :param cmd: str or int
        :param data_length:
        """
        # call super with
        # without header
        # with \r as footer
        # seq_nr = 0
        # payload = 0
        # overhead = 0
        # no crc
        super(regloSerialPacket, self).__init__("", b"\r\n", 0, cmd, data_length, 0, False)
        # set pump id
        self._pump = pump

    @classmethod
    def createCommandPacket(cls, cmd):
        """
        create a regloSerialPacket2 with a given cmd and seqNr.
        :type cmd: int
        :param cmd: Command code of this packet
        :rtype: regloSerialPacket2
        :return: regloSerialPacket2 object with command code and sequence, without data
        """
        return cls(cmd=cmd)

    @classmethod
    def parseByteArray(cls, array, f="", check_crc=False):
        """
        Parse a reglo packet bytearray into a regloSerialPacket object
        :param array: ByteArray - Packet in the form of bytearray
        :type array: bytearray
        :param f: A Python struct format string , which specifies how to parse the data. Only specify the type,
        not the length!
        :type f: str
        :param check_crc: Whether to check the crc of this packet (Default: False)
        :type check_crc: bool
        :return: new instance with data of array or None on failure
        :rtype: regloSerialPacket
        """
        p = None
        array_length = len(array)
        if (array_length == 1):
            # if the response has length one -> this is a "command" response (*/# or +/-)
            p = cls.createCommandPacket(struct.unpack_from("c", array[:1])[0])
        elif (array_length > 1):
            # if reponse has a larger length -> we can parse it
            # reglo will send only 1 format at the time!
            fType = f[-1]  # get type
            fSize = array_length - 2  # data size = packet - len(\r\l)
            f = "{}{}".format(fSize, fType)
            try:
                if fType == cls.FORMAT_CHAR:
                    # unpack will return a list of chars -> join it
                    data = ''.join(struct.unpack_from(f, array[:fSize]))
                elif fType == cls.FORMAT_STRING:
                    # struct will automatically join the chars to one string
                    data = struct.unpack_from(f, array[:fSize])[0]
                else:
                    # ATTENTION: reglo send the number as a string
                    # we parse it as a string and then parse it into a number
                    f = "{}s".format(fSize)  # use string formatting instead
                    data = struct.unpack_from(f, array[:fSize])[0]
                    # parse into number: support INT and FLOAT
                    if fType == cls.FORMAT_FLOAT:
                        data = float(data)
                    elif fType == cls.FORMAT_INT:
                        data = int(data)
                p = cls(data_length=array_length)
                p.addDataField(f, data)
            except Exception:
                pass  # ignore -> we will skip this datafield
        return p

    def createByteArray(self):
        """
        Create a bytearray representation of this object.
        :rtype: bytearray
        :return: Bytearray representation of this object.
        """
        p = bytearray()
        p.extend(struct.pack(self.FORMAT_CHAR, str(self._pump)))  # set destination
        p.extend(struct.pack("{}s".format(len(self._cmd)), self._cmd))  # set cmd
        # then load data field
        for f in self._dataFields:
            # strings should be parsed as 1, combinations of bytes as seperate
            if len(f[0]) > 1 and f[0][-1] != self.FORMAT_STRING:
                p.extend(struct.pack(f[0], *f[1]))
            else:
                p.extend(struct.pack(f[0], f[1]))
        # add footer if we have a footer
        if self._footer: p.extend(struct.pack(self.FORMAT_FOOTER, self._footer))
        self._bytearray = p
        self._changed = False
        return p

    def getPumpId(self):
        """
        Get the current selected pump ID
        :return: Current ID of the pump. Domain=<1,8>
        :rtype: int
        """
        return self._pump

    def setPumpId(self, pump):
        """
        Set the pump ID to indicate the destination of this packet
        :param pump: ID of the pump to send this packet to. Domain=<1,8>
        :type pump: int
        :rtype: int
        """
        # max 8
        if 1 > pump > 8: raise ValueError("Pump ID is out of range <1,8>. Given ID = {}".format(pump))
        self._pump = pump
        self._changed = True
        return pump

    def setDataLength(self, length):
        """
        Set the length of the data part of the packet excluding the command code bytes:
        1D<data>13 = len(data)
        :param length: Length in bytes of the data part, excluding command code bytes
        :type length: int
        :rtype: int
        """
        self._payloadLength = length
        self._changed = True
        return length

    def getDataLength(self):
        """
        Get the length of the data part of the packet excluding the length of the command code bytes
        :rtype: int
        :return: Data length of the packet excluding the command code bytes. Domain=<0,>
        """
        return self._payloadLength
