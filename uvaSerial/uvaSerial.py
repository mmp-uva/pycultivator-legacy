# coding=utf-8
"""
Basic module implementing the pySerial module
"""

import serial, termios, os, logging
from time import sleep
from uvaSerialPacket import SerialPacket
import uvaSerialException
from pycultivator_legacy.core import BaseObject

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Serial(BaseObject):
    """
    Class implementing the low-level communication with the serial port through pySerial
    """

    OS_UNKNOWN = 0
    OS_WINDOWS = 1
    OS_LINUX = 2

    def __init__(self):
        super(Serial, self).__init__()
        # initialize variables with default values
        if os.name == "posix":
            self._os = self.OS_LINUX
            self.getLog().info("posix detected.")
        elif os.name == "nt":
            self._os = self.OS_WINDOWS
            self.getLog().info("windows detected.")
        self._serial = serial.Serial()
        self._port = None
        self._isOpen = False
        self._settings = {}
        self._rate = 115200
        self._timeout = 5
        self._writeTimeout = None
        self._readTimeout = 100  # 100 * 0,01 = 1 second timeout
        self._byteSize = serial.EIGHTBITS
        self._parity = serial.PARITY_NONE
        self._stopBits = serial.STOPBITS_ONE
        self._rtscts = False  # do not use!
        self._dsrdtr = False
        self._xonxoff = False
        pass

    def configure(self, port=None, rate=None, byteSize=None, parity=None, stopBits=None,
                  timeout=None, writeTimeout=None, readTimeout=None, rtscts=None, dsrdtr=None, xonxoff=None, **kwargs):
        """Configure the port based on supplied arguments or default values

        :param port: Port to use (/dev/ttyXX on unix systems, COMX on windows)
        :type port: str
        :param rate: Baud rate of the link
        :type rate: int
        :param byteSize: number of bits in a byte
        :type byteSize: int
        :param parity: How to calculate the parity bit
        :type parity: str
        :param stopBits: number of stop bits
        :type stopBits: int
        :param timeout: set serial link read timeout
        :type timeout: int
        :param writeTimeout: set serial link write timeout
        :type writeTimeout: int
        :param readTimeout: number of times to try to read data
        :type readTimeout: int
        :param rtscts: enable hardware flow control (RTS/CTS)
        :type rtscts: bool
        :param dsrdtr: enable hardware flow control (DSR/DTR)
        :type dsrdtr: bool
        :param xonxoff: enable software flow control
        :type xonxoff: bool
        :return: Reference to this instance
        :rtype: psiSerial2
        """
        wasOpen = self.close() if self._isOpen else False
        self._settings = kwargs
        self._port = (self._port if port is None else port)
        self._serial.port = self._port
        self._rate = (self._rate if rate is None else rate)
        self._serial.baudrate = self._rate
        self._timeout = (self._timeout if timeout is None else timeout)
        self._serial.timeout = self._timeout
        self._writeTimeout = (self._writeTimeout if writeTimeout is None else writeTimeout)
        self._serial.writeTimeout = self._writeTimeout
        self._readTimeout = (self._readTimeout if readTimeout is None else readTimeout)
        self._byteSize = (self._byteSize if byteSize is None else byteSize)
        self._serial.bytesize = self._byteSize
        self._parity = (self._parity if parity is None else parity)
        self._serial.parity = self._parity
        self._stopBits = (self._stopBits if stopBits is None else stopBits)
        self._serial.stopbits = self._stopBits
        self._rtscts = (self._rtscts if rtscts is None else rtscts)
        self._serial.rtscts = self._rtscts
        self._dsrdtr = (self._dsrdtr if dsrdtr is None else dsrdtr)
        self._serial.dsrdtr = self._dsrdtr
        self._xonxoff = (self._xonxoff if xonxoff is None else xonxoff)
        self._serial.xonxoff = self._xonxoff
        if wasOpen: self.open()
        return self

    def open(self):
        """Open the serial port and report success

        :return: True on success, False on failure
        :rtype: bool
        """
        if self._isOpen:
            self.close()
        try:
            self.getLog().info("Connect to {} (rate={},parity={},stopBits={})".format(
                self.getPort(), self._rate, self._parity, self._stopBits
            ))
            self._serial.open()
            self._isOpen = True
        except OSError as os:
            self.getLog().critical("OSError: {}".format(os))
        except serial.SerialException as se:
            self.getLog().critical("Unable to open port: {}".format(se))
        except ValueError as ve:
            self.getLog().critical("Unable to open port with these options {}".format(ve))
        return self._isOpen

    def close(self):
        """Close the serial port and report success.

        :return: True on success, False on failure
        :rtype: bool
        """
        try:
            if self._serial.isOpen():
                self._serial.close()
                self._isOpen = False
        except serial.SerialException as se:
            self.getLog().warn("Unable to close port: {}".format(se))
        return not self._isOpen  # return True if the port is closed

    def reopen(self, pause=0.5):
        """Reopen a connection by first closing and then opening it again"""
        self.close()
        sleep(pause)
        return self.open()

    def write(self, data, attempt=1):
        """Write data to the serial port. Return the number of bytes written

        :param data: Data to write
        :type data: bytearray
        :return: number of bytes written
        :rtype: int
        """
        length = 0
        failure = False
        try:
            # remove anything waiting for sending or receiving
            self.flush()
            if self._isOpen:
                length = self._serial.write(data)
                self.logData(data, "Message written:")
        except termios.error as te:
            self.getLog().warning("Terminal error when writing to port: {}:\n{}".format(
                self.getPort(), te
            ))
            failure = True
        except serial.SerialException as se:
            self.getLog().warning("In/Output Error when writing to port: {}:\n{}".format(
                self.getPort(), se
            ))
            failure = True
        except IOError as ioe:
            self.getLog().warning("In/Output Error when writing to port: {}:\n{}".format(
                self.getPort(), ioe
            ))
            failure = True
        # retry on failure
        if failure and attempt > 0:
            self.getLog().warning("Failed to write to {}, reopen connection".format(self.getPort()))
            result = self.reopen(1)
            self.getLog().info("Reconnect to {}: {}".format(self.getPort(), result))
            self.getLog().info("Retry write to {}, {} attempts left".format(
                self.getPort(), attempt - 1)
            )
            length = self.write(data, attempt=attempt - 1)
        return length

    def getWaiting(self):
        """Return number of bytes waiting to be read"""
        length = 0
        try:
            length = self._serial.in_waiting
        except termios.error:
            self.getLog().warning("Failed to count bytes waiting (termios.Error)")
        except serial.SerialException:
            self.getLog().warning("Failed to count bytes waiting (SerialException)")
        except IOError:
            self.getLog().warning("Failed to count bytes waiting (IOError)")
        return length

    def read(self, length, attempt=1, log=True, clean=True):
        """Blocking-read until some bytes are received, with a maximum size of `length`.

        :param length: maximum number of bytes to read
        :type length: int
        :return: bytes read or None if nothing was received
        :rtype: bytearray
        """
        data = bytearray()
        failure = False
        try:
            data = self._serial.read(length)
        except termios.error as te:
            self.getLog().warning("Terminal error when reading from port: {}:\n{}".format(
                self.getPort(), te
            ))
            failure = True
        except serial.SerialException as se:
            self.getLog().warning("In/Output Error when writing to port: {}:\n{}".format(
                self.getPort(), se
            ))
            failure = True
        except IOError as ioe:
            self.getLog().warning("In/Output Error when reading from port: {}:\n{}".format(
                self.getPort(), ioe
            ))
            failure = True
        # retry on failure
        if failure and attempt > 0:
            self.getLog().warning("Failed to read from {}, reopen connection".format(self.getPort()))
            result = self.reopen(1)
            self.getLog().info("Reconnect to {}: {}".format(self.getPort(), result))
            self.getLog().info("Retry to read from {}, {} attempts left".format(
                self.getPort(), attempt-1)
            )
            data = self.read(length, attempt=attempt - 1, log=False, clean=False)
        if log:
            self.logRead(data)
        if clean:
            self.flush()
        return data

    def read_after(self, signal, length, attempts=None, log=True, clean=True):
        """Discards all characters until a certain signal and then reads a number of characters"""
        data = bytearray()
        if attempts is None:
            attempts = self._readTimeout
        if not self.isConnected():
            raise SerialException(None, "Not connected")
        has_signal, attempt, no_data = False, 0, 0
        while not has_signal and attempt < attempts and no_data < 50:
            sleep(0.01)
            # receive data if any
            chars = bytearray()
            # get number of bytes available
            n = self.getWaiting()
            if n > 0:
                no_data = 0  # reset when we receive data
                chars = self.read(n, log=False, clean=False)
            else:
                no_data += 1  # increase no data timer
            for c in chars:
                has_signal = has_signal or c == signal
                if has_signal:
                    data.append(c)
            if has_signal:
                break
            # end of for
            attempt += 1
        # end of while
        # now read bytes still waiting
        if has_signal and (length - len(data)) > 0:
            data.extend(
                self.read_length(length - len(data), attempts=attempts, log=False, clean=False)
            )
        if log:
            self.logRead(data, attempt, attempts=attempts, no_data=no_data)
        if clean:
            self.flush()
        return data

    def read_length(self, length, attempts=None, log=True, clean=True):
        """Reads until a certain number of characters have been received"""
        data = bytearray()
        if attempts is None:
            attempts = self._readTimeout
        if not self.isConnected():
            raise SerialException(None, "Not connected")
        n, attempt, no_data = 0, 0, 0
        while n < length and attempt < attempts and no_data < 50:
            sleep(0.01)
            if n == self.getWaiting():
                no_data += 1  # increase no data timer
            else:
                no_data = 0  # reset when we receive data
            n = self.getWaiting()
            attempt += 1
        if n > 0:
            data = self.read(n if n <= length else length, log=False, clean=False)
        if log:
            self.logRead(data, attempt, attempts=attempts, no_data=no_data)
        if clean:
            self.flush()
        return data

    def read_until(self, signal, attempts=None, log=True, clean=True):
        """Reads until a signal is received"""
        data = bytearray()
        if attempts is None:
            attempts = self._readTimeout
        if not self.isConnected():
            raise SerialException(None, "Not connected")
        has_signal, attempt, no_data = False, 0, 0
        while not has_signal and attempt < attempts and no_data < 50:
            sleep(0.01)
            # receive data if any
            chars = bytearray()
            n = self.getWaiting()
            if n > 0:
                no_data = 0  # reset when we receive data
                chars = self.read(n, log=False, clean=False)
            else:
                no_data += 1  # increase no data timer
            # parse chars
            for c in chars:
                if c != signal:
                    data.extend(c)
                else:
                    has_signal = True
                    break
            attempt += 1
        if log:
            self.logRead(data, attempt, attempts=attempts, no_data=no_data)
        if clean:
            self.flush()
        return data

    def read_all(self, min_length=-1, max_no_data=7, min_no_data=0, log=True, clean=True):
        """Reads until no data is received for n cycles"""
        data = bytearray()
        if not self.isConnected():
            raise SerialException(None, "Not connected")
        no_data = 0
        do_read = True
        while do_read:
            n = self.getWaiting()
            if n > 0:
                chars = self.read(n, log=False, clean=False)
                data.extend(chars)
                no_data = 0
            else:
                no_data += 1
                sleep(0.01)
            if len(data) > min_length >= 0 and no_data >= min_no_data:
                do_read = False
                self.getLog().debug("Reached minimum length: {} > {}".format(
                    len(data), min_length)
                )
            if no_data > max_no_data:
                do_read = False
                self.getLog().debug("Reached no_data timeout")
        if log:
            self.logRead(data, no_data=0, no_data_max=1)
        if clean:
            self.flush()
        return data

    def flush(self):
        self.flushInput()
        self.flushOutput()

    def flushInput(self):
        try:
            self._serial.reset_input_buffer()
        except termios.error:
            self.getLog().warning("Failed to flush input (termios.error)")
        except serial.SerialException:
            self.getLog().warning("Failed to flush input (SerialException)")
        except IOError:
            self.getLog().warning("Failed to flush input (IOError)")

    def flushOutput(self):
        try:
            self._serial.reset_output_buffer()
        except termios.error:
            self.getLog().warning("Failed to flush input (termios.error)")
        except serial.SerialException:
            self.getLog().warning("Failed to flush output (SerialException)")
        except IOError:
            self.getLog().warning("Failed to flush output (IOError)")

    def logRead(self, data, attempt=0, attempts=None, no_data=0, no_data_max=10):
        if attempts is None:
            attempts = self._readTimeout
        if attempt >= attempts:
            self.logData(
                data, "MAX Attempts hit ({} >= {}). Message received:".format(
                    attempt, attempts
                ), level=logging.WARNING
            )
        elif no_data >= no_data_max:
            self.logData(
                data, "Received no data for {} cycles. Message received:".format(
                    no_data
                ), level=logging.WARNING
            )
        elif len(data) > 0:
            self.logData(data, "Message received:")
        else:
            self.getLog().warning("No data received")

    def logData(self, data, msg=None, level=1):
        """Write a log message, describing the bytearray

        :param data: Data received or sent
        :type data: bytearray
        :param msg: Optional message to print
        :type msg: str
        """
        SerialPacket.ByteArrayToLog(self.getLog(), data=data, msg=msg, level=level)

    def isConnected(self):
        """Returns the connection state of this serial object

        :return: True if connected else False
        :rtype: bool
        """
        return self._isOpen and self._serial.isOpen()

    def getPort(self):
        """Returns the port to the serial device

        :return: the port of the serial device
        :rtype: str or None
        """
        return self._port


class SerialException(uvaSerialException.SerialException):
    def __init__(self, code, msg):
        super(SerialException, self).__init__(code, msg)
