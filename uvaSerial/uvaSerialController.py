# coding=utf-8
"""
Basic module layer that facilitates the translation of commands to packets and also keeps track of the protocol
"""

from pycultivator_legacy.core import BaseObject
from uvaSerial import Serial
from uvaSerialPacket import SerialPacket
from uvaSerialException import SerialException

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SerialController(BaseObject):
    """
    ABSTRACT class implementing basic Protocol handling and communication with underlying classes.
    """

    _namespace = "serial.controller"
    _default_settings = BaseObject.mergeDefaultSettings(
        {
            "rate": 9600,
            "port": None
        }, namespace=_namespace
    )

    def __init__(self, settings):
        """
        Constructs a SerialController instance
        """
        super(SerialController, self).__init__(settings=settings)
        # self._port = port
        self._isOpen = False
        self._configured = False
        self._serial = Serial()
        self._test_rates = [9600]
        self._seqNr = 0

    def connect(self):
        """
        Connect the lower-level psiSerial object to the serial port.
        Configure should be called before calling this method, otherwise this will result in a False
        :return: True on success, False on failure
        :rtype: bool
        """
        if self._isOpen:
            self.getLog().info("reconnect...")
        self._isOpen = False
        if self._configured or self.configure():
            self._isOpen = self._serial.open()
            self.getLog().info("Connected to {}".format(self.getSerial().getPort()))
            self.reset()
        return self._isOpen

    def configure(self, **kwargs):
        """ Configure the SerialPort

        :return: True on success or False on failure
        :rtype: bool
        """
        # create settings dictionary
        settings = self.reduceNameSpace(
            self.mergeDictionary(self._settings, kwargs, source_namespace=self.getNameSpace()),
            self.getNameSpace()
        )
        self._configured = False
        if self.retrieveFromDict(settings, "port", namespace="") is None:
            raise SerialControllerException(None, "No port defined")
        # configure with default values
        self._serial.configure(**settings)
        # test speeds, use the first that works
        rates = self.testRates(testAll=False)
        # select highest rate
        if len(rates) >= 1:
            settings["rate"] = sorted(rates)[-1]
            self._serial.configure(**settings)
            self.mergeSettings(settings)
            self._configured = True
        return self._configured

    def isConnected(self):
        return self._isOpen and self._serial.isConnected()

    def testRates(self, speeds=None, testAll=True):
        """
        Test which BAUDrates work with the serial device
        :param speeds: list with speeds to test (even when only one rate is given)
        :type speeds: list
        :param testAll: whether to test all speeds, or stop after one working speed is found
        :type testAll: bool
        :return: list with working rates (even when only one rate is returned)
        :rtype: list
        """
        raise NotImplementedError

    def disconnect(self):
        """
        Disconnect the lower-level psiSerial object from the serial port
        :return: True on success, False on failure
        :rtype: bool
        """
        if self._isOpen:
            # if close returns True -> set isOpen to False
            self._isOpen = not self._serial.close()
        return not self._isOpen

    def reset(self):
        """
        Reset the sequence counter (and more when implemented)
        """
        self._seqNr = 0

    def takeSeqNum(self):
        """
        Get the current sequence number (will increment on call)
        :return: The current sequence number
        :rtype: int
        """
        self._seqNr += 1
        if self._seqNr > 255:
            self._seqNr = 0
        return self._seqNr

    def getSeqNum(self):
        return self._seqNr

    def getSerial(self):
        """
        :rtype: Serial
        """
        return self._serial

    def getPort(self):
        return self.retrieveSetting(name="port", default=self.getDefaultSettings())

    def getTestRates(self):
        """
        :rtype: list
        """
        return self._test_rates

    def setTestRates(self, rates):
        self._test_rates = rates
        return self

    def addTestRate(self, rate, position=None):
        if rate is int and rate not in self._test_rates:
            if position is None:
                self._test_rates.append(rate)
            elif position is int:
                self._test_rates.insert(position, rate)
        return self

    def removeTestRate(self, rate):
        if rate in self._test_rates:
            self._test_rates.remove(rate)
        return self

    def clearTestRates(self):
        self._test_rates = []
        return self

    def getRate(self):
        return self.retrieveSetting(name="rate", default=self.getDefaultSettings())

    def send(self, packet):
        """
        send psiSerialPacket2 packet, don't wait for answer
        :param packet: The packet to send to the serial device
        :type packet: SerialPacket
        :return: True when at least the length of `packet` was written, otherwise False
        :rtype: bool
        """
        length = 0
        if self.isConnected():
            try:
                length = self._serial.write(packet.getByteArray())
            except SerialException as se:
                self.getLog().error(se)
        return length >= packet.getPacketLength()

    def sendWAnswerOk(self, packet):
        """
        Send psiSerialPacket2 packet, and see if the answer is OK (code 100)
        :param packet: The packet to send to the serial device
        :type packet: SerialPacket
        :return: True when the answer was OK (code 100)
        :rtype: bool
        """
        return self.send(packet) and self.receiveOK()

    def receive(self, expLen=0, dataFormat=""):
        """
        Receive a packet, with
        :param expLen: Expected DATA length (excluding command code size)
        :type expLen: int
        :param dataFormat: Struct Format string, specifying how to interpret the data
        :type dataFormat: str
        :return: The received packet or None if nothing was received or on failure
        :rtype: SerialPacket or None
        """
        raise NotImplementedError

    def receiveAndCheck(self, expLen, expCmd, dataFormat=""):
        """
        Try to receive a packet of given length, with a given command code and an optional data format.
        Will check if length and command code are correct.
        :param expLen: Expected DATA length (EXcluding command code size)
        :type expLen: int
        :param expCmd: Expected command code
        :type expCmd: int
        :param dataFormat: struct format string, specifying how to interpret the data
        :type dataFormat: str
        :return: The received packet or None if nothing was received or on failure
        :rtype: SerialPacket, None
        """
        p = self.receive(expLen, dataFormat)
        # next check if we have data at all, correct length and correct cmd code
        return p if p and p.getDataLength() == expLen and p.getCommandCode() == expCmd else None

    def receiveOK(self):
        """
        Try to receive an OK packet
        :return: True on success and False on incorrect response
        :rtype: bool
        """
        return self.receiveAndCheck(0, 100) is not None


class SerialControllerException(SerialException):

    def __init__(self, code, msg):
        super(SerialControllerException, self).__init__(code, msg)
