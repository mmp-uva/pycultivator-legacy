"""
  Error reporting class on all errors that might occur during a psiSerial communication
"""

from uvaCultivator import uvaException

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SerialException(uvaException.UVAException):

    # Constants
    PROTOCOL_ERROR = "P"
    PROTOCOL_ANSWER = 0
    PROTOCOL_NO_ANSWER = PROTOCOL_ERROR + str(PROTOCOL_ANSWER + 1)
    PROTOCOL_INVALID_ANSWER = PROTOCOL_ERROR + str(PROTOCOL_ANSWER + 2)

    SERIAL_ERROR = "S"
    SERIAL_NOT_OPEN = SERIAL_ERROR + str(0)
    SERIAL_NO_DEVICE = SERIAL_ERROR + str(1)
    SERIAL_TIMEOUT = SERIAL_ERROR + str(2)

    MESSAGE_ERROR = "M"
    PACKET_ERROR = MESSAGE_ERROR
    PACKET_LOG_ERROR = PACKET_ERROR + str(1)
    PACKET_CRC_ERROR = PACKET_ERROR + str(2)
    PACKET_INVALID_LENGTH = PACKET_ERROR + str(3)

    def __init__(self, code, msg):
        super(SerialException, self).__init__(msg)
        # string Sxxx = Serial Errors, Pxxx = Protocol Errors, Mxxx = Message Errors
        self._code = code

    def getCode(self):
        return self._code

    def __str__(self):
        code = ""
        if self.getCode() is not None:
            code = ": {}".format(self.getCode())
        return "[{}{}]: {}".format(
            self.getName(), code, self.getMessage()
        )
