# coding=utf-8
"""
Module that implements the basic functionality to parse of bytearrays into objects
"""

from pycultivator_legacy.core import BaseObject

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SerialPacket(BaseObject):
    """
    ABSTRACT class that implements the basic functionality to parse bytearrays
    """

    # Constants
    FORMAT_BYTE = "b"
    FORMAT_UBYTE = "B"
    FORMAT_USHORT = "H"
    FORMAT_UINT = "I"
    FORMAT_INT = "i"
    FORMAT_FLOAT = "f"
    FORMAT_STRING = "s"
    FORMAT_CHAR = "c"
    FORMAT_DICT = {FORMAT_BYTE: 1, FORMAT_UBYTE: 1, FORMAT_USHORT: 2, FORMAT_UINT: 4,
                   FORMAT_FLOAT: 4, FORMAT_STRING: 1, FORMAT_CHAR: 1}

    def __init__(self, header, footer, seq_nr, cmd, payload_length, overhead_length, crc):
        super(SerialPacket, self).__init__()
        self._header = header  # header: fixed sequence of bytes that prefix a packet
        self._footer = footer  # footer: fixed sequence of bytes that suffix a packet
        self._seqNr = seq_nr  # the sequence number of the packet
        self._payloadLength = payload_length  # length of data + cmd
        self._overheadLength = overhead_length  # length of header + seq_nr + crc
        self._cmd = cmd  # the command code of the packet
        self._crc = crc  # the crc byte
        self._bytearray = None  # byte array representation
        self._dataFields = []  # the data contained in the data part of packet
        self._changed = True  # whether the object is no longer in sync with the bytearray

    @staticmethod
    def calculate_crc(array):
        """Calculate the crc of the array

        :return: calculated crc
        :rtype: int
        """
        crc = 0
        for i in range(len(array)):
            crc ^= array[i]
        return crc

    @staticmethod
    def check_crc(array):
        """Checks if the crc reported by the array equals the manually calculated crc

        :param array: ByteArray representation of the packet
        :type array: bytearray
        :return: True if the last byte of the packet equals the calculated CrC of the rest of the packet.
        :rtype: bool
        """
        packetCRC = array[-1]  # select last byte
        calcCRC = SerialPacket.calculate_crc(array[0:-1])  # calculate CrC from array without last byte
        return packetCRC == calcCRC  # check if calculated CrC equals the CrC of the packet

    @staticmethod
    def ByteArrayToHex(data):
        """Converts each byte in the given bytearray to a 2 digit hex value in a nice string format.

        :param data: data to convert
        :type data: bytearray
        :return: Hex translation of bytes
        :rtype: str
        """
        return " ".join("%02x " % i for i in data)

    @staticmethod
    def ByteArrayToInt(data):
        """Converts each byte of the given bytearry to a 2 digit int value in a nice string format.

        :param data: data to convert
        :type data: bytearray
        :return: Int translation of bytes
        :rtype: str
        """
        return " ".join("%02i " % i for i in data)

    @staticmethod
    def ByteArrayToString(data):
        """Converts each byte to readable character or int, and formats it in a nice string.

        :param data: data to convert
        :type data: bytearray
        :return: Character translation of bytes
        :rtype: str
                """
        s = ""
        for b in data:
            if int(b) >= 30:
                s += "{: <2c} ".format(b)  # OK, translate!
            else:
                s += "{:02d} ".format(b)  # probably an integer, keep it
        return s.strip(" ")

    @staticmethod
    def ByteArrayToLog(log, data, msg=None, level=1):
        """Print a log message with the bytearray when debugging is on

        :param log: Logger to write to
        :type log: logging.Logger
        :param data: Data received or sent
        :type data: bytearray
        :param msg: Optional message to print
        :type msg: str
        :param level: The level of the log message
        :type level: int
        """
        # check if we should invest energy in logging
        if log.getEffectiveLevel() <= level:
            # make sure its a bytearray
            msg = bytearray(msg)
            msg = "{}\n HEX:\t{}".format(msg, SerialPacket.ByteArrayToHex(data))
            msg = "{}\n INT:\t{}".format(msg, SerialPacket.ByteArrayToInt(data))
            msg = "{}\n STR:\t{}".format(msg, SerialPacket.ByteArrayToString(data))
            log.log(level, msg)  # write to log

    @classmethod
    def parseByteArray(cls, array, f="", check_crc=False):
        """ABSTRACT Factory method, which will

        :param array:
        :type array:
        :param f:
        :type f:
        :param check_crc:
        :type check_crc:
        :return:
        :rtype:
        """
        return None

    def addDataField(self, f, value):
        """Add a new data field to the packet

        :param f: String containing struct formatting options
        :type f: str
        :param value: Value to store to data field
        :type value: object
        :return: self
        :rtype: psiSerialPacket2
        """
        self._dataFields.append((f, value))  # add field
        self._changed = True
        return self

    def getDataField(self, index):
        """Return the data field with a given index (might be -x, to select x'th element counting from last)

        :param index: Index of data field to be returned. Domain=<-,len(data)>
        :type index: int
        :return: Data field tuple = ( format string, data ) or ( "" , "" )
        :rtype: tuple
        """
        return self._dataFields[index] if index < len(self._dataFields) else ("", "")

    def getDataFieldValue(self, index):
        """Return the value of the data field with a given index.

        :param index: Index of data field, which value to be returned <-len(data),len(data)>
        :type index: int
        :return: Value of the object stored in the data field
        :rtype: int, str, None
        """
        return self._dataFields[index][1] if index < len(self._dataFields) else None

    def getDataFieldFormat(self, index):
        """Return the struct format string of the data field with a given index.

        :param index: Index of data field, which format string to be returned <-len(data),len(data)>
        :type index: int
        :return: Struct format string of the object stored in the data field
        :rtype: str, None
        """
        return self._dataFields[index][0] if index < len(self._dataFields) else None

    # Removes a field with a given index and returns it
    def removeDataField(self, index):
        """Removes a field from the list

        :param index: Index of field to remove
        :type index: int
        :return: Field that was removed
        :rtype: int
        """
        f = self._dataFields.pop(index)
        self._changed = True
        return f

    def clearDataFields(self):
        """Clear all fields of this packet"""
        self._dataFields = []
        self._changed = True

    def setPacketHeader(self, header):
        """Set the packet header string

        :param header: header string
        :type header: str,bytearray
        :return: header
        :rtype: str,bytearray
        """
        self._header = header
        self._changed = True
        return self._header

    def setSequenceNumber(self, seqNr):
        self._seqNr = seqNr
        self._changed = True
        return self._seqNr

    def setDataLength(self, length):
        """Set the length of the data part of the packet excluding the command code bytes

        :param length: Length in bytes of the data part, excluding command code bytes
        :type length: int
        :rtype: int
        """
        self._payloadLength = length - self._overheadLength
        self._changed = True
        return length

    def setPacketLength(self, length):
        """Set the TOTAL length of the packet including the command code, header, seqNr, length and CRC bytes

        :type length: int
        :param length: TOTAL length of the packet. Domain=<11,>
        """
        self._payloadLength = length - self._overheadLength  # only store cmd code + data length <2,>
        self._changed = True
        return length

    def setCommandCode(self, cmd):
        self._cmd = cmd
        self._changed = True
        return cmd

    def getCommandCode(self):
        return self._cmd

    def getSequenceNumber(self):
        return self._seqNr

    def getDataLength(self):
        """Get the length of the data part of the packet excluding the length of the command code bytes

        :rtype: int
        :return: Data length of the packet excluding the command code bytes. Domain=<0,>
        """
        return self._payloadLength

    def getPacketLength(self):
        """Get the TOTAL length of the packet including the command code, header, seqNr, length and CRC bytes

        :rtype: int
        :return: Total packet length. Domain=<11,>
        """
        return self._payloadLength + self._overheadLength

    def getByteArray(self):
        """Get the bytearray representation of this object.
            This will regenerate the bytearray if changes to the object were detected.

        :return: Bytearray representation
        :rtype: bytearray
        """
        return self.createByteArray() if self._changed else self._bytearray

    def createByteArray(self):
        """Parse this object into a bytearray

        :return:
        :rtype:
        """
        raise NotImplementedError

    def setCrcState(self, state):
        """Set the CRC Check State

        :param state: True for CRC check, or False to disable it
        :type state: bool
        :return: new CRC state
        :rtype: bool
        """
        self._crc = state
        return self._crc

    def getCrcState(self):
        """Returns the current CRC Check State

        :return: True when CRC is enabled, False when disabled
        :rtype: bool
        """
        return self._crc

    # implement hex conversion
    def __hex__(self):
        """Converts each byte in the bytearray representation to a 2 digit hex value in a nice string format.

        :return: Hex translation of bytes
        :rtype: str
        """
        return self.ByteArrayToString(self._bytearray)

    # implement integer conversion
    def __int__(self):
        """Converts each byte of the bytearry representation to a 2 digit int value in a nice string format.

        :return: Int translation of bytes
        :rtype: str
        """
        return self.ByteArrayToInt(self._bytearray)

    # implement string conversion
    def __str__(self):
        """Converts each byte to readable character or int, and formats it in a nice string.

        :return: Character translation of bytes
        :rtype: str
        """
        return self.ByteArrayToString(self._bytearray)
