#!/usr/bin/env python
# coding=utf-8
"""
Analyse the background light contamination. For each given channel it will measure the contamination at different
light intensities of the neighbours
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import logging, sys, datetime, traceback
from os import path as OP
cwd = OP.dirname(OP.realpath(__file__))
sys.path.insert(1, OP.realpath("{}/../".format(cwd)))
from time import sleep
from pycultivator_legacy.core import log

# minimum level of messages to log
LOG_LEVEL = logging.INFO
# Channels to subject to measurements
CHANNELS = [0, 1, 2, 3, 4, 5, 6, 7] #[1] #[1,3,5,7]  #[0, 1, 2, 3, 4, 5, 6, 7]
# Store light intensity
LIGHT_INTENSITY = 25  # in uE/m2/s
# Store OD LED
OD_LED = 1  # 0 == 680 1 == 720
# Store OD repeats
OD_REPEATS = 3
# Store OD LED
OD_LED = 1 # 0 == 680 1 == 720
# Set time to wait after turning gas pump off
GAS_PAUSE = 10
TEST_INTENSITIES = [0,5,10,25,50,100,150,200,250,300]

if __name__ == "__main__":
	t_start = datetime.datetime.now()
	device = "/dev/ttyUSB0"
	project_id = "test_report"
	use_fake = False
	if len(sys.argv) >= 2:
		project_id = sys.argv[1]
	if len(sys.argv) >= 3:
		use_fake = sys.argv[2] == "1"

	logfile = "{}/../log/{}_{}.log".format(cwd, datetime.datetime.now().strftime('%Y%m%d'), project_id)
	fp = "{}/../data/{}_{}.csv".format(cwd, datetime.datetime.now().strftime('%Y%m%d'), project_id)
	conf = OP.realpath("{}/../conf/{}.xml".format(cwd, project_id))

	logger = log.getLogger()
	logger.addFileHandler(logfile, level=LOG_LEVEL)
	logger.addStreamHandler(level=LOG_LEVEL)
	logger.setLevel(LOG_LEVEL)
	logger.info(" === LOG START AT: {} ===".format(t_start.strftime('%Y-%m-%d %H:%M:%S')))

	# load modules
    from pycultivator_legacy.config import XMLConfig

    if use_fake:
		from uvaSerial.psiFakeSerialController import psiFakeSerialController as pSC
	else:
		from uvaSerial.psiSerialController import psiSerialController as pSC
	from uvaCultivator import uvaCultivatorCSVHandler as uCCH

	try:
		conf = xmlCultivatorConfig.loadConfig(conf, project_id)
		serial = conf.loadController(pSC, True)
		if serial.connect():
			logger.info("Connected to {}".format(device))
			logger.info("Will write measurements to {}".format(fp))
			# load configuration file
			cult = conf.loadCultivator(serial)
			measurements = []
			if cult.getSerial().setPumpState():
				logger.info("Pump has been switched off, wait 10 seconds")
				sleep(GAS_PAUSE)  # get rid of the bubbles
				for c in CHANNELS:
					# turn light off for this channel
					prev_state = [(cult.getChannel(c).getLightState(), cult.getChannel(c).getLightIntensity())]
					if 0 < c < 7:  # has two neighbours
						prev_state.append([(cult.getChannel(c + 1).getLightState(),
						                  cult.getChannel(c + 1).getLightIntensity()
						                 )])
						prev_state.append([(cult.getChannel(c - 1).getLightState(),
						                  cult.getChannel(c - 1).getLightIntensity()
						                 )])
					elif c == 0:
						prev_state.append([(cult.getChannel(c + 1).getLightState(),
						                  cult.getChannel(c + 1).getLightIntensity()
						                 )])
					elif c == 7:
						prev_state.append([(cult.getChannel(c - 1).getLightState(),
						                  cult.getChannel(c - 1).getLightIntensity()
						                 )])
					if cult.getChannel(c).setLightState(False):
						# turn light on at the neighbours
						for intensity in TEST_INTENSITIES:
							cult.getChannel(c).setLightIntensity(intensity)
							if c > 0:
								# measure left
								cult.getChannel(c - 1).setLightIntensity(intensity)
								cult.getChannel(c - 1).setLightState(True)
								measurements.append(cult.getChannel(c).measureOD(OD_REPEATS, OD_LED))
								# turn light off
								cult.getChannel(c - 1).setLightState(False)
							if c < 7:
								# measure right
								cult.getChannel(c + 1).setLightIntensity(intensity)
								cult.getChannel(c + 1).setLightState(True)
								measurements.append(cult.getChannel(c).measureOD(OD_REPEATS, OD_LED))
								# turn light off
								cult.getChannel(c + 1).setLightState(False)
					# restore
					cult.getChannel(c).setLightState(prev_state[0][0])
					cult.getChannel(c).setLightIntensity(prev_state[0][1])
					if 0 < c < 7:  # has two neighbours
						cult.getChannel(c+1).setLightState(prev_state[0][0])
						cult.getChannel(c+1).setLightIntensity(prev_state[0][1])
						cult.getChannel(c-1).setLightState(prev_state[0][0])
						cult.getChannel(c-1).setLightIntensity(prev_state[0][1])
					elif c == 0:
						cult.getChannel(c + 1).setLightState(prev_state[0][0])
						cult.getChannel(c + 1).setLightIntensity(prev_state[0][1])
					elif c == 7:
						cult.getChannel(c - 1).setLightState(prev_state[0][0])
						cult.getChannel(c - 1).setLightIntensity(prev_state[0][1])
				logger.info("Completed all OD measurements.")
			else:
				serial.getLog().critical("Unable to switch gas pump OFF")
			# turn gas back on
			if not cult.getSerial().setPumpState(True):
				serial.getLog().critical("Unable to switch gas pump ON")
			# restore light
			if not cult.adjustLightsSettings():
				serial.getLog().critical("Unable to restore light")
			# now process measurements
			if len(measurements) >= 1:
				exporter = uCCH.CultivatorPortraitCSVHandler(fp, channels=1, append=False )
				exporter.addODMeasurements(measurements)
		else:
			serial.getLog().critical("Unable to configure or to connect to {}".format(device))
	except Exception as e:
		msg = "Uncatched Error:\n{}".format(traceback.format_exc())
		if logger:
			logger.critical(msg)
		else:
			print msg
	finally:
		if serial is not None and serial.isConnected():
			t_end = datetime.datetime.now()
			logger.info(" === RUN TIME: {} sec.===".format((t_end-t_start).seconds))
			logger.info(" === LOG END AT: {} ===".format(t_end.strftime('%Y-%m-%d %H:%M:%S')))
			serial.disconnect()
