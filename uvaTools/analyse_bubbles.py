#!/usr/bin/env python
# coding=utf-8
"""
Analyse the OD Error distribution.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import logging, sys, traceback
from datetime import datetime as dt
from os import path as OP
cwd = OP.dirname(OP.realpath(__file__))
sys.path.insert(1, OP.realpath("{}/../".format(cwd)))
from time import sleep
from uvaSerial.psiSerialController import psiSerialController as pSC2
#from uvaSerial.psiFakeSerialController import psiFakeSerialController as pFSC
from uvaSerial import uvaSerialLogger as uSL

LOG_LEVEL = logging.INFO        # minimum level of messages to log
CHANNELS = [0, 2, 4, 6] #[1] #[1,3,5,7]  #[0, 1, 2, 3, 4, 5, 6, 7]
# Number of measurements made by the MC and averaged before sending to the computer
OD_REPEATS = 1
# Store OD LED
OD_LEDS = [0, 1] # 0 == 680 1 == 720
GAS_PHASES = [
	{ 'state' : False, 'pause' : 20},
	{ 'state' : True, 'pause' : 10},
	{ 'state' : False, 'pause' : 0}
]
# Set whether it should try to restore the light
LIGHT_RESTORE = False
# Number of (sets of) measurements to be performed by the computer
OD_MEASUREMENTS = 1000

if __name__ == "__main__":
	t_start = dt.now()
	settings = {
		"port" : "/dev/ttyMC0",
		#"port": "/dev/tty.usbserial-FTAK08XU",# mc 0
		#"port" : "/dev/tty.usbserial-FTGZPIMR", # mc 2
		# "port": "/dev/tty.usbserial-FTGZOFXY", # mc 1
		"stopBits" : 1,
		"parity" : "N",
		"byteSize" : 8
	}
	fp = "{}/../data/{}_analyse_od_{}.csv".format(cwd, t_start.strftime('%Y%m%d'), '{}')
	logfile = "{}/../log/{}_analyse_od.log".format(cwd, t_start.strftime('%Y%m%d'))
	# if len(sys.argv) >= 2:
	# 	device = sys.argv[1]
	# if len(sys.argv) >= 3:
	# 	fp = sys.argv[2]        # use a different report file
	# if len(sys.argv) >= 4:
	# 	logfile = sys.argv[3]
	# if len(sys.argv) >= 5:
	# 	OD_MEASUREMENTS = int(sys.argv[4])
	logger = uSL.addUvaLogFileHandler(uSL.connectLogger(), logfile, LOG_LEVEL)
	logger.setLevel(LOG_LEVEL)
	uSL.addUvaLogStreamHandler(logger, LOG_LEVEL)
	print "Connected FileHandler to path {} and logger {}".format(logfile, logger.name)
	conn = None
	total_measurements = len(CHANNELS) * len(OD_LEDS) * OD_MEASUREMENTS
	try:
		logger.info(" === LOG START AT: {} ===".format(t_start.strftime('%Y-%m-%d %H:%M:%S')))
		conn = pSC2(settings=settings)
		conn.configure(**settings)
		""" :type: uvaSerial.psiSerialController.psiSerialController |
		uvaSerial.psiFakeSerialController.psiFakeSerialController """
		if not conn.connect():
			raise Exception("Unable to connect to {}".format(conn.getPort()))
		logger.info("Connected to {}".format(conn.getPort()))
		if not conn.setAllLightStates():
			raise Exception("Unable to turn lights OFF")
		logger.info("Light has been switched off, start measurements")
		# iterate over gas phases
		gas_phase_idx = 0
		for gas_phase in GAS_PHASES:
			gas_phase_idx += 1
			data = {}  # dt : { attributes }
			idx = 0
			desired_state = gas_phase["state"]
			desired_pause = gas_phase["pause"]
			if not conn.setPumpState(desired_state):
				raise Exception("Unable to set gas state")
			if desired_pause > 0:
				logger.info("Pump has been switched off, wait {} seconds".format(desired_pause))
				sleep(desired_pause)
			logger.info("Measure with gas {} and a pause of {} seconds".format(
				"On" if desired_state is True else "Off",
				desired_pause
			))
			for i in range(OD_MEASUREMENTS):
				for c in CHANNELS:
					for od_led in OD_LEDS:
						# Increment current index
						t_now = dt.now()
						reading = conn.getODReading(channel=c, led=od_led, repeats=OD_REPEATS)
						sample = {
							"LED" : 680 if od_led == 0 else 720,
							"channel" : c,
							"OD" : reading[2]
						}
						if t_now in data:
							data[t_now].append(sample)
						else:
							data[t_now] = [sample]
						# Show every 10 percent progress
						if idx % (total_measurements * 0.1) == 0:
							logger.info("Measured {}% (idx = {})".format(
								idx / (total_measurements * 0.01),
								idx
							))
						idx += 1
					# all repeats done
				# all channels done
			logger.info("Completed measurements.")
			# now process measurements
			if len(data.keys()) > 0:
				logger.info("Write measurements to {}".format(fp.format(gas_phase_idx)))
				with open(fp.format(gas_phase_idx), "w") as dest:
					headers = ["channel", "LED", "OD"]
					#dest.write("{};{}\n".format(t_start.strftime("%Y-%m-%d %H:%M:%S"), ";".join(headers)))
					for key in sorted(data.keys()):
						for idx in range(len(data[key])):
							variables = []
							# use time difference
							line = (t_start - key).total_seconds()
							for header in headers:
								if header in data[key][idx].keys():
									line = "{};{}".format(line, data[key][idx][header] )
							dest.write("{}\n".format(line))
						# end of for
					# end of for
				# end of with
			# end of if
		# end of for
		if not conn.setPumpState(True):
			raise Exception("Unable to restore gas flow")
	except Exception as e:
		msg = "Error:\n{}".format(traceback.format_exc())
		if logger:
			logger.critical(msg)
		else:
			print msg
	finally:
		if conn is not None and conn.isConnected():
			t_end = dt.now()
			conn.getLog().info(" === RUN TIME: {} sec.===".format((t_end-t_start).seconds))
			conn.getLog().info(" === LOG END AT: {} ===".format(t_end.strftime('%Y-%m-%d %H:%M:%S')))
			conn.disconnect()
