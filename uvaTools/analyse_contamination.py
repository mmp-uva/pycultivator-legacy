
#!/usr/bin/env python
# coding=utf-8
"""
Application to measure neighbour contamination with the possibility to sync this to an external program.
It will vary the light in the subject vessel and it's neighbours. For each voltage, measure:
- light without neighbour
- light with left neighbour at x voltages
- light with right neighbour at x voltages
- light with both neighbours at x voltages
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import logging, sys, datetime, traceback
from os import path as OP
cwd = OP.dirname(OP.realpath(__file__))
sys.path.insert(1, OP.realpath("{}/../".format(cwd)))
from uvaSerial import uvaSerialLogger as uSL
from uvaCultivator.uvaCultivatorLight import CultivatorLight as cL
from time import sleep
# Set minimum level of messages to write to log
LOG_LEVEL = logging.INFO
MEASURE_CHANNELS = [3] #range(8) #[0,1,2,3,4,5,6,7] #
N_VOLTAGES = range(0, 120, 20)
N_RIGHT_VOLTAGES = N_VOLTAGES
N_LEFT_VOLTAGES = []
N_BOTH_VOLTAGES = []
CYCLE_SECONDS = 5
DT_FORMAT = "%Y-%m-%d %H:%M:%S"
T_FORMAT = "%S.%f"

def sleepTimeLeft(t_cycle_start, message=None):
	t_cycle = CYCLE_SECONDS - (datetime.datetime.now() - t_cycle_start).total_seconds()
	# t_cycle = each period takes X seconds but spent some time in changing the intensities, rest is to sleep 
	if message is not None:
		logger.info("{}, sleep: {} sec. ...".format(message, t_cycle))
	#logger.info("Set lights to {} V ({} %), will wait {} sec.".format(voltage, percentage, t_cycle))
	if t_cycle > 0:
		sleep(t_cycle)
	else:
		logger.error("Help I have trouble to keep the pace, had {} seconds left".format(t_cycle))

if __name__ == "__main__":
	t_start = datetime.datetime.now()
	device = "/dev/ttyUSB0"
	fn = OP.basename(OP.realpath(__file__))
	fn = fn[:fn.rfind(".")]
	project_id = "test_report"
	use_fake = False
	serial = None
	cult = None
	conf = None
	if len(sys.argv) >= 2:
		project_id = sys.argv[1]
	if len(sys.argv) >= 3:
		use_fake = sys.argv[2] == "1"

	logfile = "{}/../log/{}_{}_{}.log".format(cwd, datetime.datetime.now().strftime('%Y%m%d'), project_id, fn, __name__)
	fp = []
	fp.append("{}/../data/{}_{}_{}.csv".format(cwd, datetime.datetime.now().strftime('%Y%m%d'),
	                                                      project_id, fn, __name__))
	conf = OP.realpath("{}/../conf/{}.xml".format(cwd, project_id))

	logger = uSL.connectLogger()
	uSL.addUvaLogFileHandler(logger, logfile, LOG_LEVEL)
	uSL.addUvaLogStreamHandler(logger, LOG_LEVEL)
	logger.setLevel(LOG_LEVEL)
	logger.info(" === LOG START AT: {} ===".format(t_start.strftime(DT_FORMAT)))

	# load modules
    from pycultivator_legacy.config import xmlCultivatorConfig

    if use_fake:
		from uvaSerial.psiFakeSerialController import psiFakeSerialController as pSC
	else:
		from uvaSerial.psiSerialController import psiSerialController as pSC

	try:
		conf = xmlCultivatorConfig.loadConfig(conf, project_id)
		exp_start_date = conf.loadStartDate()
		serial = conf.loadController(pSC, True)
		if serial.connect():
			logger.info("Connected to {}".format(device)) 
			logger.info("Will write measurements to {}".format(fp))
			# load configuration file
			cult = conf.loadCultivator(serial)
			with open(fp[0], "w") as dest:
				# write header: <DATE>;Voltage_n-1;Intensity_n-1;Voltage_n;Intensity_n;Voltage_n+1;Intensity_n+1\n
				dest.write("{};Voltage n-1;Intensity n-1;Voltage_n;Intensity_n;Voltage_n+1;Intensity_n+1\n".format(
					t_start.strftime(DT_FORMAT)
				))
				cult.setLightsState(True)
				for channel in range(8):
					cult.getChannel(channel).setLightIntensity(0)
				v_left = 0
				v_right = 0
				i_left = 0
				i_right = 0
				intensity = 0
				for channel in MEASURE_CHANNELS:
					for voltage in N_VOLTAGES:
						cult.getChannel(channel).setLightIntensity(voltage, cL.LIGHT_INTENSITY_VOLTAGE)
						intensity = cult.getChannel(channel).getLightIntensity()
						logger.info("Changed voltage of current channel to {} V".format(voltage))
						if voltage == 0:
							raw_input('Press <any key> to start ...')
						# vary left neighbour
						if channel > 0 and len(N_LEFT_VOLTAGES) > 0:
							for v_left in N_LEFT_VOLTAGES:
								t_now = datetime.datetime.now()
								cult.getChannel(channel-1).setLightIntensity(v_left, cL.LIGHT_INTENSITY_VOLTAGE)
								i_left = cult.getChannel(channel-1).getLightIntensity()
								dest.write("{};{};{};{};{};{};{}\n".format(datetime.datetime.now().strftime(T_FORMAT),
								                                           v_left, i_left, voltage, intensity, v_right,
								                                           i_right
								))
								sleepTimeLeft(t_now, "Changed left neighbour to {} V, sleep time left...".format(v_left))
							v_left = 0
							cult.getChannel(channel-1).setLightIntensity(v_left, cL.LIGHT_INTENSITY_VOLTAGE)
							i_left = cult.getChannel(channel-1).getLightIntensity()
							# BLANK!
							cult.getChannel(channel).setLightIntensity(0)
							sleepTimeLeft(datetime.datetime.now(), "##BLANK")
							cult.getChannel(channel).setLightIntensity(voltage, cL.LIGHT_INTENSITY_VOLTAGE)
						if channel < 7 and len(N_RIGHT_VOLTAGES) > 0:
							# vary right neighbour
							for v_right in N_RIGHT_VOLTAGES:
								t_now = datetime.datetime.now()
								cult.getChannel(channel+1).setLightIntensity(v_right, cL.LIGHT_INTENSITY_VOLTAGE)
								i_right = cult.getChannel(channel+1).getLightIntensity()
								dest.write("{};{};{};{};{};{};{}\n".format(datetime.datetime.now().strftime(T_FORMAT),
								                                           v_left, i_left, voltage, intensity, v_right,
								                                           i_right
								))
								sleepTimeLeft(t_now, "Changed right neighbour to {} V".format(v_right))
							v_right = 0
							cult.getChannel(channel+1).setLightIntensity(v_right, cL.LIGHT_INTENSITY_VOLTAGE)
							i_right = cult.getChannel(channel+1).getLightIntensity()
							# BLANK
							cult.getChannel(channel).setLightIntensity(0)
							sleepTimeLeft( datetime.datetime.now(), "##BLANK")
							cult.getChannel(channel).setLightIntensity(voltage, cL.LIGHT_INTENSITY_VOLTAGE)
						if 0 < channel < 7 and len(N_BOTH_VOLTAGES) > 0:
							# vary both
							for v_both in N_BOTH_VOLTAGES:
								v_left = v_right = v_both
								t_now = datetime.datetime.now()
								cult.getChannel(channel-1).setLightIntensity(v_both, cL.LIGHT_INTENSITY_VOLTAGE)
								cult.getChannel(channel+1).setLightIntensity(v_both, cL.LIGHT_INTENSITY_VOLTAGE)
								i_left = cult.getChannel(channel-1).getLightIntensity()
								i_right = cult.getChannel(channel+1).getLightIntensity()
								dest.write("{};{};{};{};{};{};{}\n".format(datetime.datetime.now().strftime(T_FORMAT),
								                                           v_left, i_left, voltage, intensity, v_right,
								                                           i_right
								))
								sleepTimeLeft(t_now, "Changed both neighbours to {} V, sleep time left...".format(v_both))
							v_left = v_right = v_both = 0
							cult.getChannel(channel-1).setLightIntensity(v_both, cL.LIGHT_INTENSITY_VOLTAGE)
							cult.getChannel(channel+1).setLightIntensity(v_both, cL.LIGHT_INTENSITY_VOLTAGE)
							i_left = cult.getChannel(channel-1).getLightIntensity()
							i_right = cult.getChannel(channel+1).getLightIntensity()
							# BLANK
							cult.getChannel(channel).setLightIntensity(0)
							sleepTimeLeft(datetime.datetime.now(), "##BLANK")
							cult.getChannel(channel).setLightIntensity(voltage, cL.LIGHT_INTENSITY_VOLTAGE)

				cult.setLightsState(False)
				for channel in range(8):
					cult.getChannel(channel).setLightIntensity(0)
		elif not conf.loadState():
			logger.info("Do not measure: this cultivator is not active.")
		else:
			logger.critical("Unable to configure or to connect to {}".format(device))
	except Exception as e:
		msg = "Error:\n{}".format(traceback.format_exc())
		if logger:
			logger.critical(msg)
		else:
			print msg
	finally:
		t_end = datetime.datetime.now()
		if serial is not None and serial.isConnected():
			serial.disconnect()
		logger.info(" === RUN TIME: {} sec.===".format((t_end-t_start).seconds))
		logger.info(" === LOG END AT: {} ===".format(t_end.strftime('%Y-%m-%d %H:%M:%S')))

