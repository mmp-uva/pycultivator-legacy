#!/usr/bin/env python
# coding=utf-8
"""
Application to measure the OD of a set of channels and subsequently adjust light settings based on a regime.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import sys, argparse
from os import path
cwd = path.dirname(__file__)
sys.path.insert(1, path.join(cwd, ".."))

from uvaCultivatorScript import BaseCultivatorScript
from pycultivator_legacy.config.xmlCultivatorConfig import xmlCultivatorConfig as xCC

# Channels to subject to measurements
CHANNELS = [0, 1, 2, 3, 4, 5, 6, 7] #[1] #[1,3,5,7]  #[0, 1, 2, 3, 4, 5, 6, 7]
# channels=range(8),
# 		test_unit="percentage",
# 		test_values=[0, 5, 10, 15, 20, 25, 50, 70, 90, 100],

class LightCollector(uvaObject, BaseCultivatorScript):

	_namespace = "collector"
	_default_settings = uvaObject.mergeDictionary(
			uvaObject._default_settings,
			{
				"unit" : "percentage",
				"channels" : range(8),
				"values" : [0, 5, 10, 15, 20, 25, 50, 70, 90, 100],
			}
	)

	def loadArgParse(self, args):
		result = False
		try:
			if not self.verifyPath(args.log_dir, check_dir=True, quiet=False):
				raise ValueError("Log dir is not correct: {}".format(args.log_dir))
			self.setLogDir(args.log_dir)
			self.runVerbose(args.run_verbose)
			self.doSimulation(args.simulate)
			self.useFakeSerialController(args.use_fake)
			self.setTestChannels(args.test_channels)
			self.setTestValues(args.test_values)
			self.setTestUnit(args.test_unit)
			self.connectLog()
			if not self.verifyPath(args.output_dir, check_dir=True, quiet=False):
				raise ValueError("Output dir is not correct: {}".format(args.output_dir))
			self.setOutputDir(args.output_dir)
			self.overwriteOutputFile(args.overwrite)
			if args.config_path is not None:
				if args.config_path[-3:] != ".xml":
					args.config_path = "{}.xml".format(args.config_path)
				config_path = args.config_path
				if self.verifyPath(args.config_path):
					# config dir already included, remove it
					self.setConfigDir("")
					self.setConfigFile(args.config_path)
				if self.verifyPath(path.join(self.getConfigDir(), args.config_path)):
					# keep the config dir
					self.setConfigFile(args.config_path)
				else:
					raise ValueError("Config path is not correct: {}".format(config_path))
			result = True
		except Exception as e:
			sys.stderr.write("Unable to parse given arguments:\n {}".format(e))
		return result

	def setTestChannels(self, channels):
		if not isinstance(channels, list):
			raise ValueError("setTestChannels: Unsupported value for channels.")
		self.setSetting("channels", channels)

	def getTestChannels(self):
		return self.retrieveSetting("channels")

	def setTestValues(self, values):
		if not isinstance(values, list):
			raise ValueError("setTestChannels: Unsupported value for channels.")
		self.setSetting("values", values)

	def getTestValues(self):
		return self.retrieveSetting("values")

	def setTestUnit(self, unit):
		if not isinstance(unit, str):
			raise ValueError("setTestUnit: Unsupported value for test unit.")
		self.setSetting("unit", unit)

	def getTestUnit(self):
		return self.retrieveSetting("unit")

	def getProjectId(self):
		return self.retrieveSetting("project_id")

	def getDeviceName(self):
		return self.getProjectId().split("_")[0]

	def getConfig(self):
		"""
		:return:
		:rtype: uvaCultivator.uvaCultivatorConfig.uvaCultivatorConfig.AbstractCultivatorConfig
		"""
		return self._config

	def overwriteOutputFile(self, overwrite):
		self.setSetting("output.overwrite", overwrite is True, namespace="")

	def overwritesOutputFile(self):
		self.retrieveSetting("output.overwrite", namespace="") is True

	def __init__(self, script, settings=None):
		BaseCultivatorScript.__init__(self, script)
		uvaObject.__init__(self, settings=settings)
		self._config = None

	def execute(self):
		"""
		Prepares and starts
		:return:
		:rtype:
		"""
		try:
			# load given settings (so far)
			settings = self.getSettings()
			if self.getConfigFile() is None:
				raise ValueError("No config file available")
			self._config = xCC.loadConfig(self.getConfigFile())
			# load settings in configuration
			config_settings = self.getConfig().loadExperimentSettings(force=True)
			# overwrite current settings with config settings
			self._settings = self.mergeDictionary(settings, config_settings)
			project_id = self.getProjectId()
			if project_id is None:
				raise ValueError("No project_id is given")
			self.setOutputFile("{}/{}_{}_light_values.csv".format(
					self.getOutputDir(), dt.now().strftime(self.DT_FORMAT_YMD), self.getDeviceName()
			))
			self.getLog().info("Will write to {}".format(self.getOutputFile()))
			serial = self.getConfig().loadController(self.getControllerType())
			if serial is None:
				raise ValueError("Unable to load serial object")
			self.collect(serial)
		except ValueError as ve:
			self.getLog().critical("execute: Preparing for data collection failed:\n\t{}".format(ve))

	def clean(self, serial):
		"""
		Stops and cleans-up
		:return:
		:rtype:
		"""
		if serial is not None and serial.isConnected():
			self.getLog().info("Closing connection to {}".format(serial.getPort()))
			serial.disconnect()
		serial = None

	def setTestValue(self, serial, value):
		"""
		:param serial:
		:type serial: uvaSerial.psiSerialController.psiSerialController
		:return:
		:rtype: float or None
		"""
		result = True
		for c in self.getTestChannels():
			if self.getTestUnit() == "percentage":
				response = serial.setLightPercentage(c, value)
			elif self.getTestUnit() == "absolute":
				response = serial.setLightAbsolute(c, value)
			elif self.getTestUnit() == "voltage":
				response = serial.setLightVoltage(c, value)
			else:
				raise ValueError("Incorrect test unit")
			result = result and response is not None
		return result

	def collect(self, serial):
		"""

		:param serial:
		:type serial: uvaSerial.psiSerialController.psiSerialController
		:return:
		:rtype:
		"""
		measurements = {c: {} for c in CHANNELS}
		try:
			if not serial.connect():
				raise ValueError("Failed to configure or connect to {}".format(serial.getPort()))
			for v in self.getTestValues():
				if not self.setTestValue(serial, v):
					raise ValueError("Failed to set light intensities.")
				for c in self.getTestChannels():
					value = serial.getLightSettings(c)
					measurements[c][v] = value
				# end for channel
			# end for v
		except Exception as e:
			# print error
			self.getLog().critical("collect@{}: {}".format(self.getDeviceName(),e))
		finally:
			self.clean(serial)
		# now start writing
		if not self.isSimulating():
			if path.exists(self.getOutputFile()) and not self.overwritesOutputFile():
				self.getLog().info("Output file exists, update file with new measurement")
				self.update_csv(measurements)
			else:
				self.getLog().info("File does not yet exist or forced to overwrite, (re)create file")
				self.write_csv(measurements)

	def write_content(self, dest, m):
		formatters = ["{}", "{f.0}", "{}","{f.4}", "{f.4}", "{f.4}", "{f.4}"]
		try:
			for c in m.keys():
				for v in sorted(m[c].keys()):
					line = [dt.now().strftime(self.DT_FORMAT), c, self.getTestUnit(), v,
					        m[c][v][3], m[c][v][2], m[c][v][1]
					]
					dest.write("{}\n".format(";".join(map(str, line))))
		except Exception as e:
			self.getLog().critical("Unable to write CSV line:\n {}".format(e))

	def write_csv(self, m):
		"""
		Writes a csv, overwriting an existing file, use update_csv to append to that file
		:param m:
		:type m:
		:return:
		:rtype:
		"""
		with open(self.getOutputFile(), "w") as dest:
			# write header
			headers = ["time","channel", "unit", "test.value", "response.ue", "response.percentage", "response.voltage"]
			dest.write("{}\n".format(";".join(headers)))
			self.write_content(dest, m)

	def update_csv(self, m):
		with open(self.getOutputFile(), "a") as dest:
			self.write_content(dest, m)

if __name__ == "__main__":
	t_start = dt.now()
	# Setup argument parsing
	script = LightCollector(__file__)
	parser = argparse.ArgumentParser(
		description='Collects Light values from the Multi-Cultivator.'
	)
	parser.add_argument('config_path', action="store",
	                    help='Path to the configuration file or the FileName (without .xml).')
	parser.add_argument('--config_dir', action="store", dest="config_dir",
	                    help="Path to the configuration files.")
	parser.add_argument('--output_dir', action="store", dest='output_dir',
	                    help='Path to the directory where the CSV file(s) will be stored.')
	parser.add_argument('--overwrite', action="store_true", dest='overwrite',
	                    help='Will force the program to re-download all data')
	parser.add_argument('--log_dir', action="store", dest="log_dir",
	                    help='Path to the directory where the log file(s) will be stored.')
	parser.add_argument('--test-channels', action="store", dest="test_channels", nargs="+", type=int,
	                    help="List of channel numbers (0-8) to test")
	parser.add_argument('--test-unit', action="store", dest="test_unit", choices=['absolute', 'percentage'],
	                    help="Set the unit of the test values")
	parser.add_argument('--test-values', action="store", dest="test_values", nargs="+", type=int,
	                    help="List of values to set the light (NOTE: max value depends on test unit!)")
	parser.add_argument('-s', action="store_true", dest='simulate',
	                    help='Do not store the obtained data')
	parser.add_argument('-n', action="store_true", dest='use_fake',
	                    help='Simulate the Multi-Cultivator (Fake) instead of using the real multicultivator')
	parser.add_argument('-S', action="store_true", dest='simulate_fake',
	                    help='Simulate the Multi-Cultivator and do not store data (same as to -sn).')
	parser.add_argument('-v', action="store_true", dest='run_verbose',
	                    help='Print log messages to the console.')
	# set defaults
	parser.set_defaults(
		log_dir=script.getLogDir(),
		config_dir=script.getConfigDir(),
		output_dir=script.getOutputDir(),
		overwrite=script.overwritesOutputFile(),
		run_verbose=script.isVerbose(),
		use_fake=script.hasFakeSerialController(),
		simulate=script.isSimulating(),
		test_channels=range(8),
		test_unit="percentage",
		test_values=[0, 5, 10, 15, 20, 25, 50, 70, 90, 100],
	)
	# parse the arguments
	args = parser.parse_args()
	if args.simulate_fake:
		args.simulate = True
		args.use_fake = True
	if not script.loadArgParse(args):
		sys.exit(1)
	# set all settings
	script.start(t_start=t_start)
