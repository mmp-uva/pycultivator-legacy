#!/usr/bin/env python
# coding=utf-8
"""
Analyse the OD Error distribution.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import logging, sys, traceback
from datetime import datetime as dt
from os import path as OP

cwd = OP.dirname(OP.realpath(__file__))
sys.path.insert(1, OP.realpath("{}/../".format(cwd)))
from time import sleep
from uvaSerial.psiSerialController import psiSerialController as pSC2
# from uvaSerial.psiFakeSerialController import psiFakeSerialController as pFSC
from uvaCultivator import uvaLog

LOG_LEVEL = logging.INFO  # minimum level of messages to log
CHANNELS = [0, 2, 4, 6]  # [1] #[1,3,5,7]  #[0, 1, 2, 3, 4, 5, 6, 7]
# Number of measurements made by the MC and averaged before sending to the computer
OD_REPEATS = 1
# Store OD LED
OD_LEDS = [0, 1]  # 0 == 680 1 == 720
# Set whether it should stop the gas
GAS_STOP = True
# Set time to wait after turning gas pump off
GAS_PAUSE = 0
# Set whether it should try to restore the light
LIGHT_RESTORE = False
# Number of (sets of) measurements to be performed by the computer
OD_MEASUREMENTS = 1000


def prepare(connection):
    result = connection.connect()
    if result:
        logger.info("Connected to {}".format(connection.getPort()))
    else:
        logger.critical("Unable to connect to {}".format(connection.getPort()))
    if result:
        if GAS_STOP:
            result = connection.setPumpState()
            if result:
                logger.info("Pump has been switched off, wait {} seconds".format(GAS_PAUSE))
                sleep(GAS_PAUSE)  # wait until bubbles are gone
            else:
                logger.critica("Unable to turn GAS pump OFF")
    if result:
        result = connection.setAllLightStates()
        if result:
            logger.info("Light has been switched off, start measurements")
        else:
            logger.critica("Unable to turn LIGHT OFF")
    return result

def collect():
    data = []
    total_measurements = OD_MEASUREMENTS * len(CHANNELS) * len(OD_LEDS)
    for i in range(OD_MEASUREMENTS):
        for c in CHANNELS:
            for od_led in OD_LEDS:
                m = measure(channel=c, od_led=od_led, replicate=i)
                data.append(m)
                report_progress(i, total_measurements)
    return data


def measure(connection, channel, od_led, replicate=1):
    # collect measurement
    reading = connection.getODReading(
        channel=channel, led=od_led, repeats=OD_REPEATS
    )
    # save data
    measurement = {
        "time": dt.now(),
        "channel": channel,
        "replicate": replicate,
        "od_led": 680 if od_led == 0 else 720,
        "od_value": reading[2],
        "od_repeats": OD_REPEATS
    }
    return measurement


def clean(connection):
    # restore light
    if LIGHT_RESTORE and not connection.setAllLightStates(state=True):
        logger.critical("Unable to restore light")
    if GAS_STOP and not connection.setPumpState(state=True):
        logger.critical("Unable to restore gas")
    if not connection.disconnect():
        logger.critical("Unable to disconnect from Multi-Cultivator")
    return True


def export(fp, measurements, headers=None):
    logger.info("Will write measurements to {}".format(fp))
    # determine headers if necessary
    if headers is None:
        if len(measurements) > 0:
            headers = measurements[0].keys()
        else:
            headers = []
    # now process measurements
    with open(fp, "w") as dest:
        # write header line
        if len(headers) > 0:
            write_line(dest, headers)
        for measurement in measurements:
            write_values(dest, measurement, headers)
            # end of for
        # end of for
    # end of with


def write_values(destination, values, headers=None):
    if isinstance(values, dict):
        if headers is None:
            headers = values.keys()
        # remake values list
        values = [values.get(h) for h in headers]
    write_line(destination, values)


def write_line(destination, values):
    items = []
    for value in values:
        if isinstance(value, float):
            items.append("{:.6f}".format(value))
        elif isinstance(value, int):
            items.append("{:d}".format(value))
        elif isinstance(value, dt):
            items.append(value.strftime("%Y-%m-%d %H:%M:S.%f"))
    destination.write("{}\n".format(";".join(items)))


def report_progress(idx, total):
    # Show every 10 percent progress
    if (idx / total) * 100 % 10 == 0:
        logger.info("Measured {}% ({} of {})".format(
            idx / (total * 0.01),
            idx, total
        ))


if __name__ == "__main__":
    t_start = dt.now()
    settings = {
        # "port" : "/dev/ttyMC2"
        "port": "/dev/tty.usbserial-FTAK08XU",  # mc 0
        # "port" : "/dev/tty.usbserial-FTGZPIMR", # mc 2
        # "port": "/dev/tty.usbserial-FTGZOFXY", # mc 1
        "stopBits": 1,
        "parity": "N",
        "byteSize": 8
    }
    fp = "./data/{}_analyse_od.csv".format(t_start.strftime('%Y%m%d'))
    logfile = "./log/{}_analyse_od.log".format(t_start.strftime('%Y%m%d'))
    # if len(sys.argv) >= 2:
    # 	device = sys.argv[1]
    # if len(sys.argv) >= 3:
    # 	fp = sys.argv[2]        # use a different report file
    # if len(sys.argv) >= 4:
    # 	logfile = sys.argv[3]
    # if len(sys.argv) >= 5:
    # 	OD_MEASUREMENTS = int(sys.argv[4])
    logger = uvaLog.getLogger()
    logger.setLevel(LOG_LEVEL)
    logger.addFileHandler(logfile, level=LOG_LEVEL)
    logger.addStreamHandler(level=LOG_LEVEL)
    print "Connected FileHandler to path {} and logger {}".format(logfile, logger.name)
    conn = None
    data = []  # dt : { attributes }
    try:
        conn = pSC2(settings=settings)
        conn.configure(**settings)
        """ :type: uvaSerial.psiSerialController.psiSerialController |
        uvaSerial.psiFakeSerialController.psiFakeSerialController """
        # conn = pFSC(port=device)
        logger.info(" === LOG START AT: {} ===".format(t_start.strftime('%Y-%m-%d %H:%M:%S')))
        logger.info("Will write measurements to {}".format(fp))
        result = False
        # prepare multi-cultivator
        result = prepare(conn)
        if result:
            data = collect()
            logger.info("Completed all OD measurements.")
        # restore settings and close connection
        clean(conn)
        if len(data) > 0:
            export(fp, data)
    except Exception as e:
        msg = "Error:\n{}".format(traceback.format_exc())
        if logger:
            logger.critical(msg)
        else:
            print msg
    finally:
        if conn is not None and conn.isConnected():
            conn.disconnect()
        t_end = dt.now()
        logger.info(" === RUN TIME: {} sec.===".format((t_end - t_start).seconds))
        logger.info(" === LOG END AT: {} ===".format(t_end.strftime('%Y-%m-%d %H:%M:%S')))
