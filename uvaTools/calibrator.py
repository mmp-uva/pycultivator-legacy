#!/usr/bin/env python
# coding=utf-8
"""Stub to starting the pycultivator-legacy Calibrator App"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from pycultivator_legacy.contrib.calibrator.application import start


if __name__ == "__main__":
    start()
