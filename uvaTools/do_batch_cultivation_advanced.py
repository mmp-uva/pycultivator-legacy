#!/usr/bin/env python
# coding=utf-8
"""
Application to measure od's and adjust light for all configuration files found in the folder conf
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import sys, os, argparse
cwd = os.path.dirname(__file__)
sys.path.insert(1, os.path.realpath(os.path.join(cwd, "..")))
# get all necessary imports
from uvaScript import batchScript
from uvaCultivator.uvaCultivatorProtocol import AbstractParallelProtocol
from do_cultivation_advanced import AdvancedCultivationProtocol


class BatchCultivationProtocol(
    AbstractParallelProtocol, AdvancedCultivationProtocol
):
    """Measure the OD in batch, executing all active configuration files"""

    _name = "advanced_batch_cultivation"
    _default_settings = AdvancedCultivationProtocol.mergeDefaultSettings(
        {

        }
    )

    def __init__(self, config_file, settings=None):
        AdvancedCultivationProtocol.__init__(self, config_file=config_file, settings=settings)

    def getLogFile(self):
        return self.retrieveSetting(name="file", default=self.getDefaultSettings(), namespace="log")

    def _prepare(self):
        return super(BatchCultivationProtocol, self)._prepare()
        # result = uvaCultivatorProtocol.AbstractParallelProtocol._prepare(self)
        # return AdvancedCultivationProtocol._prepare(self) and result

    def _measure(self):
        return AdvancedCultivationProtocol._measure(self)
        # return super(BatchCultivationProtocol, self)._measure()

    def _react(self):
        return AdvancedCultivationProtocol._react(self)
        # return super(BatchCultivationProtocol, self)._react()

    def _report(self, measurements=None):
        return AdvancedCultivationProtocol._report(self)
        # return super(BatchCultivationProtocol, self)._report()

    def _clean(self):
        return super(BatchCultivationProtocol, self)._clean()
        # result = uvaCultivatorProtocol.AbstractParallelProtocol._clean(self)
        # return AdvancedCultivationProtocol._clean(self) and result

if __name__ == "__main__":
    protocol = BatchCultivationProtocol
    bm = batchScript.BatchScript(__file__, protocol)
    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Run the cultivation protocol with all active config files.')
    # configuration settings
    parser.add_argument('--config-dir', action="store", dest="config_dir",
                        help='Path to the directory containing the config files.')
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that contains default values.')
    # path settings
    parser.add_argument('--output-dir', action="store", dest='output_dir',
                        help='Path to the directory where data files will be written.')
    # log settings
    parser.add_argument('--log-dir', action="store", dest='log_dir',
                        help='Path to the directory where log file will be written.')
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    parser.add_argument('-V', action="store_true", dest="verbose",
                        help="Connect logger to console")
    # protocol settings
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Simulate a Multi-Cultivator connection instead of using a real connection.')
    parser.add_argument('-s', action="store_true", dest='simulate',
                        help='Prevent script from exporting data.')
    parser.add_argument('-S', action="store_true", dest='simulate_fake',
                        help='Simulate connection and do not export data (equal to -sn).')
    # set defaults
    parser.set_defaults(
        # config_dir=bm.getConfigDir(), log_dir=bm.getLogDir(), output_dir=bm.getOutputDir(),
        verbose=bm.isVerbose(), verbosity=0, simulate_fake=False, ini_path=None,
        simulate=not bm.isReporting(), use_fake=bm.fakesConnection
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the script
    if not bm.loadArguments(args, settings={"protocol.class": protocol}, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    bm.start()
