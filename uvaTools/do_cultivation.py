#!/usr/bin/env python
# coding=utf-8
"""
A script executing a protocol that measures OD and subsequently adjusts the light settings based on the a regime.
"""

import sys, os, argparse, logging
cwd = os.path.dirname(__file__)
sys.path.insert(1, os.path.realpath(os.path.join(cwd, "..")))
# get all necessary imports
from uvaScript import simpleScript
from uvaCultivator.uvaCultivatorProtocol import odProtocol, pfsProtocol, timeProtocol

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# Set minimum level of messages that will be written to the log
LOG_LEVEL = logging.INFO


class CultivationProtocol(
    odProtocol.ODProtocol, timeProtocol.TimeProtocol, pfsProtocol.PhotonFluxoStatProtocol
):
    """Measures OD and adjusts the light"""

    _name = "cultivation"

    _default_settings = odProtocol.ODProtocol.mergeDefaultSettings(
        timeProtocol.TimeProtocol.mergeDefaultSettings(
            pfsProtocol.PhotonFluxoStatProtocol.mergeDefaultSettings(
                {
                    # add default
                }, namespace=""
            ), namespace=""
        ), namespace=""
    )


if __name__ == "__main__":
    protocol = CultivationProtocol
    cs = simpleScript.SimpleScript(__file__, protocol)

    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Run a cultivation using one experiment configuration.')
    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Measure OD using one experiment configuration.')
    parser.add_argument('config_path', action="store",
                        help='Path to the configuration file or the file name (with or without .xml).')
    parser.add_argument('--config-dir', action="store", dest="config_dir",
                        help="Path to the configuration files.")
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that should be used.')
    # path settings
    parser.add_argument('--output-dir', action="store", dest='output_dir',
                        help='Path to the directory where data file(s) will be stored.')
    parser.add_argument('--log-dir', action="store", dest="log_dir",
                        help='Path to the directory where log file(s) will be stored.')
    # protocol settings
    parser.add_argument('-f', action="store_true", dest='force_run',
                        help='Force running the (inactive) configuration.')
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Simulate a Multi-Cultivator connection instead of using a real connection.')
    parser.add_argument('-s', action="store_true", dest='simulate',
                        help='Prevent script from exporting data.')
    parser.add_argument('-S', action="store_true", dest='simulate_fake',
                        help='Simulate connection and do not export data (equal to -sn).')
    # script settings
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    # set defaults
    parser.set_defaults(
        # config_dir=cs.getConfigDir(), log_dir=cs.getLogDir(), output_dir=cs.getOutputDir(),
        force_run=cs.isForced(), verbosity=0, simulate_fake=False, ini_path=None,
        simulate=not cs.isReporting(), use_fake=cs.fakesConnection
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the object, we enforce our protocol (so it is not set by the ini file)
    if not cs.loadArguments(args, settings={"protocol.class": protocol}, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    cs.start()
