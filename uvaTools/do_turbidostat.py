#!/usr/bin/env python
# coding=utf-8
"""Script that implements a turbidostat"""

import sys, os, argparse
cwd = os.path.dirname(__file__)
sys.path.insert(1, os.path.realpath(os.path.join(cwd, "..")))
# get all necessary imports
from uvaScript import simpleScript
from uvaSerial.regloSerialController import regloSerialController
from uvaCultivator.uvaCultivatorProtocol.turbidostatProtocol import TurbidostatProtocol
from uvaCultivator.uvaAggregation import ODMeasurementAggregation

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class DiscontinuousTurbidostatProtocol(TurbidostatProtocol):
    """Uses the previously obtained measurements (collected with measure_od or cultivate) to trigger a pump"""

    def storeOrientMeasurement(self, db, channel, measurement, decision):
        raise NotImplementedError("OrientDB Support is deprecated")

    _name = "turbidostat"
    _default_settings = TurbidostatProtocol.mergeDefaultSettings(
        {
            "turbidostat.pump.serial.class": regloSerialController,
            "turbidostat.collect.time_span": 60,  # retrieve measurements of last hour
            "turbidostat.decision.threshold": 0.35,
            "turbidostat.decision.od_led": 720,
            "turbidostat.decision.breach_metric": "mean",  # possible: all, mean, median, mode
            "turbidostat.decision.breach_size": 2,  # number of consecutive breaches need before deciding to dilute
            "turbidostat.pump.mapping": {
                # Pump setting of the form:
                # 0: {"in": "/dev/ttyRP0@1", "out": "/dev/ttyRP1@1"},
            },
            "turbidostat.pump.time": None,  # in seconds, takes precedence over flow - volume
            "turbidostat.pump.rate": 100,  # in RPM
            "turbidostat.pump.flow": 7.7,  # in mL/min
            "turbidostat.pump.volume": 2.4,  # 2.4, # in mL
            "turbidostat.pump.pause": 200,  # wait time in seconds between finishing pump in and pumping out
            "turbidostat.pump.out": True,
            "reporter.name": "csv",
        },
        namespace=""
    )

    def _measure(self):
        return True

    def decide(self, measurements):
        """Makes a decision on whether it should dilute this channel or not

        :param measurements: Dictionary with timepoints and
        :type measurements: dict[ datetime.datetime, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]
        :return: a list of size 2 with decision,measurement
        :rtype: dict[str, bool or int or float or None]
        """
        # initialize results
        result = {
            "decision": False,
            "od_value": None,
            "t_point": None,
            "measurement_id": None
        }
        # initialize metric
        metric = self.getBreachMetric()
        if metric == "all":
            metric = "min"
        # parse measurements
        measurements = self.reduce(measurements)
        # aggregate
        measurement = ODMeasurementAggregation.aggregate(metric, measurements)
        if measurement is not None:
            result["t_point"] = measurement.getTimePoint()
            result["od_value"] = measurement.getCalibratedODValue()
            result["measurement_id"] = measurement.getAttribute("id")
        if result["od_value"] is not None:
            enough_points = len(measurements) == self.getBreachSize()
            breach_threshold = result["od_value"] > self.getThreshold()
            # make decision
            result["decision"] = enough_points and breach_threshold
            self.getLog().info(
                "Deciding over (n = {} >= {}): {:0.3f} (OD) > {:0.3f} (Threshold) => Will {} Dilute".format(
                    len(measurements), self.getBreachSize(),
                    result["od_value"], self.getThreshold(), "" if result["decision"] else "NOT"
                )
            )
        else:
            self.getLog().info("No valid OD Value was found")
        return result


# Settings to be applied
# NOTE: CONFIGURATION SETTINGS WILL OVERWRITE THESE SETTINGS!
# TODO: serialize into an ini
settings = {
    "turbidostat.pump.mapping": {
        0: {"in": "/dev/ttyRP0@1", "out": "/dev/ttyRP1@1"},
        1: {"in": "/dev/ttyRP0@2", "out": "/dev/ttyRP1@2"},
        2: {"in": "/dev/ttyRP0@3", "out": "/dev/ttyRP1@3"},
        3: {"in": "/dev/ttyRP0@4", "out": "/dev/ttyRP1@4"},
        4: {"in": "/dev/ttyRP2@1", "out": "/dev/ttyRP3@1"},
        5: {"in": "/dev/ttyRP2@2", "out": "/dev/ttyRP3@2"},
        6: {"in": "/dev/ttyRP2@3", "out": "/dev/ttyRP3@3"},
        7: {"in": "/dev/ttyRP2@4", "out": "/dev/ttyRP3@4"},
    },
}

if __name__ == "__main__":
    protocol = DiscontinuousTurbidostatProtocol
    ts = simpleScript.SimpleScript(__file__, protocol)

    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Run measure OD in batch mode on all active config files.')

    # Configuration settings
    parser.add_argument('config_path', action="store",
                        help='Path to the configuration file or the FileName (without .xml).')
    parser.add_argument('--config-dir', action="store", dest="config_dir",
                        help="Path to the configuration files.")
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that should be used.')
    # path settings
    parser.add_argument('--output-dir', action="store", dest='output_dir',
                        help='Path to the directory where the CSV file(s) will be stored.')
    parser.add_argument('--log-dir', action="store", dest="log_dir",
                        help='Path to the directory where the log file(s) will be stored.')
    # protocol settings
    parser.add_argument('-f', action="store_true", dest='force_run',
                        help='Force running the (inactive) configuration.')
    parser.add_argument('-S', action="store_true", dest='simulate_fake',
                        help='Simulate external connections and prevent data export (equal to -sn).')
    parser.add_argument('-s', action="store_true", dest='simulate',
                        help='Do not import or export the data.')
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Simulate the connections to external devices')
    # script settings
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    # set defaults
    parser.set_defaults(
        # config_dir=ts.getConfigDir(), log_dir=ts.getLogDir(), output_dir=ts.getOutputDir(),
        force_run=ts.isForced(), verbosity=0, ini_path=None,
        simulate_fake=False, simulate=not ts.isReporting(),
        use_fake=ts.fakesConnection()
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the settings dictionary
    settings["protocol.class"] = protocol
    # load arguments and settings into the loader
    if not ts.loadArguments(args, settings=settings, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    ts.start()
