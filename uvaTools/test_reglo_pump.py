#!/usr/bin/env python
# coding=utf-8
"""Script tests the connection and usage of REGLO ICC Pumps"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import sys, os, argparse, logging
cwd = os.path.dirname(__file__)
sys.path.insert(1, os.path.realpath(os.path.join(cwd, "..")))
# get all necessary imports
from uvaScript import simpleScript
from uvaCultivator.uvaProtocol.cultivationProtocol import SimpleProtocol

# Set minimum level of messages that will be written to the log
LOG_LEVEL = logging.INFO


class TestPumpScript(simpleScript.SimpleScript):
    """Class implementing the script"""

    _default_settings = simpleScript.SimpleScript.mergeDefaultSettings(
        {
            "pump.port": None
        },
        namespace=""
    )

    def getPumpPort(self):
        return self.retrieveSetting("port", namespace="pump")

    def setPumpPort(self, port):
        self.setSetting("port", port, namespace="pump")
        return self.getPumpPort()

    def loadArguments(self, args, settings=None, namespace=None):
        result = super(TestPumpScript, self).loadArguments(args, settings=settings, namespace=namespace)
        # go and fetch configuration settings
        try:
            self.loadPumpArguments(args)
        except:
            raise
        return result

    def loadPumpArguments(self, args):
        if args.get("pump_port") is not None:
            self.setPumpPort(args.get("pump_port"))
        return True


class TestPumpProtocol(SimpleProtocol):
    """Class implementing a basic protocol for testing a REGLO ICC Pump."""

    _name = "test_pump"

    def __init__(self, config_file, settings=None):
        super(TestPumpProtocol, self).__init__(config_file, settings=settings)
        self._pump = None

    def _prepare(self):
        """Prepare for the protocol: in this case make sure we are connected"""
        result = super(TestPumpProtocol, self)._prepare()
        # load pump
        
        # return success: we should be connected now
        self.getLog().info("Connect to Multi-Cultivator: {}".format(connected))
        return connected

    def _execute(self):
        """The actual protocol"""
        result = False
        # do protocol
        return result

    def _clean(self):
        # do normal clean stuff
        result = super(TestPumpProtocol, self)._clean()
        # check pump connection


        return result and not connected

if __name__ == "__main__":
    protocol = TestPumpProtocol
    cs = simpleScript.SimpleScript(__file__, protocol)

    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Test the usage of REGLO ICC Pump.')
    parser.add_argument('pump_port', action="store",
                        help='Port to the pump to use.')
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that should be used.')
    # path settings
    parser.add_argument('--log-dir', action="store", dest="log_dir",
                        help='Path to the directory where log file(s) will be stored.')
    # protocol settings
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Simulate connection to pump')
    # script settings
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    # set defaults
    parser.set_defaults(
        log_dir=cs.getLogDir(), verbosity=0, ini_path=None,
        use_fake=cs.fakesConnection
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the
    if not cs.loadArguments(args, settings={"protocol.class": protocol}, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    cs.start()
