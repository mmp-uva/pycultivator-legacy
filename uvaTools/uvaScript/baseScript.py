# coding=utf-8
"""A controller class for scripts (command-line executables)"""

from uvaCultivator import uvaLog
from pycultivator_legacy.core import BaseObject
from datetime import datetime as dt
from abc import abstractmethod
import os, sys, glob, ConfigParser

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class BaseScript(BaseObject):
    """A basic script controller, implementing the minimal required infrastructure for running a pyCultivator program.

    This class needs to be subclassed in order to be useful, by overloading the 'execute' method.
    """

    INI_COMPATIBLE_VERSION = 1

    _default_settings = BaseObject.mergeDefaultSettings(
        {
            "script.t_start": None,
            "script.t_end": None,
            "script.path": None,
            "script.name": None,
            "script.cwd": None,
            "log.verbose": False,
            "log.level": 60,
            "log.dir_fmt": os.path.join("{cwd}", "..", "log"),
            "log.dir": None,
            "log.file.name": None,
            "log.file.ext": ".log",
            "log.file.name_fmt": "{date}_{script}{ext}",
            "ini.dir_fmt": os.path.join("{cwd}"),
            "ini.dir": None,
            "ini.file.name": None,
            "ini.file.ext": ".ini",
            "ini.file.name_fmt": "{name}{ext}",
            "config.dir_fmt": os.path.join("{cwd}", "..", "conf"),
            "config.dir": None,
            "config.file.name": None,
            "config.file.name_fmt": "{name}{ext}",
            "config.file.ext": ".xml",
            "exporter.dir_fmt": os.path.join("{cwd}", "..", "data"),
            "exporter.dir": None,
            "exporter.file.name": None,
            "exporter.file.name_fmt": "{date}_{project_id}_{script}_{led}{ext}",
            "exporter.file.ext": ".csv",
            "exporter.state": True,
            "protocol.use_fake": False,
            "protocol.force": False,
            "protocol.class": None,
        }, namespace=""
    )

    def __init__(self, script_path, settings=None):
        """Initializes all the settings and parameters.

        Will also set initial paths to the log, configuration and ouput
        directories and files. Your implementation of this class should change these paths if they do not apply to
        your situation.

        :param script_path: The path to the script that you will call from Python.
        :type script_path: str
        """
        super(BaseScript, self).__init__(settings=settings)
        if settings is None:
            settings = {}
        # load path information
        self.locateScript(script_path)
        # load settings from the ini
        success, ini_settings = self.findIni(
            paths=None, settings=settings, extend=True
        )
        if success:
            # first overwrite ini_settings with given settings via parameters
            settings = self.mergeDictionary(ini_settings, settings)
            # then store all settings in object, do not copy
            self.mergeDictionary(self.getSettings(), settings, copy_target=False)
        # record start time
        self.setSetting("t_start", dt.now(), "script")

    @staticmethod
    def prependExtension(ext):
        """Adds a dot to the extension if the first character is not a dot"""
        if ext not in (None, "") and ext[0] != ".":
            ext = ".{}".format(ext)
        return ext

    def locateScript(self, path=__file__):
        """Will determine and store the path information of this script.

        :param path: The path of this script, defaults to the value of the __file__ variable.
        """
        if self.getScriptPath() is None:
            self.setSetting("path", os.path.realpath(path), namespace="script")
        if self.getScriptName() is None:
            name = os.path.basename(path)
            self.setSetting("name", name[:name.rfind(".")], namespace="script")
        if self.getScriptCWD() is None:
            self.setSetting("cwd", os.path.dirname(path), namespace="script")
        # add current working directory to the path environment variable
        if self.getScriptCWD() not in sys.path:
            sys.path.insert(1, self.retrieveSetting("script.cwd"))

    def getStartTime(self):
        """Start time of this script object, None if not started yet

        :rtype: datetime.datetime
        """
        return self.retrieveSetting("t_start", default=self.getDefaultSettings(), namespace="script")

    def setStartTime(self, t_start):
        self.setSetting("t_start", t_start, namespace="script")
        return self.getStartTime()

    def getEndTime(self):
        """Finish time of this script object, None if not finished (or not started)

        :rtype: datetime.datetime
        """
        return self.retrieveSetting("t_end", default=self.getDefaultSettings(), namespace="script")

    def setEndTime(self, t_end):
        """Sets the time when the script finished"""
        self.setSetting("t_end", t_end, namespace="script")
        return self.getEndTime()

    def getDuration(self):
        """Returns the duration of the script in seconds"""
        return (self.getEndTime() - self.getStartTime()).total_seconds()

    def getScriptPath(self):
        """Path to the script file that is being executed"""
        return self.retrieveSetting("path", default=self.getDefaultSettings(), namespace="script")

    def getScriptCWD(self):
        """Working Directory in which this script is being run"""
        return self.retrieveSetting("cwd", default=self.getDefaultSettings(), namespace="script")

    def getScriptName(self):
        """Filename (without .py) of the script"""
        return self.retrieveSetting("name", default=self.getDefaultSettings(), namespace="script")

    def getLogDirFormat(self):
        """Returns a string format used to dynamically create the path to the log directory

        :return: String format to be used with the .format function
        :rtype: str
        """
        return self.retrieveSetting("dir_fmt", default=self.getDefaultSettings(), namespace="log")

    def getLogDir(self):
        """Returns the directory where log files are written"""
        result = self.retrieveSetting("dir", default=self.getDefaultSettings(), namespace="log")
        if result in (None, ""):
            result = self.generateLogDir()
        return result

    def setLogDir(self, log_dir):
        """Sets the directory where log files are written, should exists otherwise the old value is retained"""
        log_dir = os.path.realpath(log_dir)
        if self.verifyPath(log_dir):
            self.setSetting("dir", log_dir, "log")
        return self.getLogDir()

    def generateLogDir(self, cwd=None, fmt=None):
        """Creates the log directory path from the log format"""
        if cwd is None:
            cwd = self.getScriptCWD()
        if fmt is None:
            fmt = self.getLogDirFormat()
        return os.path.realpath(fmt.format(cwd=cwd))

    def getLogPath(self, directory=None, name=None):
        """Returns the path to the log file"""
        result = None
        if directory is None:
            directory = self.getLogDir()
        if name is None:
            name = self.getLogFileName()
        if None not in (directory, name):
            result = os.path.join(directory, name)
        return result

    def setLogPath(self, fp):
        """Sets the path to the log file, if path does not exist the old value is retaiend"""
        if fp is not None:
            self.setLogDir(os.path.split(fp)[0])
            self.setLogFileName(os.path.split(fp)[1])
        return self.getLogPath()

    def getLogFileExtension(self):
        """Returns the extension of the log file"""
        return self.retrieveSetting("file.ext", default=self.getDefaultSettings(), namespace="log")

    def setLogFileExtension(self, ext):
        self.setSetting("file.ext", ext, namespace="log")
        return self.getLogFileExtension()

    def getLogFileNameFormat(self):
        """Returns the string format used to dynamically create the log file names"""
        return self.retrieveSetting("file.name_fmt", default=self.getDefaultSettings(), namespace="log")

    def getLogFileName(self):
        """Returns the currently used file name of the log file"""
        result = self.retrieveSetting("file.name", default=self.getDefaultSettings(), namespace="log")
        if result in (None, ""):
            result = self.generateLogFileName()
        return result

    def setLogFileName(self, name):
        if name is not None and self.verifyPath(os.path.join(self.getLogDir(), name), True):
            self.setSetting("file.name", name, "log")
        return self.getLogFileName()

    def generateLogFileName(self, date=None, script=None, ext=None, fmt=None):
        """Creates the log file name from the log format"""
        if date is None:
            date = dt.now().strftime(self.DT_FORMAT_YMD)
        if script is None:
            script = self.getScriptName()
        if ext is None:
            ext = self.getLogFileExtension()
        if fmt is None:
            fmt = self.getLogFileNameFormat()
        return fmt.format(date=date, script=script, ext=self.prependExtension(ext))

    def getLogLevel(self):
        return self.retrieveSetting("level", default=self.getDefaultSettings(), namespace="log")

    def setLogLevel(self, level):
        if isinstance(level, str):
            if level in uvaLog.LEVELS:
                # convert to integer level
                level = uvaLog.LEVELS[level]
        self.setSetting("level", level, "log")
        return self.getLogLevel()

    def isVerbose(self):
        return self.getLogLevel() < 60

    def getConfigDirFormat(self):
        return self.retrieveSetting("dir_fmt", default=self.getDefaultSettings(), namespace="config")

    def getConfigDir(self):
        result = self.retrieveSetting("dir", default=self.getDefaultSettings(), namespace="config")
        if result in (None, ""):
            result = self.generateConfigDir()
        return result

    def setConfigDir(self, config_dir):
        result = False
        if self.verifyPath(config_dir, quiet=False):
            self.setSetting("dir", config_dir, namespace="config")
            result = True
        return result

    def generateConfigDir(self, cwd=None, fmt=None):
        if cwd is None:
            cwd = self.getScriptCWD()
        if fmt is None:
            fmt = self.getConfigDirFormat()
        return os.path.realpath(fmt.format(cwd=cwd))

    def getConfigPath(self, directory=None, name=None):
        result = None
        if directory is None:
            directory = self.getConfigDir()
        if name is None:
            name = self.getConfigFileName()
        if None not in (directory, name):
            result = os.path.join(directory, name)
        return result

    def setConfigPath(self, path):
        if path is not None:
            self.setConfigDir(os.path.split(path)[0])
            self.setConfigFileName(os.path.split(path)[1])
        return self.getConfigPath()

    def getConfigFileExtension(self):
        return self.retrieveSetting("file.ext", default=self.getDefaultSettings(), namespace="config")

    def setConfigFileExtension(self, ext):
        self.setSetting("file.ext", ext, namespace="config")
        return self.getConfigFileExtension()

    def getConfigFileNameFormat(self):
        return self.retrieveSetting("file.name_fmt", default=self.getDefaultSettings(), namespace="config")

    def getConfigFileName(self):
        return self.retrieveSetting("file.name", default=self.getDefaultSettings(), namespace="config")

    def setConfigFileName(self, name=None):
        if self.verifyPath(os.path.join(self.getConfigDir(), name), True):
            self.setSetting("file.name", name, namespace="config")
        return self.getConfigFileName()

    def generateConfigFileName(self, name, ext=None, fmt=None):
        if ext is None:
            ext = self.getConfigFileExtension()
        if fmt is None:
            fmt = self.getConfigFileNameFormat()
        return fmt.format(name=name, ext=self.prependExtension(ext))

    def getOutputDirFormat(self):
        return self.retrieveSetting("dir_fmt", default=self.getDefaultSettings(), namespace="exporter")

    def getOutputDir(self):
        result = self.retrieveSetting("dir", default=self.getDefaultSettings(), namespace="exporter")
        if result in (None, ""):
            result = self.generateOutputDir()
        return result

    def setOutputDir(self, fd):
        if fd is None:
            fd = os.getcwd()
        if self.verifyPath(fd):
            self.setSetting("dir", fd, namespace="exporter")
        return self.getOutputDir()

    def generateOutputDir(self, cwd=None, fmt=None):
        if cwd is None:
            cwd = self.getScriptCWD()
        if fmt is None:
            fmt = self.getOutputDirFormat()
        return fmt.format(cwd=cwd)

    def getOutputFileExtension(self):
        return self.retrieveSetting("file.ext", default=self.getDefaultSettings(), namespace="exporter")

    def getOutputPath(self, directory=None, name=None):
        """Returns the path to the output file"""
        result = None
        if directory is None:
            directory = self.getOutputDir()
        if name is None:
            name = self.getOutputFileName()
        if None not in (directory, name):
            result = os.path.join(directory, name)
        return result

    def setOutputPath(self, fp):
        if fp is not None:
            self.setOutputDir(os.path.split(fp)[0])
            self.setOutputFileName(os.path.split(fp)[1])
        return self.getOutputPath()

    def getOutputFileNameFormat(self):
        return self.retrieveSetting("file.name_fmt", default=self.getDefaultSettings(), namespace="exporter")

    def getOutputFileName(self):
        return self.retrieveSetting("file.name", default=self.getDefaultSettings(), namespace="exporter")

    def setOutputFileName(self, fn):
        if self.verifyPath(os.path.join(self.getOutputDir(), fn), True):
            self.setSetting("file.name", fn, namespace="exporter")
        return self.getOutputFileName()

    def generateOutputFileName(self, project_id, led, date=None, script=None, ext=None, fmt=None):
        if date is None:
            date = dt.now().strftime(self.DT_FORMAT_YMD)
        if script is None:
            script = self.getScriptName()
        if ext is None:
            ext = self.getOutputFileExtension()
        if fmt is None:
            fmt = self.getOutputFileNameFormat()
        return fmt.format(date=date, project_id=project_id, led=led, script=script, ext=ext)

    def getIniDirFormat(self):
        """Returns a string format used to dynamically create the path to the directory with the ini file

        :return: String format to be used with the .format function
        :rtype: str
        """
        return self.retrieveSetting("dir_fmt", default=self.getDefaultSettings(), namespace="ini")

    def getIniDir(self):
        """Returns the directory where the ini file should be stored"""
        result = self.retrieveSetting("dir", default=self.getDefaultSettings(), namespace="ini")
        if result in (None, ""):
            result = self.generateIniDir()
        return result

    def setIniDir(self, log_dir):
        """Sets the directory where ini file is located, should exists otherwise the old value is retained"""
        if self.verifyPath(log_dir):
            self.setSetting("dir", log_dir, namespace="ini")
        return self.getIniDir()

    def generateIniDir(self, cwd=None, fmt=None):
        """Creates the ini directory path from the ini directory format"""
        if cwd is None:
            cwd = self.getScriptCWD()
        if fmt is None:
            fmt = self.getIniDirFormat()
        return os.path.realpath(fmt.format(cwd=cwd))

    def getIniPath(self, directory=None, name=None):
        """Returns the path to the ini file"""
        result = None
        if directory is None:
            directory = self.getIniDir()
        if name is None:
            name = self.getIniFileName()
        if None not in (directory, name):
            result = os.path.join(directory, name)
        return result

    def setIniPath(self, fp):
        """Sets the path to the ini file, if path does not exist the old value is retained"""
        if fp is not None:
            self.setIniDir(os.path.split(fp)[0])
            self.setIniFileName(os.path.split(fp)[1])
        return self.getIniPath()

    def getIniFileExtension(self):
        """Returns the extension of the ini file"""
        return self.retrieveSetting("file.ext", default=self.getDefaultSettings(), namespace="ini")

    def setIniFileExtension(self, ext):
        """Sets the extension of the ini file"""
        self.setSetting("file.ext", ext, namespace="ini")

    def getIniFileNameFormat(self):
        """Returns the string format used to dynamically create the ini file names"""
        return self.retrieveSetting("file.name_fmt", default=self.getDefaultSettings(), namespace="ini")

    def getIniFileName(self):
        """Returns the currently used file name of the ini file"""
        result = self.retrieveSetting("file.name", default=self.getDefaultSettings(), namespace="ini")
        if result in (None, ""):
            result = self.generateIniFileName()
        return result

    def setIniFileName(self, name):
        if name is not None and self.verifyPath(os.path.join(self.getIniDir(), name), True):
            self.setSetting("file.name", name, "ini")
        return self.getIniFileName()

    def generateIniFileName(self, name=None, ext=None, fmt=None):
        """Creates the ini file name from the ini name format"""
        if name is None:
            name = "pycultivator"
        if ext is None:
            ext = self.getIniFileExtension()
        if fmt is None:
            fmt = self.getIniFileNameFormat()
        return fmt.format(name=name, ext=self.prependExtension(ext))

    def doReporting(self, state):
        self.setSetting("state", state is True, namespace="exporter")

    def isReporting(self):
        return self.retrieveSetting("state", default=self.getDefaultSettings(), namespace="exporter") is True

    def fakeConnection(self, state):
        self.setSetting("use_fake", state is True, namespace="protocol")

    def fakesConnection(self):
        return self.retrieveSetting("use_fake", default=self.getDefaultSettings(), namespace="protocol") is True

    def forceRun(self, state):
        self.setSetting("force", state is True, namespace="protocol")

    def isForced(self):
        return self.retrieveSetting("force", default=self.getDefaultSettings(), namespace="protocol") is True

    def start(self):
        """Start the execution of this script.

        Will prepare the script for execution and cleans after the script has ended.
        """
        try:
            self.prepare()
            self.execute()
        except KeyboardInterrupt:
            self.warn("Interrupted by user, quit gracefully")
        except Exception as e:
            import traceback
            self.error(
                "Unexpected {} while executing protocol:\n{}".format(
                    type(e), traceback.print_exc()
                ),
            )
        finally:
            self.stop()

    def prepare(self):
        """Prepares for execution of the script"""
        self.setStartTime(dt.now())
        self.inform(" === LOG START AT: {} ===".format(self.getStartTime().strftime(self.DT_FORMAT)))

    @abstractmethod
    def execute(self):
        """Execute this script

        DO NOT run execute directly! Use start(), this will ensure proper logging and clean-up.
        """
        raise NotImplementedError

    def stop(self):
        """Clean after the script is finished

        :return:
        :rtype:
        """
        self.setEndTime(dt.now())
        self.inform(" === RUN TIME: {:.0f} sec.=== ".format(self.getDuration()))
        self.inform(" === LOG END AT: {} === ".format(self.getEndTime().strftime(self.DT_FORMAT)))

    @classmethod
    def inform(cls, msg, start="", end="\n"):
        log_level = cls.getLog().getEffectiveLevel()
        if log_level > 20:
            sys.stdout.write("{}{}{}".format(start, msg, end))
            sys.stdout.flush()
        else:
            cls.getLog().info(msg)

    @classmethod
    def warn(cls, msg, start="", end="\n"):
        log_level = cls.getLog().getEffectiveLevel()
        if log_level > 20:
            sys.stderr.write("{}{}{}".format(start, msg, end))
            sys.stderr.flush()
        else:
            cls.getLog().warning(msg)

    @classmethod
    def error(cls, msg, start="", end="\n"):
        log_level = cls.getLog().getEffectiveLevel()
        if log_level > 20:
            sys.stderr.write("{}{}{}".format(start, msg, end))
            sys.stderr.flush()
        else:
            cls.getLog().error(msg)

    def connectLog(self, log_file=None, log_level=None):
        """Will connect the desired log handlers"""
        if log_file is not None:
            self.setLogPath(log_file)
        if log_level is not None:
            self.setLogLevel(log_level)
        # connect console logger
        if self.isVerbose():
            self.connectConsoleLog()
        # connect file logger
        log_path = self.getLogPath()
        if log_path is not None:
            self.connectFileLog(log_path)

    def connectConsoleLog(self, log=None, log_level=None):
        if log is None:
            log = self.getRootLogger()
        if log_level is None:
            log_level = self.getLogLevel()
        self.getLog().setLevel(log_level)
        self.getLog().root.setLevel(log_level)
        uvaLog.connectStreamHandler(log, level=log_level, propagate=True)

    def connectFileLog(self, log_path, log=None, log_level=None):
        if log is None:
            log = self.getRootLogger()
        if log_level is None:
            log_level = self.getLogLevel()
        self.getLog().debug("Write log file to: {}".format(log_path))
        log_dir = os.path.dirname(log_path)
        if os.path.exists(log_dir):
            self.getLog().root.setLevel(log_level)
            uvaLog.connectFileHandler(log, log_path, level=log_level, propagate=True)

    def findIni(self, paths=None, names=None, settings=None, extend=True):
        """Tries multiple paths to locate the ini file for wrapper around locateIni

        :type paths: None or list[str]
        :type names: None or list[str]
        :type settings: None or dict[str, object]
        :type extend: bool
        :rtype: tuple[bool, dict[str, object]]
        """
        result = False
        if paths is None:
            paths = [self.getIniDir(), os.path.join(self.getIniDir(), "..")]
        if extend:
            paths += [
                os.getcwd(),
                self.getScriptCWD(),
                os.path.join(self.getScriptCWD(), ".."),
                os.path.dirname(__file__),
                os.path.join(os.path.dirname(__file__), ".."),
            ]
        if names is None:
            names = [self.getIniFileName()]
        if settings is None:
            settings = {}
        for path in paths:
            for name in names:
                success, ini_settings = self.locateIni(path=path, name=name, settings=settings)
                if success:
                    settings = self.mergeDictionary(settings, ini_settings)
                    result = True
        return result, settings

    def locateIni(self, path=None, name=None, settings=None):
        """Locates an ini file in a directory

        :param path: file path to the directory where should be looked, if None will look in current directory.
        :return: A tuple with whether settings were loaded from an ini and the new settings dictionary
        :rtype: tuple[bool, dict]
        """
        result = False
        # if no settings are given use object settings
        if settings is None:
            settings = self.getSettings()
        # if no path is given use the default one.
        if path is None:
            path = self.getIniDir()
        # if no name is given use default one.
        if name is None:
            name = self.getIniFileName()
        # find all ini files that match the file name
        ini_files = glob.glob(self.getIniPath(path, name))
        # try to parse all
        for ini in ini_files:
            self.getLog().info("Parse ini file at {}".format(ini))
            # load the ini file and retrieve settings
            success, ini_settings = self.parseIni(ini)
            # if successful merge the settings from the ini
            if success:
                settings = self.mergeDictionary(settings, ini_settings)
                result = True
        return result, settings

    def parseIni(self, path=None, settings=None):
        """Parses the script options stored in the ini file and returns the settings dictionary

        :param path: file path to the ini file
        :param settings: The settings dictionary in which the loaded settings will be stored (overwriting).
        :return: A tuple with whether parsing was successfull and the new settings object
        :rtype: tuple[bool, dict]
        """
        result = False
        # if no default settings are given use empty dict
        if settings is None:
            settings = {}
        # if no path is given try the default one
        if path is None:
            path = self.getIniPath()
        # check if ini file is supported
        if self.checkIni(path=path):
            cfg = ConfigParser.SafeConfigParser()
            cfg.read(path)
            # now parse the settings
            for section in cfg.sections():
                # get all options and store them appropriately
                for option in cfg.options(section):
                    try:
                        # obtain value
                        value = cfg.get(section, option)
                        # try to parse
                        value = self.parseIniValue(value, default=value)
                        # store setting in settings dict
                        settings["{}.{}".format(section, option)] = value
                    except ConfigParser.Error as cpe:
                        self.getLog().error("Error while parsing {}:\n{}".format(path, cpe))
            # parse settings, rename if necessary
            settings = self.parseIniSettings(settings)
            # set successful
            result = True
        return result, settings

    def parseIniValue(self, value, vtype=None, default=None):
        """ Parses a string into the given format (if None, tries to detect format)

        :param value: Input string to convert
        :type value: str
        :param vtype: Type to convert to, if None detect type
        :type vtype: type or None
        :param default: Default value to return when conversion fails
        :type default: object or None
        :return: Converted value or default
        :rtype: object or None
        """
        result = default
        if vtype is not None:
            result = self.validateValue(value, vtype, default)
        if vtype is None:
            # try boolean
            if value.lower() in ("1", "true"):
                result = True
            if value.lower() in ("0", "false"):
                result = False
        return result

    def parseIniSettings(self, settings):
        """ Transforms some of ini setting names to pycultivator setting names

        :param settings:
        :type settings: dict
        :return:
        :rtype:
        """
        result = settings.copy()
        for setting in settings.keys():
            if setting.startswith("pycultivator"):
                # go over cases
                if setting.endswith("root"):
                    # only take existing directories
                    if os.path.exists(settings[setting]) and os.path.isdir(settings[setting]):
                        result["script.dir"] = os.path.realpath(settings[setting])

                if setting.endswith("config"):
                    if os.path.isdir(settings[setting]):
                        result["config.dir"] = os.path.realpath(settings[setting])
                    else:
                        conf_dir = os.path.realpath(os.path.split(settings[setting])[0])
                        if os.path.exists(conf_dir):
                            result["config.dir"] = conf_dir
                            result["config.file.name"] = os.path.split(settings[setting])[1]
                if setting.endswith("data"):
                    # only take existing directories
                    if os.path.exists(settings[setting]) and os.path.isdir(settings[setting]):
                        result["exporter.dir"] = os.path.realpath(settings[setting])
                if setting.endswith("log"):
                    # only take existing directories
                    if os.path.exists(settings[setting]) and os.path.isdir(settings[setting]):
                        result["log.dir"] = os.path.realpath(settings[setting])
                    else:
                        log_dir = os.path.realpath(os.path.split(settings[setting])[0])
                        if os.path.exists(log_dir):
                            result["log.dir"] = log_dir
                            result["log.file.name"] = os.path.split(settings[setting])[1]
                # end if cases
            # end if pycultivator
        return result

    def checkIni(self, path=None):
        """Checks if the given ini file is correct"""
        result = os.path.exists(path)
        if result:
            try:
                cfg = ConfigParser.SafeConfigParser()
                # read ini
                cfg.read(path)
                # check version
                result = cfg.getint("pycultivator", "version") == self.INI_COMPATIBLE_VERSION
            except ValueError:
                self.getLog().warning("Unable to read version information from {}".format(os.path.basename(path)))
                result = False
        return result

    def loadIni(self, path=None):
        """Checks and loads the ini file into the object settings, alos stores ini path in settings"""
        success, settings = self.parseIni(path=path)
        if success:
            self.setIniPath(path)
            self.mergeDictionary(self.getSettings(), settings, copy_target=False)
        return success

    def parseArguments(self, args):
        """Parses the given arguments into a dictionary

        :raises ValueError: When parsing failed
        :type args: dict or argparse.Namespace
        :rtype: dict
        """
        import argparse
        # if Namespace object, convert to dict
        if isinstance(args, argparse.Namespace):
            args = vars(args)
        if not isinstance(args, dict):
            raise ValueError("Need a dict with arguments")
        return args

    @abstractmethod
    def loadArguments(self, args, settings=None, namespace=None):
        """Loads the given arguments into the object settings

        :param args: Arguments to parse and load
        :type args: dict or argparse.Namespace
        :param settings: Settings dictionary that will be loaded at the end
        :type settings: None or dict
        :param namespace: Namespace to be prepended to all setting names
        :type namespace: str
        """
        raise NotImplementedError

    def loadLogArguments(self, args):
        """Loads the log settings and connects the log using the given arguments"""
        log_dir = args.get("log_dir")
        if log_dir not in (None, ""):
            if not self.verifyPath(log_dir, check_dir=True, quiet=False):
                raise ValueError("Invalid path to log files: {}".format(log_dir))
            self.setLogDir(log_dir)
        level = args.get("verbosity", 0)
        if level is None:
            level = 0
        if level >= 0:
            level = 60 - (level * 10)
        if level < 0:
            level = 0
        self.setLogLevel(level)
        self.connectLog()
        return True

    def loadExporterArguments(self, args):
        """Loads the exporter settings from the given arguments"""
        output_dir = args.get("output_dir")
        if output_dir not in (None, ""):
            if not self.verifyPath(output_dir, check_dir=True, quiet=False):
                raise ValueError("Invalid path to config files: {}".format(output_dir))
            self.setOutputDir(output_dir)
        return True

    def loadProtocolArguments(self, args):
        """Loads the protocol settings from the given arguments"""
        if args.get("simulate_fake", False):
            args["simulate"] = True
            args["use_fake"] = True
        self.doReporting(not args.get("simulate", False))
        self.fakeConnection(args.get("use_fake", False))
        self.forceRun(args.get("force_run", False))
        return True

    def verifyPath(self, file_path, check_dir=False, quiet=True):
        """
        Verify if a given path exists or if necessary only verify whether the nearest directory exists.
        :param file_path:
        :type file_path: str
        :param check_dir: Only check whether the directory exists. For instance when you want to create a file but
        need to now that you can reach the directory.
        :type check_dir: bool
        :return: True on success, False otherwise
        :rtype: bool
        """
        success = True
        if file_path is None or (not check_dir and not os.path.exists(file_path)) or \
                (check_dir and not os.path.exists(os.path.dirname(file_path))):
            if not quiet:
                msg = "ERROR: {} does not exist".format(file_path)
                self.getLog().critical(msg)
                if self.getLogPath() is None:
                    print msg
            success = False
        return success
