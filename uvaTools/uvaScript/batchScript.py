# coding=utf-8
"""A class for executing multiple protocols in parallel from a script (command-line executable)"""

from simpleScript import SimpleScript
from time import sleep
import multiprocessing
import glob
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class BatchScript(SimpleScript):
    """Class implementing the execution of multiple protocols from one script"""

    _default_settings = SimpleScript.mergeDefaultSettings(
        {
            "log.verbose": False,
            "script.process.tries": 300,
            "script.process.pause": 1,
        }, namespace=""
    )

    def __init__(self, script_path, protocol, settings=None):
        SimpleScript.__init__(self, script_path=script_path, protocol=protocol, settings=settings)
        # keep track of the associated processes
        self._processes = {}

    def __del__(self):
        # stop all processes
        for process in self.getProcesses().keys():
            # ask to stop a process or force it
            self.removeProcess(process, True)

    def isVerbose(self):
        return self.retrieveSetting("verbose", default=self.getDefaultSettings(), namespace="log") is True

    def setVerbose(self, state):
        self.setSetting("verbose", state is True, namespace="log")

    def getPauseTime(self):
        """Returns the number of seconds to wait before trying to join the processes again"""
        return self.retrieveSetting("process.pause", default=self.getDefaultSettings(), namespace="script")

    def getCleanTries(self):
        """Returns how often to attempt to clean dead processes up"""
        return self.retrieveSetting("process.tries", default=self.getDefaultSettings(), namespace="script")

    def getProcesses(self):
        """Returns the dictionary holding the processes associated with this object

        :rtype: dict[str, multiprocessing.Process]
        """
        return self._processes

    def countProcesses(self):
        """Counts the number of processes registered to this script"""
        return len(self.getProcesses().keys())

    def hasProcesses(self):
        """Returns whether there is at least one process registered to this script"""
        return self.countProcesses() > 0

    def getProcess(self, name):
        """Returns the process registered to this object under the given name, fails if name is not known

        :rtype: multiprocessing.Process
        """
        if not self.hasProcess(name):
            raise KeyError("Unable to get process {}, name is not known".format(name))
        return self.getProcesses().get(name)

    def hasProcess(self, name):
        """Returns whether a process with the given name is associated with this object"""
        return name in self.getProcesses().keys()

    def addProcess(self, name, process):
        """Registers a new process to this object, will fail if the name is already taken

        :param name: Name of the process, cannot be in use.
        :type name: str
        :param process: The actual process to register
        :type process: multiprocessing.Process
        :raises: ValueError, when name is already used
        """
        if self.hasProcess(name):
            raise KeyError("Unable to register {} as process name, name is already taken".format(name))
        self.getProcesses()[name] = process
        return self

    def removeProcess(self, name, terminate=False):
        """Deregisters a process from this object

        Will try to stop and remove a process. It will first try to join it (waits for 0.1 second). If the process
        did not stop in the mean time, it will try to terminate the process (if the terminate flag is set) or
        else it will do nothing. Processes are only removed once they are joined or terminated.

        :raises: KeyError If name is not known.
        :rtype: bool
        :return: Whether successfull in stopping and removing the process (relevant if terminate is False).
        """
        result = False
        if not self.hasProcess(name):
            raise KeyError("Unable to deregister {}, name is not known".format(name))
        # inform process to stop
        self.getProcess(name).join(0.1)
        # check if process has finished
        if not self.getProcess(name).is_alive():
            self.getLog().info("Process {} finished".format(name))
            self.getProcesses().pop(name)
            result = True
        # if not finished and we are not allowed to try again, terminate it
        elif terminate:
            self.getLog().warning("Was not able to stop process {} gracefully, so I terminated it".format(name))
            self.getProcess(name).terminate()
            self.getProcesses().pop(name)
            result = True
        return result

    def cleanProcesses(self, tries=900, pause=1):
        """Remove all dead processes until there are no processes left or until all tries have been used.

        :return: True if all protocols are finished
        :rtype: bool
        """
        while tries > 0:
            for process in self._processes.keys():
                # ask the process to stop
                self.removeProcess(process)
            if self.hasProcesses():
                sleep(pause)
            tries -= 1
        return self.hasProcesses()

    def loadArguments(self, args, settings=None, namespace=None):
        if settings is None:
            settings = {}
        # if Namespace object, convert to dict
        args = self.parseArguments(args)
        result = super(BatchScript, self).loadArguments(args=args, settings=settings, namespace=namespace)
        return result

    def loadLogArguments(self, args):
        self.setVerbose(args.get("verbose", False))
        result = super(BatchScript, self).loadLogArguments(args)
        return result

    def loadConfigArguments(self, args):
        """Loads the config directory from the arguments"""
        if args.get("config_dir") is not None:
            if not self.verifyPath(args["config_dir"], check_dir=True):
                raise ValueError("Path to configuration files {} is not valid".format(args["config_dir"]))
            self.setConfigDir(args["config_dir"])
        return True

    def locateConfigFiles(self, path=None, name=None, ext=None, fmt=None):
        """Returns a list of configurations file that look promising"""
        result = []
        if path is None:
            path = self.getConfigDir()
        if name is None:
            name = "*"
        if os.path.exists(path):
            # create the pattern
            pattern = self.getConfigPath(path, self.generateConfigFileName(name=name, ext=ext, fmt=fmt))
            # find the configuration files
            result = glob.glob(pattern)
        return result

    def loadProcess(self, path):
        """Loads a process by creating a protocol from the given configuration"""
        result = None
        protocol = self.loadProtocol(path=path)
        if protocol is not None:
            try:
                result = multiprocessing.Process(name=protocol.getProjectId(), target=protocol.start)
                self.addProcess(protocol.getProjectId(), process=result)
            except KeyError as ke:
                self.getLog().warning("Unable to register {} as process: {}".format(protocol.getProjectId(), ke))
        return result

    def loadProcesses(self, paths):
        return [self.loadProcess(path) for path in paths]

    def execute(self):
        result = False
        # get a list of all xml files in the directory
        self.getLog().info("Searching for configuration files in {}".format(self.getConfigDir()))
        conf_files = self.locateConfigFiles()
        self.getLog().info("Found {} configuration files, loading them into memory.".format(len(conf_files)))
        # load configuration files into memory and check them
        self.loadProcesses(conf_files)
        if self.countProcesses() > 0:
            self.getLog().info("Found {} active configurations, starting them".format(self.countProcesses()))
            for process in self.getProcesses().values():
                if process.exitcode is None:
                    self.getLog().info("Start experiment {}".format(process.name))
                    process.start()
            self.getLog().info(
                "All processes are running, waiting max. {} seconds to have them finish".format(
                    self.getCleanTries() * self.getPauseTime()
                )
            )
            result = self.cleanProcesses(self.getCleanTries(), self.getPauseTime())
            self.getLog().info("All processes finished, done")
        return result

    @classmethod
    def inform(cls, msg, start="", end=""):
        cls.getLog().info("{}{}{}".format(start, msg, end))

    @classmethod
    def error(cls, msg, start="", end=""):
        cls.getLog().error("{}{}{}".format(start, msg, end))

    @classmethod
    def warn(cls, msg, start="", end=""):
        cls.getLog().warning("{}{}{}".format(start, msg, end))
