# coding=utf-8
"""A module with classes for simple scripts (command-line executables)"""

from pycultivator_legacy.contrib.scripts import BaseScript
from pycultivator_legacy.config import XMLConfig
import os, argparse

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SimpleScript(BaseScript):
    """A simple script controller, implementing a script controller capable of executing dynamic protocol classes"""

    _default_settings = BaseScript.mergeDefaultSettings(
        {
            "protocol.class": None
        },
        namespace=""
    )

    def __init__(self, script_path, protocol, settings=None):
        """Initialise the simple script

        :param script_path: Location of the script that received the call
        :type script_path: str
        :param protocol: Reference to the Protocol Class that should be executed
        :type protocol: object
        :param settings: Settings dictionary containing the configuration
        :type settings: dict
        """
        BaseScript.__init__(self, script_path=script_path, settings=settings)
        self.setSetting("class", protocol, "protocol")

    def getProtocolClass(self):
        result = self.retrieveSetting("class", default=self.getDefaultSettings(), namespace="protocol")
        if isinstance(result, str):
            path = result
            # parse reference
            try:
                result = self.importFromPath(path)
                if not isinstance(result, type):
                    raise ImportError("Unable to load module %s" % path)
            except ImportError:
                self.getLog().critical("Unable to load %s, no such class" % path)
                result = None
        return result

    @staticmethod
    def importFromPath(path, name=None, package=None):
        """Import a class or module from a path specification

        :param path: Path specification (in dot notation) to class
        :type path: str
        :param name: Name of the class to import. If None will try to derive the class name from the path
        specification.
        :type name: str
        :param package: Sets the package name, required when loading relative paths (starts with dot).
        :type package: str
        :return: class reference
        :rtype: object
        """
        result = None
        # make dummy variable, we will manipulate p not path
        p = path
        # if relative and no package is set, raise Error
        if package is None and path[0] == ".":
            raise ValueError("Cannot import relative path - %s - without package specification" % path)
        # if no name is given, derive from path
        if name is None:
            # take last element
            name = path.split(".")[-1]
            # if path is decomposable into multiple parts, we remove class name
            if len(path.split(".")) > 1:
                p = ".".join(path.split(".")[:-1])
        import importlib
        try:
            # load module
            m = importlib.import_module(p, package)
            result = getattr(m, name)
        except ImportError:
            raise
        return result

    def loadArguments(self, args, settings=None, namespace=None):
        """Parses arguments given from argparse

        :param args: Arguments to parse and load
        :type args: dict or argparse.Namespace
        :param settings: Optional settings dict, to be loaded after all arguments have been parsed.
        :type settings: None or dict
        :param namespace: Optional namespace to be added to settings dict
        :type namespace: str
        :rtype: bool
        """
        result = False
        if settings is None:
            settings = {}
        # if Namespace object, convert to dict
        args = self.parseArguments(args)
        # go and fetch configuration settings
        try:
            if args.get("ini_path") is not None:
                self.loadIni(args["ini_path"])
            self.loadLogArguments(args)
            self.loadConfigArguments(args)
            self.loadExporterArguments(args)
            self.loadProtocolArguments(args)
            # apply given settings
            self.mergeSettings(settings=settings, namespace=namespace)
            result = True
        except:
            raise
        return result

    def loadConfigArguments(self, args):
        """Loads the config settings from the given arguments"""
        # preload configuration directory setting, if given
        if args.get("config_dir") is not None:
            self.setConfigDir(args["config_dir"])
        if args.get("config_path") is None:
            raise ValueError("Unable to proceed, missing configuration file")
        config_dir = os.path.split(args["config_path"])[0]
        config_name = os.path.split(args["config_path"])[1]
        if config_dir != "":
            # if we have a directory, this is a path not a name
            config_files = self.findConfigFile(paths=[config_dir], names=[config_name])
        else:
            # if no directory was found, assume it is a name (not a path)
            config_files = self.findConfigFile(names=[args["config_path"]])
        # test if we found a config file
        if len(config_files) == 0:
            raise ValueError("Unable to find configuration file {}".format(args["config_path"]))
        # store config file location
        if config_dir != "":
            self.setConfigPath(args["config_path"])
        else:
            self.setConfigFileName(args["config_path"])
        return True

    def findConfigFile(self, paths=None, names=None, ext=None, fmt=None, extend=False):
        """Locates a configuration file by searching at multiple places

        :type paths: None or list[str]
        :param paths: A list of paths that will be searched.
        :param names: A list of names that will be tried.
        :param ext: The extension of the configuration file
        :param fmt: The format of the configuration file name (when using dynamically created names)
        :param extend: whether to add default names and paths to the search lists.
        :return: Returns a list of paths to configuration files
        :rtype: list[str]
        """
        result = []
        if paths is None:
            paths = []
        # add default paths if extend is set, or when no paths are given
        if extend or len(paths) == 0:
            paths.extend([self.getConfigDir()])
        if names is None:
            names = []
        # add default names if extend is set, or when no names are given
        if extend or len(names) == 0:
            names.extend([self.getConfigFileName()])
        for path in paths:
            for name in names:
                config = self.locateConfigFile(path=path, name=name, ext=ext, fmt=fmt)
                if config is not None:
                    result.append(config)
        return result

    def locateConfigFile(self, path=None, name=None, ext=None, fmt=None):
        """Locates a configuration file

        :type path: None or str
        :param path: The directory that will be searched
        :param name: The name of the config file
        :param ext: The extension of the configuration file
        :param fmt: The format of the configuration file name (when using dynamically created names)
        :rtype: str
        :return: Path to the configuration file or None if file was not found
        """
        result = None
        if path is None:
            path = self.getConfigDir()
        if name is None:
            name = self.getConfigFileName()
        # check if name has extension
        if os.path.splitext(name)[1] != '':
            ext = ""
        # build file path
        fp = self.getConfigPath(path, self.generateConfigFileName(name=name, ext=ext, fmt=fmt))
        if os.path.exists(fp):
            result = fp
        return result

    def checkConfigFile(self, path):
        """Loads a configuration file into memory

        :rtype: bool
        """
        result = os.path.exists(path)
        fn = os.path.basename(path)
        msg = "{fn} {message}"
        if not result:
            self.warn(msg.format(fn=path, message="does not exist"))
        if result and not XMLConfig.isSupported(path):
            self.warn(msg.format(fn=fn, message="is not supported"))
            result = False
        if result and not XMLConfig.isValid(path):
            self.warn(msg.format(fn=fn, message="is not valid (schema failure"))
            result = False
        if result and not XMLConfig.readState(path):
            is_forced = ""
            result = False
            if self.isForced():
                is_forced = ", but will force it to run"
                result = True
            self.inform(msg.format(fn=fn, message="is not active{}".format(is_forced)))
        return result

    def loadConfigFile(self, path):
        """Returns the basic settings retrieved from the configuration file

        Assumes the path was already checked to be valid (i.e. exists, schema is compatible, xml is valid and
        configuration is active (or forced)).

        :rtype: dict
        """
        return {
            "project_id": XMLConfig.readProjectId(path),
            "time_zero": XMLConfig.readTimeZero(path),
            "state": XMLConfig.readState(path)
        }

    def loadProtocol(self, path):
        """Reads the configuration from the path and loads the given protocol class

        :param path: path to the configuration file
        :type path: str
        :return: The process when the configration is active, otherwise None
        :rtype: uvaCultivator.uvaCultivatorProtocol.uvaCultivatorProtocol.AbstractCultivatorProtocol
        """
        result = None
        # check configuration file
        if self.checkConfigFile(path):
            settings = self.loadConfigFile(path)
            if settings.get("project_id") is not None:
                # create log name by prepending the script name with the project_id
                log_name = self.generateLogFileName(
                    script="{}_{}".format(settings["project_id"], self.getScriptName())
                )
                #
                log_file = self.getLogPath(self.getLogDir(), log_name)
                settings = self.mergeDictionary(
                    self._settings,
                    {
                        "protocol.project_id": settings["project_id"],
                        "protocol.force": self.isForced(),
                        "log.file": log_file,
                        "log.level": self.getLogLevel(),
                        "exporter.state": self.isReporting(),
                        "exporter.file.dir": self.getOutputDir(),
                        "connection.fake": self.fakesConnection()
                    }
                )
                protocol = self.getProtocolClass()
                if protocol is not None:
                    result = protocol(config_file=path, settings=settings)
            else:
                self.getLog().warning("Unable to read project_id from configuration")
        return result

    def execute(self):
        result = False
        self.inform("Trying to open configuration file at {}".format(self.getConfigPath()))
        conf_file = self.locateConfigFile()
        if conf_file is not None:
            self.inform("Found configuration file, trying to load protocol")
            protocol = self.loadProtocol(path=conf_file)
            if protocol is not None:
                self.inform("Protocol loaded, starting...")
                result = protocol.start()
                self.inform("Protocol finished")
        return result
